#include "LanguageFeatures.h"
#include "BasicTypes.h"
#include "TypeSystem.h"

namespace System
{
	string *Type::ToString()
	{
		throw NotImplementedException();
	}

	Type::~Type()
	{
	}

	string *RuntimeType::ToString()
	{
		// TODO FIXME: Should be 'fullname' = (Namespace == null ? "" : Namespace + ".") + Name
		return name;
	}

	RuntimeAssembly::RuntimeAssembly(const wchar_t* codebase, const wchar_t* fqn, bool gac, wchar_t* dotNetVersion) :
		codebase(new string(codebase)), fqn(new string(fqn)), gac(gac), dotNetVersion(new string(dotNetVersion)) {}

	RuntimeModule::RuntimeModule(const wchar_t* fqn, const wchar_t* name, RuntimeAssembly* assembly) :
		fqn(new string(fqn)), name(new string(name)), assembly(assembly) {}

	RuntimeType::RuntimeType(const wchar_t* name, const wchar_t* ns, RuntimeModule* module) :
		name(new string(name)), namesp(new string(ns)), module(module){}

	Object* Activator::CreateInstance(Type* t)
	{
		return t->CreateNewWithDefaultConstructor();
	}

}