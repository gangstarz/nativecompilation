#include "TestApplication.IInterface1.h"

namespace TestApplication
{
	_IInterface1_RTTI::_IInterface1_RTTI() : RuntimeType(L"IInterface1", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_IInterface1_RTTI::CreateNewWithDefaultConstructor()
	{
		throw System::MissingMemberException();
	}
	
	System::Object *_IInterface1_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_IInterface1_RTTI::~_IInterface1_RTTI()
	{
	}
}


