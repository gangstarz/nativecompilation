#include "TestApplication.IInterface2.h"

namespace TestApplication
{
	_IInterface2_RTTI::_IInterface2_RTTI() : RuntimeType(L"IInterface2", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_IInterface2_RTTI::CreateNewWithDefaultConstructor()
	{
		throw System::MissingMemberException();
	}
	
	System::Object *_IInterface2_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_IInterface2_RTTI::~_IInterface2_RTTI()
	{
	}
}


