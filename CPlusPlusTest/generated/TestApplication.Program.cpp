#include "TestApplication.Program.h"

namespace TestApplication
{
	static _int _init0[5] = { 1, 2, 3, 4, 5 };
	static string* _str0 = new string(L"Error in \'TestIf1\'");
	static string* _str1 = new string(L"Error in \'TestIf2\'");
	static string* _str2 = new string(L"Error in \'TestImplicitArrayAssign\'");
	static string* _str3 = new string(L"Error in \'TestFor\'");
	static string* _str4 = new string(L"Error in \'TestWhile\'");
	static string* _str5 = new string(L"Error in \'TestDoWhile\'");
	static string* _str6 = new string(L"Error in \'TestSwitch\'");
	
	Program::Program()
	{
	}
	
	Program::Program(int _shouldInit)
	{
		_constructor();
	}
	
	void Program::_constructor()
	{
		this->System::Object::_constructor();
	}
	
	void Program::TestIf1(_int n, _int m)
	{
		_bool i = n != m && (n <= m && n < m);
		_bool val = 0;
		if (!i)
		{
			System::Console::WriteLine(_str0);
		}
	}
	
	void Program::TestIf2(_int n, _int m)
	{
		if ((n != m && n >= m) && n > m)
		{
			goto label_0027;
		}
		
		object obj = 0;
		_int i1 = m <= 2 && (n <= 2 || m <= 10);
		goto label_0028;
	label_0027:
		i1 = 1;
	label_0028:
		_bool val = 0;
		if (!i1)
		{
			System::Console::WriteLine(_str1);
		}
	}
	
	void Program::TestImplicitArrayAssign()
	{
		_int i1 = 0;
		System::Array<_int> array;
		System::Array<_int> array1;
		System::Array<_int> array2 = _init0;
		_int i = 0;
		goto label_002e;
	label_001d:
		_int i2 = 0;
		i1 = i1 + array2[i];
		++i;
	label_002e:
		_bool val = 0;
		if (i < static_cast<_int>(array2.get_Length()))
		{
			goto label_001d;
		}
		
		if (i1 != 15)
		{
			System::Console::WriteLine(_str2);
		}
	}
	
	void Program::TestFor()
	{
		_int i = 0;
		_int i1 = 0;
		goto label_0011;
	label_0007:
		i = i + i1;
		++i1;
	label_0011:
		_bool val = 0;
		if (i1 < 10)
		{
			goto label_0007;
		}
		
		if (i != 45)
		{
			System::Console::WriteLine(_str3);
		}
	}
	
	void Program::TestWhile()
	{
		_int i1 = 0;
		_int i = 0;
		goto label_0011;
	label_0007:
		i1 = i1 + (i + 1);
	label_0011:
		_bool val = 0;
		if (i1 < 45)
		{
			goto label_0007;
		}
		
		if (i1 != 45)
		{
			System::Console::WriteLine(_str4);
		}
	}
	
	void Program::TestDoWhile()
	{
		_int i1 = 0;
		_int i = 0;
	label_0005:
		i1 = i1 + (i + 1);
		_bool val = 0;
		if (i1 < 45)
		{
			goto label_0005;
		}
		
		if (i1 != 45)
		{
			System::Console::WriteLine(_str5);
		}
	}
	
	void Program::TestSwitch(_int id)
	{
		_int i = 0;
		switch (id - 1)
		{
		case 0:
			break;
		case 1:
		case 2:
			Program::TestSwitch(id - 1);
			break;
		default:
			System::Console::WriteLine(_str6);
			break;
		}
	}
	
	void Program::Main(System::Array<string *> args)
	{
	}
	
	void Program::GC_MarkChildren(int flag)
	{
	}
	
	System::Type *Program::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(6);
	}
	
	_Program_RTTI::_Program_RTTI() : RuntimeType(L"Program", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_Program_RTTI::CreateNewWithDefaultConstructor()
	{
		return new Program(1);
	}
	
	System::Object *_Program_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_Program_RTTI::~_Program_RTTI()
	{
	}
}


