#include "TestApplication.DerivedClass1.h"
#include "TestApplication.BaseClass.h"

namespace TestApplication
{
	static string* _str0 = new string(L"Virtual call should call current DerivedClass1.VirtualCall()");
	static string* _str1 = new string(L"Abstract call should call current DerivedClass1.AbstractCall()");
	static string* _str2 = new string(L"Virtual call with explicit base should call BaseClass.VirtualCall()");
	static string* _str3 = new string(L"Non-virtual call should call BaseClass.Foo()");
	static string* _str4 = new string(L"Non-virtual call should call DerivedClass1.NewFoo()");
	
	DerivedClass1::DerivedClass1()
	{
	}
	
	DerivedClass1::DerivedClass1(int _shouldInit)
	{
		_constructor();
	}
	
	void DerivedClass1::_constructor()
	{
		this->BaseClass::_constructor();
	}
	
	void DerivedClass1::Test()
	{
		_bool val = 0;
		if (this->VirtualCall() != 4)
		{
			System::Console::WriteLine(_str0);
		}
		
		if (this->AbstractCall() != 3)
		{
			System::Console::WriteLine(_str1);
		}
		
		if (this->BaseClass::VirtualCall() != 2)
		{
			System::Console::WriteLine(_str2);
		}
		
		if (this->BaseClass::Foo() != 1)
		{
			System::Console::WriteLine(_str3);
		}
		
		if (this->DerivedClass1::NewFoo() != 6)
		{
			System::Console::WriteLine(_str4);
		}
		
		this->BaseClass::TestInBase();
	}
	
	_int DerivedClass1::AbstractCall()
	{
		_int i = 0;
		return 3;
	}
	
	_int DerivedClass1::VirtualCall()
	{
		_int i = 0;
		return 4;
	}
	
	_int DerivedClass1::NewFoo()
	{
		_int i = 0;
		return 6;
	}
	
	void DerivedClass1::GC_MarkChildren(int flag)
	{
	}
	
	System::Type *DerivedClass1::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(4);
	}
	
	_DerivedClass1_RTTI::_DerivedClass1_RTTI() : RuntimeType(L"DerivedClass1", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_DerivedClass1_RTTI::CreateNewWithDefaultConstructor()
	{
		return new DerivedClass1(1);
	}
	
	System::Object *_DerivedClass1_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_DerivedClass1_RTTI::~_DerivedClass1_RTTI()
	{
	}
}


