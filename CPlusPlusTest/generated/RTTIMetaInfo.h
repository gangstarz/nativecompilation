#ifndef RTTI_META_INFO
#define RTTI_META_INFO

#include "../LanguageFeatures.h"
#include <vector>
#include <string>

class RTTIMetaInfo
{
	RTTIMetaInfo();
	
public:
	System::RuntimeType** types;
	System::RuntimeModule** modules;
	System::RuntimeAssembly** assemblies;
	
	inline static RTTIMetaInfo& Instance()
	{
		static RTTIMetaInfo instance;
		return instance;
	}
	
	static inline System::RuntimeType *GetTypeById(int id)
	{
		return Instance().types[id];
	}
};

#endif

