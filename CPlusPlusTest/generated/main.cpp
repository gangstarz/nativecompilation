#include "../LanguageFeatures.h"
#include "TestApplication.Program.h"
#include <string>
#include <iostream>

using namespace System;

int main(int argc, char** argv)
{
	try
	{
		Array<string*> ar(argc);
		for (int i=0; i<argc; ++i)
		{
			ar[i] = new string(argv[i]);
		}
		TestApplication::Program::Main(ar);
		
		std::string s;
		std::getline(std::cin, s);
		
		return 0;
	}
	catch (...)
	{
		return 1;
	}
}

