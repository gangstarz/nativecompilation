#ifndef AUTOGENERATED_TestApplicationQ002eGenericsInterfaceQ_Generic
#define AUTOGENERATED_TestApplicationQ002eGenericsInterfaceQ_Generic

#include <cstdint>
#include "../LanguageFeatures.h"
#include "RTTIMetaInfo.h"


namespace TestApplication
{
	class IInterface1;
	class GenericsInterface_Generic_1
	{
	private:
		System::Type *_generic0;
		
	public:
		virtual void Test() = 0;
		
	protected:
		~GenericsInterface_Generic_1()
		{
		}
	};
	
	class _GenericsInterface_Generic_1_RTTI : public System::RuntimeType
	{
		_GenericsInterface_Generic_1_RTTI();
	public:
		inline static _GenericsInterface_Generic_1_RTTI* Instance()
		{
			static _GenericsInterface_Generic_1_RTTI instance;
			return &instance;
		}
		
		virtual System::Object *CreateNewWithDefaultConstructor();
		virtual System::Object *CreateNew(System::Array<System::Object*> parameters);
		virtual ~_GenericsInterface_Generic_1_RTTI();
	};
}

#endif

