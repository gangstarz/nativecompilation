#include "TestApplication.TestEvent.h"
#include "System.AsyncCallback.h"
#include "System.IAsyncResult.h"
#include "System.ICloneable.h"
#include "System.IntPtr.h"
#include "System.MulticastDelegate.h"
#include "System.Runtime.Serialization.ISerializable.h"

namespace TestApplication
{
	TestEvent::TestEvent()
	{
	}
	
	TestEvent::TestEvent(int _shouldInit, object object, System::IntPtr method)
	{
		_constructor(object, method);
	}
	
	void TestEvent::_constructor(object object, System::IntPtr method)
	{
	}
	
	void TestEvent::Invoke(_int id)
	{
	}
	
	System::IAsyncResult* TestEvent::BeginInvoke(_int id, System::AsyncCallback* callback, object object)
	{
	}
	
	void TestEvent::EndInvoke(System::IAsyncResult* result)
	{
	}
	
	void TestEvent::GC_MarkChildren(int flag)
	{
		_target->GC_Mark(flag);
		_methodBase->GC_Mark(flag);
	}
	
	System::Type *TestEvent::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(14);
	}
	
	_TestEvent_RTTI::_TestEvent_RTTI() : RuntimeType(L"TestEvent", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_TestEvent_RTTI::CreateNewWithDefaultConstructor()
	{
		throw System::MissingMemberException();
	}
	
	System::Object *_TestEvent_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_TestEvent_RTTI::~_TestEvent_RTTI()
	{
	}
}


