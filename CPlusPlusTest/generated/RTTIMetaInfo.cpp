#include "RTTIMetaInfo.h"
#include "TestApplication.TestInterface.h"
#include "TestApplication.IInterface2.h"
#include "TestApplication.IInterface1.h"
#include "TestApplication.IInterface3.h"
#include "TestApplication.DerivedClass1.h"
#include "TestApplication.BaseClass.h"
#include "TestApplication.Program.h"
#include "TestApplication.ValueTypes.h"
#include "TestApplication.MyValueType.h"
#include "TestApplication.StringTest.h"
#include "TestApplication.InnerClassTest_Generic.h"
#include "TestApplication.MyInterface.h"
#include "TestApplication.InnerClass_Generic.h"
#include "TestApplication.InnerClassTest_Generic.h"
#include "TestApplication.GenericsTest_Generic.h"
#include "TestApplication.GenericsInterface_Generic.h"
#include "TestApplication.ExceptionTest.h"
#include "TestApplication.EventTest.h"
#include "TestApplication.TestEvent.h"
#include "TestApplication.DerivedDelegateTest.h"
#include "TestApplication.DelegateTest.h"
#include "TestApplication.TestDelegate.h"

using namespace System;

Type* Type::GetTypeFromHandle(int handle)
{
	return ::RTTIMetaInfo::GetTypeById(handle);
}

RTTIMetaInfo::RTTIMetaInfo()
{
	types = new RuntimeType*[22];
	modules = new RuntimeModule*[2];
	assemblies = new RuntimeAssembly*[2];
	
	// Assemblies
	assemblies[0] = new RuntimeAssembly(L"file:///C:/Windows/Microsoft.NET/Framework/v4.0.30319/mscorlib.dll", L"mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", 1, L"v4.0.30319");
	assemblies[1] = new RuntimeAssembly(L"file:///C:/Users/Stefan/Desktop/Eigen projectjes/Tests/NativeCompilationTest/NativeCompiler/bin/Debug/TestApplication.EXE", L"TestApplication, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", 0, L"v4.0.30319");
	
	// Modules
	modules[0] = new RuntimeModule(L"C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\mscorlib.dll", L"mscorlib.dll", assemblies[0]);
	assemblies[0]->modules.push_back(modules[0]);
	modules[1] = new RuntimeModule(L"C:\\Users\\Stefan\\Desktop\\Eigen projectjes\\Tests\\NativeCompilationTest\\NativeCompiler\\bin\\Debug\\TestApplication.exe", L"TestApplication.exe", assemblies[1]);
	assemblies[1]->modules.push_back(modules[1]);
	
	// Types
	types[0] = TestApplication::_TestInterface_RTTI::Instance();
	modules[1]->types.push_back(types[0]);
	assemblies[1]->types.push_back(types[0]);
	types[1] = TestApplication::_IInterface2_RTTI::Instance();
	modules[1]->types.push_back(types[1]);
	assemblies[1]->types.push_back(types[1]);
	types[2] = TestApplication::_IInterface1_RTTI::Instance();
	modules[1]->types.push_back(types[2]);
	assemblies[1]->types.push_back(types[2]);
	types[3] = TestApplication::_IInterface3_RTTI::Instance();
	modules[1]->types.push_back(types[3]);
	assemblies[1]->types.push_back(types[3]);
	types[4] = TestApplication::_DerivedClass1_RTTI::Instance();
	modules[1]->types.push_back(types[4]);
	assemblies[1]->types.push_back(types[4]);
	types[5] = TestApplication::_BaseClass_RTTI::Instance();
	modules[1]->types.push_back(types[5]);
	assemblies[1]->types.push_back(types[5]);
	types[6] = TestApplication::_Program_RTTI::Instance();
	modules[1]->types.push_back(types[6]);
	assemblies[1]->types.push_back(types[6]);
	types[7] = TestApplication::_ValueTypes_RTTI::Instance();
	modules[1]->types.push_back(types[7]);
	assemblies[1]->types.push_back(types[7]);
	types[8] = TestApplication::_MyValueType_RTTI::Instance();
	modules[1]->types.push_back(types[8]);
	assemblies[1]->types.push_back(types[8]);
	types[9] = TestApplication::_StringTest_RTTI::Instance();
	modules[1]->types.push_back(types[9]);
	assemblies[1]->types.push_back(types[9]);
	types[10] = TestApplication::_InnerClassTest_Generic_0_RTTI::Instance();
	modules[1]->types.push_back(types[10]);
	assemblies[1]->types.push_back(types[10]);
	types[11] = TestApplication::_MyInterface_RTTI::Instance();
	modules[1]->types.push_back(types[11]);
	assemblies[1]->types.push_back(types[11]);
	types[12] = TestApplication::_InnerClass_Generic_2_RTTI::Instance();
	modules[1]->types.push_back(types[12]);
	assemblies[1]->types.push_back(types[12]);
	types[13] = TestApplication::_InnerClassTest_Generic_1_RTTI::Instance();
	modules[1]->types.push_back(types[13]);
	assemblies[1]->types.push_back(types[13]);
	types[14] = TestApplication::_GenericsTest_Generic_1_RTTI::Instance();
	modules[1]->types.push_back(types[14]);
	assemblies[1]->types.push_back(types[14]);
	types[15] = TestApplication::_GenericsInterface_Generic_1_RTTI::Instance();
	modules[1]->types.push_back(types[15]);
	assemblies[1]->types.push_back(types[15]);
	types[16] = TestApplication::_ExceptionTest_RTTI::Instance();
	modules[1]->types.push_back(types[16]);
	assemblies[1]->types.push_back(types[16]);
	types[17] = TestApplication::_EventTest_RTTI::Instance();
	modules[1]->types.push_back(types[17]);
	assemblies[1]->types.push_back(types[17]);
	types[18] = TestApplication::_TestEvent_RTTI::Instance();
	modules[1]->types.push_back(types[18]);
	assemblies[1]->types.push_back(types[18]);
	types[19] = TestApplication::_DerivedDelegateTest_RTTI::Instance();
	modules[1]->types.push_back(types[19]);
	assemblies[1]->types.push_back(types[19]);
	types[20] = TestApplication::_DelegateTest_RTTI::Instance();
	modules[1]->types.push_back(types[20]);
	assemblies[1]->types.push_back(types[20]);
	types[21] = TestApplication::_TestDelegate_RTTI::Instance();
	modules[1]->types.push_back(types[21]);
	assemblies[1]->types.push_back(types[21]);
}

