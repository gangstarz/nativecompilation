#include "TestApplication.GenericsTest_Generic.h"
#include "TestApplication.GenericsInterface_Generic.h"
#include "TestApplication.IInterface1.h"

namespace TestApplication
{
	static string* _str0 = new string(L"Generics parameter should resolve to IInterface1.Foo() -> TestInterface.Foo().");
	
	GenericsTest_Generic_1::GenericsTest_Generic_1(System::Type *t0)
	{
		this->_generic0 = t0;
	}
	
	GenericsTest_Generic_1::GenericsTest_Generic_1(int _shouldInit, System::Type *t0)
	{
		this->_generic0 = t0;
		_constructor(t0);
	}
	
	void GenericsTest_Generic_1::_constructor(System::Type *t0)
	{
		this->System::Object::_constructor();
	}
	
	void GenericsTest_Generic_1::Test()
	{
		object* t = 0;
		t = 0;
		object* t1 = 0;
		if (!t)
		{
			t1 = System::Activator::CreateInstance(_generic0);
		}
		else
		{
			t = 0;
			t1 = t;
		}
		
		object* t2 = 0;
		_bool val = 0;
		if (dynamic_cast<IInterface1*>(t1)->Foo() != 1)
		{
			System::Console::WriteLine(_str0);
		}
	}
	
	void GenericsTest_Generic_1::GC_MarkChildren(int flag)
	{
	}
	
	System::Type *GenericsTest_Generic_1::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(10);
	}
	
	_GenericsTest_Generic_1_RTTI::_GenericsTest_Generic_1_RTTI() : RuntimeType(L"GenericsTest", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_GenericsTest_Generic_1_RTTI::CreateNewWithDefaultConstructor()
	{
		throw System::MissingMemberException();
	}
	
	System::Object *_GenericsTest_Generic_1_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_GenericsTest_Generic_1_RTTI::~_GenericsTest_Generic_1_RTTI()
	{
	}
}


