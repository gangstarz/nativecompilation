#include "TestApplication.TestDelegate.h"
#include "System.AsyncCallback.h"
#include "System.IAsyncResult.h"
#include "System.ICloneable.h"
#include "System.IntPtr.h"
#include "System.MulticastDelegate.h"
#include "System.Runtime.Serialization.ISerializable.h"

namespace TestApplication
{
	TestDelegate::TestDelegate()
	{
	}
	
	TestDelegate::TestDelegate(int _shouldInit, object object, System::IntPtr method)
	{
		_constructor(object, method);
	}
	
	void TestDelegate::_constructor(object object, System::IntPtr method)
	{
	}
	
	_int TestDelegate::Invoke(_int id)
	{
	}
	
	System::IAsyncResult* TestDelegate::BeginInvoke(_int id, System::AsyncCallback* callback, object object)
	{
	}
	
	_int TestDelegate::EndInvoke(System::IAsyncResult* result)
	{
	}
	
	void TestDelegate::GC_MarkChildren(int flag)
	{
		_target->GC_Mark(flag);
		_methodBase->GC_Mark(flag);
	}
	
	System::Type *TestDelegate::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(4);
	}
	
	_TestDelegate_RTTI::_TestDelegate_RTTI() : RuntimeType(L"TestDelegate", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_TestDelegate_RTTI::CreateNewWithDefaultConstructor()
	{
		throw System::MissingMemberException();
	}
	
	System::Object *_TestDelegate_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_TestDelegate_RTTI::~_TestDelegate_RTTI()
	{
	}
}


