#include "TestApplication.TestInterface.h"
#include "TestApplication.IInterface1.h"
#include "TestApplication.IInterface2.h"
#include "TestApplication.IInterface3.h"

namespace TestApplication
{
	static string* _str0 = new string(L"Interface should resolve to TestInterface.Foo().");
	static string* _str1 = new string(L"Interface should resolve to TestInterface.Bar().");
	static string* _str2 = new string(L"Class is instance of IInterface1");
	static string* _str3 = new string(L"Class is instance of IInterface2");
	static string* _str4 = new string(L"Class is NOT instance of IInterface3");
	static string* _str5 = new string(L"Class cast to interface1 should call virtual method impl of Foo.");
	static string* _str6 = new string(L"Class IS actually an IInterface1.");
	static string* _str7 = new string(L"Class is actually NOT an IInterface1.");
	
	TestInterface::TestInterface()
	{
	}
	
	TestInterface::TestInterface(int _shouldInit)
	{
		_constructor();
	}
	
	void TestInterface::_constructor()
	{
		this->System::Object::_constructor();
	}
	
	void TestInterface::Test1(IInterface1* intf1)
	{
		_bool val = 0;
		if (intf1->Foo() != 1)
		{
			System::Console::WriteLine(_str0);
		}
	}
	
	void TestInterface::Test2(IInterface2* intf2)
	{
		_bool val = 0;
		if (intf2->Foo() != 1)
		{
			System::Console::WriteLine(_str0);
		}
		
		if (intf2->Bar() != 2)
		{
			System::Console::WriteLine(_str1);
		}
	}
	
	void TestInterface::Test3(TestInterface* intf3)
	{
		_bool val = 0;
		if (intf3->Foo() != 1)
		{
			System::Console::WriteLine(_str0);
		}
		
		if (intf3->Bar() != 2)
		{
			System::Console::WriteLine(_str1);
		}
	}
	
	void TestInterface::Test4(TestInterface* cl)
	{
		IInterface1* iInterface1 = cl;
		_bool val = 0;
		if (iInterface1 == 0)
		{
			System::Console::WriteLine(_str2);
		}
		
		IInterface2* iInterface2 = 0;
		if (cl == 0)
		{
			System::Console::WriteLine(_str3);
		}
		
		IInterface3* iInterface3 = 0;
		if (dynamic_cast<IInterface3*>(cl) != 0)
		{
			System::Console::WriteLine(_str4);
		}
		
		IInterface1* iInterface11 = 0;
		if (iInterface1->Foo() != 1)
		{
			System::Console::WriteLine(_str5);
		}
		
		IInterface2* iInterface21 = 0;
		if (dynamic_cast_or_throw<IInterface2*>(iInterface1)->Foo() != 1)
		{
			System::Console::WriteLine(_str5);
		}
		
		if (cl == 0)
		{
			System::Console::WriteLine(_str6);
		}
		
		if (cl == 0)
		{
			System::Console::WriteLine(_str6);
		}
		
		if (dynamic_cast<IInterface3*>(cl) > 0)
		{
			System::Console::WriteLine(_str7);
		}
	}
	
	void TestInterface::Test()
	{
		this->TestInterface::Test1(this);
		this->TestInterface::Test2(this);
		this->TestInterface::Test3(this);
		this->TestInterface::Test4(this);
	}
	
	_int TestInterface::Foo()
	{
		_int i = 0;
		return 1;
	}
	
	_int TestInterface::Bar()
	{
		_int i = 0;
		return 2;
	}
	
	void TestInterface::GC_MarkChildren(int flag)
	{
	}
	
	System::Type *TestInterface::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(0);
	}
	
	_TestInterface_RTTI::_TestInterface_RTTI() : RuntimeType(L"TestInterface", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_TestInterface_RTTI::CreateNewWithDefaultConstructor()
	{
		return new TestInterface(1);
	}
	
	System::Object *_TestInterface_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_TestInterface_RTTI::~_TestInterface_RTTI()
	{
	}
}


