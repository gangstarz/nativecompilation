#include "TestApplication.ValueTypes.h"
#include "TestApplication.MyValueType.h"

namespace TestApplication
{
	static string* _str0 = new string(L"Error: value type items shouldn\'t be the same - should be 1 and 2");
	static string* _str1 = new string(L"Boxing then unboxing should call a copy constructor");
	static string* _str2 = new string(L"Equals call on value type should result in a not-equals");
	static string* _str3 = new string(L"Equals call on value type should result in a equals");
	static string* _str4 = new string(L"Since vt3 is copied, it should still have value 1");
	static string* _str5 = new string(L"Value type vt1 should have been copied instead of passed by reference");
	static string* _str6 = new string(L"Value type vt1 should have been passed by reference");
	static string* _str7 = new string(L"Create value type and returning it should have copied it.");
	static string* _str8 = new string(L"Returning value type should have copied it.");
	static string* _str9 = new string(L"Boxing value type should have copied it.");
	static string* _str10 = new string(L"Basic type boxing/wrapping doesn\'t work as it should.");
	
	ValueTypes::ValueTypes()
	{
	}
	
	ValueTypes::ValueTypes(int _shouldInit)
	{
		_constructor();
	}
	
	void ValueTypes::_constructor()
	{
		this->System::Object::_constructor();
	}
	
	void ValueTypes::TestValueTypes()
	{
		MyValueType myValueType = 0;
		&myValueType->Item = 1;
		MyValueType myValueType1 = 0;
		&myValueType1 = (new MyValueType(1));
		&myValueType1->Item = 2;
		MyValueType myValueType3 = myValueType1;
		_bool i = Item != Item && (Item == 1 && Item == 2);
		_bool val = 0;
		if (!i)
		{
			System::Console::WriteLine(_str0);
		}
		
		object obj = 0;
		MyValueType myValueType2 = (*(new MyValueType(myValueType)));
		if (System::Object::ReferenceEquals((new MyValueType(myValueType)), (new MyValueType(myValueType2))) != 0)
		{
			System::Console::WriteLine(_str1);
		}
		
		if (&myValueType->Equals((new MyValueType(myValueType3))) != 0)
		{
			System::Console::WriteLine(_str2);
		}
		
		&myValueType->Item = 2;
		if (!&myValueType->Equals((new MyValueType(myValueType3))))
		{
			System::Console::WriteLine(_str3);
		}
		
		_bool i1 = 0;
		if (&myValueType2->Equals((new MyValueType(myValueType3))) || Item != 1)
		{
			System::Console::WriteLine(_str4);
		}
		
		this->ValueTypes::TestCopyByPass(myValueType);
		if (Item != 2)
		{
			System::Console::WriteLine(_str5);
		}
		
		this->ValueTypes::TestCopyByRef(&myValueType);
		if (Item != 5)
		{
			System::Console::WriteLine(_str6);
		}
		
		MyValueType myValueType4 = this->ValueTypes::CreateValueType(6);
		if (Item != 6)
		{
			System::Console::WriteLine(_str7);
		}
		
		MyValueType myValueType6 = this->ValueTypes::CreateValueType(&myValueType4);
		MyValueType myValueType5 = 0;
		++Item;
		if (Item != 7)
		{
			System::Console::WriteLine(_str7);
		}
		
		if (Item != 6)
		{
			System::Console::WriteLine(_str8);
		}
		
		object obj1 = 0;
		if (System::Object::ReferenceEquals(this->ValueTypes::BoxValueType(&myValueType6), (new MyValueType(myValueType6))) != 0)
		{
			System::Console::WriteLine(_str9);
		}
		
		this->ValueTypes::TestBasicTypeWrapping();
	}
	
	void ValueTypes::TestBasicTypeWrapping()
	{
		_int i = 1;
		_int i1 = 1;
		object obj = (new _int(i));
		++i;
		_int i2 = 0;
		_bool val = 0;
		if (!&(*obj)->System::Int32::Equals(i1))
		{
			System::Console::WriteLine(_str10);
		}
	}
	
	MyValueType ValueTypes::CreateValueType(_int val)
	{
		MyValueType myValueType = 0;
		&myValueType = (new MyValueType(1));
		&myValueType->Item = val;
		MyValueType myValueType1 = 0;
		return myValueType;
	}
	
	MyValueType ValueTypes::CreateValueType(& vt)
	{
		MyValueType myValueType = 0;
		return *vt;
	}
	
	object ValueTypes::BoxValueType(& vt)
	{
		object obj = 0;
		return (new MyValueType(*vt));
	}
	
	void ValueTypes::TestCopyByPass(MyValueType vt)
	{
		&vt->Item = 4;
	}
	
	void ValueTypes::TestCopyByRef(& vt)
	{
		vt->Item = 5;
	}
	
	void ValueTypes::GC_MarkChildren(int flag)
	{
	}
	
	System::Type *ValueTypes::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(7);
	}
	
	_ValueTypes_RTTI::_ValueTypes_RTTI() : RuntimeType(L"ValueTypes", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_ValueTypes_RTTI::CreateNewWithDefaultConstructor()
	{
		return new ValueTypes(1);
	}
	
	System::Object *_ValueTypes_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_ValueTypes_RTTI::~_ValueTypes_RTTI()
	{
	}
}


