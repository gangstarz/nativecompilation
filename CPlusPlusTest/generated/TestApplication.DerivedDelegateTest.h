#ifndef AUTOGENERATED_TestApplicationQ002eDerivedDelegateTest
#define AUTOGENERATED_TestApplicationQ002eDerivedDelegateTest

#include <cstdint>
#include "../LanguageFeatures.h"
#include "RTTIMetaInfo.h"
#include "TestApplication.DelegateTest.h"


namespace TestApplication
{
	class TestDelegate;
	class DerivedDelegateTest : public virtual DelegateTest
	{
	public:
		DerivedDelegateTest();
		DerivedDelegateTest(int _shouldInit);
		void _constructor();
		
		_int add;
		
		virtual _int Test1(_int id);
		void TestDerived1();
		virtual System::Type* GetType();
		
		
	protected:
		virtual void GC_MarkChildren(int flag);
	};
	
	class _DerivedDelegateTest_RTTI : public System::RuntimeType
	{
		_DerivedDelegateTest_RTTI();
	public:
		inline static _DerivedDelegateTest_RTTI* Instance()
		{
			static _DerivedDelegateTest_RTTI instance;
			return &instance;
		}
		
		virtual System::Object *CreateNewWithDefaultConstructor();
		virtual System::Object *CreateNew(System::Array<System::Object*> parameters);
		virtual ~_DerivedDelegateTest_RTTI();
	};
}

#endif

