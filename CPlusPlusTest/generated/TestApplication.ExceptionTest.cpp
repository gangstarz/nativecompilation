#include "TestApplication.ExceptionTest.h"
#include "System.ArgumentException.h"
#include "System.NullReferenceException.h"

namespace TestApplication
{
	static string* _str0 = new string(L"foo");
	static string* _str1 = new string(L"Expected message in exception to say \'foo\'");
	static string* _str2 = new string(L"Incorrect exception found.");
	static string* _str3 = new string(L"Incorrect exception caught; base exception shouldn\'t be caught.");
	static string* _str4 = new string(L"Try/catch not called in the correct order or not at all");
	static string* _str5 = new string(L"Expected catch before finally.");
	static string* _str6 = new string(L"Try/catch/finally not called in the correct order or not at all");
	static string* _str7 = new string(L"Exceptions not caught in the right order or exception was not re-thrown.");
	static string* _str8 = new string(L"Expected message in exception to say \'foo\'; overload failed");
	static string* _str9 = new string(L"Exceptions not caught in the right order or exception catching was not overloaded correctly.");
	
	ExceptionTest::ExceptionTest()
	{
	}
	
	ExceptionTest::ExceptionTest(int _shouldInit)
	{
		_constructor();
	}
	
	void ExceptionTest::_constructor()
	{
		this->System::Object::_constructor();
	}
	
	void ExceptionTest::Test()
	{
		this->ExceptionTest::TestTryCatch();
		this->ExceptionTest::TestTryCatchFinally();
		this->ExceptionTest::TestNestingExceptions1();
		this->ExceptionTest::TestNestingExceptions2();
	}
	
	void ExceptionTest::TestTryCatch()
	{
		_int i = 0;
		_bool val = 0;
		try
		{
			this->ExceptionTest::Throw1();
			goto label_005b;
		}
		catch (System::ArgumentException* argumentException)
		{
			if (System::String::op_Inequality(argumentException->get_Message(), _str0) != 0)
			{
				System::Console::WriteLine(_str1);
			}
			
			i = 1;
			goto label_005b;
		}
		catch (System::NullReferenceException* nullReferenceException)
		{
			nullReferenceException;
			System::Console::WriteLine(_str2);
			goto label_005b;
		}
		catch (System::Exception * ex)
		{
			ex;
			System::Console::WriteLine(_str3);
			goto label_005b;
		}
	label_005b:
		if (i != 1)
		{
			System::Console::WriteLine(_str4);
		}
	}
	
	void ExceptionTest::TestTryCatchFinally()
	{
		_int i = 0;
		_bool val = 0;
		try
		{
			try
			{
				i = 0;
				this->ExceptionTest::Throw1();
				goto label_004b;
			}
			catch (System::ArgumentException* argumentException)
			{
				if (System::String::op_Inequality(argumentException->get_Message(), _str0) != 0)
				{
					System::Console::WriteLine(_str1);
				}
				
				i = 1;
				goto label_004b;
			}
			catch (System::NullReferenceException* nullReferenceException)
			{
				nullReferenceException;
				System::Console::WriteLine(_str2);
				goto label_004b;
			}
		label_004b:
			goto label_0068;
		}
		finally
		{
			if (i != 1)
			{
				System::Console::WriteLine(_str5);
			}
			
			i = 2;
		}
	label_0068:
		if (i != 2)
		{
			System::Console::WriteLine(_str6);
		}
	}
	
	void ExceptionTest::TestNestingExceptions1()
	{
		_int i = 0;
		System::ArgumentException* argumentException2 = 0;
		_bool val = 0;
		try
		{
			try
			{
				this->ExceptionTest::Throw1();
				goto label_003a;
			}
			catch (System::ArgumentException* argumentException)
			{
				if (System::String::op_Inequality(argumentException->get_Message(), _str0) != 0)
				{
					System::Console::WriteLine(_str1);
				}
				
				i = 10;
				throw;
			}
		label_003a:
			goto label_006b;
		}
		catch (System::ArgumentException* argumentException1)
		{
			if (System::String::op_Inequality(argumentException1->get_Message(), _str0) != 0)
			{
				System::Console::WriteLine(_str1);
			}
			
			++i;
			goto label_006b;
		}
	label_006b:
		if (i != 11)
		{
			System::Console::WriteLine(_str7);
		}
	}
	
	void ExceptionTest::TestNestingExceptions2()
	{
		_int i = 0;
		System::Exception * ex2 = 0;
		_bool val = 0;
		try
		{
			try
			{
				this->ExceptionTest::Throw1();
				goto label_003a;
			}
			catch (System::Exception * ex)
			{
				if (System::String::op_Inequality(ex->get_Message(), _str0) != 0)
				{
					System::Console::WriteLine(_str1);
				}
				
				i = 10;
				throw;
			}
		label_003a:
			goto label_006b;
		}
		catch (System::Exception * ex1)
		{
			if (System::String::op_Inequality(ex1->get_Message(), _str0) != 0)
			{
				System::Console::WriteLine(_str8);
			}
			
			++i;
			goto label_006b;
		}
	label_006b:
		if (i != 11)
		{
			System::Console::WriteLine(_str9);
		}
	}
	
	void ExceptionTest::Throw1()
	{
		throw *(new System::ArgumentException(1, _str0));
	}
	
	void ExceptionTest::GC_MarkChildren(int flag)
	{
	}
	
	System::Type *ExceptionTest::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(1);
	}
	
	_ExceptionTest_RTTI::_ExceptionTest_RTTI() : RuntimeType(L"ExceptionTest", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_ExceptionTest_RTTI::CreateNewWithDefaultConstructor()
	{
		return new ExceptionTest(1);
	}
	
	System::Object *_ExceptionTest_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_ExceptionTest_RTTI::~_ExceptionTest_RTTI()
	{
	}
}


