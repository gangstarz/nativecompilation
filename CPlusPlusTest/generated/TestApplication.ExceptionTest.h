#ifndef AUTOGENERATED_TestApplicationQ002eExceptionTest
#define AUTOGENERATED_TestApplicationQ002eExceptionTest

#include <cstdint>
#include "../LanguageFeatures.h"
#include "RTTIMetaInfo.h"


namespace System
{
	class ArgumentException;
	class NullReferenceException;
}

namespace TestApplication
{
	class ExceptionTest : public virtual object
	{
	public:
		ExceptionTest();
		ExceptionTest(int _shouldInit);
		void _constructor();
		
		void Test();
		void TestTryCatch();
		void TestTryCatchFinally();
		void TestNestingExceptions1();
		void TestNestingExceptions2();
		void Throw1();
		virtual System::Type* GetType();
		
		
	protected:
		virtual void GC_MarkChildren(int flag);
	};
	
	class _ExceptionTest_RTTI : public System::RuntimeType
	{
		_ExceptionTest_RTTI();
	public:
		inline static _ExceptionTest_RTTI* Instance()
		{
			static _ExceptionTest_RTTI instance;
			return &instance;
		}
		
		virtual System::Object *CreateNewWithDefaultConstructor();
		virtual System::Object *CreateNew(System::Array<System::Object*> parameters);
		virtual ~_ExceptionTest_RTTI();
	};
}

#endif

