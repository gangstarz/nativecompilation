#include "TestApplication.EventTest.h"
#include "TestApplication.TestEvent.h"

namespace TestApplication
{
	EventTest::EventTest()
	{
	}
	
	EventTest::EventTest(int _shouldInit)
	{
		_constructor();
	}
	
	void EventTest::_constructor()
	{
	}
	
	void EventTest::add_MyEvent(TestEvent* value)
	{
	}
	
	void EventTest::remove_MyEvent(TestEvent* value)
	{
	}
	
	void EventTest::IncDel1(_int id)
	{
	}
	
	void EventTest::IncDel2(_int id)
	{
	}
	
	void EventTest::TestEvent()
	{
	}
	
	void EventTest::GC_MarkChildren(int flag)
	{
		MyEvent->GC_Mark(flag);
	}
	
	System::Type *EventTest::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(13);
	}
	
	_EventTest_RTTI::_EventTest_RTTI() : RuntimeType(L"EventTest", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_EventTest_RTTI::CreateNewWithDefaultConstructor()
	{
		return new EventTest(1);
	}
	
	System::Object *_EventTest_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_EventTest_RTTI::~_EventTest_RTTI()
	{
	}
}


