#include "TestApplication.BaseClass.h"

namespace TestApplication
{
	static string* _str0 = new string(L"Non-virtual call should call BaseClass.Foo()");
	static string* _str1 = new string(L"Virtual call with overload base should call DerivedClass1.VirtualCall()");
	static string* _str2 = new string(L"Non-call with new implementation should call BaseClass.NewFoo()");
	
	BaseClass::BaseClass()
	{
	}
	
	BaseClass::BaseClass(int _shouldInit)
	{
		_constructor();
	}
	
	void BaseClass::_constructor()
	{
		this->System::Object::_constructor();
	}
	
	void BaseClass::TestInBase()
	{
		_bool val = 0;
		if (this->BaseClass::Foo() != 1)
		{
			System::Console::WriteLine(_str0);
		}
		
		if (this->VirtualCall() != 4)
		{
			System::Console::WriteLine(_str1);
		}
		
		if (this->BaseClass::NewFoo() != 5)
		{
			System::Console::WriteLine(_str2);
		}
	}
	
	_int BaseClass::Foo()
	{
		_int i = 0;
		return 1;
	}
	
	_int BaseClass::VirtualCall()
	{
		_int i = 0;
		return 2;
	}
	
	_int BaseClass::NewFoo()
	{
		_int i = 0;
		return 5;
	}
	
	void BaseClass::GC_MarkChildren(int flag)
	{
	}
	
	System::Type *BaseClass::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(5);
	}
	
	_BaseClass_RTTI::_BaseClass_RTTI() : RuntimeType(L"BaseClass", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_BaseClass_RTTI::CreateNewWithDefaultConstructor()
	{
		throw System::MissingMemberException();
	}
	
	System::Object *_BaseClass_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_BaseClass_RTTI::~_BaseClass_RTTI()
	{
	}
}


