#include "TestApplication.IInterface3.h"

namespace TestApplication
{
	_IInterface3_RTTI::_IInterface3_RTTI() : RuntimeType(L"IInterface3", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_IInterface3_RTTI::CreateNewWithDefaultConstructor()
	{
		throw System::MissingMemberException();
	}
	
	System::Object *_IInterface3_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_IInterface3_RTTI::~_IInterface3_RTTI()
	{
	}
}


