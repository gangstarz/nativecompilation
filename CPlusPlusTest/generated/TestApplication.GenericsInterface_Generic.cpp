#include "TestApplication.GenericsInterface_Generic.h"

namespace TestApplication
{
	_GenericsInterface_Generic_1_RTTI::_GenericsInterface_Generic_1_RTTI() : RuntimeType(L"GenericsInterface", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_GenericsInterface_Generic_1_RTTI::CreateNewWithDefaultConstructor()
	{
		throw System::MissingMemberException();
	}
	
	System::Object *_GenericsInterface_Generic_1_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_GenericsInterface_Generic_1_RTTI::~_GenericsInterface_Generic_1_RTTI()
	{
	}
}


