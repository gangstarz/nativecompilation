#include "TestApplication.DelegateTest.h"
#include "TestApplication.TestDelegate.h"

namespace TestApplication
{
	static string* _str0 = new string(L"Delegate 1 with \'this\' ptr should return 2.");
	static string* _str1 = new string(L"Delegate 2 with \'this\' ptr should return 3.");
	static string* _str2 = new string(L"Delegate 2 with \'this\' ptr should return 4 after changing variable.");
	static string* _str3 = new string(L"Delegate 3 with static should return 2.");
	static string* _str4 = new string(L"Delegate 4 with static should return 3.");
	static string* _str5 = new string(L"Passed delegate value != val");
	
	DelegateTest::DelegateTest()
	{
	}
	
	DelegateTest::DelegateTest(int _shouldInit)
	{
		_constructor();
	}
	
	void DelegateTest::_constructor()
	{
		this->add = 2;
		this->System::Object::_constructor();
	}
	
	void DelegateTest::TestDerived2()
	{
		DelegateTest* delegateTest = 0;
		TestDelegate testDelegate = std::bind(&DelegateTest::Test1, this, std::placeholders::_1);
		_bool val = 0;
		if (testDelegate(1) != 12)
		{
			System::Console::WriteLine(_str0);
		}
		
		this->DelegateTest::Test2(testDelegate, 12);
		testDelegate = std::bind(static_cast<_int(DelegateTest::*)(_int)>(&DelegateTest::Test2), this, std::placeholders::_1);
		if (testDelegate(1) != 3)
		{
			System::Console::WriteLine(_str1);
		}
		
		this->DelegateTest::Test2(testDelegate, 3);
		this->add = 3;
		if (testDelegate(1) != 4)
		{
			System::Console::WriteLine(_str2);
		}
		
		this->DelegateTest::Test2(testDelegate, 4);
	}
	
	_int DelegateTest::Foo()
	{
		_int i = 0;
		return add;
	}
	
	_int DelegateTest::Test1(_int id)
	{
		_int i = 0;
		return id + 1;
	}
	
	_int DelegateTest::Test2(_int id)
	{
		_int i = 0;
		return id + this->DelegateTest::Foo();
	}
	
	_int DelegateTest::STest1(_int id)
	{
		_int i = 0;
		return id + 1;
	}
	
	_int DelegateTest::STest2(_int id)
	{
		_int i = 0;
		return id + 2;
	}
	
	void DelegateTest::Test()
	{
		DelegateTest* delegateTest = 0;
		TestDelegate testDelegate = std::bind(&DelegateTest::Test1, this, std::placeholders::_1);
		_bool val = 0;
		if (testDelegate(1) != 2)
		{
			System::Console::WriteLine(_str0);
		}
		
		this->DelegateTest::Test2(testDelegate, 2);
		testDelegate = std::bind(static_cast<_int(DelegateTest::*)(_int)>(&DelegateTest::Test2), this, std::placeholders::_1);
		if (testDelegate(1) != 3)
		{
			System::Console::WriteLine(_str1);
		}
		
		this->DelegateTest::Test2(testDelegate, 3);
		this->add = 3;
		if (testDelegate(1) != 4)
		{
			System::Console::WriteLine(_str2);
		}
		
		this->DelegateTest::Test2(testDelegate, 4);
		testDelegate = std::bind(&DelegateTest::STest1, std::placeholders::_1);
		if (testDelegate(1) != 2)
		{
			System::Console::WriteLine(_str3);
		}
		
		this->DelegateTest::Test2(testDelegate, 2);
		testDelegate = std::bind(&DelegateTest::STest2, std::placeholders::_1);
		if (testDelegate(1) != 3)
		{
			System::Console::WriteLine(_str4);
		}
		
		this->DelegateTest::Test2(testDelegate, 3);
	}
	
	void DelegateTest::Test2(TestDelegate& del, _int val)
	{
		_bool val1 = 0;
		if (del(1) != val)
		{
			System::Console::WriteLine(_str5);
		}
	}
	
	void DelegateTest::GC_MarkChildren(int flag)
	{
	}
	
	System::Type *DelegateTest::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(3);
	}
	
	_DelegateTest_RTTI::_DelegateTest_RTTI() : RuntimeType(L"DelegateTest", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_DelegateTest_RTTI::CreateNewWithDefaultConstructor()
	{
		return new DelegateTest(1);
	}
	
	System::Object *_DelegateTest_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_DelegateTest_RTTI::~_DelegateTest_RTTI()
	{
	}
}


