#include "TestApplication.DerivedDelegateTest.h"
#include "TestApplication.DelegateTest.h"
#include "TestApplication.TestDelegate.h"

namespace TestApplication
{
	static string* _str0 = new string(L"Delegate 1 with \'this\' ptr should return 2.");
	static string* _str1 = new string(L"Delegate 2 with \'this\' ptr should return 3.");
	static string* _str2 = new string(L"Delegate 2 with \'this\' ptr should return 4 after changing variable.");
	
	DerivedDelegateTest::DerivedDelegateTest()
	{
	}
	
	DerivedDelegateTest::DerivedDelegateTest(int _shouldInit)
	{
		_constructor();
	}
	
	void DerivedDelegateTest::_constructor()
	{
		this->DelegateTest::_constructor();
	}
	
	_int DerivedDelegateTest::Test1(_int id)
	{
		_int i = 0;
		return this->DelegateTest::Test1(id) + 10;
	}
	
	void DerivedDelegateTest::TestDerived1()
	{
		DerivedDelegateTest* derivedDelegateTest = 0;
		TestDelegate testDelegate = std::bind(&DelegateTest::Test1, this, std::placeholders::_1);
		_bool val = 0;
		if (testDelegate(1) != 12)
		{
			System::Console::WriteLine(_str0);
		}
		
		this->DelegateTest::Test2(testDelegate, 12);
		testDelegate = std::bind(static_cast<_int(DelegateTest::*)(_int)>(&DelegateTest::Test2), this, std::placeholders::_1);
		if (testDelegate(1) != 3)
		{
			System::Console::WriteLine(_str1);
		}
		
		this->DelegateTest::Test2(testDelegate, 3);
		this->add = 3;
		if (testDelegate(1) != 4)
		{
			System::Console::WriteLine(_str2);
		}
		
		this->DelegateTest::Test2(testDelegate, 4);
	}
	
	void DerivedDelegateTest::GC_MarkChildren(int flag)
	{
	}
	
	System::Type *DerivedDelegateTest::GetType()
	{
		return ::RTTIMetaInfo::GetTypeById(2);
	}
	
	_DerivedDelegateTest_RTTI::_DerivedDelegateTest_RTTI() : RuntimeType(L"DerivedDelegateTest", L"TestApplication", RTTIMetaInfo::Instance().modules[1])
	{
	}
	
	System::Object *_DerivedDelegateTest_RTTI::CreateNewWithDefaultConstructor()
	{
		return new DerivedDelegateTest(1);
	}
	
	System::Object *_DerivedDelegateTest_RTTI::CreateNew(System::Array<Object*> parameters)
	{
		throw System::NotImplementedException();
	}
	
	_DerivedDelegateTest_RTTI::~_DerivedDelegateTest_RTTI()
	{
	}
}


