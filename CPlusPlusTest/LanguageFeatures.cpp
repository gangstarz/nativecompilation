#include "LanguageFeatures.h"
#include "BasicTypes.h"

namespace System
{
	// Object.

	_bool Object::Equals(object *o)
	{
		std::cout << "Base equals obj called" << std::endl;
		return this == o;
	}

	_bool Object::Equals(object *lhs, object *rhs)
	{
		if (lhs == rhs) { return true; }
		if (!lhs && !rhs) { return true; }
		if (!lhs || !rhs) { return false; }
		return lhs->Equals(rhs);
	}

	_int Object::GetHashCode() {
		return reinterpret_cast<int>(static_cast<void*>(this)); // memory address or something like that...
	}

	_bool Object::ReferenceEquals(object *lhs, object *rhs)
	{
		return lhs == rhs;
	}

	void* Object::operator new(size_t s)
	{
		void *res = malloc(s);

		if (res == 0)
		{
			throw std::bad_alloc();
		}

		GC::Instance().RegisterForCollector(res);
		return res;
	}

		void Object::operator delete(void*) {
	}

	// GC

	int GC::generations[11] = { 0, 1, 0, 1, 2, 0, 1, 0, 1, 2, 3 };

	_int GC::get_MaxGeneration() {
		return 3;
	}
	
	void GC::AddMemoryPressure(_long bytesAllocated) 
	{
	}

	void GC::WaitForFullGCComplete()
	{
	}

	void GC::WaitForFullGCComplete(_int timeoutInSeconds){
		// Trigger GC.Collect or not at all
	}

	void GC::Collect() {
		Instance().GarbageCollect();
	}

	void GC::Collect(_int generations) {
		Instance().GarbageCollect(generations);
	}

	void GC::Collect(_int generations, GCCollectionMode mode) {
		Instance().GarbageCollect(generations);
	}

	void GC::Collect(_int generations, GCCollectionMode mode, _bool blocking) {
		Instance().GarbageCollect((int)generations);
	}

	_int GC::CollectionCount(_int generation) {
		return Instance().count;
	}

}