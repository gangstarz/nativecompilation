#ifndef SYSTEM_CONSOLE_H
#define SYSTEM_CONSOLE_H

#include "LanguageFeatures.h"
#include "BasicTypes.h"

namespace System 
{
	struct Console : public virtual Object
	{
		static void WriteLine();
		static void WriteLine(string *s);
		static void WriteLine(_double d);
		static void WriteLine(_int i);
	};

}
#endif