#include "BasicTypes.h"

#include <string>
#include <codecvt>

namespace System
{
	// String
	String::String(const wchar_t* ch)
	{
		if (sizeof(wchar_t) == 2)
		{
			int len = 0;
			const wchar_t* i;
			for (i = ch; *i; ++i, ++len) {}

			this->data = new uint16_t[i - ch];
			this->length = len;

			uint16_t* ptr = data;
			for (const wchar_t* j = ch; *j; ++j)
			{
				*ptr++ = *j;
			}
		}
		else // 4 bytes per item
		{
			int len = 0;
			const wchar_t* i;
			for (i = ch; *i; ++i) 
			{
				if ((*i) >= 0xd800)
				{ 
					len += 2; 
				}
				else 
				{ 
					len++;
				}
			}

			this->data = new uint16_t[len];
			this->length = len;

			uint16_t* ptr = data;
			for (i = ch; *i; ++i)
			{
				if ((*i) >= 0xd800)
				{
					*ptr++ = ((*i) / 0x400) + 0xd800;
					*ptr++ = ((*i) % 0x400) + 0xdc00;

					i += 2;
				}
				else
				{
					*ptr++ = *i++;
				}
			}
		}
	}

	// Void

	_bool Void::Equals(object *o){
		return dynamic_cast<Void*>(o) != 0;
	}

	_int Void::GetHashCode() {
		return 0x4382;
	}

	_int ValueType::GetHashCode() {
		// Not implemented...
		return 0;
	}
}