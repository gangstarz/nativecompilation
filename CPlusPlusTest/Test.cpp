#include "LanguageFeatures.h"
#include "BasicTypes.h"
#include <iostream>
#include <string>

using namespace System;

struct Foo : Object, IComparableGeneric<Foo*>, System::IDisposable
{
	Foo() {
		static int n = 0;
		this->n = n++;
		std::cout << "Foo " << this->n << " constructed" << std::endl;
	}

	int n;

	Foo(const Foo& foo) {
		std::cout << "Foo " << n << " copied" << std::endl;
	}

	virtual _bool Equals(object *o) {
		std::cout << "Derived obj called" << std::endl;
		Foo* foo = dynamic_cast<Foo*>(o);
		return foo!=0;
	}

	virtual _bool Equals(Foo* other) {
		std::cout << "Foo " << n << " overload called" << std::endl;
		return 1;
	}

	virtual void Dispose() {
		std::cout << "Dispose" << n << " now " << std::endl;
		GC::SuppressFinalize(this);
	}

	virtual ~Foo() {
		std::cout << "Foo " << n << " destructed" << std::endl;
	}

protected:
};

struct Bar : Object {
	Bar(Foo* ptr) {
		std::cout << "Bar constructed" << std::endl;
		this->ptr = ptr;
	}

	Foo *ptr;

	virtual ~Bar() {
		std::cout << "Bar destructed" << std::endl;
	}

protected:
	virtual void GC_MarkChildren(int flag) {
		ptr->GC_Mark(flag);
	}
};

StackPtr<Bar> Handle() {
	StackPtr<Foo> ptr1 = new Foo();
	StackPtr<Foo> ptr2 = new Foo();

	std::cout << (ptr1.Get()->Equals(ptr2.Get())) << std::endl;

	ptr1.Get()->Dispose();
	
	return StackPtr<Bar>(new Bar(ptr1.Get()));
}

void test() {
	StackPtr<Bar> ptr = Handle();
	std::cout << "Collect 1." << std::endl;
	GC::Collect();
}


int maintje() 
{
	test();
	std::cout << "Collect 2." << std::endl;
	GC::Collect();
	std::string s;
	std::cin >> s;
	return 0;
}
