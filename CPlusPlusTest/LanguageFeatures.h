#ifndef LANGUAGEFEATURES_H
#define LANGUAGEFEATURES_H

#include <iostream>
#include <cstdint>
#include <vector>
#include <functional>

namespace System
{
	// Forward declarations
	class Object;
	class String;
	class Type;
	class Boolean;
	class Char;
	class Byte;
	class SByte;
	class Int16;
	class Int32;
	class Int64;
	class UInt16;
	class UInt32;
	class UInt64;
	class Single;
	class Double;
	class Decimal;
	class DateTime;
}

typedef System::String string;
typedef System::Object object;
typedef bool _bool;
typedef uint16_t _char;
typedef uint8_t _byte;
typedef int8_t _sbyte;
typedef int16_t _short;
typedef int32_t _int;
typedef int64_t _long;
typedef uint16_t _ushort;
typedef uint32_t _uint;
typedef uint64_t _ulong;
typedef float _float;
typedef double _double;
typedef System::Decimal _decimal; // not sure

namespace System
{
	struct IDisposable
	{
		virtual void Dispose() { }

	protected:
		~IDisposable() {}
	};

	// Forward decl
	class GC;

	class Object
	{
		friend class GC;

		int flags;

	protected:
		virtual void GC_MarkChildren(int flag) {
			// for all children: Mark();
		}

	public:
		void _constructor(){}

		void* operator new(size_t s);
		void operator delete(void*);

		inline void GC_Mark(int flag) {
			if ((flags & flag) == 0)
			{
				flags = (flags & ~0xC) | flag;

				this->GC_MarkChildren(flag);
			}
		}

		Object() : flags(0)
		{
			//std::cout << "Object " << std::endl;
		}

		virtual _bool Equals(object *o);

		static _bool Equals(object *lhs, object *rhs);

		virtual _int GetHashCode();

		inline void Finalize()
		{
			if ((flags & 0x10000) == 0) // Check suppress finalize flag
			{
				System::IDisposable* ptr = dynamic_cast<System::IDisposable*>(this);
				if (ptr) {
					ptr->Dispose();
				}
			}
		}

		static _bool ReferenceEquals(object *lhs, object *rhs);

		// todo GetType()
		// todo ToString() => = GetType().NameSpace + "." + GetType().Name
		// todo MemberwiseClone()

		virtual ~Object()
		{
			// Finalizer is called from the GC.
		}
	};

	template <typename T>
	class WeakReference {
		T* ptr;
	public:
		WeakReference(T* ptr) : ptr(ptr)
		{
		}

		bool TryGetTarget(T** target) {
			*target = ptr.get();
		}

		void SetTarget(T* ptr) {
			this->ptr = ptr
		}

		// GetObjectData 
	};

	struct GCCollectionMode {
		static const int32_t Default = 0;
		static const int32_t Forced = 1;
		static const int32_t Optimized = 2;
	};

	class StackPtrBase {
	public:
		virtual void GC_Mark(int flag) = 0;
		virtual ~StackPtrBase(){}
	};

	template <typename T>
	class StackPtr : public StackPtrBase {
		friend class GC;

		T* ptr;
	public:
		StackPtr(T* ptr) : ptr(ptr) {
			GC::Instance().AddStackFrame(this);
		}

		virtual void GC_Mark(int flag) {
			ptr->GC_Mark(flag);
		}

		inline T* Get() {
			return ptr;
		}

		virtual ~StackPtr()
		{
			GC::Instance().RemoveStackFrame(this);
		}
	};

	class GC
	{
		static size_t threshold;
		friend class Object;

		template <class T>
		friend class StackPtr;

		friend class StackPtrBase;

		//
		// The PointerSet is basically an open addressing hash set using the pointer as both hash code and value.
		//
		class PointerSet
		{
			int capacity;
			int count;
			Object** ptrs;

			void Grow()
			{
				if (count * 3 > capacity * 2)
				{
					int newCapacity;
					if (capacity > 1024 * 1024)
					{
						newCapacity = capacity + 1024 * 1024;
					}
					else
					{
						newCapacity = capacity * 2;
					}

					// Create new hash table
					Object** newptrs = new Object*[newCapacity];
					for (Object** i = newptrs; i != newptrs + newCapacity; ++i)
					{
						*i = 0;
					}

					// Rebuild hash table
					for (Object** i = ptrs; i != ptrs + capacity; ++i)
					{
						if ((*i) != 0)
						{
							int hashcode = (reinterpret_cast<int>(*i) * 7577) & 0x7FFFFFFF;
							int idx = hashcode % newCapacity;
							while (newptrs[idx] != 0) {
								idx = (idx + 1) % newCapacity;
							}

							newptrs[idx] = *i;
						}
					}

					std::swap(newptrs, ptrs);
					std::swap(newCapacity, capacity);
				}
			}

		public:
			PointerSet() {
				ptrs = new Object*[1024];
				for (int i = 0; i < 1024; ++i) {
					ptrs[i] = 0;
				}

				count = 0;
				capacity = 1024;
			}

			typedef Object** iterator;

			int Count() { return count; }
			int Capacity() { return capacity; }

			iterator begin()
			{
				return ptrs;
			}

			iterator end()
			{
				return ptrs + capacity;
			}

			void Add(Object* ptr)
			{
				int hashcode = (reinterpret_cast<int>(ptr)* 7577) & 0x7FFFFFFF;
				int idx = hashcode % capacity;
				while (ptrs[idx] != 0) {
					idx = (idx + 1) % capacity;
				}

				ptrs[idx] = ptr;
				++count;

				Grow();
			}

			void Remove(Object** item)
			{
				Object** start = ptrs;
				int idx = item - start;

				int cap = capacity;
				int i = idx;
				int j = idx;

				while (1)
				{
					ptrs[i] = 0;
				r2:
					j = (j + 1) % cap;
					if (!ptrs[j])
					{
						return;
					}

					int k = ((reinterpret_cast<int>(ptrs[j]) * 7577) & 0x7FFFFFFF) % cap;

					// determine if k lies cyclically in ]i,j]
					// |    i.k.j |
					// |....j i.k.| or  |.k..j i...|
					if ((i <= j) ? ((i < k) && (k <= j)) : ((i < k) || (k <= j)))
					{
						goto r2;
					}

					ptrs[i] = ptrs[j];
					i = j;

				}
			}
		};

		GC() {
		}

		void RegisterForCollector(void* ptr) {
			objectsGen0.Add(reinterpret_cast<Object*>(ptr));
		}

		int count;

		typedef PointerSet objectList;

		static int generations[11];

		// For now, we only have generation 0. TODO FIXME: Copy-paste into gen 1-2-3.
		objectList objectsGen0;

		objectList rootObjects;
		std::vector<StackPtrBase*> stackFrames;
		bool collectorRunning = false;

		void GarbageCollect()
		{
			// Determine current generation
			int gen = generations[count % 11];
			GarbageCollect(gen);
		}

		static GC& Instance() {
			static GC instance;
			return instance;
		}

		template <class T>
		void AddStackFrame(StackPtr<T>* ptr)
		{
			stackFrames.push_back(ptr);
		}

		template <class T>
		void RemoveStackFrame(StackPtr<T>* ptr)
		{
			// Normally the first frame is removed.
			// TODO FIXME: Multiple threads?
			std::vector<StackPtrBase*>::iterator it;
			for (it = stackFrames.end() - 1; it != stackFrames.begin(); --it) {
				if ((*it) == ptr) {
					stackFrames.erase(it);
					return;
				}
			}

			if (it != stackFrames.end() && (*it) == ptr) {
				stackFrames.erase(it);
				return;
			}
		}

		void GarbageCollect(int gen)
		{
			// TODO FIXME: Use interlocked to ensure there's only 1 thread running.
			if (collectorRunning) return;
			collectorRunning = true;

			// Determine the flag to use for GC
			int flag = ((count & 1) + 1) << 2;
			count = (count + 1) & INT32_MAX;

			// Spinlock, grab pointers and counts of what we're going to collect
			// TODO FIXME: Spinlock stuff
			int collectCount = objectsGen0.Capacity();

			// Phase 1: mark
			for (objectList::iterator i = rootObjects.begin(); i != rootObjects.end(); ++i)
			{
				if (*i) {
					(*i)->GC_Mark(flag);
				}
			}

			for (std::vector<StackPtrBase*>::iterator it = stackFrames.begin(); it != stackFrames.end(); ++it)
			{
				(*it)->GC_Mark(flag);
			}

			// Phase 2: sweep.
			for (objectList::iterator i = objectsGen0.begin(); i != objectsGen0.end(); ++i)
			{
				if ((*i) && ((*i)->flags & 0xC) != flag)
				{
					Object* ptrToRemove = *i;

					// Collect.
					try {
						// Remove it from the collectable objects
						objectsGen0.Remove(i);

						try {
							// Check if we should finalize
							ptrToRemove->Finalize();
						}
						catch (...)
						{
						}

						// Kill it.
						delete ptrToRemove;
					}
					catch (...)
					{
					}
				}
			}

			collectorRunning = false;
		}


	public:
		static void SuppressFinalize(object *o) {
			o->flags |= 0x10000;
		}

		static void ReRegisterForFinalize(object *o) {
			o->flags |= 0x10000;
		}

		static _int get_MaxGeneration();

		static void AddMemoryPressure(_long bytesAllocated);

		static void RemoveMemoryPressure(_long bytesAllocated);
		static void WaitForFullGCComplete();
		static void WaitForFullGCComplete(_int timeoutInSeconds);

		static void Collect();
		static void Collect(_int generations);
		static void Collect(_int generations, GCCollectionMode mode);
		static void Collect(_int generations, GCCollectionMode mode, _bool blocking);

		static _int CollectionCount(_int generation);

	};

	template <typename T>
	class Array : public Object
	{
	public:
		T* data;
		int count;

		inline Array() : count(0), data(0) { }
		inline Array(Array& o) : count(o.count), data(o.data) { }
		inline Array(int length) : count(length), data(new T[length]) { }

		template <int S>
		inline Array(const T(&initializer)[S]) {
			count = S;
			this->data = new T[S];
			for (int i = 0; i < S; ++i){
				this->data[i] = initializer[i];
			}
		}

		inline T& operator[](int idx)
		{
			return data[idx];
		};

		inline int get_Length() { return count; }
	};

}

#endif

#include "BasicTypes.h"
#include "TypeSystem.h"
#include "Console.h"
