#include "Console.h"
#include <iostream>
#include <string>
#include <codecvt>

namespace System
{
	void Console::WriteLine()
	{
		std::wcout << std::endl;
	}

	void Console::WriteLine(string *s)
	{
		uint16_t *end = s->data + s->length;

		std::wstring_convert<std::codecvt_utf16<wchar_t, 0x10ffff, std::little_endian>, wchar_t> conv;
		std::wstring ws = conv.from_bytes(
			reinterpret_cast<const char*> (s->data),
			reinterpret_cast<const char*> (s->data + s->length));

		std::wcout << ws << std::endl;
	}

	void Console::WriteLine(_double d) {
		std::cout << d << std::endl;
	}
	void Console::WriteLine(_int i) {
		std::cout << i << std::endl;
	}

}