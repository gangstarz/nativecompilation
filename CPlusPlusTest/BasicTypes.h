#ifndef BASICTYPES_H
#define BASICTYPES_H

#include "LanguageFeatures.h"

namespace System
{
	// Forward declarations

	class String;
	class Type;
	class Boolean;
	class Char;
	class Byte;
	class SByte;
	class Int16;
	class Int32;
	class Int64;
	class UInt16;
	class UInt32;
	class UInt64;
	class Single;
	class Double;
	class Decimal;
	class DateTime;

	class NotImplementedException;
	class FormatException;
	class ArgumentException;
	class ArgumentNullException;
	class InvalidCastException;

	// Basic interfaces

	class IComparable {
	public:
		virtual _bool Equals(object other) = 0;

	protected:
		~IComparable() {}
	};

	template <typename T>
	class IComparableGeneric {
	public:
		virtual _bool Equals(T other) = 0;

	protected:
		~IComparableGeneric(){}
	};

	class IFormatProvider {
	public:
		virtual object GetFormat(Type *formatType) = 0;

	protected:
		~IFormatProvider() {}
	};

	class TypeCode {
	public:
		const int Boolean = 3;
		const int Byte = 6;
		const int Char = 4;
		const int DateTime = 16;
		const int DBNull = 2;
		const int Decimal = 15;
		const int Double = 14;
		const int Empty = 0;
		const int Int16 = 7;
		const int Int32 = 9;
		const int Int64 = 11;
		const int Object = 1;
		const int SByte = 5;
		const int Single = 13;
		const int String = 0x12;
		const int UInt16 = 8;
		const int UInt32 = 10;
		const int UInt64 = 12;
	};

	class IFormattable {
	public:
		virtual string ToString(string format, IFormatProvider* provider) = 0;
	protected:
		~IFormattable() {}
	};

	template <typename T>
	class IEquatableGeneric
	{
	public:
		virtual _bool Equals(T other) = 0;
	protected:
		~IEquatableGeneric() {}
	};


	// Basic types

	class ValueType : public Object 
	{
		virtual _int GetHashCode();
	};

	class Void : ValueType
	{
	public:
		virtual _bool Equals(object *o);
		virtual _int GetHashCode();
	};

	class Boolean : public ValueType {
	public:
		bool value;

		Boolean() : value(false) {}
		Boolean(bool b) : value(b) { }

		operator bool() const { return value; }
	};

	class Byte : public ValueType
	{
	public:
		uint8_t value;

		Byte() : value(0) {}
		Byte(uint8_t value);
		Byte(const _byte &value);

		operator uint8_t() const { return value; }
	};

	class SByte : public ValueType
	{
	public:
		int8_t value;

		SByte() : value(0) {}
		SByte(int8_t value);
		SByte(const _sbyte &value);

		operator int8_t() const { return value; }
	};

	class Int16 : public ValueType
	{
	public:
		int16_t value;

		Int16() : value(0) {}
		Int16(int16_t value);
		Int16(const _short &value);

		operator int16_t() const { return value; }
	};

	class Int32 : public ValueType
	{
	public:
		int32_t value;

		Int32() : value(0) {}
		Int32(int32_t value) : value(value){}
		Int32(const Int32 &value) : value(value.value) {}

		operator int32_t() const { return value; }
	};

	class Int64 : public ValueType
	{
	public:
		int64_t value;

		Int64() : value(0) {}
		Int64(int64_t value);
		Int64(const _long &value);

		operator int64_t() const { return value; }
	};

	class UInt16: public ValueType
	{
	public:
		uint16_t value;

		UInt16() : value(0) {}
		UInt16(uint16_t o) : value(o){}
		UInt16(const UInt16 &o) : value(o.value){}

		operator uint16_t() const { return value; }
	};

	class UInt32 : public ValueType
	{
	public:
		uint32_t value;

		UInt32() : value(0) {}
		UInt32(uint32_t o) : value(o){}
		UInt32(const UInt32 &o) : value(o.value){}

		operator uint32_t() const { return value; }
	};

	class UInt64 : public ValueType
	{
	public:
		uint64_t value;

		UInt64() : value(0) {}
		UInt64(uint64_t o) : value(o){}
		UInt64(const UInt64 &o) : value(o.value){}

		operator uint64_t() const { return value; }
	};

	class Char : public ValueType
	{
	public:
		uint16_t data;

		Char() : data(0){}
		Char(char ch) : data(ch) { }
		Char(wchar_t ch) : data(ch) { }
		Char(uint16_t ch) : data(ch){ }

		operator uint16_t() const { return data; }
	};

	class Single : public ValueType
	{
	public:
		float value;

		Single() : value(0) {}
		Single(float o) : value(o){}
		Single(const Single &o) : value(o.value){}

		operator float() const { return value; }
	};

	class Double: public ValueType
	{
	public:
		double value;

		Double() : value(0) {}
		Double(double o) : value(o){}
		Double(const Double &o) : value(o.value){}

		operator double() const { return value; }
	};

	class Decimal : public ValueType
	{
	public:
		// TODO: Implement. See: http://msdn.microsoft.com/en-us/library/system.decimal(v=vs.110).aspx
		int64_t value;

		Decimal() : value(0) {}
		Decimal(uint32_t o) : value(o){}

		static Decimal MinValue;
		static Decimal MaxValue;
	};

	class DateTime: public ValueType
	{
	public:
		// TODO: Implement. See: http://msdn.microsoft.com/en-us/library/system.decimal(v=vs.110).aspx
		uint64_t ticks;

		DateTime() : ticks(0) {}
		DateTime(uint64_t ticks) : ticks(ticks){}
	};

	class String : public Object
	{
	public:
		// No one is going to use this except for us, so this might as well just be public.
		uint16_t* data;
		int length;

		String(const char* ch) {
			const char* i;
			for (i = ch; *i != 0; ++i){}
			int len = i - ch;
			this->length = len;

			this->data = new uint16_t[len];
			uint16_t* wptr = this->data;
			for (i = ch; *i != 0; ++i)
			{
				*wptr++ = *i;
			}
		}

		String(const wchar_t* ch);

		String(Char* data)
		{
			Char* i;
			for (i = data; *i != 0; ++i){}
			int len = i - data;
			this->length = len;

			this->data = new uint16_t[len];
			uint16_t* wptr = this->data;
			for (i = data; *i != 0; ++i)
			{
				*wptr++ = i->data;
			}
		}

		String(SByte* data)
		{
			SByte* i;
			for (i = data; *i != 0; ++i){}
			int len = i - data;
			this->length = len;

			this->data = new uint16_t[len];
			uint16_t* wptr = this->data;
			for (i = data; *i != 0; ++i)
			{
				*wptr++ = *i;
			}
		}

		String(Array<Char> *data) {
			this->data = new uint16_t[data->count];
			this->length = data->count;

			uint16_t* wptr = this->data;
			Char* limit = data->data + data->count;
			for (Char *ptr = data->data; ptr != limit; ++ptr) {
				*(wptr++) = ptr->data;
			}
		}

		String(Char &ch, int count) {
			this->data = new uint16_t[count];
			this->length = count;

			for (uint16_t* ptr = this->data; ptr != this->data + count; ++ptr) {
				*ptr = ch.data;
			}
		}

		~String()
		{
			delete[] data;
		}
	};

	class Exception : public Object
	{
	public:
		string *str;
		Exception* inner;
		
		Exception() {}
		Exception(string *str) : str(str) { }
		Exception(string *str, Exception *inner) : str(str), inner(inner) { }
	};

	class ArgumentException : Exception
	{
	public:
		ArgumentException(string *str) : Exception(str) {}
		virtual ~ArgumentException(){}
	};

	class ArgumentNullException : Exception
	{
	public:
		ArgumentNullException(string *str) : Exception(str) {}
	};

	class InvalidCastException : Exception
	{
	public:
		InvalidCastException(string *str) : Exception(str) {}
	};

	class FormatException : Exception
	{
	public:
		FormatException(string *str) : Exception(str) {}
	};

	class NotImplementedException : Exception
	{
	public:
		NotImplementedException() : Exception() {}
		NotImplementedException(string *str) : Exception(str) {}
	};

	class SystemException : Exception
	{
	public:
		SystemException() : Exception() {}
	};

	class MemberAccessException : SystemException
	{
	public:
		MemberAccessException() : SystemException() {}
	};
	
	class MissingMemberException : MemberAccessException
	{
	public:
		MissingMemberException() : MemberAccessException() {}
	};

	class MissingMethodException : MissingMemberException
	{
	public:
		MissingMethodException() : MissingMemberException() {}
	};
}

template <typename T, typename U>
inline T dynamic_cast_or_throw(U* o) 
{
	static System::String invalidCast = System::String("Cannot cast object to target type.");
	T result = dynamic_cast<T>(o);
	if (!result) {
		throw System::InvalidCastException(&invalidCast);
	}
	return result;
}


#endif