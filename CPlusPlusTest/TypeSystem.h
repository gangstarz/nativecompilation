#ifndef TYPESYSTEM_H
#define TYPESYSTEM_H

#include "BasicTypes.h"
#include <string>
#include <vector>

namespace System 
{
	class Type : public Object
	{
	public:
		static Type *GetTypeFromHandle(int handle);

		virtual Object *CreateNewWithDefaultConstructor() = 0;
		virtual Object *CreateNew(System::Array<Object*> parameters) = 0;

		virtual string *ToString();
		virtual ~Type();
	};

	// Forward declarations
	class RuntimeModule;
	class RuntimeAssembly;
	class RuntimeType;

	// Runtime class definitions
	class RuntimeAssembly 
	{
		string *codebase;
		string *fqn;
		bool gac;
		string *dotNetVersion;

	public:
		RuntimeAssembly(const wchar_t* codebase, const wchar_t* fqn, bool gac, wchar_t* dotNetVersion);
		std::vector<RuntimeModule*> modules;
		std::vector<RuntimeType*> types;

		virtual ~RuntimeAssembly() {}
	};

	class RuntimeModule 
	{
		string *fqn;
		string *name;
		RuntimeAssembly* assembly;

	public:
		RuntimeModule(const wchar_t* fqn, const wchar_t* name, RuntimeAssembly* assembly);
		std::vector<RuntimeType*> types;

		virtual ~RuntimeModule() {}
	};
	
	class RuntimeType : public Type {
		string *name;
		string *namesp;
		RuntimeModule* module;

	public:
		RuntimeType(const wchar_t* name, const wchar_t* ns, RuntimeModule* module);

		virtual string *ToString();

		virtual Object *CreateNewWithDefaultConstructor() = 0;
		virtual Object *CreateNew(System::Array<Object*> parameters) = 0;

		virtual ~RuntimeType() {}
	};

	// These are actually .NET calls, but they are so 'primitive' and frequently used, that I prefer to have them here:
	class Activator : public Object 
	{
	public:
		static Object* CreateInstance(Type* t);
	};
}

#endif