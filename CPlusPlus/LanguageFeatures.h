#ifndef LANGUAGEFEATURES_H
#define LANGUAGEFEATURES_H

#include <vector>

#include "Corlib/System.Array.h"

namespace System
{
	struct Pointer
	{
		int MarkSweep;
	};

	template <typename T>
	class Pointer<T> : Pointer
	{
		T* data;

	public:
		Pointer(GC *gc, T* data) {
			GC::getInstance()->AddObject(this);
		}

		Pointer<T>& operator*() {
			return *data;
		}

		Pointer<T> operator->() {
			return data;
		}

		~Pointer() {
		}
	};

	class GC
	{
	public:
		static GC& getInstance()
		{
			static GC instance; // Guaranteed to be destroyed.
			
			// Instantiated on first use.
			return instance;
		}

		void AddObject(Pointer *p) {
			pointers.push_back(p);
		}
	private:
		GC() 
		{
			
		}

		GC(GC const&);
		void operator=(GC const&);
		std::vector<Pointer*> pointers;
	};
	struct GC 
	{
		std::vector<Pointer> pointers;

		template <typename T>
		Pointer<T> Alloc<T>()
		{
			Pointer<T> ptr(this, new T());
		}
	};

	struct Object {

	};

	template <class T>
	struct NormalArray : Array {

	};

	template <class T, int N>
	struct MultiDimensionalArray : Array {

	};
}

#endif