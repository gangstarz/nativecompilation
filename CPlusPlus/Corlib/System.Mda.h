#ifndef __SYSTEM_MDA
#define __SYSTEM_MDA

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Exception;
	class String;
	class ValueType;

	class Mda : public System::Object
	{
	public:
		int IsStreamWriterBufferedDataLostEnabled();
		void MemberInfoCacheCreation();
		int IsInvalidGCHandleCookieProbeEnabled();
		void ReportStreamWriterBufferedDataLost(System::String* text);
		int IsStreamWriterBufferedDataLostCaptureAllocatedCallStack();
		void DateTimeInvalidLocalFormat();
		void FireInvalidGCHandleCookieProbe(int* cookie);
		void ReportErrorSafeHandleRelease(Exception* ex);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
