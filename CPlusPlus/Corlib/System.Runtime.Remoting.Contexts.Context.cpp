#include "System.Runtime.Remoting.Contexts.Context.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace Remoting
		{
			namespace Contexts
			{
				void Context::SetupInternalContext(int bDefault)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void Context::ExecuteCallBackInEE(int* privateData)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void Context::CleanupInternalContext()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

			}
		}
	}
}
