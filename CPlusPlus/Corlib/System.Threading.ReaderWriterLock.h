#ifndef __SYSTEM_THREADING_READERWRITERLOCK
#define __SYSTEM_THREADING_READERWRITERLOCK

#include "../gc.h"
#include <cstdint>
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"

namespace System
{
	class ValueType;
	namespace Threading
	{
		class LockCookie;

		class ReaderWriterLock : public System::Runtime::ConstrainedExecution::CriticalFinalizerObject
		{
		public:
			void AcquireReaderLockInternal(int32_t millisecondsTimeout);
			void AcquireWriterLockInternal(int32_t millisecondsTimeout);
			void ReleaseReaderLockInternal();
			void ReleaseWriterLockInternal();
			void DowngradeFromWriterLockInternal(LockCookie& lockCookie);
			void RestoreLockInternal(LockCookie& lockCookie);
			int PrivateGetIsReaderLockHeld();
			int PrivateGetIsWriterLockHeld();
			int32_t PrivateGetWriterSeqNum();
			int AnyWritersSince(int32_t seqNum);
			void FCallUpgradeToWriterLock(LockCookie& result, int32_t millisecondsTimeout);
			void FCallReleaseLock(LockCookie& result);
			void PrivateInitialize();
			void PrivateDestruct();

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
