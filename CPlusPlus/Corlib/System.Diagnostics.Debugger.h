#ifndef __SYSTEM_DIAGNOSTICS_DEBUGGER
#define __SYSTEM_DIAGNOSTICS_DEBUGGER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;
	namespace Diagnostics
	{
		class ICustomDebuggerNotification;

		class Debugger : public System::Object
		{
		public:
			int IsDebuggerAttached();
			int IsLogging();
			void Log(int32_t level, System::String* category, System::String* message);
			void BreakInternal();
			int LaunchInternal();
			void CustomNotification(ICustomDebuggerNotification* data);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
