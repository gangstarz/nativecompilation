#ifndef __SYSTEM_SECURITY_PRINCIPAL_WIN32
#define __SYSTEM_SECURITY_PRINCIPAL_WIN32

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace Microsoft
{
	namespace Win32
	{
		namespace SafeHandles
		{
			class SafeHandleZeroOrMinusOneIsInvalid;
			class SafeTokenHandle;
		}
	}
}
namespace System
{
	class Enum;
	class ValueType;
	namespace Runtime
	{
		namespace ConstrainedExecution
		{
			class CriticalFinalizerObject;
		}
		namespace InteropServices
		{
			class SafeHandle;
		}
	}
	namespace Security
	{
		namespace Principal
		{
			class TokenAccessLevels;
			class WinSecurityContext;

			class Win32 : public System::Object
			{
			public:
				int32_t OpenThreadToken(TokenAccessLevels dwDesiredAccess, WinSecurityContext OpenAs, Microsoft::Win32::SafeHandles::SafeTokenHandle** phThreadToken);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
