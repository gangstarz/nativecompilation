#ifndef __SYSTEM_RUNTIME_COMPILERSERVICES_TRYCODE
#define __SYSTEM_RUNTIME_COMPILERSERVICES_TRYCODE

#include "../gc.h"
#include <cstdint>
#include "System.MulticastDelegate.h"

namespace System
{
	namespace Runtime
	{
		namespace CompilerServices
		{
			class TryCode : public System::MulticastDelegate
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
