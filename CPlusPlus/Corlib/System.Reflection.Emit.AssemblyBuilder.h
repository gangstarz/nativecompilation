#ifndef __SYSTEM_REFLECTION_EMIT_ASSEMBLYBUILDER
#define __SYSTEM_REFLECTION_EMIT_ASSEMBLYBUILDER

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Reflection.Assembly.h"

namespace System
{
	class AppDomain;
	class Enum;
	class MarshalByRefObject;
	class ValueType;
	namespace Reflection
	{
		class AssemblyName;
		class Module;
		class RuntimeAssembly;
		class RuntimeModule;
		namespace Emit
		{
			class AssemblyBuilderAccess;
			class DynamicAssemblyFlags;
		}
	}
	namespace Security
	{
		class PermissionSet;
		class SecurityContextSource;
		namespace Policy
		{
			class Evidence;
		}
	}
	namespace Threading
	{
		class StackCrawlMark;
	}

	namespace Reflection
	{
		namespace Emit
		{
			class AssemblyBuilder : public System::Reflection::Assembly
			{
			public:
				System::Reflection::RuntimeModule* GetInMemoryAssemblyModule(System::Reflection::RuntimeAssembly* assembly);
				System::Reflection::Assembly* nCreateDynamicAssembly(System::AppDomain* domain, System::Reflection::AssemblyName* name, System::Security::Policy::Evidence* identity, System::Threading::StackCrawlMark& stackMark, System::Security::PermissionSet* requiredPermissions, System::Security::PermissionSet* optionalPermissions, System::Security::PermissionSet* refusedPermissions, System::NormalArray<uint8_t> securityRulesBlob, System::NormalArray<uint8_t> aptcaBlob, AssemblyBuilderAccess access, DynamicAssemblyFlags flags, System::Security::SecurityContextSource securityContextSource);
				System::Reflection::RuntimeModule* GetOnDiskAssemblyModule(System::Reflection::RuntimeAssembly* assembly);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
