#ifndef __MICROSOFT_WIN32_FILE_TIME
#define __MICROSOFT_WIN32_FILE_TIME

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace Microsoft
{
	namespace Win32
	{
		class FILE_TIME : public System::ValueType
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
