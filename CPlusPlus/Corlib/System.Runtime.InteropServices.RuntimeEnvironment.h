#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_RUNTIMEENVIRONMENT
#define __SYSTEM_RUNTIME_INTEROPSERVICES_RUNTIMEENVIRONMENT

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;

	namespace Runtime
	{
		namespace InteropServices
		{
			class RuntimeEnvironment : public System::Object
			{
			public:
				System::String* GetModuleFileName();
				System::String* GetDeveloperPath();
				System::String* GetHostBindingFile();
				System::String* GetRuntimeDirectoryImpl();

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
