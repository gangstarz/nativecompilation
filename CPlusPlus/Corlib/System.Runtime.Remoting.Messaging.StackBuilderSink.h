#ifndef __SYSTEM_RUNTIME_REMOTING_MESSAGING_STACKBUILDERSINK
#define __SYSTEM_RUNTIME_REMOTING_MESSAGING_STACKBUILDERSINK

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace Runtime
	{
		namespace Remoting
		{
			namespace Messaging
			{
				class StackBuilderSink : public System::Object
				{
				public:
					System::Object* _PrivateProcessMessage(int* md, System::NormalArray<System::Object*>* args, System::Object* server, System::NormalArray<System::Object*>** outArgs);

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
