#ifndef __SYSTEM_IO_STREAM
#define __SYSTEM_IO_STREAM

#include "../gc.h"
#include <cstdint>
#include "System.MarshalByRefObject.h"

namespace System
{
	namespace IO
	{
		class Stream : public System::MarshalByRefObject
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
