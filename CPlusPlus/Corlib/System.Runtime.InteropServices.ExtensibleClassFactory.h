#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_EXTENSIBLECLASSFACTORY
#define __SYSTEM_RUNTIME_INTEROPSERVICES_EXTENSIBLECLASSFACTORY

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Delegate;
	class MulticastDelegate;
	class ValueType;
	namespace Runtime
	{
		namespace InteropServices
		{
			class ObjectCreationDelegate;

			class ExtensibleClassFactory : public System::Object
			{
			public:
				void RegisterObjectCreationCallback(ObjectCreationDelegate* callback);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
