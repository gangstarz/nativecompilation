#ifndef __SYSTEM_APPDOMAINMANAGER
#define __SYSTEM_APPDOMAINMANAGER

#include "../gc.h"
#include <cstdint>
#include "System.MarshalByRefObject.h"

namespace System
{
	class ValueType;

	class AppDomainManager : public MarshalByRefObject
	{
	public:
		int HasHost();

		// [place holder for auto-generated recompiled members]
	};
}

#endif
