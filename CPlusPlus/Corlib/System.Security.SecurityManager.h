#ifndef __SYSTEM_SECURITY_SECURITYMANAGER
#define __SYSTEM_SECURITY_SECURITYMANAGER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace Security
	{
		class SecurityManager : public System::Object
		{
		public:
			int _SetThreadSecurity(int bThreadSecurity);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
