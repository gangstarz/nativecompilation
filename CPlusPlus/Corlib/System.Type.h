#ifndef __SYSTEM_TYPE
#define __SYSTEM_TYPE

#include "../gc.h"
#include <cstdint>
#include "System.Reflection.MemberInfo.h"

namespace System
{
	class RuntimeType;
	class RuntimeTypeHandle;
	class ValueType;
	namespace Reflection
	{
		class TypeInfo;
	}

	class Type : public System::Reflection::MemberInfo
	{
	public:
		RuntimeType* GetTypeFromHandleUnsafe(int* handle);
		Type* GetTypeFromHandle(RuntimeTypeHandle handle);
		int op_Equality(Type* left, Type* right);
		int op_Inequality(Type* left, Type* right);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
