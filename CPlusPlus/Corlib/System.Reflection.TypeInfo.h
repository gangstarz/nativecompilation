#ifndef __SYSTEM_REFLECTION_TYPEINFO
#define __SYSTEM_REFLECTION_TYPEINFO

#include "../gc.h"
#include <cstdint>
#include "System.Type.h"

namespace System
{
	namespace Reflection
	{
		class TypeInfo : public System::Type
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
