#ifndef __SYSTEM_MATH
#define __SYSTEM_MATH

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	class Math : public System::Object
	{
	public:
		double Acos(double d);
		double Asin(double d);
		double Atan(double d);
		double Atan2(double y, double x);
		double Ceiling(double a);
		double Cos(double d);
		double Cosh(double value);
		double Floor(double d);
		double Sin(double a);
		double Tan(double a);
		double Sinh(double value);
		double Tanh(double value);
		double Round(double a);
		double Sqrt(double d);
		double Log(double d);
		double Log10(double d);
		double Exp(double d);
		double Pow(double x, double y);
		float Abs(float value);
		double Abs(double value);
		double SplitFractionDouble(double* value);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
