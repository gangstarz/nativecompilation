#ifndef __SYSTEM_REFLECTION_CUSTOMATTRIBUTECTORPARAMETER
#define __SYSTEM_REFLECTION_CUSTOMATTRIBUTECTORPARAMETER

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Reflection
	{
		class CustomAttributeCtorParameter : public System::ValueType
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
