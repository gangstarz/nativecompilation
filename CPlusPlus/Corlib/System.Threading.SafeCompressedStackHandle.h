#ifndef __SYSTEM_THREADING_SAFECOMPRESSEDSTACKHANDLE
#define __SYSTEM_THREADING_SAFECOMPRESSEDSTACKHANDLE

#include "../gc.h"
#include <cstdint>
#include "System.Runtime.InteropServices.SafeHandle.h"

namespace System
{
	namespace Threading
	{
		class SafeCompressedStackHandle : public System::Runtime::InteropServices::SafeHandle
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
