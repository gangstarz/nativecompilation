#ifndef __SYSTEM_RUNTIME_SERIALIZATION_SERIALIZATIONINFO
#define __SYSTEM_RUNTIME_SERIALIZATION_SERIALIZATIONINFO

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	namespace Runtime
	{
		namespace Serialization
		{
			class SerializationInfo : public System::Object
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
