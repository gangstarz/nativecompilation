#ifndef __SYSTEM_SECURITY_SECURITYRUNTIME
#define __SYSTEM_SECURITY_SECURITYRUNTIME

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Enum;
	class ValueType;
	namespace Security
	{
		class FrameSecurityDescriptor;
	}
	namespace Threading
	{
		class StackCrawlMark;
	}

	namespace Security
	{
		class SecurityRuntime : public System::Object
		{
		public:
			FrameSecurityDescriptor* GetSecurityObjectForFrame(System::Threading::StackCrawlMark& stackMark, int create);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
