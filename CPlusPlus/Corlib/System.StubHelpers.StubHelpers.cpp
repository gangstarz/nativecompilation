#include "System.StubHelpers.StubHelpers.h"
#include "System.NotImplementedException.h"
#include "System.Decimal.h"
#include "System.Delegate.h"
#include "System.Exception.h"
#include "System.Object.h"
#include "System.StubHelpers.CleanupWorkList.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		void StubHelpers::InitDeclaringType(int* pMD)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetDelegateTarget(System::Delegate* pThis, int*& pStubArg)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::DemandPermission(int* pNMD)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::SetLastError()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Exception* StubHelpers::InternalGetCOMHRExceptionObject(int32_t hr, int* pCPCMD, System::Object* pThis, int fForWinRT)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::CreateCustomMarshalerHelper(int* pMD, int32_t paramToken, int* hndManagedType)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetCOMIPFromRCW(System::Object* objSrc, int* pCPCMD, int** ppTarget, int* pfNeedsRelease)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetCOMIPFromRCW_WinRT(System::Object* objSrc, int* pCPCMD, int** ppTarget)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetCOMIPFromRCW_WinRTSharedGeneric(System::Object* objSrc, int* pCPCMD, int** ppTarget)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetCOMIPFromRCW_WinRTDelegate(System::Object* objSrc, int* pCPCMD, int** ppTarget)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int StubHelpers::ShouldCallWinRTInterface(System::Object* objSrc, int* pCPCMD)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetDelegateInvokeMethod(System::Delegate* pThis)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Object* StubHelpers::GetWinRTFactoryObject(int* pCPCMD)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetWinRTFactoryReturnValue(System::Object* pThis, int* pCtorEntry)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetOuterInspectable(System::Object* pThis, int* pCtorMD)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::CheckCollectedDelegateMDA(int* pEntryThunk)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t StubHelpers::strlen(int8_t* ptr)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::FmtClassUpdateNativeInternal(System::Object* obj, uint8_t* pNative, CleanupWorkList*& pCleanupWorkList)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::FmtClassUpdateCLRInternal(System::Object* obj, uint8_t* pNative)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::LayoutDestroyNativeInternal(uint8_t* pNative, int* pMT)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Object* StubHelpers::AllocateInternal(int* typeHandle)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetStubContext()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int StubHelpers::IsQCall(int* pMD)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetNDirectTarget(int* pMD)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::GetFinalStubTarget(int* pStubArg, int* pUnmngThis, int32_t dwStubFlags)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::ThrowInteropParamException(int32_t resID, int32_t paramIdx)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Exception* StubHelpers::InternalGetHRExceptionObject(int32_t hr)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Delegate* StubHelpers::GetTargetForAmbiguousVariantCall(System::Object* objSrc, int* pMT, int* fUseString)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::StubRegisterRCW(System::Object* pThis)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::StubUnregisterRCW(System::Object* pThis)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Exception* StubHelpers::TriggerExceptionSwallowedMDA(System::Exception* ex, int* pManagedTarget)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* StubHelpers::ProfilerBeginTransitionCallback(int* pSecretParam, int* pThread, System::Object* pThis)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::ProfilerEndTransitionCallback(int* pMD, int* pThread)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::DecimalCanonicalizeInternal(System::Decimal& dec)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::MarshalToUnmanagedVaListInternal(int* va_list, uint32_t vaListSize, int* pArgIterator)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::MarshalToManagedVaListInternal(int* va_list, int* pArgIterator)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		uint32_t StubHelpers::CalcVaListSize(int* va_list)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::ValidateObject(System::Object* obj, int* pMD, System::Object* pThis)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::ValidateByref(int* byref, int* pMD, System::Object* pThis)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void StubHelpers::TriggerGCForMDA()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
