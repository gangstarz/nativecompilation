#ifndef __SYSTEM_SECURITY_CRYPTOGRAPHY_SAFEKEYHANDLE
#define __SYSTEM_SECURITY_CRYPTOGRAPHY_SAFEKEYHANDLE

#include "../gc.h"
#include <cstdint>
#include "Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid.h"

namespace System
{
	namespace Security
	{
		namespace Cryptography
		{
			class SafeKeyHandle : public Microsoft::Win32::SafeHandles::SafeHandleZeroOrMinusOneIsInvalid
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
