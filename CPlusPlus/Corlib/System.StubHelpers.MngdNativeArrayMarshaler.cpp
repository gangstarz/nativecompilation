#include "System.StubHelpers.MngdNativeArrayMarshaler.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		void MngdNativeArrayMarshaler::CreateMarshaler(int* pMarshalState, int* pMT, int32_t dwFlags)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdNativeArrayMarshaler::ConvertSpaceToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdNativeArrayMarshaler::ConvertContentsToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdNativeArrayMarshaler::ConvertSpaceToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome, int32_t cElements)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdNativeArrayMarshaler::ConvertContentsToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdNativeArrayMarshaler::ClearNative(int* pMarshalState, int* pNativeHome, int32_t cElements)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdNativeArrayMarshaler::ClearNativeContents(int* pMarshalState, int* pNativeHome, int32_t cElements)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
