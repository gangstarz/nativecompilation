#ifndef __SYSTEM_REFLECTION_MODULE
#define __SYSTEM_REFLECTION_MODULE

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	namespace Reflection
	{
		class Module : public System::Object
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
