#include "System.Reflection.PseudoCustomAttribute.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Reflection.Module.h"
#include "System.Reflection.RuntimeModule.h"
#include "System.ValueType.h"


namespace System
{
	namespace Reflection
	{
		void PseudoCustomAttribute::_GetSecurityAttributes(RuntimeModule* module, int32_t token, int assembly, System::NormalArray<System::Object*>** securityAttributes)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
