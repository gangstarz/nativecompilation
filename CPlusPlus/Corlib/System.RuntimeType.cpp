#include "System.RuntimeType.h"
#include "System.NotImplementedException.h"
#include "System.Enum.h"
#include "System.Guid.h"
#include "System.Object.h"
#include "System.Reflection.BindingFlags.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.TypeInfo.h"
#include "System.String.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	int RuntimeType::IsWindowsRuntimeObjectType(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeType::IsTypeExportedToWindowsRuntime(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* RuntimeType::_CreateEnum(RuntimeType* enumType, int64_t value)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	Type* RuntimeType::GetTypeFromProgIDImpl(System::String* progID, System::String* server, int throwOnError)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	Type* RuntimeType::GetTypeFromCLSIDImpl(Guid clsid, System::String* server, int throwOnError)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void RuntimeType::GetGUID(Guid& result)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeType::CanValueSpecialCast(RuntimeType* valueType, RuntimeType* targetType)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* RuntimeType::AllocateValueType(RuntimeType* type, System::Object* value, int fForceTypeChange)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* RuntimeType::InvokeDispMethod(System::String* name, System::Reflection::BindingFlags invokeAttr, System::Object* target, System::NormalArray<System::Object*>* args, System::NormalArray<int> byrefModifiers, int32_t culture, System::NormalArray<System::String*>* namedParameters)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
