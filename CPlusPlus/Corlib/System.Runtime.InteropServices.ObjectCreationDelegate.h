#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_OBJECTCREATIONDELEGATE
#define __SYSTEM_RUNTIME_INTEROPSERVICES_OBJECTCREATIONDELEGATE

#include "../gc.h"
#include <cstdint>
#include "System.MulticastDelegate.h"

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			class ObjectCreationDelegate : public System::MulticastDelegate
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
