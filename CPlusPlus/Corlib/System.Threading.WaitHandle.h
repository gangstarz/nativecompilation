#ifndef __SYSTEM_THREADING_WAITHANDLE
#define __SYSTEM_THREADING_WAITHANDLE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.MarshalByRefObject.h"

namespace Microsoft
{
	namespace Win32
	{
		namespace SafeHandles
		{
			class SafeHandleZeroOrMinusOneIsInvalid;
			class SafeWaitHandle;
		}
	}
}
namespace System
{
	class ValueType;
	namespace Runtime
	{
		namespace ConstrainedExecution
		{
			class CriticalFinalizerObject;
		}
		namespace InteropServices
		{
			class SafeHandle;
		}
	}

	namespace Threading
	{
		class WaitHandle : public System::MarshalByRefObject
		{
		public:
			int32_t WaitMultiple(System::NormalArray<WaitHandle*>* waitHandles, int32_t millisecondsTimeout, int exitContext, int WaitAll);
			int32_t WaitOneNative(System::Runtime::InteropServices::SafeHandle* waitableSafeHandle, uint32_t millisecondsTimeout, int hasThreadAffinity, int exitContext);
			int32_t SignalAndWaitOne(Microsoft::Win32::SafeHandles::SafeWaitHandle* waitHandleToSignal, Microsoft::Win32::SafeHandles::SafeWaitHandle* waitHandleToWaitOn, int32_t millisecondsTimeout, int hasThreadAffinity, int exitContext);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
