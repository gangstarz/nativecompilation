#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_COMTYPES_ITYPELIB
#define __SYSTEM_RUNTIME_INTEROPSERVICES_COMTYPES_ITYPELIB

#include "../gc.h"
#include <cstdint>

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			namespace ComTypes
			{
				class ITypeLib
				{
				public:

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
