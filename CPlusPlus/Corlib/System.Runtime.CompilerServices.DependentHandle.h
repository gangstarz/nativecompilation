#ifndef __SYSTEM_RUNTIME_COMPILERSERVICES_DEPENDENTHANDLE
#define __SYSTEM_RUNTIME_COMPILERSERVICES_DEPENDENTHANDLE

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Runtime
	{
		namespace CompilerServices
		{
			class DependentHandle : public System::ValueType
			{
			public:
				void nInitialize(System::Object* primary, System::Object* secondary, int** dependentHandle);
				void nGetPrimary(int* dependentHandle, System::Object** primary);
				void nGetPrimaryAndSecondary(int* dependentHandle, System::Object** primary, System::Object** secondary);
				void nFree(int* dependentHandle);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
