#ifndef __SYSTEM_DIAGNOSTICS_STACKTRACE
#define __SYSTEM_DIAGNOSTICS_STACKTRACE

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Exception;
	class ValueType;
	namespace Diagnostics
	{
		class StackFrameHelper;

		class StackTrace : public System::Object
		{
		public:
			void GetStackFramesInternal(StackFrameHelper* sfh, int32_t iSkip, System::Exception* e);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
