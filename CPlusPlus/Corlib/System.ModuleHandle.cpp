#include "System.ModuleHandle.h"
#include "System.NotImplementedException.h"
#include "System.IRuntimeMethodInfo.h"
#include "System.Object.h"
#include "System.Reflection.Emit.DynamicMethod.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.MethodBase.h"
#include "System.Reflection.MethodInfo.h"
#include "System.Reflection.Module.h"
#include "System.Reflection.RuntimeModule.h"
#include "System.Resolver.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	IRuntimeMethodInfo* ModuleHandle::GetDynamicMethod(System::Reflection::Emit::DynamicMethod* method, System::Reflection::RuntimeModule* module, System::String* name, System::NormalArray<uint8_t> sig, Resolver* resolver)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t ModuleHandle::GetToken(System::Reflection::RuntimeModule* module)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t ModuleHandle::GetMDStreamVersion(System::Reflection::RuntimeModule* module)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int* ModuleHandle::_GetMetadataImport(System::Reflection::RuntimeModule* module)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
