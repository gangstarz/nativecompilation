#ifndef __SYSTEM_DIAGNOSTICS_LOG
#define __SYSTEM_DIAGNOSTICS_LOG

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;
	namespace Diagnostics
	{
		class LogSwitch;

		class Log : public System::Object
		{
		public:
			void AddLogSwitch(LogSwitch* logSwitch);
			void ModifyLogSwitch(int32_t iNewLevel, System::String* strSwitchName, System::String* strParentName);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
