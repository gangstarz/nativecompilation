#include "System.Threading.RegisteredWaitHandleSafe.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"
#include "System.Runtime.InteropServices.SafeHandle.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		void RegisteredWaitHandleSafe::WaitHandleCleanupNative(int* handle)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int RegisteredWaitHandleSafe::UnregisterWaitNative(int* handle, System::Runtime::InteropServices::SafeHandle* waitObject)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
