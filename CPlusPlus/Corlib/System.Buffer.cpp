#include "System.Buffer.h"
#include "System.NotImplementedException.h"
#include "System.Array.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	void Buffer::BlockCopy(Array* src, int32_t srcOffset, Array* dst, int32_t dstOffset, int32_t count)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Buffer::InternalBlockCopy(Array* src, int32_t srcOffsetBytes, Array* dst, int32_t dstOffsetBytes, int32_t byteCount)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Buffer::IsPrimitiveTypeArray(Array* array)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	uint8_t Buffer::_GetByte(Array* array, int32_t index)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Buffer::_SetByte(Array* array, int32_t index, uint8_t value)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Buffer::_ByteLength(Array* array)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
