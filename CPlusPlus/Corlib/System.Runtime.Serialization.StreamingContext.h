#ifndef __SYSTEM_RUNTIME_SERIALIZATION_STREAMINGCONTEXT
#define __SYSTEM_RUNTIME_SERIALIZATION_STREAMINGCONTEXT

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Runtime
	{
		namespace Serialization
		{
			class StreamingContext : public System::ValueType
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
