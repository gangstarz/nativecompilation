#ifndef __SYSTEM_CLRCONFIG
#define __SYSTEM_CLRCONFIG

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	class CLRConfig : public System::Object
	{
	public:
		int CheckThrowUnobservedTaskExceptions();
		int CheckLegacyManagedDeflateStream();

		// [place holder for auto-generated recompiled members]
	};
}

#endif
