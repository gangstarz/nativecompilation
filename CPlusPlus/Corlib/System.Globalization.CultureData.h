#ifndef __SYSTEM_GLOBALIZATION_CULTUREDATA
#define __SYSTEM_GLOBALIZATION_CULTUREDATA

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;
	namespace Globalization
	{
		class NumberFormatInfo;

		class CultureData : public System::Object
		{
		public:
			System::String* LCIDToLocaleName(int32_t lcid);
			int32_t LocaleNameToLCID(System::String* localeName);
			int nativeInitCultureData(CultureData* cultureData);
			int nativeGetNumberFormatInfoValues(System::String* localeName, NumberFormatInfo* nfi, int useUserOverride);
			System::NormalArray<System::String*>* nativeEnumTimeFormats(System::String* localeName, uint32_t dwFlags, int useUserOverride);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
