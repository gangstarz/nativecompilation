#include "System.Runtime.CompilerServices.RuntimeHelpers.h"
#include "System.NotImplementedException.h"
#include "System.Array.h"
#include "System.Delegate.h"
#include "System.IRuntimeMethodInfo.h"
#include "System.MulticastDelegate.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.Module.h"
#include "System.Reflection.RuntimeModule.h"
#include "System.Reflection.TypeInfo.h"
#include "System.Runtime.CompilerServices.CleanupCode.h"
#include "System.Runtime.CompilerServices.TryCode.h"
#include "System.RuntimeFieldHandle.h"
#include "System.RuntimeType.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace CompilerServices
		{
			void RuntimeHelpers::InitializeArray(System::Array* array, System::RuntimeFieldHandle fldHandle)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* RuntimeHelpers::GetObjectValue(System::Object* obj)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RuntimeHelpers::_RunClassConstructor(System::RuntimeType* type)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RuntimeHelpers::PrepareDelegate(System::Delegate* d)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RuntimeHelpers::PrepareContractedDelegate(System::Delegate* d)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t RuntimeHelpers::GetHashCode(System::Object* o)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int RuntimeHelpers::Equals(System::Object* o1, System::Object* o2)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RuntimeHelpers::EnsureSufficientExecutionStack()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RuntimeHelpers::ProbeForSufficientStack()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RuntimeHelpers::ExecuteCodeWithGuaranteedCleanup(TryCode* code, CleanupCode* backoutCode, System::Object* userData)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RuntimeHelpers::_RunModuleConstructor(System::Reflection::RuntimeModule* module)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RuntimeHelpers::_PrepareMethod(System::IRuntimeMethodInfo* method, int** pInstantiation, int32_t cInstantiation)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
