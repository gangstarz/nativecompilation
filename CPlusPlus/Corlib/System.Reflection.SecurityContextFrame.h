#ifndef __SYSTEM_REFLECTION_SECURITYCONTEXTFRAME
#define __SYSTEM_REFLECTION_SECURITYCONTEXTFRAME

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Reflection
	{
		class Assembly;
		class RuntimeAssembly;

		class SecurityContextFrame : public System::ValueType
		{
		public:
			void Push(RuntimeAssembly* assembly);
			void Pop();

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
