#ifndef __SYSTEM_REFLECTION_BINDER
#define __SYSTEM_REFLECTION_BINDER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	namespace Reflection
	{
		class Binder : public System::Object
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
