#ifndef __SYSTEM_REFLECTION_METHODBASE
#define __SYSTEM_REFLECTION_METHODBASE

#include "../gc.h"
#include <cstdint>
#include "System.Reflection.MemberInfo.h"

namespace System
{
	namespace Reflection
	{
		class MethodBase : public MemberInfo
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
