#ifndef __MICROSOFT_WIN32_SAFEHANDLES_SAFEWAITHANDLE
#define __MICROSOFT_WIN32_SAFEHANDLES_SAFEWAITHANDLE

#include "../gc.h"
#include <cstdint>
#include "Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid.h"

namespace Microsoft
{
	namespace Win32
	{
		namespace SafeHandles
		{
			class SafeWaitHandle : public SafeHandleZeroOrMinusOneIsInvalid
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
