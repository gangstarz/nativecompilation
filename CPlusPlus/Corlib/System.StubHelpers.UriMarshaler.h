#ifndef __SYSTEM_STUBHELPERS_URIMARSHALER
#define __SYSTEM_STUBHELPERS_URIMARSHALER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;

	namespace StubHelpers
	{
		class UriMarshaler : public System::Object
		{
		public:
			System::String* GetRawUriFromNative(int* pUri);
			int* CreateNativeUriInstance(System::String* rawUri);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
