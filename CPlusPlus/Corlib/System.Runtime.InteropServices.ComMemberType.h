#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_COMMEMBERTYPE
#define __SYSTEM_RUNTIME_INTEROPSERVICES_COMMEMBERTYPE

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			class ComMemberType
			{
			public:
				static const int32_t Method = 0;
				static const int32_t PropGet = 1;
				static const int32_t PropSet = 2;

			};
		}
	}
}

#endif
