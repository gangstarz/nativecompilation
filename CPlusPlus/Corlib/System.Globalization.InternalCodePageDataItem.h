#ifndef __SYSTEM_GLOBALIZATION_INTERNALCODEPAGEDATAITEM
#define __SYSTEM_GLOBALIZATION_INTERNALCODEPAGEDATAITEM

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Globalization
	{
		class InternalCodePageDataItem : public System::ValueType
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
