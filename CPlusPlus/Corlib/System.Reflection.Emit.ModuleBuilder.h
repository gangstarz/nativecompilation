#ifndef __SYSTEM_REFLECTION_EMIT_MODULEBUILDER
#define __SYSTEM_REFLECTION_EMIT_MODULEBUILDER

#include "../gc.h"
#include <cstdint>
#include "System.Reflection.Module.h"

namespace System
{
	class String;
	class ValueType;

	namespace Reflection
	{
		namespace Emit
		{
			class ModuleBuilder : public System::Reflection::Module
			{
			public:
				int* nCreateISymWriterForDynamicModule(System::Reflection::Module* module, System::String* filename);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
