#include "System.Threading.Thread.h"
#include "System.NotImplementedException.h"
#include "System.AppDomain.h"
#include "System.Delegate.h"
#include "System.Enum.h"
#include "System.Globalization.CultureInfo.h"
#include "System.MarshalByRefObject.h"
#include "System.MulticastDelegate.h"
#include "System.Object.h"
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"
#include "System.Runtime.InteropServices.SafeHandle.h"
#include "System.Runtime.Remoting.Contexts.Context.h"
#include "System.Security.Principal.IPrincipal.h"
#include "System.String.h"
#include "System.Threading.InternalCrossContextDelegate.h"
#include "System.Threading.SafeCompressedStackHandle.h"
#include "System.Threading.StackCrawlMark.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		int32_t Thread::get_ManagedThreadId()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* Thread::SetAppDomainStack(SafeCompressedStackHandle* csHandle)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::RestoreAppDomainStack(int* appDomainStack)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* Thread::InternalGetCurrentThread()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::AbortInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::SuspendInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::ResumeInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::InterruptInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t Thread::GetPriorityNative()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::SetPriorityNative(int32_t priority)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Thread::get_IsAlive()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Thread::get_IsThreadPoolThread()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Thread::JoinInternal(int32_t millisecondsTimeout)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::SleepInternal(int32_t millisecondsTimeout)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::SpinWaitInternal(int32_t iterations)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Thread::YieldInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		Thread* Thread::GetCurrentThreadNative()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::InternalFinalize()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::DisableComObjectEagerCleanup()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Thread::IsBackgroundNative()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::SetBackgroundNative(int isBackground)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t Thread::GetThreadStateNative()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t Thread::GetApartmentStateNative()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Runtime::Remoting::Contexts::Context* Thread::GetContextInternal(int* id)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Object* Thread::InternalCrossContextCallback(System::Runtime::Remoting::Contexts::Context* ctx, int* ctxID, int32_t appDomainID, InternalCrossContextDelegate* ftnToCall, System::NormalArray<System::Object*>* args)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::BeginCriticalRegion()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::EndCriticalRegion()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::BeginThreadAffinity()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::EndThreadAffinity()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::MemoryBarrier()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::SetAbortReason(System::Object* o)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Object* Thread::GetAbortReason()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::ClearAbortReason()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::StartInternal(System::Security::Principal::IPrincipal* principal, StackCrawlMark& stackMark)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::ResetAbortNative()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::SetStart(System::Delegate* start, int32_t maxStackSize)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t Thread::SetApartmentStateNative(int32_t state, int fireMDAOnMismatch)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Thread::StartupSetApartmentStateInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Thread::nativeGetSafeCulture(Thread* t, int32_t appDomainId, int isUI, System::Globalization::CultureInfo*& safeCulture)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Thread::nativeSetThreadUILocale(System::String* locale)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::AppDomain* Thread::GetDomainInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::AppDomain* Thread::GetFastDomainInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
