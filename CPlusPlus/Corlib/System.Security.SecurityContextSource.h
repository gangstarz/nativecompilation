#ifndef __SYSTEM_SECURITY_SECURITYCONTEXTSOURCE
#define __SYSTEM_SECURITY_SECURITYCONTEXTSOURCE

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Security
	{
		class SecurityContextSource
		{
		public:
			static const int32_t CurrentAppDomain = 0;
			static const int32_t CurrentAssembly = 1;

		};
	}
}

#endif
