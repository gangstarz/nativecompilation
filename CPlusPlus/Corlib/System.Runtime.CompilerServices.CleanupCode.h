#ifndef __SYSTEM_RUNTIME_COMPILERSERVICES_CLEANUPCODE
#define __SYSTEM_RUNTIME_COMPILERSERVICES_CLEANUPCODE

#include "../gc.h"
#include <cstdint>
#include "System.MulticastDelegate.h"

namespace System
{
	namespace Runtime
	{
		namespace CompilerServices
		{
			class CleanupCode : public System::MulticastDelegate
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
