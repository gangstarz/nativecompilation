#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_MARSHAL
#define __SYSTEM_RUNTIME_INTEROPSERVICES_MARSHAL

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Array;
	class Delegate;
	class Enum;
	class Exception;
	class Guid;
	class IRuntimeFieldInfo;
	class IRuntimeMethodInfo;
	class RuntimeType;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class Assembly;
		class MemberInfo;
		class RuntimeAssembly;
		class TypeInfo;
	}
	namespace Runtime
	{
		namespace ConstrainedExecution
		{
			class CriticalFinalizerObject;
		}
		namespace InteropServices
		{
			class ComMemberType;
			namespace ComTypes
			{
				class ITypeInfo;
				class ITypeLib;
			}
		}
	}
	namespace Threading
	{
		class Thread;
	}

	namespace Runtime
	{
		namespace InteropServices
		{
			class Marshal : public System::Object
			{
			public:
				uint32_t SizeOfType(System::Type* type);
				uint32_t AlignedSizeOfType(System::Type* type);
				int32_t SizeOfHelper(System::Type* t, int throwIfNotMarshalable);
				int* UnsafeAddrOfPinnedArrayElement(System::Array* arr, int32_t index);
				void CopyToNative(System::Object* source, int32_t startIndex, int* destination, int32_t length);
				void CopyToManaged(int* source, System::Object* destination, int32_t startIndex, int32_t length);
				int32_t GetLastWin32Error();
				void SetLastWin32Error(int32_t error);
				int* GetExceptionPointers();
				int32_t GetExceptionCode();
				void StructureToPtr(System::Object* structure, int* ptr, int fDeleteOld);
				void PtrToStructureHelper(int* ptr, System::Object* structure, int allowValueClasses);
				void DestroyStructure(int* ptr, System::Type* structuretype);
				void ThrowExceptionForHRInternal(int32_t errorCode, int* errorInfo);
				System::Exception* GetExceptionForHRInternal(int32_t errorCode, int* errorInfo);
				int32_t GetHRForException(System::Exception* e);
				int32_t GetHRForException_WinRT(System::Exception* e);
				int* GetUnmanagedThunkForManagedMethodPtr(int* pfnMethodToWrap, int* pbSignature, int32_t cbSignature);
				int* GetManagedThunkForUnmanagedMethodPtr(int* pfnMethodToWrap, int* pbSignature, int32_t cbSignature);
				int32_t GetTypeLibLcid(System::Runtime::InteropServices::ComTypes::ITypeLib* typelib);
				void GetTypeLibVersion(System::Runtime::InteropServices::ComTypes::ITypeLib* typeLibrary, int32_t* major, int32_t* minor);
				int* GetITypeInfoForType(System::Type* t);
				int* GetIUnknownForObjectNative(System::Object* o, int onlyInContext);
				int* GetRawIUnknownForComObjectNoAddRef(System::Object* o);
				int* GetIDispatchForObjectNative(System::Object* o, int onlyInContext);
				int* GetComInterfaceForObjectNative(System::Object* o, System::Type* t, int onlyInContext, int fEnalbeCustomizedQueryInterface);
				System::Object* GetObjectForIUnknown(int* pUnk);
				System::Object* GetUniqueObjectForIUnknown(int* unknown);
				System::Object* GetTypedObjectForIUnknown(int* pUnk, System::Type* t);
				int* CreateAggregatedObject(int* pOuter, System::Object* o);
				void CleanupUnusedObjectsInCurrentContext();
				int AreComObjectsAvailableForCleanup();
				int IsComObject(System::Object* o);
				int32_t InternalReleaseComObject(System::Object* o);
				void InternalFinalReleaseComObject(System::Object* o);
				int IsTypeVisibleFromCom(System::Type* t);
				int32_t QueryInterface(int* pUnk, System::Guid& iid, int** ppv);
				int32_t AddRef(int* pUnk);
				int32_t Release(int* pUnk);
				void GetNativeVariantForObject(System::Object* obj, int* pDstNativeVariant);
				System::Object* GetObjectForNativeVariant(int* pSrcNativeVariant);
				System::NormalArray<System::Object*>* GetObjectsForNativeVariants(int* aSrcNativeVariant, int32_t cVars);
				int32_t GetStartComSlot(System::Type* t);
				int32_t GetEndComSlot(System::Type* t);
				System::Reflection::MemberInfo* GetMethodInfoForComSlot(System::Type* t, int32_t slot, ComMemberType& memberType);
				int InternalSwitchCCW(System::Object* oldtp, System::Object* newtp);
				System::Object* InternalWrapIUnknownWithComObject(int* i);
				void ChangeWrapperHandleStrength(System::Object* otp, int fIsWeak);
				void InitializeWrapperForWinRT(System::Object* o, int*& pUnk);
				void InitializeManagedWinRTFactoryObject(System::Object* o, System::RuntimeType* runtimeClassType);
				System::Object* GetNativeActivationFactory(System::Type* type);
				System::Delegate* GetDelegateForFunctionPointerInternal(int* ptr, System::Type* t);
				int* GetFunctionPointerForDelegateInternal(System::Delegate* d);
				int32_t GetSystemMaxDBCSCharSize();
				int* OffsetOfHelper(System::IRuntimeFieldInfo* f);
				System::Threading::Thread* InternalGetThreadFromFiberCookie(int32_t cookie);
				void FCallGetTypeLibGuid(System::Guid& result, System::Runtime::InteropServices::ComTypes::ITypeLib* pTLB);
				void FCallGetTypeInfoGuid(System::Guid& result, System::Runtime::InteropServices::ComTypes::ITypeInfo* typeInfo);
				void FCallGetTypeLibGuidForAssembly(System::Guid& result, System::Reflection::RuntimeAssembly* assembly);
				void _GetTypeLibVersionForAssembly(System::Reflection::RuntimeAssembly* inputAssembly, int32_t* majorVersion, int32_t* minorVersion);
				System::Type* GetLoadedTypeForGUID(System::Guid& guid);
				System::Object* InternalCreateWrapperOfType(System::Object* o, System::Type* t);
				int32_t InternalGetComSlotForMethodInfo(System::IRuntimeMethodInfo* m);
				void FCallGenerateGuidForType(System::Guid& result, System::Type* type);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
