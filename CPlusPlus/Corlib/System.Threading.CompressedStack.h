#ifndef __SYSTEM_THREADING_COMPRESSEDSTACK
#define __SYSTEM_THREADING_COMPRESSEDSTACK

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Enum;
	class ValueType;
	namespace Runtime
	{
		namespace ConstrainedExecution
		{
			class CriticalFinalizerObject;
		}
		namespace InteropServices
		{
			class SafeHandle;
		}
	}
	namespace Security
	{
		class PermissionListSet;
	}
	namespace Threading
	{
		class DomainCompressedStack;
		class SafeCompressedStackHandle;
		class StackCrawlMark;

		class CompressedStack : public System::Object
		{
		public:
			SafeCompressedStackHandle* GetDelayedCompressedStack(StackCrawlMark& stackMark, int walkStack);
			void DestroyDelayedCompressedStack(int* unmanagedCompressedStack);
			void DestroyDCSList(SafeCompressedStackHandle* compressedStack);
			int32_t GetDCSCount(SafeCompressedStackHandle* compressedStack);
			int IsImmediateCompletionCandidate(SafeCompressedStackHandle* compressedStack, CompressedStack** innerCS);
			DomainCompressedStack* GetDomainCompressedStack(SafeCompressedStackHandle* compressedStack, int32_t index);
			void GetHomogeneousPLS(System::Security::PermissionListSet* hgPLS);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
