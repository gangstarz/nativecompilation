#ifndef __SYSTEM_TIMESPAN
#define __SYSTEM_TIMESPAN

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class TimeSpan : public ValueType
	{
	public:
		int LegacyFormatMode();

		// [place holder for auto-generated recompiled members]
	};
}

#endif
