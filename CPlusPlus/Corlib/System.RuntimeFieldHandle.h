#ifndef __SYSTEM_RUNTIMEFIELDHANDLE
#define __SYSTEM_RUNTIMEFIELDHANDLE

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class Enum;
	class RuntimeFieldHandleInternal;
	class RuntimeType;
	class String;
	class Type;
	namespace Reflection
	{
		class FieldAttributes;
		class FieldInfo;
		class MemberInfo;
		class RtFieldInfo;
		class RuntimeFieldInfo;
		class TypeInfo;
	}

	class RuntimeFieldHandle : public ValueType
	{
	public:
		System::String* GetName(System::Reflection::RtFieldInfo* field);
		int MatchesNameHash(RuntimeFieldHandleInternal handle, uint32_t hash);
		System::Reflection::FieldAttributes GetAttributes(RuntimeFieldHandleInternal field);
		RuntimeType* GetApproxDeclaringType(RuntimeFieldHandleInternal field);
		int32_t GetToken(System::Reflection::RtFieldInfo* field);
		System::Object* GetValue(System::Reflection::RtFieldInfo* field, System::Object* instance, RuntimeType* fieldType, RuntimeType* declaringType, int& domainInitialized);
		void SetValue(System::Reflection::RtFieldInfo* field, System::Object* obj, System::Object* value, RuntimeType* fieldType, System::Reflection::FieldAttributes fieldAttr, RuntimeType* declaringType, int& domainInitialized);
		RuntimeFieldHandleInternal GetStaticFieldForGenericType(RuntimeFieldHandleInternal field, RuntimeType* declaringType);
		int AcquiresContextFromThis(RuntimeFieldHandleInternal field);
		void* _GetUtf8Name(RuntimeFieldHandleInternal field);
		System::Object* GetValueDirect(System::Reflection::RtFieldInfo* field, RuntimeType* fieldType, void* pTypedRef, RuntimeType* contextType);
		void SetValueDirect(System::Reflection::RtFieldInfo* field, RuntimeType* fieldType, void* pTypedRef, System::Object* value, RuntimeType* contextType);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
