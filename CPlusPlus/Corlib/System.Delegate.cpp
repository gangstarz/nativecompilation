#include "System.Delegate.h"
#include "System.NotImplementedException.h"
#include "System.DelegateBindingFlags.h"
#include "System.Enum.h"
#include "System.IRuntimeMethodInfo.h"
#include "System.MulticastDelegate.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.TypeInfo.h"
#include "System.RuntimeType.h"
#include "System.String.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	MulticastDelegate* Delegate::InternalAllocLike(Delegate* d)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Delegate::InternalEqualTypes(System::Object* a, System::Object* b)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int* Delegate::GetMulticastInvoke()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int* Delegate::GetInvokeMethod()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	IRuntimeMethodInfo* Delegate::FindMethodHandle()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Delegate::InternalEqualMethodHandles(Delegate* left, Delegate* right)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int* Delegate::AdjustTarget(System::Object* target, int* methodPtr)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int* Delegate::GetCallStub(int* methodPtr)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Delegate::CompareUnmanagedFunctionPtrs(Delegate* d1, Delegate* d2)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Delegate::BindToMethodName(System::Object* target, RuntimeType* methodType, System::String* method, DelegateBindingFlags flags)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Delegate::BindToMethodInfo(System::Object* target, IRuntimeMethodInfo* method, RuntimeType* methodType, DelegateBindingFlags flags)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	MulticastDelegate* Delegate::InternalAlloc(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Delegate::DelegateConstruct(System::Object* target, int* slot)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
