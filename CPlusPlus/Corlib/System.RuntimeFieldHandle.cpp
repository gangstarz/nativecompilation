#include "System.RuntimeFieldHandle.h"
#include "System.NotImplementedException.h"
#include "System.Enum.h"
#include "System.Object.h"
#include "System.Reflection.FieldAttributes.h"
#include "System.Reflection.FieldInfo.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.RtFieldInfo.h"
#include "System.Reflection.RuntimeFieldInfo.h"
#include "System.Reflection.TypeInfo.h"
#include "System.RuntimeFieldHandleInternal.h"
#include "System.RuntimeType.h"
#include "System.String.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	System::String* RuntimeFieldHandle::GetName(System::Reflection::RtFieldInfo* field)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeFieldHandle::MatchesNameHash(RuntimeFieldHandleInternal handle, uint32_t hash)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Reflection::FieldAttributes RuntimeFieldHandle::GetAttributes(RuntimeFieldHandleInternal field)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	RuntimeType* RuntimeFieldHandle::GetApproxDeclaringType(RuntimeFieldHandleInternal field)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t RuntimeFieldHandle::GetToken(System::Reflection::RtFieldInfo* field)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* RuntimeFieldHandle::GetValue(System::Reflection::RtFieldInfo* field, System::Object* instance, RuntimeType* fieldType, RuntimeType* declaringType, int& domainInitialized)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void RuntimeFieldHandle::SetValue(System::Reflection::RtFieldInfo* field, System::Object* obj, System::Object* value, RuntimeType* fieldType, System::Reflection::FieldAttributes fieldAttr, RuntimeType* declaringType, int& domainInitialized)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	RuntimeFieldHandleInternal RuntimeFieldHandle::GetStaticFieldForGenericType(RuntimeFieldHandleInternal field, RuntimeType* declaringType)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeFieldHandle::AcquiresContextFromThis(RuntimeFieldHandleInternal field)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void* RuntimeFieldHandle::_GetUtf8Name(RuntimeFieldHandleInternal field)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* RuntimeFieldHandle::GetValueDirect(System::Reflection::RtFieldInfo* field, RuntimeType* fieldType, void* pTypedRef, RuntimeType* contextType)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void RuntimeFieldHandle::SetValueDirect(System::Reflection::RtFieldInfo* field, RuntimeType* fieldType, void* pTypedRef, System::Object* value, RuntimeType* contextType)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
