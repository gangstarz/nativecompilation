#ifndef __SYSTEM_RUNTIME_SERIALIZATION_FORMATTERSERVICES
#define __SYSTEM_RUNTIME_SERIALIZATION_FORMATTERSERVICES

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class RuntimeType;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
		class TypeInfo;
	}

	namespace Runtime
	{
		namespace Serialization
		{
			class FormatterServices : public System::Object
			{
			public:
				System::Object* nativeGetUninitializedObject(System::RuntimeType* type);
				int GetEnableUnsafeTypeForwarders();
				System::Object* nativeGetSafeUninitializedObject(System::RuntimeType* type);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
