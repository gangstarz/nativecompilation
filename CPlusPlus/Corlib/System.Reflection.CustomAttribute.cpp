#include "System.Reflection.CustomAttribute.h"
#include "System.NotImplementedException.h"
#include "System.IRuntimeMethodInfo.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.Module.h"
#include "System.Reflection.RuntimeModule.h"
#include "System.Reflection.TypeInfo.h"
#include "System.RuntimeType.h"
#include "System.String.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	namespace Reflection
	{
		void CustomAttribute::_ParseAttributeUsageAttribute(int* pCa, int32_t cCa, int32_t* targets, int* inherited, int* allowMultiple)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Object* CustomAttribute::_CreateCaObject(RuntimeModule* pModule, System::IRuntimeMethodInfo* pCtor, uint8_t** ppBlob, uint8_t* pEndBlob, int32_t* pcNamedArgs)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void CustomAttribute::_GetPropertyOrFieldData(RuntimeModule* pModule, uint8_t** ppBlobStart, uint8_t* pBlobEnd, System::String** name, int* bIsProperty, System::RuntimeType** type, System::Object** value)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
