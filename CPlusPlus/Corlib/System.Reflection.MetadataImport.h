#ifndef __SYSTEM_REFLECTION_METADATAIMPORT
#define __SYSTEM_REFLECTION_METADATAIMPORT

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class Guid;
	class String;
	namespace Reflection
	{
		class ConstArray;
		class MetadataEnumResult;

		class MetadataImport : public System::ValueType
		{
		public:
			void _Enum(int* scope, int32_t type, int32_t parent, MetadataEnumResult* result);
			System::String* _GetDefaultValue(int* scope, int32_t mdToken, int64_t* value, int32_t* length, int32_t* corElementType);
			void _GetName(int* scope, int32_t mdToken, void** name);
			void _GetNamespace(int* scope, int32_t mdToken, void** namesp);
			void _GetEventProps(int* scope, int32_t mdToken, void** name, int32_t* eventAttributes);
			void _GetFieldDefProps(int* scope, int32_t mdToken, int32_t* fieldAttributes);
			void _GetPropertyProps(int* scope, int32_t mdToken, void** name, int32_t* propertyAttributes, ConstArray* signature);
			void _GetParentToken(int* scope, int32_t mdToken, int32_t* tkParent);
			void _GetParamDefProps(int* scope, int32_t parameterToken, int32_t* sequence, int32_t* attributes);
			void _GetScopeProps(int* scope, System::Guid* mvid);
			void _GetSigOfMethodDef(int* scope, int32_t methodToken, ConstArray& signature);
			void _GetMemberRefProps(int* scope, int32_t memberTokenRef, ConstArray* signature);
			void _GetCustomAttributeProps(int* scope, int32_t customAttributeToken, int32_t* constructorToken, ConstArray* signature);
			int _GetFieldOffset(int* scope, int32_t typeTokenDef, int32_t fieldTokenDef, int32_t* offset);
			void _GetSigOfFieldDef(int* scope, int32_t fieldToken, ConstArray& fieldMarshal);
			void _GetFieldMarshal(int* scope, int32_t fieldToken, ConstArray& fieldMarshal);
			int _IsValidToken(int* scope, int32_t token);
			void _GetMarshalAs(int* pNativeType, int32_t cNativeType, int32_t* unmanagedType, int32_t* safeArraySubType, System::String** safeArrayUserDefinedSubType, int32_t* arraySubType, int32_t* sizeParamIndex, int32_t* sizeConst, System::String** marshalType, System::String** marshalCookie, int32_t* iidParamIndex);
			void _GetUserString(int* scope, int32_t mdToken, void** name, int32_t* length);
			void _GetGenericParamProps(int* scope, int32_t genericParameter, int32_t* flags);
			void _GetSignatureFromToken(int* scope, int32_t methodToken, ConstArray& signature);
			void _GetClassLayout(int* scope, int32_t typeTokenDef, int32_t* packSize, int32_t* classSize);
			void _GetPInvokeMap(int* scope, int32_t token, int32_t* attributes, void** importName, void** importDll);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
