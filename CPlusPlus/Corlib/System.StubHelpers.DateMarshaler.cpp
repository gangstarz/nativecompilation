#include "System.StubHelpers.DateMarshaler.h"
#include "System.NotImplementedException.h"
#include "System.DateTime.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		double DateMarshaler::ConvertToNative(System::DateTime managedDate)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int64_t DateMarshaler::ConvertToManaged(double nativeDate)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
