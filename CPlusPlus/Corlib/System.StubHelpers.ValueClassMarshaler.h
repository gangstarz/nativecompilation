#ifndef __SYSTEM_STUBHELPERS_VALUECLASSMARSHALER
#define __SYSTEM_STUBHELPERS_VALUECLASSMARSHALER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;
	namespace StubHelpers
	{
		class CleanupWorkList;

		class ValueClassMarshaler : public System::Object
		{
		public:
			void ConvertToNative(int* dst, int* src, int* pMT, CleanupWorkList*& pCleanupWorkList);
			void ConvertToManaged(int* dst, int* src, int* pMT);
			void ClearNative(int* dst, int* pMT);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
