#ifndef __SYSTEM_ENVIRONMENT
#define __SYSTEM_ENVIRONMENT

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace Microsoft
{
	namespace Win32
	{
		class OSVERSIONINFO;
		class OSVERSIONINFOEX;
	}
}
namespace System
{
	class CompatibilityFlag;
	class Enum;
	class Exception;
	class String;
	class ValueType;

	class Environment : public System::Object
	{
	public:
		int32_t get_TickCount();
		int32_t get_ExitCode();
		void set_ExitCode(int32_t value);
		void FailFast(System::String* message);
		void FailFast(System::String* message, uint32_t exitCode);
		void FailFast(System::String* message, Exception* exception);
		System::String* nativeGetEnvironmentVariable(System::String* variable);
		int GetVersion(Microsoft::Win32::OSVERSIONINFO* osVer);
		int GetVersionEx(Microsoft::Win32::OSVERSIONINFOEX* osVer);
		System::String* GetResourceFromDefault(System::String* key);
		int get_HasShutdownStarted();
		int GetCompatibilityFlag(CompatibilityFlag flag);
		System::NormalArray<System::String*>* GetCommandLineArgsNative();

		// [place holder for auto-generated recompiled members]
	};
}

#endif
