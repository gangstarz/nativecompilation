#include "System.Security.CodeAccessSecurityEngine.h"
#include "System.NotImplementedException.h"
#include "System.Collections.ArrayList.h"
#include "System.Enum.h"
#include "System.Object.h"
#include "System.Security.CodeAccessPermission.h"
#include "System.Security.FrameSecurityDescriptor.h"
#include "System.Security.PermissionToken.h"
#include "System.Security.PermissionType.h"
#include "System.Threading.StackCrawlMark.h"
#include "System.ValueType.h"


namespace System
{
	namespace Security
	{
		void CodeAccessSecurityEngine::SpecialDemand(PermissionType whatPermission, System::Threading::StackCrawlMark& stackMark)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void CodeAccessSecurityEngine::Check(System::Object* demand, System::Threading::StackCrawlMark& stackMark, int isPermSet)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int CodeAccessSecurityEngine::QuickCheckForAllDemands()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int CodeAccessSecurityEngine::AllDomainsHomogeneousWithNoStackModifiers()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		FrameSecurityDescriptor* CodeAccessSecurityEngine::CheckNReturnSO(PermissionToken* permToken, CodeAccessPermission* demand, System::Threading::StackCrawlMark& stackMark, int32_t create)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void CodeAccessSecurityEngine::GetZoneAndOriginInternal(System::Collections::ArrayList* zoneList, System::Collections::ArrayList* originList, System::Threading::StackCrawlMark& stackMark)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
