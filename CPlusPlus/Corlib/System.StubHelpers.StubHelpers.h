#ifndef __SYSTEM_STUBHELPERS_STUBHELPERS
#define __SYSTEM_STUBHELPERS_STUBHELPERS

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Decimal;
	class Delegate;
	class Exception;
	class ValueType;
	namespace StubHelpers
	{
		class CleanupWorkList;

		class StubHelpers : public System::Object
		{
		public:
			void InitDeclaringType(int* pMD);
			int* GetDelegateTarget(System::Delegate* pThis, int*& pStubArg);
			void DemandPermission(int* pNMD);
			void SetLastError();
			System::Exception* InternalGetCOMHRExceptionObject(int32_t hr, int* pCPCMD, System::Object* pThis, int fForWinRT);
			int* CreateCustomMarshalerHelper(int* pMD, int32_t paramToken, int* hndManagedType);
			int* GetCOMIPFromRCW(System::Object* objSrc, int* pCPCMD, int** ppTarget, int* pfNeedsRelease);
			int* GetCOMIPFromRCW_WinRT(System::Object* objSrc, int* pCPCMD, int** ppTarget);
			int* GetCOMIPFromRCW_WinRTSharedGeneric(System::Object* objSrc, int* pCPCMD, int** ppTarget);
			int* GetCOMIPFromRCW_WinRTDelegate(System::Object* objSrc, int* pCPCMD, int** ppTarget);
			int ShouldCallWinRTInterface(System::Object* objSrc, int* pCPCMD);
			int* GetDelegateInvokeMethod(System::Delegate* pThis);
			System::Object* GetWinRTFactoryObject(int* pCPCMD);
			int* GetWinRTFactoryReturnValue(System::Object* pThis, int* pCtorEntry);
			int* GetOuterInspectable(System::Object* pThis, int* pCtorMD);
			void CheckCollectedDelegateMDA(int* pEntryThunk);
			int32_t strlen(int8_t* ptr);
			void FmtClassUpdateNativeInternal(System::Object* obj, uint8_t* pNative, CleanupWorkList*& pCleanupWorkList);
			void FmtClassUpdateCLRInternal(System::Object* obj, uint8_t* pNative);
			void LayoutDestroyNativeInternal(uint8_t* pNative, int* pMT);
			System::Object* AllocateInternal(int* typeHandle);
			int* GetStubContext();
			int IsQCall(int* pMD);
			int* GetNDirectTarget(int* pMD);
			int* GetFinalStubTarget(int* pStubArg, int* pUnmngThis, int32_t dwStubFlags);
			void ThrowInteropParamException(int32_t resID, int32_t paramIdx);
			System::Exception* InternalGetHRExceptionObject(int32_t hr);
			System::Delegate* GetTargetForAmbiguousVariantCall(System::Object* objSrc, int* pMT, int* fUseString);
			void StubRegisterRCW(System::Object* pThis);
			void StubUnregisterRCW(System::Object* pThis);
			System::Exception* TriggerExceptionSwallowedMDA(System::Exception* ex, int* pManagedTarget);
			int* ProfilerBeginTransitionCallback(int* pSecretParam, int* pThread, System::Object* pThis);
			void ProfilerEndTransitionCallback(int* pMD, int* pThread);
			void DecimalCanonicalizeInternal(System::Decimal& dec);
			void MarshalToUnmanagedVaListInternal(int* va_list, uint32_t vaListSize, int* pArgIterator);
			void MarshalToManagedVaListInternal(int* va_list, int* pArgIterator);
			uint32_t CalcVaListSize(int* va_list);
			void ValidateObject(System::Object* obj, int* pMD, System::Object* pThis);
			void ValidateByref(int* byref, int* pMD, System::Object* pThis);
			void TriggerGCForMDA();

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
