#ifndef __SYSTEM_STUBHELPERS_OBJECTMARSHALER
#define __SYSTEM_STUBHELPERS_OBJECTMARSHALER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace StubHelpers
	{
		class ObjectMarshaler : public System::Object
		{
		public:
			void ConvertToNative(System::Object* objSrc, int* pDstVariant);
			System::Object* ConvertToManaged(int* pSrcVariant);
			void ClearNative(int* pVariant);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
