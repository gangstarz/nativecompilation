#ifndef __SYSTEM_LOADEROPTIMIZATION
#define __SYSTEM_LOADEROPTIMIZATION

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	class LoaderOptimization
	{
	public:
		static const int32_t NotSpecified = 0;
		static const int32_t SingleDomain = 1;
		static const int32_t MultiDomain = 2;
		static const int32_t MultiDomainHost = 3;
		static const int32_t DomainMask = 3;
		static const int32_t DisallowBindings = 4;

	};
}

#endif
