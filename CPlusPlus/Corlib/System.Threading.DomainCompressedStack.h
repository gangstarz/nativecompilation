#ifndef __SYSTEM_THREADING_DOMAINCOMPRESSEDSTACK
#define __SYSTEM_THREADING_DOMAINCOMPRESSEDSTACK

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;
	namespace Reflection
	{
		class Assembly;
	}
	namespace Security
	{
		class FrameSecurityDescriptor;
		class PermissionSet;
	}

	namespace Threading
	{
		class DomainCompressedStack : public System::Object
		{
		public:
			int32_t GetDescCount(int* dcs);
			void GetDomainPermissionSets(int* dcs, System::Security::PermissionSet** granted, System::Security::PermissionSet** refused);
			int GetDescriptorInfo(int* dcs, int32_t index, System::Security::PermissionSet** granted, System::Security::PermissionSet** refused, System::Reflection::Assembly** assembly, System::Security::FrameSecurityDescriptor** fsd);
			int IgnoreDomain(int* dcs);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
