#ifndef __MICROSOFT_WIN32_SAFEHANDLES_SAFEHANDLEZEROORMINUSONEISINVALID
#define __MICROSOFT_WIN32_SAFEHANDLES_SAFEHANDLEZEROORMINUSONEISINVALID

#include "../gc.h"
#include <cstdint>
#include "System.Runtime.InteropServices.SafeHandle.h"

namespace Microsoft
{
	namespace Win32
	{
		namespace SafeHandles
		{
			class SafeHandleZeroOrMinusOneIsInvalid : public System::Runtime::InteropServices::SafeHandle
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
