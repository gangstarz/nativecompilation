#ifndef __SYSTEM_REFLECTION_RUNTIMEFIELDINFO
#define __SYSTEM_REFLECTION_RUNTIMEFIELDINFO

#include "../gc.h"
#include <cstdint>
#include "System.Reflection.FieldInfo.h"

namespace System
{
	namespace Reflection
	{
		class RuntimeFieldInfo : public FieldInfo
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
