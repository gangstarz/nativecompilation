#include "System.StubHelpers.MngdRefCustomMarshaler.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		void MngdRefCustomMarshaler::CreateMarshaler(int* pMarshalState, int* pCMHelper)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdRefCustomMarshaler::ConvertContentsToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdRefCustomMarshaler::ClearNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdRefCustomMarshaler::ConvertContentsToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdRefCustomMarshaler::ClearManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
