#ifndef __SYSTEM_SECURITY_CRYPTOGRAPHY_X509CERTIFICATES_SAFECERTSTOREHANDLE
#define __SYSTEM_SECURITY_CRYPTOGRAPHY_X509CERTIFICATES_SAFECERTSTOREHANDLE

#include "../gc.h"
#include <cstdint>
#include "Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid.h"

namespace System
{
	class ValueType;

	namespace Security
	{
		namespace Cryptography
		{
			namespace X509Certificates
			{
				class SafeCertStoreHandle : public Microsoft::Win32::SafeHandles::SafeHandleZeroOrMinusOneIsInvalid
				{
				public:
					void _FreeCertStoreContext(int* hCertStore);

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
