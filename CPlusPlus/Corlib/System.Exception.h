#ifndef __SYSTEM_EXCEPTION
#define __SYSTEM_EXCEPTION

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class IRuntimeMethodInfo;
	class String;
	class ValueType;

	class Exception : public System::Object
	{
	public:
		void GetStackTracesDeepCopy(Exception* exception, System::Object** currentStackTrace, System::Object** dynamicMethodArray);
		void SaveStackTracesFromDeepCopy(Exception* exception, System::Object* currentStackTrace, System::Object* dynamicMethodArray);
		int IsImmutableAgileException(Exception* e);
		IRuntimeMethodInfo* GetMethodFromStackTrace(System::Object* stackTrace);
		void PrepareForForeignExceptionRaise();
		System::Object* CopyStackTrace(System::Object* currentStackTrace);
		System::Object* CopyDynamicMethods(System::Object* currentDynamicMethods);
		System::String* StripFileInfo(System::String* stackTrace, int isRemoteStackTrace);
		int nIsTransient(int32_t hr);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
