#ifndef __SYSTEM_APPDOMAINSETUP
#define __SYSTEM_APPDOMAINSETUP

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;

	class AppDomainSetup : public System::Object
	{
	public:
		void UpdateContextProperty(int* fusionContext, System::String* key, System::Object* value);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
