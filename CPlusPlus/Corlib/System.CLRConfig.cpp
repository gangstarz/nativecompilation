#include "System.CLRConfig.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	int CLRConfig::CheckThrowUnobservedTaskExceptions()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int CLRConfig::CheckLegacyManagedDeflateStream()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
