#ifndef __SYSTEM_REFLECTION_MEMBERINFO
#define __SYSTEM_REFLECTION_MEMBERINFO

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	namespace Reflection
	{
		class MemberInfo : public System::Object
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
