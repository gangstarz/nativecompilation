#ifndef __SYSTEM_BCLDEBUG
#define __SYSTEM_BCLDEBUG

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	class BCLDebug : public System::Object
	{
	public:
		int32_t GetRegistryLoggingValues(int* loggingEnabled, int* logToConsole, int32_t* logLevel, int* perfWarnings, int* correctnessWarnings, int* safeHandleStackTraces);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
