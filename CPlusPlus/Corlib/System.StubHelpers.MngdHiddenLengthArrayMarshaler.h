#ifndef __SYSTEM_STUBHELPERS_MNGDHIDDENLENGTHARRAYMARSHALER
#define __SYSTEM_STUBHELPERS_MNGDHIDDENLENGTHARRAYMARSHALER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace StubHelpers
	{
		class MngdHiddenLengthArrayMarshaler : public System::Object
		{
		public:
			void CreateMarshaler(int* pMarshalState, int* pMT, int* cbElementSize, uint16_t vt);
			void ConvertSpaceToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ConvertContentsToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ConvertSpaceToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome, int32_t elementCount);
			void ConvertContentsToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ClearNativeContents(int* pMarshalState, int* pNativeHome, int32_t cElements);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
