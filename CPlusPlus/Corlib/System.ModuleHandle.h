#ifndef __SYSTEM_MODULEHANDLE
#define __SYSTEM_MODULEHANDLE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class IRuntimeMethodInfo;
	class Resolver;
	class String;
	namespace Reflection
	{
		class MemberInfo;
		class MethodBase;
		class MethodInfo;
		class Module;
		class RuntimeModule;
		namespace Emit
		{
			class DynamicMethod;
		}
	}

	class ModuleHandle : public ValueType
	{
	public:
		IRuntimeMethodInfo* GetDynamicMethod(System::Reflection::Emit::DynamicMethod* method, System::Reflection::RuntimeModule* module, System::String* name, System::NormalArray<uint8_t> sig, Resolver* resolver);
		int32_t GetToken(System::Reflection::RuntimeModule* module);
		int32_t GetMDStreamVersion(System::Reflection::RuntimeModule* module);
		int* _GetMetadataImport(System::Reflection::RuntimeModule* module);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
