#include "System.Enum.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.TypeInfo.h"
#include "System.RuntimeType.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	RuntimeType* Enum::InternalGetUnderlyingType(RuntimeType* enumType)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* Enum::InternalGetValue()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Enum::Equals(System::Object* obj)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Enum::InternalCompareTo(System::Object* o1, System::Object* o2)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* Enum::InternalBoxEnum(RuntimeType* enumType, int64_t value)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Enum::InternalHasFlag(Enum* flags)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
