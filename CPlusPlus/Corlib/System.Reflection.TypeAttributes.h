#ifndef __SYSTEM_REFLECTION_TYPEATTRIBUTES
#define __SYSTEM_REFLECTION_TYPEATTRIBUTES

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Reflection
	{
		class TypeAttributes
		{
		public:
			static const int32_t NotPublic = 0;
			static const int32_t AutoLayout = 0;
			static const int32_t AnsiClass = 0;
			static const int32_t Class = 0;
			static const int32_t Public = 1;
			static const int32_t NestedPublic = 2;
			static const int32_t NestedPrivate = 3;
			static const int32_t NestedFamily = 4;
			static const int32_t NestedAssembly = 5;
			static const int32_t NestedFamANDAssem = 6;
			static const int32_t NestedFamORAssem = 7;
			static const int32_t VisibilityMask = 7;
			static const int32_t SequentialLayout = 8;
			static const int32_t ExplicitLayout = 16;
			static const int32_t LayoutMask = 24;
			static const int32_t Interface = 32;
			static const int32_t ClassSemanticsMask = 32;
			static const int32_t Abstract = 128;
			static const int32_t Sealed = 256;
			static const int32_t SpecialName = 1024;
			static const int32_t RTSpecialName = 2048;
			static const int32_t Import = 4096;
			static const int32_t Serializable = 8192;
			static const int32_t WindowsRuntime = 16384;
			static const int32_t UnicodeClass = 65536;
			static const int32_t AutoClass = 131072;
			static const int32_t StringFormatMask = 196608;
			static const int32_t CustomFormatClass = 196608;
			static const int32_t HasSecurity = 262144;
			static const int32_t ReservedMask = 264192;
			static const int32_t BeforeFieldInit = 1048576;
			static const int32_t CustomFormatMask = 12582912;

		};
	}
}

#endif
