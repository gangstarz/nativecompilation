#ifndef __MICROSOFT_WIN32_OSVERSIONINFO
#define __MICROSOFT_WIN32_OSVERSIONINFO

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace Microsoft
{
	namespace Win32
	{
		class OSVERSIONINFO : public System::Object
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
