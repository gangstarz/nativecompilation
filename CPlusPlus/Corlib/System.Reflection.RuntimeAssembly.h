#ifndef __SYSTEM_REFLECTION_RUNTIMEASSEMBLY
#define __SYSTEM_REFLECTION_RUNTIMEASSEMBLY

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Reflection.Assembly.h"

namespace System
{
	class Enum;
	class String;
	class ValueType;
	namespace Reflection
	{
		class AssemblyName;
		class Module;
		class RuntimeModule;
	}
	namespace Security
	{
		class SecurityContextSource;
		namespace Policy
		{
			class Evidence;
		}
	}
	namespace Threading
	{
		class StackCrawlMark;
	}

	namespace Reflection
	{
		class RuntimeAssembly : public Assembly
		{
		public:
			RuntimeModule* GetManifestModule(RuntimeAssembly* assembly);
			int AptcaCheck(RuntimeAssembly* targetAssembly, RuntimeAssembly* sourceAssembly);
			int32_t GetToken(RuntimeAssembly* assembly);
			RuntimeAssembly* _nLoad(AssemblyName* fileName, System::String* codeBase, System::Security::Policy::Evidence* assemblySecurity, RuntimeAssembly* locationHint, System::Threading::StackCrawlMark& stackMark, int* pPrivHostBinder, int throwOnFileNotFound, int forIntrospection, int suppressSecurityChecks);
			int IsFrameworkAssembly(AssemblyName* assemblyName);
			int IsReflectionOnly(RuntimeAssembly* assembly);
			System::NormalArray<System::String*>* GetManifestResourceNames(RuntimeAssembly* assembly);
			System::NormalArray<AssemblyName*>* GetReferencedAssemblies(RuntimeAssembly* assembly);
			int IsGlobalAssemblyCache(RuntimeAssembly* assembly);
			int FCallIsDynamic(RuntimeAssembly* assembly);
			int IsNewPortableAssembly(AssemblyName* assemblyName);
			RuntimeAssembly* nLoadFile(System::String* path, System::Security::Policy::Evidence* evidence);
			RuntimeAssembly* nLoadImage(System::NormalArray<uint8_t> rawAssembly, System::NormalArray<uint8_t> rawSymbolStore, System::Security::Policy::Evidence* evidence, System::Threading::StackCrawlMark& stackMark, int fIntrospection, System::Security::SecurityContextSource securityContextSource);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
