#ifndef __SYSTEM_SYSTEMEXCEPTION
#define __SYSTEM_SYSTEMEXCEPTION

#include "../gc.h"
#include <cstdint>
#include "System.Exception.h"

namespace System
{
	class SystemException : public Exception
	{
	public:

		// [place holder for auto-generated recompiled members]
	};
}

#endif
