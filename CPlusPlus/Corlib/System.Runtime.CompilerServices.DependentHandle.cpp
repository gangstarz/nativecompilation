#include "System.Runtime.CompilerServices.DependentHandle.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace CompilerServices
		{
			void DependentHandle::nInitialize(System::Object* primary, System::Object* secondary, int** dependentHandle)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void DependentHandle::nGetPrimary(int* dependentHandle, System::Object** primary)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void DependentHandle::nGetPrimaryAndSecondary(int* dependentHandle, System::Object** primary, System::Object** secondary)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void DependentHandle::nFree(int* dependentHandle)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
