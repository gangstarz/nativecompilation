#ifndef __MICROSOFT_WIN32_SAFEHANDLES_SAFEFILEHANDLE
#define __MICROSOFT_WIN32_SAFEHANDLES_SAFEFILEHANDLE

#include "../gc.h"
#include <cstdint>
#include "Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid.h"

namespace Microsoft
{
	namespace Win32
	{
		namespace SafeHandles
		{
			class SafeFileHandle : public SafeHandleZeroOrMinusOneIsInvalid
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
