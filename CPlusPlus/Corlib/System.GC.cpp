#include "System.GC.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	int32_t GC::GetGCLatencyMode()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void GC::SetGCLatencyMode(int32_t newLatencyMode)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t GC::GetLOHCompactionMode()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void GC::SetLOHCompactionMode(int32_t newLOHCompactionyMode)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t GC::GetMaxGeneration()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t GC::_CollectionCount(int32_t generation, int32_t getSpecialGCCount)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int GC::IsServerGC()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t GC::GetGeneration(System::Object* obj)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void GC::_SuppressFinalize(System::Object* o)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t GC::_WaitForFullGCApproach(int32_t millisecondsTimeout)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t GC::_WaitForFullGCComplete(int32_t millisecondsTimeout)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t GC::GetGenerationWR(int* handle)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void GC::_ReRegisterForFinalize(System::Object* o)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int GC::_RegisterForFullGCNotification(int32_t maxGenerationPercentage, int32_t largeObjectHeapPercentage)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int GC::_CancelFullGCNotification()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
