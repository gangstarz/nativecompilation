#ifndef __SYSTEM_RUNTIME_REMOTING_OBJREF
#define __SYSTEM_RUNTIME_REMOTING_OBJREF

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	namespace Runtime
	{
		namespace Remoting
		{
			class ObjRef : public System::Object
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
