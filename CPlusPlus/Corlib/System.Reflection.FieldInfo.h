#ifndef __SYSTEM_REFLECTION_FIELDINFO
#define __SYSTEM_REFLECTION_FIELDINFO

#include "../gc.h"
#include <cstdint>
#include "System.Reflection.MemberInfo.h"

namespace System
{
	namespace Reflection
	{
		class FieldInfo : public MemberInfo
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
