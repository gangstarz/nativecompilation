#ifndef __SYSTEM_VALUETYPE
#define __SYSTEM_VALUETYPE

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType : public System::Object
	{
	public:
		int32_t GetHashCode();
		int32_t GetHashCodeOfPtr(int* ptr);
		int CanCompareBits(System::Object* obj);
		int FastEqualsCheck(System::Object* a, System::Object* b);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
