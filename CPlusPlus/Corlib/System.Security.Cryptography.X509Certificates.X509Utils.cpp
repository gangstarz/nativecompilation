#include "System.Security.Cryptography.X509Certificates.X509Utils.h"
#include "System.NotImplementedException.h"
#include "Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid.h"
#include "Microsoft.Win32.FILE_TIME.h"
#include "System.Enum.h"
#include "System.Object.h"
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"
#include "System.Runtime.InteropServices.SafeHandle.h"
#include "System.Security.Cryptography.X509Certificates.SafeCertContextHandle.h"
#include "System.Security.Cryptography.X509Certificates.SafeCertStoreHandle.h"
#include "System.Security.Cryptography.X509Certificates.X509ContentType.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Security
	{
		namespace Cryptography
		{
			namespace X509Certificates
			{
				void X509Utils::_DuplicateCertContext(int* handle, SafeCertContextHandle*& safeCertContext)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::NormalArray<uint8_t> X509Utils::_GetCertRawData(SafeCertContextHandle* safeCertContext)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::String* X509Utils::_GetPublicKeyOid(SafeCertContextHandle* safeCertContext)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::NormalArray<uint8_t> X509Utils::_GetPublicKeyParameters(SafeCertContextHandle* safeCertContext)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::NormalArray<uint8_t> X509Utils::_GetPublicKeyValue(SafeCertContextHandle* safeCertContext)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::NormalArray<uint8_t> X509Utils::_GetThumbprint(SafeCertContextHandle* safeCertContext)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void X509Utils::_AddCertificateToStore(SafeCertStoreHandle* safeCertStoreHandle, SafeCertContextHandle* safeCertContext)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::NormalArray<uint8_t> X509Utils::_ExportCertificatesToBlob(SafeCertStoreHandle* safeCertStoreHandle, X509ContentType contentType, int* password)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void X509Utils::_GetDateNotAfter(SafeCertContextHandle* safeCertContext, Microsoft::Win32::FILE_TIME& fileTime)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void X509Utils::_GetDateNotBefore(SafeCertContextHandle* safeCertContext, Microsoft::Win32::FILE_TIME& fileTime)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::String* X509Utils::_GetIssuerName(SafeCertContextHandle* safeCertContext, int legacyV1Mode)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::String* X509Utils::_GetSubjectInfo(SafeCertContextHandle* safeCertContext, uint32_t displayType, int legacyV1Mode)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::NormalArray<uint8_t> X509Utils::_GetSerialNumber(SafeCertContextHandle* safeCertContext)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void X509Utils::_LoadCertFromBlob(System::NormalArray<uint8_t> rawData, int* password, uint32_t dwFlags, int persistKeySet, SafeCertContextHandle*& pCertCtx)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void X509Utils::_LoadCertFromFile(System::String* fileName, int* password, uint32_t dwFlags, int persistKeySet, SafeCertContextHandle*& pCertCtx)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void X509Utils::_OpenX509Store(uint32_t storeType, uint32_t flags, System::String* storeName, SafeCertStoreHandle*& safeCertStoreHandle)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				uint32_t X509Utils::_QueryCertBlobType(System::NormalArray<uint8_t> rawData)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				uint32_t X509Utils::_QueryCertFileType(System::String* fileName)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

			}
		}
	}
}
