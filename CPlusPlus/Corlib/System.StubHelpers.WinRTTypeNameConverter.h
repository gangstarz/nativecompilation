#ifndef __SYSTEM_STUBHELPERS_WINRTTYPENAMECONVERTER
#define __SYSTEM_STUBHELPERS_WINRTTYPENAMECONVERTER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
	}

	namespace StubHelpers
	{
		class WinRTTypeNameConverter : public System::Object
		{
		public:
			System::String* ConvertToWinRTTypeName(System::Type* managedType, int* isPrimitive);
			System::Type* GetTypeFromWinRTTypeName(System::String* typeName, int* isPrimitive);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
