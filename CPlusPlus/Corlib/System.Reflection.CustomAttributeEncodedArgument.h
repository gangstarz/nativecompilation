#ifndef __SYSTEM_REFLECTION_CUSTOMATTRIBUTEENCODEDARGUMENT
#define __SYSTEM_REFLECTION_CUSTOMATTRIBUTEENCODEDARGUMENT

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Reflection
	{
		class Assembly;
		class CustomAttributeCtorParameter;
		class CustomAttributeNamedParameter;
		class RuntimeAssembly;

		class CustomAttributeEncodedArgument : public System::ValueType
		{
		public:
			void ParseAttributeArguments(int* pCa, int32_t cCa, System::NormalArray<CustomAttributeCtorParameter>& CustomAttributeCtorParameters, System::NormalArray<CustomAttributeNamedParameter>& CustomAttributeTypedArgument, RuntimeAssembly* assembly);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
