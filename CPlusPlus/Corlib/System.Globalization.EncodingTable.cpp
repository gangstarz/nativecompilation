#include "System.Globalization.EncodingTable.h"
#include "System.NotImplementedException.h"
#include "System.Globalization.InternalCodePageDataItem.h"
#include "System.Globalization.InternalEncodingDataItem.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Globalization
	{
		uint8_t* EncodingTable::nativeCreateOpenFileMapping(System::String* inSectionName, int32_t inBytesToAllocate, int** mappedFileHandle)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		InternalEncodingDataItem* EncodingTable::GetEncodingData()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t EncodingTable::GetNumEncodingItems()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		InternalCodePageDataItem* EncodingTable::GetCodePageData()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
