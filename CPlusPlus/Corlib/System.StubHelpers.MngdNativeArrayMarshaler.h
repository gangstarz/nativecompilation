#ifndef __SYSTEM_STUBHELPERS_MNGDNATIVEARRAYMARSHALER
#define __SYSTEM_STUBHELPERS_MNGDNATIVEARRAYMARSHALER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace StubHelpers
	{
		class MngdNativeArrayMarshaler : public System::Object
		{
		public:
			void CreateMarshaler(int* pMarshalState, int* pMT, int32_t dwFlags);
			void ConvertSpaceToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ConvertContentsToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ConvertSpaceToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome, int32_t cElements);
			void ConvertContentsToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ClearNative(int* pMarshalState, int* pNativeHome, int32_t cElements);
			void ClearNativeContents(int* pMarshalState, int* pNativeHome, int32_t cElements);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
