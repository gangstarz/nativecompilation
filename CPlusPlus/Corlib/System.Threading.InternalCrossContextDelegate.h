#ifndef __SYSTEM_THREADING_INTERNALCROSSCONTEXTDELEGATE
#define __SYSTEM_THREADING_INTERNALCROSSCONTEXTDELEGATE

#include "../gc.h"
#include <cstdint>
#include "System.MulticastDelegate.h"

namespace System
{
	namespace Threading
	{
		class InternalCrossContextDelegate : public System::MulticastDelegate
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
