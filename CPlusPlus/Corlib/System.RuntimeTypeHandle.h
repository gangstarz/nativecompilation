#ifndef __SYSTEM_RUNTIMETYPEHANDLE
#define __SYSTEM_RUNTIMETYPEHANDLE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class Enum;
	class IRuntimeMethodInfo;
	class RuntimeMethodHandleInternal;
	class RuntimeType;
	class Type;
	namespace Reflection
	{
		class Assembly;
		class CorElementType;
		class MemberInfo;
		class Module;
		class RuntimeAssembly;
		class RuntimeModule;
		class TypeAttributes;
		class TypeInfo;
	}

	class RuntimeTypeHandle : public ValueType
	{
	public:
		int IsInstanceOfType(RuntimeType* type, System::Object* o);
		int* GetValueInternal(RuntimeTypeHandle handle);
		System::Object* CreateInstance(RuntimeType* type, int publicOnly, int noCheck, int& canBeCached, RuntimeMethodHandleInternal& ctor, int& bNeedSecurityCheck);
		System::Object* CreateCaInstance(RuntimeType* type, IRuntimeMethodInfo* ctor);
		System::Object* Allocate(RuntimeType* type);
		System::Object* CreateInstanceForAnotherGenericParameter(RuntimeType* type, RuntimeType* genericParameter);
		System::Reflection::CorElementType GetCorElementType(RuntimeType* type);
		System::Reflection::RuntimeAssembly* GetAssembly(RuntimeType* type);
		System::Reflection::RuntimeModule* GetModule(RuntimeType* type);
		RuntimeType* GetBaseType(RuntimeType* type);
		System::Reflection::TypeAttributes GetAttributes(RuntimeType* type);
		RuntimeType* GetElementType(RuntimeType* type);
		int32_t GetArrayRank(RuntimeType* type);
		int32_t GetToken(RuntimeType* type);
		RuntimeMethodHandleInternal GetMethodAt(RuntimeType* type, int32_t slot);
		int GetFields(RuntimeType* type, int** result, int32_t* count);
		System::NormalArray<Type*>* GetInterfaces(RuntimeType* type);
		int32_t GetNumVirtuals(RuntimeType* type);
		int IsComObject(RuntimeType* type, int isGenericCOM);
		int IsContextful(RuntimeType* type);
		int IsInterface(RuntimeType* type);
		int HasProxyAttribute(RuntimeType* type);
		int IsValueType(RuntimeType* type);
		int CanCastTo(RuntimeType* type, RuntimeType* target);
		RuntimeType* GetDeclaringType(RuntimeType* type);
		IRuntimeMethodInfo* GetDeclaringMethod(RuntimeType* type);
		int HasInstantiation(RuntimeType* type);
		int IsGenericTypeDefinition(RuntimeType* type);
		int IsGenericVariable(RuntimeType* type);
		int ContainsGenericVariables(RuntimeType* handle);
		int IsEquivalentTo(RuntimeType* rtType1, RuntimeType* rtType2);
		int IsEquivalentType(RuntimeType* type);
		RuntimeMethodHandleInternal GetFirstIntroducedMethod(RuntimeType* type);
		void GetNextIntroducedMethod(RuntimeMethodHandleInternal& method);
		void* _GetUtf8Name(RuntimeType* type);
		int32_t GetGenericVariableIndex(RuntimeType* type);
		int* _GetMetadataImport(RuntimeType* type);
		int CompareCanonicalHandles(RuntimeType* left, RuntimeType* right);
		int SatisfiesConstraints(RuntimeType* paramType, int** pTypeContext, int32_t typeContextLength, int** pMethodContext, int32_t methodContextLength, RuntimeType* toType);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
