#ifndef __SYSTEM_REFLECTION_BINDINGFLAGS
#define __SYSTEM_REFLECTION_BINDINGFLAGS

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Reflection
	{
		class BindingFlags
		{
		public:
			static const int32_t Default = 0;
			static const int32_t IgnoreCase = 1;
			static const int32_t DeclaredOnly = 2;
			static const int32_t Instance = 4;
			static const int32_t Static = 8;
			static const int32_t Public = 16;
			static const int32_t NonPublic = 32;
			static const int32_t FlattenHierarchy = 64;
			static const int32_t InvokeMethod = 256;
			static const int32_t CreateInstance = 512;
			static const int32_t GetField = 1024;
			static const int32_t SetField = 2048;
			static const int32_t GetProperty = 4096;
			static const int32_t SetProperty = 8192;
			static const int32_t PutDispProperty = 16384;
			static const int32_t PutRefDispProperty = 32768;
			static const int32_t ExactBinding = 65536;
			static const int32_t SuppressChangeType = 131072;
			static const int32_t OptionalParamBinding = 262144;
			static const int32_t IgnoreReturn = 16777216;

		};
	}
}

#endif
