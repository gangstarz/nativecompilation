#ifndef __SYSTEM_CURRENTSYSTEMTIMEZONE
#define __SYSTEM_CURRENTSYSTEMTIMEZONE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.TimeZone.h"

namespace System
{
	class String;
	class ValueType;

	class CurrentSystemTimeZone : public TimeZone
	{
	public:
		int32_t nativeGetTimeZoneMinuteOffset();
		System::String* nativeGetDaylightName();
		System::String* nativeGetStandardName();
		System::NormalArray<int16_t> nativeGetDaylightChanges(int32_t year);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
