#ifndef __SYSTEM_REFLECTION_RTFIELDINFO
#define __SYSTEM_REFLECTION_RTFIELDINFO

#include "../gc.h"
#include <cstdint>
#include "System.Reflection.RuntimeFieldInfo.h"

namespace System
{
	class Enum;
	class RuntimeType;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class FieldAttributes;
		class TypeInfo;

		class RtFieldInfo : public RuntimeFieldInfo
		{
		public:
			void PerformVisibilityCheckOnField(int* field, System::Object* target, System::RuntimeType* declaringType, FieldAttributes attr, uint32_t invocationFlags);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
