#include "System.Number.h"
#include "System.NotImplementedException.h"
#include "System.Decimal.h"
#include "System.Globalization.NumberFormatInfo.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	System::String* Number::FormatDecimal(System::Decimal value, System::String* format, System::Globalization::NumberFormatInfo* info)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* Number::FormatDouble(double value, System::String* format, System::Globalization::NumberFormatInfo* info)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* Number::FormatInt32(int32_t value, System::String* format, System::Globalization::NumberFormatInfo* info)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* Number::FormatUInt32(uint32_t value, System::String* format, System::Globalization::NumberFormatInfo* info)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* Number::FormatInt64(int64_t value, System::String* format, System::Globalization::NumberFormatInfo* info)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* Number::FormatSingle(float value, System::String* format, System::Globalization::NumberFormatInfo* info)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Number::NumberBufferToDecimal(uint8_t* number, System::Decimal& value)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Number::NumberBufferToDouble(uint8_t* number, double& value)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* Number::FormatUInt64(uint64_t value, System::String* format, System::Globalization::NumberFormatInfo* info)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* Number::FormatNumberBuffer(uint8_t* number, System::String* format, System::Globalization::NumberFormatInfo* info, wchar_t* allDigits)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
