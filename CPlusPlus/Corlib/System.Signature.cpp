#include "System.Signature.h"
#include "System.NotImplementedException.h"
#include "System.IRuntimeMethodInfo.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.TypeInfo.h"
#include "System.RuntimeFieldHandleInternal.h"
#include "System.RuntimeType.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	int Signature::CompareSig(Signature* sig1, Signature* sig2)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::NormalArray<Type*>* Signature::GetCustomModifiers(int32_t position, int required)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Signature::GetSignature(void* pCorSig, int32_t cCorSig, RuntimeFieldHandleInternal fieldHandle, IRuntimeMethodInfo* methodHandle, RuntimeType* declaringType)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
