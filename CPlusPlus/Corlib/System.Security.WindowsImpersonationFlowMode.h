#ifndef __SYSTEM_SECURITY_WINDOWSIMPERSONATIONFLOWMODE
#define __SYSTEM_SECURITY_WINDOWSIMPERSONATIONFLOWMODE

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Security
	{
		class WindowsImpersonationFlowMode
		{
		public:
			static const int32_t IMP_DEFAULT = 0;
			static const int32_t IMP_FASTFLOW = 0;
			static const int32_t IMP_NOFLOW = 1;
			static const int32_t IMP_ALWAYSFLOW = 2;

		};
	}
}

#endif
