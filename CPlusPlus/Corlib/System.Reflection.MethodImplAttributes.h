#ifndef __SYSTEM_REFLECTION_METHODIMPLATTRIBUTES
#define __SYSTEM_REFLECTION_METHODIMPLATTRIBUTES

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Reflection
	{
		class MethodImplAttributes
		{
		public:
			static const int32_t Managed = 0;
			static const int32_t IL = 0;
			static const int32_t Native = 1;
			static const int32_t OPTIL = 2;
			static const int32_t Runtime = 3;
			static const int32_t CodeTypeMask = 3;
			static const int32_t Unmanaged = 4;
			static const int32_t ManagedMask = 4;
			static const int32_t NoInlining = 8;
			static const int32_t ForwardRef = 16;
			static const int32_t Synchronized = 32;
			static const int32_t NoOptimization = 64;
			static const int32_t PreserveSig = 128;
			static const int32_t AggressiveInlining = 256;
			static const int32_t InternalCall = 4096;
			static const int32_t MaxMethodImplVal = 65535;

		};
	}
}

#endif
