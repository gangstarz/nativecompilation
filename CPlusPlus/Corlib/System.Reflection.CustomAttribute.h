#ifndef __SYSTEM_REFLECTION_CUSTOMATTRIBUTE
#define __SYSTEM_REFLECTION_CUSTOMATTRIBUTE

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class IRuntimeMethodInfo;
	class RuntimeType;
	class String;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
		class Module;
		class RuntimeModule;
		class TypeInfo;

		class CustomAttribute : public System::Object
		{
		public:
			void _ParseAttributeUsageAttribute(int* pCa, int32_t cCa, int32_t* targets, int* inherited, int* allowMultiple);
			System::Object* _CreateCaObject(RuntimeModule* pModule, System::IRuntimeMethodInfo* pCtor, uint8_t** ppBlob, uint8_t* pEndBlob, int32_t* pcNamedArgs);
			void _GetPropertyOrFieldData(RuntimeModule* pModule, uint8_t** ppBlobStart, uint8_t* pBlobEnd, System::String** name, int* bIsProperty, System::RuntimeType** type, System::Object** value);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
