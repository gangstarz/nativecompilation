#include "System.Runtime.Remoting.Messaging.StackBuilderSink.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace Remoting
		{
			namespace Messaging
			{
				System::Object* StackBuilderSink::_PrivateProcessMessage(int* md, System::NormalArray<System::Object*>* args, System::Object* server, System::NormalArray<System::Object*>** outArgs)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

			}
		}
	}
}
