#ifndef __SYSTEM_RUNTIME_REMOTING_MESSAGING_MESSAGE
#define __SYSTEM_RUNTIME_REMOTING_MESSAGING_MESSAGE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class AsyncCallback;
	class Delegate;
	class IAsyncResult;
	class MulticastDelegate;
	class String;
	class ValueType;

	namespace Runtime
	{
		namespace Remoting
		{
			namespace Messaging
			{
				class Message : public System::Object
				{
				public:
					void GetAsyncBeginInfo(System::AsyncCallback** acbd, System::Object** state);
					System::Object* GetThisPtr();
					int InternalHasVarArgs();
					System::Object* InternalGetArg(int32_t argNum);
					System::NormalArray<System::Object*>* InternalGetArgs();
					void PropagateOutParameters(System::NormalArray<System::Object*>* OutArgs, System::Object* retVal);
					System::IAsyncResult* GetAsyncResult();
					System::Object* GetReturnValue();
					int32_t InternalGetArgCount();
					int Dispatch(System::Object* target);
					void OutToUnmanagedDebugger(System::String* s);

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
