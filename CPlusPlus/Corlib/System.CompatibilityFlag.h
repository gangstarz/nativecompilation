#ifndef __SYSTEM_COMPATIBILITYFLAG
#define __SYSTEM_COMPATIBILITYFLAG

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	class CompatibilityFlag
	{
	public:
		static const int32_t SwallowUnhandledExceptions = 0;
		static const int32_t NullReferenceExceptionOnAV = 1;
		static const int32_t EagerlyGenerateRandomAsymmKeys = 2;
		static const int32_t FullTrustListAssembliesInGac = 3;
		static const int32_t DateTimeParseIgnorePunctuation = 4;
		static const int32_t OnlyGACDomainNeutral = 5;
		static const int32_t DisableReplacementCustomCulture = 6;

	};
}

#endif
