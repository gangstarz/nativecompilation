#include "System.Threading.OverlappedData.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Threading.NativeOverlapped.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		void OverlappedData::FreeNativeOverlapped(NativeOverlapped* nativeOverlappedPtr)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		OverlappedData* OverlappedData::GetOverlappedFromNative(NativeOverlapped* nativeOverlappedPtr)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void OverlappedData::CheckVMForIOPacket(NativeOverlapped*** pOVERLAP, uint32_t* errorCode, uint32_t* numBytes)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		NativeOverlapped* OverlappedData::AllocateNativeOverlapped()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
