#ifndef __SYSTEM_RUNTIME_COMPILERSERVICES_RUNTIMEHELPERS
#define __SYSTEM_RUNTIME_COMPILERSERVICES_RUNTIMEHELPERS

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Array;
	class Delegate;
	class IRuntimeMethodInfo;
	class MulticastDelegate;
	class RuntimeFieldHandle;
	class RuntimeType;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
		class Module;
		class RuntimeModule;
		class TypeInfo;
	}
	namespace Runtime
	{
		namespace CompilerServices
		{
			class CleanupCode;
			class TryCode;

			class RuntimeHelpers : public System::Object
			{
			public:
				void InitializeArray(System::Array* array, System::RuntimeFieldHandle fldHandle);
				System::Object* GetObjectValue(System::Object* obj);
				void _RunClassConstructor(System::RuntimeType* type);
				void PrepareDelegate(System::Delegate* d);
				void PrepareContractedDelegate(System::Delegate* d);
				int32_t GetHashCode(System::Object* o);
				int Equals(System::Object* o1, System::Object* o2);
				void EnsureSufficientExecutionStack();
				void ProbeForSufficientStack();
				void ExecuteCodeWithGuaranteedCleanup(TryCode* code, CleanupCode* backoutCode, System::Object* userData);
				void _RunModuleConstructor(System::Reflection::RuntimeModule* module);
				void _PrepareMethod(System::IRuntimeMethodInfo* method, int** pInstantiation, int32_t cInstantiation);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
