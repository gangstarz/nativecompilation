#ifndef __SYSTEM_THREADING_OVERLAPPEDDATA
#define __SYSTEM_THREADING_OVERLAPPEDDATA

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;
	namespace Threading
	{
		class NativeOverlapped;

		class OverlappedData : public System::Object
		{
		public:
			void FreeNativeOverlapped(NativeOverlapped* nativeOverlappedPtr);
			OverlappedData* GetOverlappedFromNative(NativeOverlapped* nativeOverlappedPtr);
			void CheckVMForIOPacket(NativeOverlapped*** pOVERLAP, uint32_t* errorCode, uint32_t* numBytes);
			NativeOverlapped* AllocateNativeOverlapped();

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
