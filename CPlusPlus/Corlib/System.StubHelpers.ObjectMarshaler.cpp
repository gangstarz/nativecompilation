#include "System.StubHelpers.ObjectMarshaler.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		void ObjectMarshaler::ConvertToNative(System::Object* objSrc, int* pDstVariant)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Object* ObjectMarshaler::ConvertToManaged(int* pSrcVariant)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ObjectMarshaler::ClearNative(int* pVariant)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
