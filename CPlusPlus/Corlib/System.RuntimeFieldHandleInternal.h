#ifndef __SYSTEM_RUNTIMEFIELDHANDLEINTERNAL
#define __SYSTEM_RUNTIMEFIELDHANDLEINTERNAL

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class RuntimeFieldHandleInternal : public ValueType
	{
	public:

		// [place holder for auto-generated recompiled members]
	};
}

#endif
