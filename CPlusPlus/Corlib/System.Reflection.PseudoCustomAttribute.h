#ifndef __SYSTEM_REFLECTION_PSEUDOCUSTOMATTRIBUTE
#define __SYSTEM_REFLECTION_PSEUDOCUSTOMATTRIBUTE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;
	namespace Reflection
	{
		class Module;
		class RuntimeModule;

		class PseudoCustomAttribute : public System::Object
		{
		public:
			void _GetSecurityAttributes(RuntimeModule* module, int32_t token, int assembly, System::NormalArray<System::Object*>** securityAttributes);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
