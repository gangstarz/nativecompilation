#ifndef __SYSTEM_GLOBALIZATION_CALENDARDATA
#define __SYSTEM_GLOBALIZATION_CALENDARDATA

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;

	namespace Globalization
	{
		class CalendarData : public System::Object
		{
		public:
			int32_t nativeGetCalendars(System::String* localeName, int useUserOverride, System::NormalArray<int32_t>* calendars);
			int nativeGetCalendarData(CalendarData* data, System::String* localeName, int32_t calendar);
			int32_t nativeGetTwoDigitYearMax(int32_t calID);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
