#ifndef __SYSTEM_STUBHELPERS_MNGDREFCUSTOMMARSHALER
#define __SYSTEM_STUBHELPERS_MNGDREFCUSTOMMARSHALER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace StubHelpers
	{
		class MngdRefCustomMarshaler : public System::Object
		{
		public:
			void CreateMarshaler(int* pMarshalState, int* pCMHelper);
			void ConvertContentsToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ClearNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ConvertContentsToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ClearManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
