#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_GCHANDLETYPE
#define __SYSTEM_RUNTIME_INTEROPSERVICES_GCHANDLETYPE

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			class GCHandleType
			{
			public:
				static const int32_t Weak = 0;
				static const int32_t WeakTrackResurrection = 1;
				static const int32_t Normal = 2;
				static const int32_t Pinned = 3;

			};
		}
	}
}

#endif
