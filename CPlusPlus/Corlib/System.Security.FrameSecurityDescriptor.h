#ifndef __SYSTEM_SECURITY_FRAMESECURITYDESCRIPTOR
#define __SYSTEM_SECURITY_FRAMESECURITYDESCRIPTOR

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace Security
	{
		class FrameSecurityDescriptor : public System::Object
		{
		public:
			void IncrementOverridesCount();
			void DecrementOverridesCount();
			void IncrementAssertCount();
			void DecrementAssertCount();

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
