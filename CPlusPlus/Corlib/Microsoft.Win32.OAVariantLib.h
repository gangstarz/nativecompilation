#ifndef __MICROSOFT_WIN32_OAVARIANTLIB
#define __MICROSOFT_WIN32_OAVARIANTLIB

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;
	class Variant;
}

namespace Microsoft
{
	namespace Win32
	{
		class OAVariantLib : public System::Object
		{
		public:
			void ChangeTypeEx(System::Variant& result, System::Variant& source, int32_t lcid, int* typeHandle, int32_t cvType, int16_t flags);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
