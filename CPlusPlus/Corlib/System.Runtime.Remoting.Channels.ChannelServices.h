#ifndef __SYSTEM_RUNTIME_REMOTING_CHANNELS_CHANNELSERVICES
#define __SYSTEM_RUNTIME_REMOTING_CHANNELS_CHANNELSERVICES

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;
	namespace Runtime
	{
		namespace Remoting
		{
			namespace Channels
			{
				class Perf_Contexts;

				class ChannelServices : public System::Object
				{
				public:
					Perf_Contexts* GetPrivateContextsPerfCounters();

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
