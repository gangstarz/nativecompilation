#include "System.Threading.ReaderWriterLock.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Threading.LockCookie.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		void ReaderWriterLock::AcquireReaderLockInternal(int32_t millisecondsTimeout)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ReaderWriterLock::AcquireWriterLockInternal(int32_t millisecondsTimeout)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ReaderWriterLock::ReleaseReaderLockInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ReaderWriterLock::ReleaseWriterLockInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ReaderWriterLock::DowngradeFromWriterLockInternal(LockCookie& lockCookie)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ReaderWriterLock::RestoreLockInternal(LockCookie& lockCookie)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int ReaderWriterLock::PrivateGetIsReaderLockHeld()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int ReaderWriterLock::PrivateGetIsWriterLockHeld()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t ReaderWriterLock::PrivateGetWriterSeqNum()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int ReaderWriterLock::AnyWritersSince(int32_t seqNum)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ReaderWriterLock::FCallUpgradeToWriterLock(LockCookie& result, int32_t millisecondsTimeout)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ReaderWriterLock::FCallReleaseLock(LockCookie& result)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ReaderWriterLock::PrivateInitialize()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ReaderWriterLock::PrivateDestruct()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
