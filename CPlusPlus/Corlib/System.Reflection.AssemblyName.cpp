#include "System.Reflection.AssemblyName.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Reflection.Assembly.h"
#include "System.Reflection.RuntimeAssembly.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Reflection
	{
		int AssemblyName::ReferenceMatchesDefinitionInternal(AssemblyName* reference, AssemblyName* definition, int parse)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void AssemblyName::nInit(RuntimeAssembly** assembly, int forIntrospection, int raiseResolveEvent)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		AssemblyName* AssemblyName::nGetFileInformation(System::String* s)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::String* AssemblyName::nToString()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::String* AssemblyName::EscapeCodeBase(System::String* codeBase)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::NormalArray<uint8_t> AssemblyName::nGetPublicKeyToken()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
