#ifndef __SYSTEM_NOTIMPLEMENTEDEXCEPTION
#define __SYSTEM_NOTIMPLEMENTEDEXCEPTION

#include "../gc.h"
#include <cstdint>
#include "System.SystemException.h"

namespace System
{
	class NotImplementedException : public SystemException
	{
	public:
		// [place holder for auto-generated recompiled members]
	};
}

#endif
