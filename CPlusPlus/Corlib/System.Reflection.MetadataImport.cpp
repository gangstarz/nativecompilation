#include "System.Reflection.MetadataImport.h"
#include "System.NotImplementedException.h"
#include "System.Guid.h"
#include "System.Object.h"
#include "System.Reflection.ConstArray.h"
#include "System.Reflection.MetadataEnumResult.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Reflection
	{
		void MetadataImport::_Enum(int* scope, int32_t type, int32_t parent, MetadataEnumResult* result)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::String* MetadataImport::_GetDefaultValue(int* scope, int32_t mdToken, int64_t* value, int32_t* length, int32_t* corElementType)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetName(int* scope, int32_t mdToken, void** name)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetNamespace(int* scope, int32_t mdToken, void** namesp)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetEventProps(int* scope, int32_t mdToken, void** name, int32_t* eventAttributes)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetFieldDefProps(int* scope, int32_t mdToken, int32_t* fieldAttributes)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetPropertyProps(int* scope, int32_t mdToken, void** name, int32_t* propertyAttributes, ConstArray* signature)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetParentToken(int* scope, int32_t mdToken, int32_t* tkParent)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetParamDefProps(int* scope, int32_t parameterToken, int32_t* sequence, int32_t* attributes)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetScopeProps(int* scope, System::Guid* mvid)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetSigOfMethodDef(int* scope, int32_t methodToken, ConstArray& signature)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetMemberRefProps(int* scope, int32_t memberTokenRef, ConstArray* signature)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetCustomAttributeProps(int* scope, int32_t customAttributeToken, int32_t* constructorToken, ConstArray* signature)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int MetadataImport::_GetFieldOffset(int* scope, int32_t typeTokenDef, int32_t fieldTokenDef, int32_t* offset)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetSigOfFieldDef(int* scope, int32_t fieldToken, ConstArray& fieldMarshal)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetFieldMarshal(int* scope, int32_t fieldToken, ConstArray& fieldMarshal)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int MetadataImport::_IsValidToken(int* scope, int32_t token)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetMarshalAs(int* pNativeType, int32_t cNativeType, int32_t* unmanagedType, int32_t* safeArraySubType, System::String** safeArrayUserDefinedSubType, int32_t* arraySubType, int32_t* sizeParamIndex, int32_t* sizeConst, System::String** marshalType, System::String** marshalCookie, int32_t* iidParamIndex)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetUserString(int* scope, int32_t mdToken, void** name, int32_t* length)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetGenericParamProps(int* scope, int32_t genericParameter, int32_t* flags)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetSignatureFromToken(int* scope, int32_t methodToken, ConstArray& signature)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetClassLayout(int* scope, int32_t typeTokenDef, int32_t* packSize, int32_t* classSize)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MetadataImport::_GetPInvokeMap(int* scope, int32_t token, int32_t* attributes, void** importName, void** importDll)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
