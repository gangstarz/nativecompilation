#ifndef __SYSTEM_THREADING_HOSTEXECUTIONCONTEXTMANAGER
#define __SYSTEM_THREADING_HOSTEXECUTIONCONTEXTMANAGER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;
	namespace Runtime
	{
		namespace ConstrainedExecution
		{
			class CriticalFinalizerObject;
		}
		namespace InteropServices
		{
			class SafeHandle;
		}
	}

	namespace Threading
	{
		class HostExecutionContextManager : public System::Object
		{
		public:
			int HostSecurityManagerPresent();
			int32_t ReleaseHostSecurityContext(int* context);
			int32_t CloneHostSecurityContext(System::Runtime::InteropServices::SafeHandle* context, System::Runtime::InteropServices::SafeHandle* clonedContext);
			int32_t SetHostSecurityContext(System::Runtime::InteropServices::SafeHandle* context, int fReturnPrevious, System::Runtime::InteropServices::SafeHandle* prevContext);
			int32_t CaptureHostSecurityContext(System::Runtime::InteropServices::SafeHandle* capturedContext);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
