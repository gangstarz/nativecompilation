#ifndef __SYSTEM_CURRENCY
#define __SYSTEM_CURRENCY

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class Decimal;

	class Currency : public ValueType
	{
	public:
		void FCallToDecimal(System::Decimal& result, Currency c);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
