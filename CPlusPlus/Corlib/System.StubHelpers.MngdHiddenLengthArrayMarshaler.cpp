#include "System.StubHelpers.MngdHiddenLengthArrayMarshaler.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		void MngdHiddenLengthArrayMarshaler::CreateMarshaler(int* pMarshalState, int* pMT, int* cbElementSize, uint16_t vt)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdHiddenLengthArrayMarshaler::ConvertSpaceToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdHiddenLengthArrayMarshaler::ConvertContentsToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdHiddenLengthArrayMarshaler::ConvertSpaceToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome, int32_t elementCount)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdHiddenLengthArrayMarshaler::ConvertContentsToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdHiddenLengthArrayMarshaler::ClearNativeContents(int* pMarshalState, int* pNativeHome, int32_t cElements)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
