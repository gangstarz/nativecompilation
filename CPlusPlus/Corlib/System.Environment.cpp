#include "System.Environment.h"
#include "System.NotImplementedException.h"
#include "Microsoft.Win32.OSVERSIONINFO.h"
#include "Microsoft.Win32.OSVERSIONINFOEX.h"
#include "System.CompatibilityFlag.h"
#include "System.Enum.h"
#include "System.Exception.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	int32_t Environment::get_TickCount()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Environment::get_ExitCode()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Environment::set_ExitCode(int32_t value)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Environment::FailFast(System::String* message)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Environment::FailFast(System::String* message, uint32_t exitCode)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Environment::FailFast(System::String* message, Exception* exception)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* Environment::nativeGetEnvironmentVariable(System::String* variable)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Environment::GetVersion(Microsoft::Win32::OSVERSIONINFO* osVer)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Environment::GetVersionEx(Microsoft::Win32::OSVERSIONINFOEX* osVer)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* Environment::GetResourceFromDefault(System::String* key)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Environment::get_HasShutdownStarted()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Environment::GetCompatibilityFlag(CompatibilityFlag flag)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::NormalArray<System::String*>* Environment::GetCommandLineArgsNative()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
