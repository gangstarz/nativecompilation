#include "System.String.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	int32_t String::nativeCompareOrdinalEx(System::String* strA, int32_t indexA, System::String* strB, int32_t indexB, int32_t count)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t String::nativeCompareOrdinalIgnoreCaseWC(System::String* strA, int8_t* strBBytes)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	wchar_t String::get_Chars(int32_t index)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t String::get_Length()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* String::FastAllocateString(int32_t length)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t String::IndexOf(wchar_t value, int32_t startIndex, int32_t count)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t String::IndexOfAny(System::NormalArray<wchar_t> anyOf, int32_t startIndex, int32_t count)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t String::LastIndexOf(wchar_t value, int32_t startIndex, int32_t count)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t String::LastIndexOfAny(System::NormalArray<wchar_t> anyOf, int32_t startIndex, int32_t count)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* String::PadHelper(int32_t totalWidth, wchar_t paddingChar, int isRightPadded)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* String::ReplaceInternal(wchar_t oldChar, wchar_t newChar)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* String::ReplaceInternal(System::String* oldValue, System::String* newValue)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int String::IsFastSort()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int String::IsAscii()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void String::SetTrailByte(uint8_t data)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int String::TryGetTrailByte(uint8_t* data)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* String::InsertInternal(int32_t startIndex, System::String* value)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* String::RemoveInternal(int32_t startIndex, int32_t count)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
