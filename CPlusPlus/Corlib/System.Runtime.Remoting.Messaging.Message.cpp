#include "System.Runtime.Remoting.Messaging.Message.h"
#include "System.NotImplementedException.h"
#include "System.AsyncCallback.h"
#include "System.Delegate.h"
#include "System.IAsyncResult.h"
#include "System.MulticastDelegate.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace Remoting
		{
			namespace Messaging
			{
				void Message::GetAsyncBeginInfo(System::AsyncCallback** acbd, System::Object** state)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::Object* Message::GetThisPtr()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				int Message::InternalHasVarArgs()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::Object* Message::InternalGetArg(int32_t argNum)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::NormalArray<System::Object*>* Message::InternalGetArgs()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void Message::PropagateOutParameters(System::NormalArray<System::Object*>* OutArgs, System::Object* retVal)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::IAsyncResult* Message::GetAsyncResult()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::Object* Message::GetReturnValue()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				int32_t Message::InternalGetArgCount()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				int Message::Dispatch(System::Object* target)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void Message::OutToUnmanagedDebugger(System::String* s)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

			}
		}
	}
}
