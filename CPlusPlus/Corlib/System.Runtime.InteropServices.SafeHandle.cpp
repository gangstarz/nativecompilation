#include "System.Runtime.InteropServices.SafeHandle.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			void SafeHandle::InternalFinalize()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void SafeHandle::InternalDispose()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void SafeHandle::SetHandleAsInvalid()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void SafeHandle::DangerousAddRef(int& success)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void SafeHandle::DangerousRelease()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
