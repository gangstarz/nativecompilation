#include "System.Decimal.h"
#include "System.NotImplementedException.h"
#include "System.Currency.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	int32_t Decimal::FCallCompare(System::Decimal& d1, System::Decimal& d2)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Decimal::GetHashCode()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	double Decimal::ToDouble(System::Decimal d)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Decimal::FCallToInt32(System::Decimal d)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	float Decimal::ToSingle(System::Decimal d)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Decimal::FCallAddSub(System::Decimal& d1, System::Decimal& d2, uint8_t bSign)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Decimal::FCallAddSubOverflowed(System::Decimal& d1, System::Decimal& d2, uint8_t bSign, int& overflowed)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Decimal::FCallDivide(System::Decimal& d1, System::Decimal& d2)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Decimal::FCallDivideOverflowed(System::Decimal& d1, System::Decimal& d2, int& overflowed)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Decimal::FCallFloor(System::Decimal& d)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Decimal::FCallMultiply(System::Decimal& d1, System::Decimal& d2)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Decimal::FCallMultiplyOverflowed(System::Decimal& d1, System::Decimal& d2, int& overflowed)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Decimal::FCallRound(System::Decimal& d, int32_t decimals)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Decimal::FCallToCurrency(Currency& result, System::Decimal d)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Decimal::FCallTruncate(System::Decimal& d)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
