#include "System.Threading.HostExecutionContextManager.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"
#include "System.Runtime.InteropServices.SafeHandle.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		int HostExecutionContextManager::HostSecurityManagerPresent()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t HostExecutionContextManager::ReleaseHostSecurityContext(int* context)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t HostExecutionContextManager::CloneHostSecurityContext(System::Runtime::InteropServices::SafeHandle* context, System::Runtime::InteropServices::SafeHandle* clonedContext)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t HostExecutionContextManager::SetHostSecurityContext(System::Runtime::InteropServices::SafeHandle* context, int fReturnPrevious, System::Runtime::InteropServices::SafeHandle* prevContext)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t HostExecutionContextManager::CaptureHostSecurityContext(System::Runtime::InteropServices::SafeHandle* capturedContext)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
