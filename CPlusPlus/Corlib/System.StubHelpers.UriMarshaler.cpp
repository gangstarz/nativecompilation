#include "System.StubHelpers.UriMarshaler.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		System::String* UriMarshaler::GetRawUriFromNative(int* pUri)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* UriMarshaler::CreateNativeUriInstance(System::String* rawUri)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
