#ifndef __SYSTEM_OBJECT
#define __SYSTEM_OBJECT

#include "../gc.h"
#include <cstdint>

namespace System
{
	class Type;
	namespace Reflection
	{
		class MemberInfo;
	}

	class Object
	{
	public:
		Type* GetType();
		System::Object* MemberwiseClone();

		// [place holder for auto-generated recompiled members]
	};
}

#endif
