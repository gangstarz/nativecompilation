#ifndef __SYSTEM_CONTEXTBOUNDOBJECT
#define __SYSTEM_CONTEXTBOUNDOBJECT

#include "../gc.h"
#include <cstdint>
#include "System.MarshalByRefObject.h"

namespace System
{
	class ContextBoundObject : public MarshalByRefObject
	{
	public:

		// [place holder for auto-generated recompiled members]
	};
}

#endif
