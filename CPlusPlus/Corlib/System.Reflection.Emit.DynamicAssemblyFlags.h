#ifndef __SYSTEM_REFLECTION_EMIT_DYNAMICASSEMBLYFLAGS
#define __SYSTEM_REFLECTION_EMIT_DYNAMICASSEMBLYFLAGS

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Reflection
	{
		namespace Emit
		{
			class DynamicAssemblyFlags
			{
			public:
				static const int32_t None = 0;
				static const int32_t AllCritical = 1;
				static const int32_t Aptca = 2;
				static const int32_t Critical = 4;
				static const int32_t Transparent = 8;
				static const int32_t TreatAsSafe = 16;

			};
		}
	}
}

#endif
