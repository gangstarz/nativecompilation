#ifndef __SYSTEM_MEMBERACCESSEXCEPTION
#define __SYSTEM_MEMBERACCESSEXCEPTION

#include "../gc.h"
#include <cstdint>
#include "System.SystemException.h"

namespace System
{
	class MemberAccessException : public SystemException
	{
	public:

		// [place holder for auto-generated recompiled members]
	};
}

#endif
