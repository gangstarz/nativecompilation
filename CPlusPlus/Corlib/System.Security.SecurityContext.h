#ifndef __SYSTEM_SECURITY_SECURITYCONTEXT
#define __SYSTEM_SECURITY_SECURITYCONTEXT

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Enum;
	class ValueType;
	namespace Security
	{
		class WindowsImpersonationFlowMode;

		class SecurityContext : public System::Object
		{
		public:
			WindowsImpersonationFlowMode GetImpersonationFlowMode();
			int IsDefaultThreadSecurityInfo();

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
