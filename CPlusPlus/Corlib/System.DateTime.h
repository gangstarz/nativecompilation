#ifndef __SYSTEM_DATETIME
#define __SYSTEM_DATETIME

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class DateTime : public ValueType
	{
	public:
		int64_t GetSystemTimeAsFileTime();

		// [place holder for auto-generated recompiled members]
	};
}

#endif
