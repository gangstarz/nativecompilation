#ifndef __SYSTEM_REFLECTION_METADATAENUMRESULT
#define __SYSTEM_REFLECTION_METADATAENUMRESULT

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Reflection
	{
		class MetadataEnumResult : public System::ValueType
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
