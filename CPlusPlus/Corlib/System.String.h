#ifndef __SYSTEM_STRING
#define __SYSTEM_STRING

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	class String : public System::Object
	{
	public:
		int32_t nativeCompareOrdinalEx(System::String* strA, int32_t indexA, System::String* strB, int32_t indexB, int32_t count);
		int32_t nativeCompareOrdinalIgnoreCaseWC(System::String* strA, int8_t* strBBytes);
		wchar_t get_Chars(int32_t index);
		int32_t get_Length();
		System::String* FastAllocateString(int32_t length);
		int32_t IndexOf(wchar_t value, int32_t startIndex, int32_t count);
		int32_t IndexOfAny(System::NormalArray<wchar_t> anyOf, int32_t startIndex, int32_t count);
		int32_t LastIndexOf(wchar_t value, int32_t startIndex, int32_t count);
		int32_t LastIndexOfAny(System::NormalArray<wchar_t> anyOf, int32_t startIndex, int32_t count);
		System::String* PadHelper(int32_t totalWidth, wchar_t paddingChar, int isRightPadded);
		System::String* ReplaceInternal(wchar_t oldChar, wchar_t newChar);
		System::String* ReplaceInternal(System::String* oldValue, System::String* newValue);
		int IsFastSort();
		int IsAscii();
		void SetTrailByte(uint8_t data);
		int TryGetTrailByte(uint8_t* data);
		System::String* InsertInternal(int32_t startIndex, System::String* value);
		System::String* RemoveInternal(int32_t startIndex, int32_t count);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
