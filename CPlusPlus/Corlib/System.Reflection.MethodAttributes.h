#ifndef __SYSTEM_REFLECTION_METHODATTRIBUTES
#define __SYSTEM_REFLECTION_METHODATTRIBUTES

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Reflection
	{
		class MethodAttributes
		{
		public:
			static const int32_t ReuseSlot = 0;
			static const int32_t PrivateScope = 0;
			static const int32_t Private = 1;
			static const int32_t FamANDAssem = 2;
			static const int32_t Assembly = 3;
			static const int32_t Family = 4;
			static const int32_t FamORAssem = 5;
			static const int32_t Public = 6;
			static const int32_t MemberAccessMask = 7;
			static const int32_t UnmanagedExport = 8;
			static const int32_t Static = 16;
			static const int32_t Final = 32;
			static const int32_t Virtual = 64;
			static const int32_t HideBySig = 128;
			static const int32_t NewSlot = 256;
			static const int32_t VtableLayoutMask = 256;
			static const int32_t CheckAccessOnOverride = 512;
			static const int32_t Abstract = 1024;
			static const int32_t SpecialName = 2048;
			static const int32_t RTSpecialName = 4096;
			static const int32_t PinvokeImpl = 8192;
			static const int32_t HasSecurity = 16384;
			static const int32_t RequireSecObject = 32768;
			static const int32_t ReservedMask = 53248;

		};
	}
}

#endif
