#ifndef __SYSTEM_VARIANT
#define __SYSTEM_VARIANT

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class Variant : public ValueType
	{
	public:
		void SetFieldsObject(System::Object* val);
		double GetR8FromVar();
		float GetR4FromVar();
		void SetFieldsR4(float val);
		void SetFieldsR8(double val);
		System::Object* BoxEnum();

		// [place holder for auto-generated recompiled members]
	};
}

#endif
