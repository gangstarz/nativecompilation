#include "System.Reflection.Emit.AssemblyBuilder.h"
#include "System.NotImplementedException.h"
#include "System.AppDomain.h"
#include "System.Enum.h"
#include "System.MarshalByRefObject.h"
#include "System.Object.h"
#include "System.Reflection.Assembly.h"
#include "System.Reflection.AssemblyName.h"
#include "System.Reflection.Emit.AssemblyBuilderAccess.h"
#include "System.Reflection.Emit.DynamicAssemblyFlags.h"
#include "System.Reflection.Module.h"
#include "System.Reflection.RuntimeAssembly.h"
#include "System.Reflection.RuntimeModule.h"
#include "System.Security.PermissionSet.h"
#include "System.Security.Policy.Evidence.h"
#include "System.Security.SecurityContextSource.h"
#include "System.Threading.StackCrawlMark.h"
#include "System.ValueType.h"


namespace System
{
	namespace Reflection
	{
		namespace Emit
		{
			System::Reflection::RuntimeModule* AssemblyBuilder::GetInMemoryAssemblyModule(System::Reflection::RuntimeAssembly* assembly)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Reflection::Assembly* AssemblyBuilder::nCreateDynamicAssembly(System::AppDomain* domain, System::Reflection::AssemblyName* name, System::Security::Policy::Evidence* identity, System::Threading::StackCrawlMark& stackMark, System::Security::PermissionSet* requiredPermissions, System::Security::PermissionSet* optionalPermissions, System::Security::PermissionSet* refusedPermissions, System::NormalArray<uint8_t> securityRulesBlob, System::NormalArray<uint8_t> aptcaBlob, AssemblyBuilderAccess access, DynamicAssemblyFlags flags, System::Security::SecurityContextSource securityContextSource)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Reflection::RuntimeModule* AssemblyBuilder::GetOnDiskAssemblyModule(System::Reflection::RuntimeAssembly* assembly)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
