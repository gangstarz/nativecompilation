#include "System.StubHelpers.InterfaceMarshaler.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		int* InterfaceMarshaler::ConvertToNative(System::Object* objSrc, int* itfMT, int* classMT, int32_t flags)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Object* InterfaceMarshaler::ConvertToManaged(int* pUnk, int* itfMT, int* classMT, int32_t flags)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Object* InterfaceMarshaler::ConvertToManagedWithoutUnboxing(int* pNative)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
