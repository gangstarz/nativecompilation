#ifndef __SYSTEM_THREADING_SYNCHRONIZATIONCONTEXT
#define __SYSTEM_THREADING_SYNCHRONIZATIONCONTEXT

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace Threading
	{
		class SynchronizationContext : public System::Object
		{
		public:
			int32_t WaitHelper(System::NormalArray<int*> waitHandles, int waitAll, int32_t millisecondsTimeout);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
