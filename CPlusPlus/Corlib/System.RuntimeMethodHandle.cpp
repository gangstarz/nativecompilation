#include "System.RuntimeMethodHandle.h"
#include "System.NotImplementedException.h"
#include "System.Enum.h"
#include "System.IRuntimeMethodInfo.h"
#include "System.Object.h"
#include "System.Reflection.LoaderAllocator.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.MethodAttributes.h"
#include "System.Reflection.MethodBody.h"
#include "System.Reflection.MethodImplAttributes.h"
#include "System.Reflection.Module.h"
#include "System.Reflection.RuntimeModule.h"
#include "System.Reflection.TypeInfo.h"
#include "System.Resolver.h"
#include "System.Runtime.Serialization.SerializationInfo.h"
#include "System.Runtime.Serialization.StreamingContext.h"
#include "System.RuntimeMethodHandleInternal.h"
#include "System.RuntimeType.h"
#include "System.Signature.h"
#include "System.String.h"
#include "System.Threading.StackCrawlMark.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	void RuntimeMethodHandle::CheckLinktimeDemands(IRuntimeMethodInfo* method, System::Reflection::RuntimeModule* module, int isDecoratedTargetSecurityTransparent)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	IRuntimeMethodInfo* RuntimeMethodHandle::_GetCurrentMethod(System::Threading::StackCrawlMark& stackMark)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Reflection::MethodAttributes RuntimeMethodHandle::GetAttributes(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Reflection::MethodImplAttributes RuntimeMethodHandle::GetImplAttributes(IRuntimeMethodInfo* method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	RuntimeType* RuntimeMethodHandle::GetDeclaringType(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t RuntimeMethodHandle::GetSlot(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t RuntimeMethodHandle::GetMethodDef(IRuntimeMethodInfo* method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* RuntimeMethodHandle::GetName(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeMethodHandle::MatchesNameHash(RuntimeMethodHandleInternal method, uint32_t hash)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* RuntimeMethodHandle::InvokeMethod(System::Object* target, System::NormalArray<System::Object*>* arguments, Signature* sig, int constructor)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	uint32_t RuntimeMethodHandle::GetSpecialSecurityFlags(IRuntimeMethodInfo* method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void RuntimeMethodHandle::PerformSecurityCheck(System::Object* obj, RuntimeMethodHandleInternal method, RuntimeType* parent, uint32_t invocationFlags)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void RuntimeMethodHandle::SerializationInvoke(IRuntimeMethodInfo* method, System::Object* target, System::Runtime::Serialization::SerializationInfo* info, System::Runtime::Serialization::StreamingContext& context)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeMethodHandle::_IsTokenSecurityTransparent(System::Reflection::RuntimeModule* module, int32_t metaDataToken)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeMethodHandle::HasMethodInstantiation(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	RuntimeMethodHandleInternal RuntimeMethodHandle::GetStubIfNeeded(RuntimeMethodHandleInternal method, RuntimeType* declaringType, System::NormalArray<RuntimeType*>* methodInstantiation)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	RuntimeMethodHandleInternal RuntimeMethodHandle::GetMethodFromCanonical(RuntimeMethodHandleInternal method, RuntimeType* declaringType)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeMethodHandle::IsGenericMethodDefinition(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeMethodHandle::IsTypicalMethodDefinition(IRuntimeMethodInfo* method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeMethodHandle::IsDynamicMethod(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	Resolver* RuntimeMethodHandle::GetResolver(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Reflection::MethodBody* RuntimeMethodHandle::GetMethodBody(IRuntimeMethodInfo* method, RuntimeType* declaringType)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeMethodHandle::IsConstructor(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Reflection::LoaderAllocator* RuntimeMethodHandle::GetLoaderAllocator(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void* RuntimeMethodHandle::_GetUtf8Name(RuntimeMethodHandleInternal method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
