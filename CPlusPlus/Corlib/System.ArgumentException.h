#ifndef __SYSTEM_ARGUMENTEXCEPTION
#define __SYSTEM_ARGUMENTEXCEPTION

#include "../gc.h"
#include <cstdint>
#include "System.SystemException.h"

namespace System
{
	class ArgumentException : public SystemException
	{
	public:

		// [place holder for auto-generated recompiled members]
	};
}

#endif
