#ifndef __SYSTEM_SECURITY_CRYPTOGRAPHY_X509CERTIFICATES_X509CONTENTTYPE
#define __SYSTEM_SECURITY_CRYPTOGRAPHY_X509CERTIFICATES_X509CONTENTTYPE

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Security
	{
		namespace Cryptography
		{
			namespace X509Certificates
			{
				class X509ContentType
				{
				public:
					static const int32_t Unknown = 0;
					static const int32_t Cert = 1;
					static const int32_t SerializedCert = 2;
					static const int32_t Pfx = 3;
					static const int32_t Pkcs12 = 3;
					static const int32_t SerializedStore = 4;
					static const int32_t Pkcs7 = 5;
					static const int32_t Authenticode = 6;

				};
			}
		}
	}
}

#endif
