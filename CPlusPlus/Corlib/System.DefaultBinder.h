#ifndef __SYSTEM_DEFAULTBINDER
#define __SYSTEM_DEFAULTBINDER

#include "../gc.h"
#include <cstdint>
#include "System.Reflection.Binder.h"

namespace System
{
	class RuntimeType;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
		class TypeInfo;
	}

	class DefaultBinder : public System::Reflection::Binder
	{
	public:
		int CanConvertPrimitiveObjectToType(System::Object* source, RuntimeType* type);
		int CanConvertPrimitive(RuntimeType* source, RuntimeType* target);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
