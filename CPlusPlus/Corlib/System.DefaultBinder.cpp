#include "System.DefaultBinder.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.TypeInfo.h"
#include "System.RuntimeType.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	int DefaultBinder::CanConvertPrimitiveObjectToType(System::Object* source, RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int DefaultBinder::CanConvertPrimitive(RuntimeType* source, RuntimeType* target)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
