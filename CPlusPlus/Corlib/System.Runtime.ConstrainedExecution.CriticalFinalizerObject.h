#ifndef __SYSTEM_RUNTIME_CONSTRAINEDEXECUTION_CRITICALFINALIZEROBJECT
#define __SYSTEM_RUNTIME_CONSTRAINEDEXECUTION_CRITICALFINALIZEROBJECT

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	namespace Runtime
	{
		namespace ConstrainedExecution
		{
			class CriticalFinalizerObject : public System::Object
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
