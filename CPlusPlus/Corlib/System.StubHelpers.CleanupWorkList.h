#ifndef __SYSTEM_STUBHELPERS_CLEANUPWORKLIST
#define __SYSTEM_STUBHELPERS_CLEANUPWORKLIST

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	namespace StubHelpers
	{
		class CleanupWorkList : public System::Object
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
