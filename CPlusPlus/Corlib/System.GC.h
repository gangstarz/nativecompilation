#ifndef __SYSTEM_GC
#define __SYSTEM_GC

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	class GC : public System::Object
	{
	public:
		int32_t GetGCLatencyMode();
		void SetGCLatencyMode(int32_t newLatencyMode);
		int32_t GetLOHCompactionMode();
		void SetLOHCompactionMode(int32_t newLOHCompactionyMode);
		int32_t GetMaxGeneration();
		int32_t _CollectionCount(int32_t generation, int32_t getSpecialGCCount);
		int IsServerGC();
		int32_t GetGeneration(System::Object* obj);
		void _SuppressFinalize(System::Object* o);
		int32_t _WaitForFullGCApproach(int32_t millisecondsTimeout);
		int32_t _WaitForFullGCComplete(int32_t millisecondsTimeout);
		int32_t GetGenerationWR(int* handle);
		void _ReRegisterForFinalize(System::Object* o);
		int _RegisterForFullGCNotification(int32_t maxGenerationPercentage, int32_t largeObjectHeapPercentage);
		int _CancelFullGCNotification();

		// [place holder for auto-generated recompiled members]
	};
}

#endif
