#ifndef __SYSTEM_REFLECTION_CONSTARRAY
#define __SYSTEM_REFLECTION_CONSTARRAY

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Reflection
	{
		class ConstArray : public System::ValueType
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
