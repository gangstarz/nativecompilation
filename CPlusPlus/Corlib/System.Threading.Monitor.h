#ifndef __SYSTEM_THREADING_MONITOR
#define __SYSTEM_THREADING_MONITOR

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace Threading
	{
		class Monitor : public System::Object
		{
		public:
			void Enter(System::Object* obj);
			void ReliableEnter(System::Object* obj, int& lockTaken);
			void Exit(System::Object* obj);
			void ReliableEnterTimeout(System::Object* obj, int32_t timeout, int& lockTaken);
			int IsEnteredNative(System::Object* obj);
			int ObjWait(int exitContext, int32_t millisecondsTimeout, System::Object* obj);
			void ObjPulse(System::Object* obj);
			void ObjPulseAll(System::Object* obj);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
