#ifndef __SYSTEM_ARRAY
#define __SYSTEM_ARRAY

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	class Array : public System::Object
	{
	public:
		void Copy(Array* sourceArray, int32_t sourceIndex, Array* destinationArray, int32_t destinationIndex, int32_t length, int reliable);
		void Clear(Array* array, int32_t index, int32_t length);
		int32_t get_Length();
		int64_t get_LongLength();
		int32_t GetLength(int32_t dimension);
		int32_t get_Rank();
		int32_t GetUpperBound(int32_t dimension);
		int32_t GetLowerBound(int32_t dimension);
		int32_t GetDataPtrOffsetInternal();
		int TrySZBinarySearch(Array* sourceArray, int32_t sourceIndex, int32_t count, System::Object* value, int32_t* retVal);
		int TrySZIndexOf(Array* sourceArray, int32_t sourceIndex, int32_t count, System::Object* value, int32_t* retVal);
		int TrySZLastIndexOf(Array* sourceArray, int32_t sourceIndex, int32_t count, System::Object* value, int32_t* retVal);
		int TrySZReverse(Array* array, int32_t index, int32_t count);
		int TrySZSort(Array* keys, Array* items, int32_t left, int32_t right);
		void Initialize();
		Array* InternalCreate(void* elementType, int32_t rank, int32_t* pLengths, int32_t* pLowerBounds);
		void InternalGetReference(void* elemRef, int32_t rank, int32_t* pIndices);
		void InternalSetValue(void* target, System::Object* value);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
