#ifndef __SYSTEM_REFLECTION_FIELDATTRIBUTES
#define __SYSTEM_REFLECTION_FIELDATTRIBUTES

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Reflection
	{
		class FieldAttributes
		{
		public:
			static const int32_t PrivateScope = 0;
			static const int32_t Private = 1;
			static const int32_t FamANDAssem = 2;
			static const int32_t Assembly = 3;
			static const int32_t Family = 4;
			static const int32_t FamORAssem = 5;
			static const int32_t Public = 6;
			static const int32_t FieldAccessMask = 7;
			static const int32_t Static = 16;
			static const int32_t InitOnly = 32;
			static const int32_t Literal = 64;
			static const int32_t NotSerialized = 128;
			static const int32_t HasFieldRVA = 256;
			static const int32_t SpecialName = 512;
			static const int32_t RTSpecialName = 1024;
			static const int32_t HasFieldMarshal = 4096;
			static const int32_t PinvokeImpl = 8192;
			static const int32_t HasDefault = 32768;
			static const int32_t ReservedMask = 38144;

		};
	}
}

#endif
