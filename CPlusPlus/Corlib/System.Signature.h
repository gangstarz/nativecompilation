#ifndef __SYSTEM_SIGNATURE
#define __SYSTEM_SIGNATURE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class IRuntimeMethodInfo;
	class RuntimeFieldHandleInternal;
	class RuntimeType;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
		class TypeInfo;
	}

	class Signature : public System::Object
	{
	public:
		int CompareSig(Signature* sig1, Signature* sig2);
		System::NormalArray<Type*>* GetCustomModifiers(int32_t position, int required);
		void GetSignature(void* pCorSig, int32_t cCorSig, RuntimeFieldHandleInternal fieldHandle, IRuntimeMethodInfo* methodHandle, RuntimeType* declaringType);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
