#include "System.Threading.ThreadPool.h"
#include "System.NotImplementedException.h"
#include "System.Enum.h"
#include "System.MarshalByRefObject.h"
#include "System.Object.h"
#include "System.Threading.NativeOverlapped.h"
#include "System.Threading.RegisteredWaitHandle.h"
#include "System.Threading.StackCrawlMark.h"
#include "System.Threading.WaitHandle.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		int ThreadPool::PostQueuedCompletionStatus(NativeOverlapped* overlapped)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int ThreadPool::SetMinThreadsNative(int32_t workerThreads, int32_t completionPortThreads)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int ThreadPool::SetMaxThreadsNative(int32_t workerThreads, int32_t completionPortThreads)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ThreadPool::GetMinThreadsNative(int32_t* workerThreads, int32_t* completionPortThreads)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ThreadPool::GetMaxThreadsNative(int32_t* workerThreads, int32_t* completionPortThreads)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ThreadPool::GetAvailableThreadsNative(int32_t* workerThreads, int32_t* completionPortThreads)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int ThreadPool::NotifyWorkItemComplete()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ThreadPool::ReportThreadStatus(int isWorking)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ThreadPool::NotifyWorkItemProgressNative()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int ThreadPool::IsThreadPoolHosted()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int ThreadPool::BindIOCompletionCallbackNative(int* fileHandle)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* ThreadPool::RegisterWaitForSingleObjectNative(WaitHandle* waitHandle, System::Object* state, uint32_t timeOutInterval, int executeOnlyOnce, RegisteredWaitHandle* registeredWaitHandle, StackCrawlMark& stackMark, int compressStack)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
