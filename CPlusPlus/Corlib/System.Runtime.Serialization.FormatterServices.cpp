#include "System.Runtime.Serialization.FormatterServices.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.TypeInfo.h"
#include "System.RuntimeType.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace Serialization
		{
			System::Object* FormatterServices::nativeGetUninitializedObject(System::RuntimeType* type)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int FormatterServices::GetEnableUnsafeTypeForwarders()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* FormatterServices::nativeGetSafeUninitializedObject(System::RuntimeType* type)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
