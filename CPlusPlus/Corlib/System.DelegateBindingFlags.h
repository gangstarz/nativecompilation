#ifndef __SYSTEM_DELEGATEBINDINGFLAGS
#define __SYSTEM_DELEGATEBINDINGFLAGS

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	class DelegateBindingFlags
	{
	public:
		static const int32_t StaticMethodOnly = 1;
		static const int32_t InstanceMethodOnly = 2;
		static const int32_t OpenDelegateOnly = 4;
		static const int32_t ClosedDelegateOnly = 8;
		static const int32_t NeverCloseOverNull = 16;
		static const int32_t CaselessMatching = 32;
		static const int32_t SkipSecurityChecks = 64;
		static const int32_t RelaxedSignature = 128;

	};
}

#endif
