#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_TYPELIBIMPORTERFLAGS
#define __SYSTEM_RUNTIME_INTEROPSERVICES_TYPELIBIMPORTERFLAGS

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			class TypeLibImporterFlags
			{
			public:
				static const int32_t None = 0;
				static const int32_t PrimaryInteropAssembly = 1;
				static const int32_t UnsafeInterfaces = 2;
				static const int32_t SafeArrayAsSystemArray = 4;
				static const int32_t TransformDispRetVals = 8;
				static const int32_t PreventClassMembers = 16;
				static const int32_t SerializableValueClasses = 32;
				static const int32_t ImportAsX86 = 256;
				static const int32_t ImportAsX64 = 512;
				static const int32_t ImportAsItanium = 1024;
				static const int32_t ImportAsAgnostic = 2048;
				static const int32_t ReflectionOnlyLoading = 4096;
				static const int32_t NoDefineVersionResource = 8192;
				static const int32_t ImportAsArm = 16384;

			};
		}
	}
}

#endif
