#ifndef __SYSTEM_WEAKREFERENCE
#define __SYSTEM_WEAKREFERENCE

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	class WeakReference : public System::Object
	{
	public:
		int get_IsAlive();
		System::Object* get_Target();
		void set_Target(System::Object* value);
		void Finalize();
		int IsTrackResurrection();
		void Create(System::Object* target, int trackResurrection);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
