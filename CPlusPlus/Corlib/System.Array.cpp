#include "System.Array.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	void Array::Copy(Array* sourceArray, int32_t sourceIndex, Array* destinationArray, int32_t destinationIndex, int32_t length, int reliable)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Array::Clear(Array* array, int32_t index, int32_t length)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Array::get_Length()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int64_t Array::get_LongLength()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Array::GetLength(int32_t dimension)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Array::get_Rank()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Array::GetUpperBound(int32_t dimension)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Array::GetLowerBound(int32_t dimension)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t Array::GetDataPtrOffsetInternal()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Array::TrySZBinarySearch(Array* sourceArray, int32_t sourceIndex, int32_t count, System::Object* value, int32_t* retVal)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Array::TrySZIndexOf(Array* sourceArray, int32_t sourceIndex, int32_t count, System::Object* value, int32_t* retVal)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Array::TrySZLastIndexOf(Array* sourceArray, int32_t sourceIndex, int32_t count, System::Object* value, int32_t* retVal)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Array::TrySZReverse(Array* array, int32_t index, int32_t count)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Array::TrySZSort(Array* keys, Array* items, int32_t left, int32_t right)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Array::Initialize()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	Array* Array::InternalCreate(void* elementType, int32_t rank, int32_t* pLengths, int32_t* pLowerBounds)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Array::InternalGetReference(void* elemRef, int32_t rank, int32_t* pIndices)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Array::InternalSetValue(void* target, System::Object* value)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
