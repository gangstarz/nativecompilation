#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_TYPELIBCONVERTER
#define __SYSTEM_RUNTIME_INTEROPSERVICES_TYPELIBCONVERTER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Enum;
	class String;
	class ValueType;
	namespace Collections
	{
		class ArrayList;
	}
	namespace Reflection
	{
		class Assembly;
		class Module;
		class RuntimeAssembly;
		class RuntimeModule;
	}
	namespace Runtime
	{
		namespace InteropServices
		{
			class ITypeLibExporterNotifySink;
			class ITypeLibImporterNotifySink;
			class TypeLibExporterFlags;
			class TypeLibImporterFlags;

			class TypeLibConverter : public System::Object
			{
			public:
				void nConvertTypeLibToMetadata(System::Object* typeLib, System::Reflection::RuntimeAssembly* asmBldr, System::Reflection::RuntimeModule* modBldr, System::String* nameSpace, TypeLibImporterFlags flags, ITypeLibImporterNotifySink* notifySink, System::Collections::ArrayList** eventItfInfoList);
				System::Object* nConvertAssemblyToTypeLib(System::Reflection::RuntimeAssembly* assembly, System::String* strTypeLibName, TypeLibExporterFlags flags, ITypeLibExporterNotifySink* notifySink);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
