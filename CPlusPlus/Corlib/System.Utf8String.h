#ifndef __SYSTEM_UTF8STRING
#define __SYSTEM_UTF8STRING

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class Utf8String : public ValueType
	{
	public:
		int EqualsCaseSensitive(void* szLhs, void* szRhs, int32_t cSz);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
