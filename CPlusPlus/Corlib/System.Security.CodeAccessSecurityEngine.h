#ifndef __SYSTEM_SECURITY_CODEACCESSSECURITYENGINE
#define __SYSTEM_SECURITY_CODEACCESSSECURITYENGINE

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Enum;
	class ValueType;
	namespace Collections
	{
		class ArrayList;
	}
	namespace Security
	{
		class CodeAccessPermission;
		class FrameSecurityDescriptor;
		class PermissionToken;
		class PermissionType;
	}
	namespace Threading
	{
		class StackCrawlMark;
	}

	namespace Security
	{
		class CodeAccessSecurityEngine : public System::Object
		{
		public:
			void SpecialDemand(PermissionType whatPermission, System::Threading::StackCrawlMark& stackMark);
			void Check(System::Object* demand, System::Threading::StackCrawlMark& stackMark, int isPermSet);
			int QuickCheckForAllDemands();
			int AllDomainsHomogeneousWithNoStackModifiers();
			FrameSecurityDescriptor* CheckNReturnSO(PermissionToken* permToken, CodeAccessPermission* demand, System::Threading::StackCrawlMark& stackMark, int32_t create);
			void GetZoneAndOriginInternal(System::Collections::ArrayList* zoneList, System::Collections::ArrayList* originList, System::Threading::StackCrawlMark& stackMark);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
