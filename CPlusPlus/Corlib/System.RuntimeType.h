#ifndef __SYSTEM_RUNTIMETYPE
#define __SYSTEM_RUNTIMETYPE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Reflection.TypeInfo.h"

namespace System
{
	class Enum;
	class Guid;
	class String;
	class ValueType;
	namespace Reflection
	{
		class BindingFlags;
	}

	class RuntimeType : public System::Reflection::TypeInfo
	{
	public:
		int IsWindowsRuntimeObjectType(RuntimeType* type);
		int IsTypeExportedToWindowsRuntime(RuntimeType* type);
		System::Object* _CreateEnum(RuntimeType* enumType, int64_t value);
		Type* GetTypeFromProgIDImpl(System::String* progID, System::String* server, int throwOnError);
		Type* GetTypeFromCLSIDImpl(Guid clsid, System::String* server, int throwOnError);
		void GetGUID(Guid& result);
		int CanValueSpecialCast(RuntimeType* valueType, RuntimeType* targetType);
		System::Object* AllocateValueType(RuntimeType* type, System::Object* value, int fForceTypeChange);
		System::Object* InvokeDispMethod(System::String* name, System::Reflection::BindingFlags invokeAttr, System::Object* target, System::NormalArray<System::Object*>* args, System::NormalArray<int> byrefModifiers, int32_t culture, System::NormalArray<System::String*>* namedParameters);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
