#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_SAFEBUFFER
#define __SYSTEM_RUNTIME_INTEROPSERVICES_SAFEBUFFER

#include "../gc.h"
#include <cstdint>
#include "Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid.h"

namespace System
{
	class TypedReference;
	class ValueType;

	namespace Runtime
	{
		namespace InteropServices
		{
			class SafeBuffer : public Microsoft::Win32::SafeHandles::SafeHandleZeroOrMinusOneIsInvalid
			{
			public:
				void PtrToStructureNative(uint8_t* ptr, System::TypedReference structure, uint32_t sizeofT);
				void StructureToPtrNative(System::TypedReference structure, uint8_t* ptr, uint32_t sizeofT);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
