#include "System.Threading.Monitor.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		void Monitor::Enter(System::Object* obj)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Monitor::ReliableEnter(System::Object* obj, int& lockTaken)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Monitor::Exit(System::Object* obj)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Monitor::ReliableEnterTimeout(System::Object* obj, int32_t timeout, int& lockTaken)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Monitor::IsEnteredNative(System::Object* obj)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Monitor::ObjWait(int exitContext, int32_t millisecondsTimeout, System::Object* obj)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Monitor::ObjPulse(System::Object* obj)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Monitor::ObjPulseAll(System::Object* obj)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
