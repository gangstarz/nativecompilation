#ifndef __SYSTEM_REFLECTION_ASSEMBLYNAME
#define __SYSTEM_REFLECTION_ASSEMBLYNAME

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;
	namespace Reflection
	{
		class Assembly;
		class RuntimeAssembly;

		class AssemblyName : public System::Object
		{
		public:
			int ReferenceMatchesDefinitionInternal(AssemblyName* reference, AssemblyName* definition, int parse);
			void nInit(RuntimeAssembly** assembly, int forIntrospection, int raiseResolveEvent);
			AssemblyName* nGetFileInformation(System::String* s);
			System::String* nToString();
			System::String* EscapeCodeBase(System::String* codeBase);
			System::NormalArray<uint8_t> nGetPublicKeyToken();

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
