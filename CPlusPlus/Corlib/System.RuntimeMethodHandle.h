#ifndef __SYSTEM_RUNTIMEMETHODHANDLE
#define __SYSTEM_RUNTIMEMETHODHANDLE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class Enum;
	class IRuntimeMethodInfo;
	class Resolver;
	class RuntimeMethodHandleInternal;
	class RuntimeType;
	class Signature;
	class String;
	class Type;
	namespace Reflection
	{
		class LoaderAllocator;
		class MemberInfo;
		class MethodAttributes;
		class MethodBody;
		class MethodImplAttributes;
		class Module;
		class RuntimeModule;
		class TypeInfo;
	}
	namespace Runtime
	{
		namespace Serialization
		{
			class SerializationInfo;
			class StreamingContext;
		}
	}
	namespace Threading
	{
		class StackCrawlMark;
	}

	class RuntimeMethodHandle : public ValueType
	{
	public:
		void CheckLinktimeDemands(IRuntimeMethodInfo* method, System::Reflection::RuntimeModule* module, int isDecoratedTargetSecurityTransparent);
		IRuntimeMethodInfo* _GetCurrentMethod(System::Threading::StackCrawlMark& stackMark);
		System::Reflection::MethodAttributes GetAttributes(RuntimeMethodHandleInternal method);
		System::Reflection::MethodImplAttributes GetImplAttributes(IRuntimeMethodInfo* method);
		RuntimeType* GetDeclaringType(RuntimeMethodHandleInternal method);
		int32_t GetSlot(RuntimeMethodHandleInternal method);
		int32_t GetMethodDef(IRuntimeMethodInfo* method);
		System::String* GetName(RuntimeMethodHandleInternal method);
		int MatchesNameHash(RuntimeMethodHandleInternal method, uint32_t hash);
		System::Object* InvokeMethod(System::Object* target, System::NormalArray<System::Object*>* arguments, Signature* sig, int constructor);
		uint32_t GetSpecialSecurityFlags(IRuntimeMethodInfo* method);
		void PerformSecurityCheck(System::Object* obj, RuntimeMethodHandleInternal method, RuntimeType* parent, uint32_t invocationFlags);
		void SerializationInvoke(IRuntimeMethodInfo* method, System::Object* target, System::Runtime::Serialization::SerializationInfo* info, System::Runtime::Serialization::StreamingContext& context);
		int _IsTokenSecurityTransparent(System::Reflection::RuntimeModule* module, int32_t metaDataToken);
		int HasMethodInstantiation(RuntimeMethodHandleInternal method);
		RuntimeMethodHandleInternal GetStubIfNeeded(RuntimeMethodHandleInternal method, RuntimeType* declaringType, System::NormalArray<RuntimeType*>* methodInstantiation);
		RuntimeMethodHandleInternal GetMethodFromCanonical(RuntimeMethodHandleInternal method, RuntimeType* declaringType);
		int IsGenericMethodDefinition(RuntimeMethodHandleInternal method);
		int IsTypicalMethodDefinition(IRuntimeMethodInfo* method);
		int IsDynamicMethod(RuntimeMethodHandleInternal method);
		Resolver* GetResolver(RuntimeMethodHandleInternal method);
		System::Reflection::MethodBody* GetMethodBody(IRuntimeMethodInfo* method, RuntimeType* declaringType);
		int IsConstructor(RuntimeMethodHandleInternal method);
		System::Reflection::LoaderAllocator* GetLoaderAllocator(RuntimeMethodHandleInternal method);
		void* _GetUtf8Name(RuntimeMethodHandleInternal method);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
