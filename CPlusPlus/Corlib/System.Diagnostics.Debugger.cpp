#include "System.Diagnostics.Debugger.h"
#include "System.NotImplementedException.h"
#include "System.Diagnostics.ICustomDebuggerNotification.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Diagnostics
	{
		int Debugger::IsDebuggerAttached()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Debugger::IsLogging()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Debugger::Log(int32_t level, System::String* category, System::String* message)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Debugger::BreakInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Debugger::LaunchInternal()
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Debugger::CustomNotification(ICustomDebuggerNotification* data)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
