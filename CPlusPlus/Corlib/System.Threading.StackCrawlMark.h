#ifndef __SYSTEM_THREADING_STACKCRAWLMARK
#define __SYSTEM_THREADING_STACKCRAWLMARK

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Threading
	{
		class StackCrawlMark
		{
		public:
			static const int32_t LookForMe = 0;
			static const int32_t LookForMyCaller = 1;
			static const int32_t LookForMyCallersCaller = 2;
			static const int32_t LookForThread = 3;

		};
	}
}

#endif
