#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_COMTYPES_ITYPEINFO
#define __SYSTEM_RUNTIME_INTEROPSERVICES_COMTYPES_ITYPEINFO

#include "../gc.h"
#include <cstdint>

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			namespace ComTypes
			{
				class ITypeInfo
				{
				public:

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
