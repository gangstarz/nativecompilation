#ifndef __SYSTEM_RUNTIME_REMOTING_CONTEXTS_CONTEXT
#define __SYSTEM_RUNTIME_REMOTING_CONTEXTS_CONTEXT

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace Runtime
	{
		namespace Remoting
		{
			namespace Contexts
			{
				class Context : public System::Object
				{
				public:
					void SetupInternalContext(int bDefault);
					void ExecuteCallBackInEE(int* privateData);
					void CleanupInternalContext();

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
