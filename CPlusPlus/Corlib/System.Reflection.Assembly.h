#ifndef __SYSTEM_REFLECTION_ASSEMBLY
#define __SYSTEM_REFLECTION_ASSEMBLY

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	namespace Reflection
	{
		class Assembly : public System::Object
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
