#include "System.Runtime.InteropServices.Marshal.h"
#include "System.NotImplementedException.h"
#include "System.Array.h"
#include "System.Delegate.h"
#include "System.Enum.h"
#include "System.Exception.h"
#include "System.Guid.h"
#include "System.IRuntimeFieldInfo.h"
#include "System.IRuntimeMethodInfo.h"
#include "System.Object.h"
#include "System.Reflection.Assembly.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.RuntimeAssembly.h"
#include "System.Reflection.TypeInfo.h"
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"
#include "System.Runtime.InteropServices.ComMemberType.h"
#include "System.Runtime.InteropServices.ComTypes.ITypeInfo.h"
#include "System.Runtime.InteropServices.ComTypes.ITypeLib.h"
#include "System.RuntimeType.h"
#include "System.Threading.Thread.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			uint32_t Marshal::SizeOfType(System::Type* type)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			uint32_t Marshal::AlignedSizeOfType(System::Type* type)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::SizeOfHelper(System::Type* t, int throwIfNotMarshalable)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::UnsafeAddrOfPinnedArrayElement(System::Array* arr, int32_t index)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::CopyToNative(System::Object* source, int32_t startIndex, int* destination, int32_t length)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::CopyToManaged(int* source, System::Object* destination, int32_t startIndex, int32_t length)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::GetLastWin32Error()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::SetLastWin32Error(int32_t error)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::GetExceptionPointers()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::GetExceptionCode()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::StructureToPtr(System::Object* structure, int* ptr, int fDeleteOld)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::PtrToStructureHelper(int* ptr, System::Object* structure, int allowValueClasses)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::DestroyStructure(int* ptr, System::Type* structuretype)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::ThrowExceptionForHRInternal(int32_t errorCode, int* errorInfo)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Exception* Marshal::GetExceptionForHRInternal(int32_t errorCode, int* errorInfo)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::GetHRForException(System::Exception* e)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::GetHRForException_WinRT(System::Exception* e)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::GetUnmanagedThunkForManagedMethodPtr(int* pfnMethodToWrap, int* pbSignature, int32_t cbSignature)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::GetManagedThunkForUnmanagedMethodPtr(int* pfnMethodToWrap, int* pbSignature, int32_t cbSignature)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::GetTypeLibLcid(System::Runtime::InteropServices::ComTypes::ITypeLib* typelib)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::GetTypeLibVersion(System::Runtime::InteropServices::ComTypes::ITypeLib* typeLibrary, int32_t* major, int32_t* minor)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::GetITypeInfoForType(System::Type* t)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::GetIUnknownForObjectNative(System::Object* o, int onlyInContext)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::GetRawIUnknownForComObjectNoAddRef(System::Object* o)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::GetIDispatchForObjectNative(System::Object* o, int onlyInContext)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::GetComInterfaceForObjectNative(System::Object* o, System::Type* t, int onlyInContext, int fEnalbeCustomizedQueryInterface)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* Marshal::GetObjectForIUnknown(int* pUnk)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* Marshal::GetUniqueObjectForIUnknown(int* unknown)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* Marshal::GetTypedObjectForIUnknown(int* pUnk, System::Type* t)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::CreateAggregatedObject(int* pOuter, System::Object* o)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::CleanupUnusedObjectsInCurrentContext()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int Marshal::AreComObjectsAvailableForCleanup()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int Marshal::IsComObject(System::Object* o)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::InternalReleaseComObject(System::Object* o)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::InternalFinalReleaseComObject(System::Object* o)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int Marshal::IsTypeVisibleFromCom(System::Type* t)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::QueryInterface(int* pUnk, System::Guid& iid, int** ppv)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::AddRef(int* pUnk)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::Release(int* pUnk)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::GetNativeVariantForObject(System::Object* obj, int* pDstNativeVariant)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* Marshal::GetObjectForNativeVariant(int* pSrcNativeVariant)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::NormalArray<System::Object*>* Marshal::GetObjectsForNativeVariants(int* aSrcNativeVariant, int32_t cVars)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::GetStartComSlot(System::Type* t)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::GetEndComSlot(System::Type* t)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Reflection::MemberInfo* Marshal::GetMethodInfoForComSlot(System::Type* t, int32_t slot, ComMemberType& memberType)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int Marshal::InternalSwitchCCW(System::Object* oldtp, System::Object* newtp)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* Marshal::InternalWrapIUnknownWithComObject(int* i)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::ChangeWrapperHandleStrength(System::Object* otp, int fIsWeak)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::InitializeWrapperForWinRT(System::Object* o, int*& pUnk)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::InitializeManagedWinRTFactoryObject(System::Object* o, System::RuntimeType* runtimeClassType)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* Marshal::GetNativeActivationFactory(System::Type* type)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Delegate* Marshal::GetDelegateForFunctionPointerInternal(int* ptr, System::Type* t)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::GetFunctionPointerForDelegateInternal(System::Delegate* d)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::GetSystemMaxDBCSCharSize()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* Marshal::OffsetOfHelper(System::IRuntimeFieldInfo* f)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Threading::Thread* Marshal::InternalGetThreadFromFiberCookie(int32_t cookie)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::FCallGetTypeLibGuid(System::Guid& result, System::Runtime::InteropServices::ComTypes::ITypeLib* pTLB)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::FCallGetTypeInfoGuid(System::Guid& result, System::Runtime::InteropServices::ComTypes::ITypeInfo* typeInfo)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::FCallGetTypeLibGuidForAssembly(System::Guid& result, System::Reflection::RuntimeAssembly* assembly)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::_GetTypeLibVersionForAssembly(System::Reflection::RuntimeAssembly* inputAssembly, int32_t* majorVersion, int32_t* minorVersion)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Type* Marshal::GetLoadedTypeForGUID(System::Guid& guid)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* Marshal::InternalCreateWrapperOfType(System::Object* o, System::Type* t)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Marshal::InternalGetComSlotForMethodInfo(System::IRuntimeMethodInfo* m)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Marshal::FCallGenerateGuidForType(System::Guid& result, System::Type* type)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
