#include "System.Security.Cryptography.Utils.h"
#include "System.NotImplementedException.h"
#include "Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid.h"
#include "System.Enum.h"
#include "System.Object.h"
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"
#include "System.Runtime.InteropServices.SafeHandle.h"
#include "System.Security.AccessControl.SecurityInfos.h"
#include "System.Security.Cryptography.CspParameters.h"
#include "System.Security.Cryptography.CspProviderFlags.h"
#include "System.Security.Cryptography.PaddingMode.h"
#include "System.Security.Cryptography.SafeKeyHandle.h"
#include "System.Security.Cryptography.SafeProvHandle.h"
#include "System.ValueType.h"


namespace System
{
	namespace Security
	{
		namespace Cryptography
		{
			int32_t Utils::_EncryptData(SafeKeyHandle* hKey, System::NormalArray<uint8_t> data, int32_t ib, int32_t cb, System::NormalArray<uint8_t>& outputBuffer, int32_t outputOffset, PaddingMode PaddingMode, int fDone)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int Utils::_GetEnforceFipsPolicySetting()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::NormalArray<uint8_t> Utils::_GetKeyParameter(SafeKeyHandle* hKey, uint32_t paramID)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Utils::_GetUserKey(SafeProvHandle* hProv, int32_t keyNumber, SafeKeyHandle*& hKey)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Utils::_ImportBulkKey(SafeProvHandle* hProv, int32_t algid, int useSalt, System::NormalArray<uint8_t> key, SafeKeyHandle*& hKey)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Utils::_OpenCSP(CspParameters* param, uint32_t flags, SafeProvHandle*& hProv)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Utils::_AcquireCSP(CspParameters* param, SafeProvHandle*& hProv)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Utils::_CreateCSP(CspParameters* param, int randomKeyContainer, SafeProvHandle*& hProv)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Utils::_DecryptData(SafeKeyHandle* hKey, System::NormalArray<uint8_t> data, int32_t ib, int32_t cb, System::NormalArray<uint8_t>& outputBuffer, int32_t outputOffset, PaddingMode PaddingMode, int fDone)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Utils::_ExportKey(SafeKeyHandle* hKey, int32_t blobType, System::Object* cspObject)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Utils::_GenerateKey(SafeProvHandle* hProv, int32_t algid, CspProviderFlags flags, int32_t keySize, SafeKeyHandle*& hKey)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::NormalArray<uint8_t> Utils::_GetKeySetSecurityInfo(SafeProvHandle* hProv, System::Security::AccessControl::SecurityInfos securityInfo, int32_t* error)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* Utils::_GetProviderParameter(SafeProvHandle* hProv, int32_t keyNumber, uint32_t paramID)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t Utils::_ImportCspBlob(System::NormalArray<uint8_t> keyBlob, SafeProvHandle* hProv, CspProviderFlags flags, SafeKeyHandle*& hKey)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void Utils::_ImportKey(SafeProvHandle* hCSP, int32_t keyNumber, CspProviderFlags flags, System::Object* cspObject, SafeKeyHandle*& hKey)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int Utils::_ProduceLegacyHmacValues()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
