#include "System.Mda.h"
#include "System.NotImplementedException.h"
#include "System.Exception.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	int Mda::IsStreamWriterBufferedDataLostEnabled()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Mda::MemberInfoCacheCreation()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Mda::IsInvalidGCHandleCookieProbeEnabled()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Mda::ReportStreamWriterBufferedDataLost(System::String* text)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Mda::IsStreamWriterBufferedDataLostCaptureAllocatedCallStack()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Mda::DateTimeInvalidLocalFormat()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Mda::FireInvalidGCHandleCookieProbe(int* cookie)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Mda::ReportErrorSafeHandleRelease(Exception* ex)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
