#ifndef __SYSTEM_BUFFER
#define __SYSTEM_BUFFER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Array;
	class ValueType;

	class Buffer : public System::Object
	{
	public:
		void BlockCopy(Array* src, int32_t srcOffset, Array* dst, int32_t dstOffset, int32_t count);
		void InternalBlockCopy(Array* src, int32_t srcOffsetBytes, Array* dst, int32_t dstOffsetBytes, int32_t byteCount);
		int IsPrimitiveTypeArray(Array* array);
		uint8_t _GetByte(Array* array, int32_t index);
		void _SetByte(Array* array, int32_t index, uint8_t value);
		int32_t _ByteLength(Array* array);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
