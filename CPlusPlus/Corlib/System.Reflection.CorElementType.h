#ifndef __SYSTEM_REFLECTION_CORELEMENTTYPE
#define __SYSTEM_REFLECTION_CORELEMENTTYPE

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Reflection
	{
		class CorElementType
		{
		public:
			static const uint8_t End = 0;
			static const uint8_t Void = 1;
			static const uint8_t Boolean = 2;
			static const uint8_t Char = 3;
			static const uint8_t I1 = 4;
			static const uint8_t U1 = 5;
			static const uint8_t I2 = 6;
			static const uint8_t U2 = 7;
			static const uint8_t I4 = 8;
			static const uint8_t U4 = 9;
			static const uint8_t I8 = 10;
			static const uint8_t U8 = 11;
			static const uint8_t R4 = 12;
			static const uint8_t R8 = 13;
			static const uint8_t String = 14;
			static const uint8_t Ptr = 15;
			static const uint8_t ByRef = 16;
			static const uint8_t ValueType = 17;
			static const uint8_t Class = 18;
			static const uint8_t Var = 19;
			static const uint8_t Array = 20;
			static const uint8_t GenericInst = 21;
			static const uint8_t TypedByRef = 22;
			static const uint8_t I = 24;
			static const uint8_t U = 25;
			static const uint8_t FnPtr = 27;
			static const uint8_t Object = 28;
			static const uint8_t SzArray = 29;
			static const uint8_t MVar = 30;
			static const uint8_t CModReqd = 31;
			static const uint8_t CModOpt = 32;
			static const uint8_t Internal = 33;
			static const uint8_t Max = 34;
			static const uint8_t Modifier = 64;
			static const uint8_t Sentinel = 65;
			static const uint8_t Pinned = 69;

		};
	}
}

#endif
