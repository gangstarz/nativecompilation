#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_REGISTRATIONSERVICES
#define __SYSTEM_RUNTIME_INTEROPSERVICES_REGISTRATIONSERVICES

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Enum;
	class Guid;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
	}
	namespace Runtime
	{
		namespace InteropServices
		{
			class RegistrationClassContext;
			class RegistrationConnectionType;

			class RegistrationServices : public System::Object
			{
			public:
				void RegisterTypeForComClientsNative(System::Type* type, System::Guid& g);
				int32_t RegisterTypeForComClientsExNative(System::Type* t, RegistrationClassContext clsContext, RegistrationConnectionType flags);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
