#ifndef __SYSTEM_DELEGATE
#define __SYSTEM_DELEGATE

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class DelegateBindingFlags;
	class Enum;
	class IRuntimeMethodInfo;
	class MulticastDelegate;
	class RuntimeType;
	class String;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
		class TypeInfo;
	}

	class Delegate : public System::Object
	{
	public:
		MulticastDelegate* InternalAllocLike(Delegate* d);
		int InternalEqualTypes(System::Object* a, System::Object* b);
		int* GetMulticastInvoke();
		int* GetInvokeMethod();
		IRuntimeMethodInfo* FindMethodHandle();
		int InternalEqualMethodHandles(Delegate* left, Delegate* right);
		int* AdjustTarget(System::Object* target, int* methodPtr);
		int* GetCallStub(int* methodPtr);
		int CompareUnmanagedFunctionPtrs(Delegate* d1, Delegate* d2);
		int BindToMethodName(System::Object* target, RuntimeType* methodType, System::String* method, DelegateBindingFlags flags);
		int BindToMethodInfo(System::Object* target, IRuntimeMethodInfo* method, RuntimeType* methodType, DelegateBindingFlags flags);
		MulticastDelegate* InternalAlloc(RuntimeType* type);
		void DelegateConstruct(System::Object* target, int* slot);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
