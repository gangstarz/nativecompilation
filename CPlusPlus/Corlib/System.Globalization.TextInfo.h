#ifndef __SYSTEM_GLOBALIZATION_TEXTINFO
#define __SYSTEM_GLOBALIZATION_TEXTINFO

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;

	namespace Globalization
	{
		class TextInfo : public System::Object
		{
		public:
			wchar_t InternalChangeCaseChar(int* handle, int* handleOrigin, System::String* localeName, wchar_t ch, int isToUpper);
			System::String* InternalChangeCaseString(int* handle, int* handleOrigin, System::String* localeName, System::String* str, int isToUpper);
			int32_t InternalGetCaseInsHash(int* handle, int* handleOrigin, System::String* localeName, System::String* str, int forceRandomizedHashing, int64_t additionalEntropy);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
