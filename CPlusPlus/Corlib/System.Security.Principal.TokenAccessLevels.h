#ifndef __SYSTEM_SECURITY_PRINCIPAL_TOKENACCESSLEVELS
#define __SYSTEM_SECURITY_PRINCIPAL_TOKENACCESSLEVELS

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Security
	{
		namespace Principal
		{
			class TokenAccessLevels
			{
			public:
				static const int32_t AssignPrimary = 1;
				static const int32_t Duplicate = 2;
				static const int32_t Impersonate = 4;
				static const int32_t Query = 8;
				static const int32_t QuerySource = 16;
				static const int32_t AdjustPrivileges = 32;
				static const int32_t AdjustGroups = 64;
				static const int32_t AdjustDefault = 128;
				static const int32_t AdjustSessionId = 256;
				static const int32_t Read = 131080;
				static const int32_t Write = 131296;
				static const int32_t AllAccess = 983551;
				static const int32_t MaximumAllowed = 33554432;

			};
		}
	}
}

#endif
