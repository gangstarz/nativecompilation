#ifndef __SYSTEM_REFLECTION_CUSTOMATTRIBUTENAMEDPARAMETER
#define __SYSTEM_REFLECTION_CUSTOMATTRIBUTENAMEDPARAMETER

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Reflection
	{
		class CustomAttributeNamedParameter : public System::ValueType
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
