#ifndef __SYSTEM_THREADING_INTERLOCKED
#define __SYSTEM_THREADING_INTERLOCKED

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class TypedReference;
	class ValueType;

	namespace Threading
	{
		class Interlocked : public System::Object
		{
		public:
			int32_t Exchange(int32_t& location1, int32_t value);
			int64_t Exchange(int64_t& location1, int64_t value);
			float Exchange(float& location1, float value);
			double Exchange(double& location1, double value);
			System::Object* Exchange(System::Object*& location1, System::Object* value);
			int* Exchange(int*& location1, int* value);
			void _Exchange(System::TypedReference location1, System::TypedReference value);
			int32_t CompareExchange(int32_t& location1, int32_t value, int32_t comparand);
			int64_t CompareExchange(int64_t& location1, int64_t value, int64_t comparand);
			float CompareExchange(float& location1, float value, float comparand);
			double CompareExchange(double& location1, double value, double comparand);
			System::Object* CompareExchange(System::Object*& location1, System::Object* value, System::Object* comparand);
			int* CompareExchange(int*& location1, int* value, int* comparand);
			void _CompareExchange(System::TypedReference location1, System::TypedReference value, System::Object* comparand);
			int32_t CompareExchange(int32_t& location1, int32_t value, int32_t comparand, int& succeeded);
			int32_t ExchangeAdd(int32_t& location1, int32_t value);
			int64_t ExchangeAdd(int64_t& location1, int64_t value);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
