#ifndef __SYSTEM_MARSHALBYREFOBJECT
#define __SYSTEM_MARSHALBYREFOBJECT

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	class MarshalByRefObject : public System::Object
	{
	public:
		int* GetComIUnknown(MarshalByRefObject* o);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
