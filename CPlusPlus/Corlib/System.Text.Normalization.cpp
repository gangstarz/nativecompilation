#include "System.Text.Normalization.h"
#include "System.NotImplementedException.h"
#include "System.Enum.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.Text.NormalizationForm.h"
#include "System.ValueType.h"


namespace System
{
	namespace Text
	{
		int32_t Normalization::nativeNormalizationNormalizeString(NormalizationForm normForm, int32_t& iError, System::String* lpSrcString, int32_t cwSrcLength, System::NormalArray<wchar_t> lpDstString, int32_t cwDstLength)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int Normalization::nativeNormalizationIsNormalizedString(NormalizationForm normForm, int32_t& iError, System::String* lpString, int32_t cwLength)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
