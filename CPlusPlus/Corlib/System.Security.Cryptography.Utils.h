#ifndef __SYSTEM_SECURITY_CRYPTOGRAPHY_UTILS
#define __SYSTEM_SECURITY_CRYPTOGRAPHY_UTILS

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace Microsoft
{
	namespace Win32
	{
		namespace SafeHandles
		{
			class SafeHandleZeroOrMinusOneIsInvalid;
		}
	}
}
namespace System
{
	class Enum;
	class ValueType;
	namespace Runtime
	{
		namespace ConstrainedExecution
		{
			class CriticalFinalizerObject;
		}
		namespace InteropServices
		{
			class SafeHandle;
		}
	}
	namespace Security
	{
		namespace AccessControl
		{
			class SecurityInfos;
		}
		namespace Cryptography
		{
			class CspParameters;
			class CspProviderFlags;
			class PaddingMode;
			class SafeKeyHandle;
			class SafeProvHandle;

			class Utils : public System::Object
			{
			public:
				int32_t _EncryptData(SafeKeyHandle* hKey, System::NormalArray<uint8_t> data, int32_t ib, int32_t cb, System::NormalArray<uint8_t>& outputBuffer, int32_t outputOffset, PaddingMode PaddingMode, int fDone);
				int _GetEnforceFipsPolicySetting();
				System::NormalArray<uint8_t> _GetKeyParameter(SafeKeyHandle* hKey, uint32_t paramID);
				int32_t _GetUserKey(SafeProvHandle* hProv, int32_t keyNumber, SafeKeyHandle*& hKey);
				void _ImportBulkKey(SafeProvHandle* hProv, int32_t algid, int useSalt, System::NormalArray<uint8_t> key, SafeKeyHandle*& hKey);
				int32_t _OpenCSP(CspParameters* param, uint32_t flags, SafeProvHandle*& hProv);
				void _AcquireCSP(CspParameters* param, SafeProvHandle*& hProv);
				void _CreateCSP(CspParameters* param, int randomKeyContainer, SafeProvHandle*& hProv);
				int32_t _DecryptData(SafeKeyHandle* hKey, System::NormalArray<uint8_t> data, int32_t ib, int32_t cb, System::NormalArray<uint8_t>& outputBuffer, int32_t outputOffset, PaddingMode PaddingMode, int fDone);
				void _ExportKey(SafeKeyHandle* hKey, int32_t blobType, System::Object* cspObject);
				void _GenerateKey(SafeProvHandle* hProv, int32_t algid, CspProviderFlags flags, int32_t keySize, SafeKeyHandle*& hKey);
				System::NormalArray<uint8_t> _GetKeySetSecurityInfo(SafeProvHandle* hProv, System::Security::AccessControl::SecurityInfos securityInfo, int32_t* error);
				System::Object* _GetProviderParameter(SafeProvHandle* hProv, int32_t keyNumber, uint32_t paramID);
				int32_t _ImportCspBlob(System::NormalArray<uint8_t> keyBlob, SafeProvHandle* hProv, CspProviderFlags flags, SafeKeyHandle*& hKey);
				void _ImportKey(SafeProvHandle* hCSP, int32_t keyNumber, CspProviderFlags flags, System::Object* cspObject, SafeKeyHandle*& hKey);
				int _ProduceLegacyHmacValues();

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
