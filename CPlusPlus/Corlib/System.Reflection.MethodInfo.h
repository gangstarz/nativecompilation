#ifndef __SYSTEM_REFLECTION_METHODINFO
#define __SYSTEM_REFLECTION_METHODINFO

#include "../gc.h"
#include <cstdint>
#include "System.Reflection.MethodBase.h"

namespace System
{
	namespace Reflection
	{
		class MethodInfo : public MethodBase
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
