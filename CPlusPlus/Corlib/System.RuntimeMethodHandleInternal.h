#ifndef __SYSTEM_RUNTIMEMETHODHANDLEINTERNAL
#define __SYSTEM_RUNTIMEMETHODHANDLEINTERNAL

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class RuntimeMethodHandleInternal : public ValueType
	{
	public:

		// [place holder for auto-generated recompiled members]
	};
}

#endif
