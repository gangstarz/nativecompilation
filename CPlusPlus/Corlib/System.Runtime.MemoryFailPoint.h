#ifndef __SYSTEM_RUNTIME_MEMORYFAILPOINT
#define __SYSTEM_RUNTIME_MEMORYFAILPOINT

#include "../gc.h"
#include <cstdint>
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"

namespace System
{
	class ValueType;

	namespace Runtime
	{
		class MemoryFailPoint : public System::Runtime::ConstrainedExecution::CriticalFinalizerObject
		{
		public:
			void GetMemorySettings(uint32_t* maxGCSegmentSize, uint64_t* topOfMemory);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
