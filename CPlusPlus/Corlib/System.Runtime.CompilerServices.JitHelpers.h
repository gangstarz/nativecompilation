#ifndef __SYSTEM_RUNTIME_COMPILERSERVICES_JITHELPERS
#define __SYSTEM_RUNTIME_COMPILERSERVICES_JITHELPERS

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace Runtime
	{
		namespace CompilerServices
		{
			class JitHelpers : public System::Object
			{
			public:
				void UnsafeSetArrayElement(System::NormalArray<System::Object*>* target, int32_t index, System::Object* element);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
