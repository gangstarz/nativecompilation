#include "System.Runtime.InteropServices.WindowsRuntime.RuntimeClass.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			namespace WindowsRuntime
			{
				int* RuntimeClass::GetRedirectedGetHashCodeMD()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				int32_t RuntimeClass::RedirectGetHashCode(int* pMD)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				int* RuntimeClass::GetRedirectedToStringMD()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::String* RuntimeClass::RedirectToString(int* pMD)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				int* RuntimeClass::GetRedirectedEqualsMD()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				int RuntimeClass::RedirectEquals(System::Object* obj, int* pMD)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

			}
		}
	}
}
