#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_TYPELIBEXPORTERFLAGS
#define __SYSTEM_RUNTIME_INTEROPSERVICES_TYPELIBEXPORTERFLAGS

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			class TypeLibExporterFlags
			{
			public:
				static const int32_t None = 0;
				static const int32_t OnlyReferenceRegistered = 1;
				static const int32_t CallerResolvedReferences = 2;
				static const int32_t OldNames = 4;
				static const int32_t ExportAs32Bit = 16;
				static const int32_t ExportAs64Bit = 32;

			};
		}
	}
}

#endif
