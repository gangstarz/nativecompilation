#ifndef __SYSTEM_ARGITERATOR
#define __SYSTEM_ARGITERATOR

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class RuntimeType;
	class Type;
	namespace Reflection
	{
		class MemberInfo;
		class TypeInfo;
	}

	class ArgIterator : public ValueType
	{
	public:
		int32_t GetRemainingCount();
		void FCallGetNextArg(void* result);
		void InternalGetNextArg(void* result, RuntimeType* rt);
		void* _GetNextArgType();

		// [place holder for auto-generated recompiled members]
	};
}

#endif
