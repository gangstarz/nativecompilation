#include "System.AppDomain.h"
#include "System.NotImplementedException.h"
#include "System.AppDomainSetup.h"
#include "System.Enum.h"
#include "System.LoaderOptimization.h"
#include "System.MarshalByRefObject.h"
#include "System.Object.h"
#include "System.Reflection.Assembly.h"
#include "System.Reflection.AssemblyName.h"
#include "System.Reflection.RuntimeAssembly.h"
#include "System.Runtime.Remoting.ObjRef.h"
#include "System.Security.HostSecurityManagerOptions.h"
#include "System.Security.Policy.Evidence.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	System::NormalArray<System::Reflection::Assembly*>* AppDomain::nGetAssemblies(int forIntrospection)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int AppDomain::IsUnloadingForcedFinalize()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int AppDomain::IsFinalizingForUnload()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void AppDomain::PublishAnonymouslyHostedDynamicMethodsAssembly(System::Reflection::RuntimeAssembly* assemblyHandle)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t AppDomain::_nExecuteAssembly(System::Reflection::RuntimeAssembly* assembly, System::NormalArray<System::String*>* args)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* AppDomain::nGetFriendlyName()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int AppDomain::IsDomainIdValid(int32_t id)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	AppDomain* AppDomain::GetDefaultDomain()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int* AppDomain::GetFusionContext()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int* AppDomain::GetSecurityDescriptor()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	AppDomain* AppDomain::nCreateDomain(System::String* friendlyName, AppDomainSetup* setup, System::Security::Policy::Evidence* providedSecurityInfo, System::Security::Policy::Evidence* creatorsSecurityInfo, int* parentSecurityDescriptor)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Runtime::Remoting::ObjRef* AppDomain::nCreateInstance(System::String* friendlyName, AppDomainSetup* setup, System::Security::Policy::Evidence* providedSecurityInfo, System::Security::Policy::Evidence* creatorsSecurityInfo, int* parentSecurityDescriptor)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void AppDomain::UpdateLoaderOptimization(LoaderOptimization optimization)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* AppDomain::IsStringInterned(System::String* str)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* AppDomain::GetOrInternString(System::String* str)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void AppDomain::nUnload(int32_t domainInternal)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t AppDomain::GetId()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int AppDomain::nMonitoringIsEnabled()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void AppDomain::nSetHostSecurityManagerFlags(System::Security::HostSecurityManagerOptions flags)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* AppDomain::nApplyPolicy(System::Reflection::AssemblyName* an)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* AppDomain::GetDynamicDir()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int AppDomain::nIsDefaultAppDomainForEvidence()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void AppDomain::nSetupFriendlyName(System::String* friendlyName)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void AppDomain::nSetDisableInterfaceCache()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void AppDomain::nChangeSecurityPolicy()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void AppDomain::nEnableMonitoring()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int64_t AppDomain::nGetTotalProcessorTime()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int64_t AppDomain::nGetTotalAllocatedMemorySize()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int64_t AppDomain::nGetLastSurvivedMemorySize()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int64_t AppDomain::nGetLastSurvivedProcessMemorySize()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
