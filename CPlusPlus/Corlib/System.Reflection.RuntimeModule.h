#ifndef __SYSTEM_REFLECTION_RUNTIMEMODULE
#define __SYSTEM_REFLECTION_RUNTIMEMODULE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Reflection.Module.h"

namespace System
{
	class RuntimeType;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
		class TypeInfo;

		class RuntimeModule : public Module
		{
		public:
			System::NormalArray<System::RuntimeType*>* GetTypes(RuntimeModule* module);
			int IsResource(RuntimeModule* module);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
