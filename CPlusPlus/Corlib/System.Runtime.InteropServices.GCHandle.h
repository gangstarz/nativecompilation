#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_GCHANDLE
#define __SYSTEM_RUNTIME_INTEROPSERVICES_GCHANDLE

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class Enum;
	namespace Runtime
	{
		namespace InteropServices
		{
			class GCHandleType;

			class GCHandle : public System::ValueType
			{
			public:
				int* InternalAlloc(System::Object* value, GCHandleType type);
				void InternalFree(int* handle);
				System::Object* InternalGet(int* handle);
				void InternalSet(int* handle, System::Object* value, int isPinned);
				System::Object* InternalCompareExchange(int* handle, System::Object* value, System::Object* oldValue, int isPinned);
				int* InternalAddrOfPinnedObject(int* handle);
				void InternalCheckDomain(int* handle);
				GCHandleType InternalGetHandleType(int* handle);

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
