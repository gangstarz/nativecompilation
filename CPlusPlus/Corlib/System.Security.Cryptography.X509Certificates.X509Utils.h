#ifndef __SYSTEM_SECURITY_CRYPTOGRAPHY_X509CERTIFICATES_X509UTILS
#define __SYSTEM_SECURITY_CRYPTOGRAPHY_X509CERTIFICATES_X509UTILS

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace Microsoft
{
	namespace Win32
	{
		class FILE_TIME;
		namespace SafeHandles
		{
			class SafeHandleZeroOrMinusOneIsInvalid;
		}
	}
}
namespace System
{
	class Enum;
	class String;
	class ValueType;
	namespace Runtime
	{
		namespace ConstrainedExecution
		{
			class CriticalFinalizerObject;
		}
		namespace InteropServices
		{
			class SafeHandle;
		}
	}
	namespace Security
	{
		namespace Cryptography
		{
			namespace X509Certificates
			{
				class SafeCertContextHandle;
				class SafeCertStoreHandle;
				class X509ContentType;

				class X509Utils : public System::Object
				{
				public:
					void _DuplicateCertContext(int* handle, SafeCertContextHandle*& safeCertContext);
					System::NormalArray<uint8_t> _GetCertRawData(SafeCertContextHandle* safeCertContext);
					System::String* _GetPublicKeyOid(SafeCertContextHandle* safeCertContext);
					System::NormalArray<uint8_t> _GetPublicKeyParameters(SafeCertContextHandle* safeCertContext);
					System::NormalArray<uint8_t> _GetPublicKeyValue(SafeCertContextHandle* safeCertContext);
					System::NormalArray<uint8_t> _GetThumbprint(SafeCertContextHandle* safeCertContext);
					void _AddCertificateToStore(SafeCertStoreHandle* safeCertStoreHandle, SafeCertContextHandle* safeCertContext);
					System::NormalArray<uint8_t> _ExportCertificatesToBlob(SafeCertStoreHandle* safeCertStoreHandle, X509ContentType contentType, int* password);
					void _GetDateNotAfter(SafeCertContextHandle* safeCertContext, Microsoft::Win32::FILE_TIME& fileTime);
					void _GetDateNotBefore(SafeCertContextHandle* safeCertContext, Microsoft::Win32::FILE_TIME& fileTime);
					System::String* _GetIssuerName(SafeCertContextHandle* safeCertContext, int legacyV1Mode);
					System::String* _GetSubjectInfo(SafeCertContextHandle* safeCertContext, uint32_t displayType, int legacyV1Mode);
					System::NormalArray<uint8_t> _GetSerialNumber(SafeCertContextHandle* safeCertContext);
					void _LoadCertFromBlob(System::NormalArray<uint8_t> rawData, int* password, uint32_t dwFlags, int persistKeySet, SafeCertContextHandle*& pCertCtx);
					void _LoadCertFromFile(System::String* fileName, int* password, uint32_t dwFlags, int persistKeySet, SafeCertContextHandle*& pCertCtx);
					void _OpenX509Store(uint32_t storeType, uint32_t flags, System::String* storeName, SafeCertStoreHandle*& safeCertStoreHandle);
					uint32_t _QueryCertBlobType(System::NormalArray<uint8_t> rawData);
					uint32_t _QueryCertFileType(System::String* fileName);

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
