#include "System.Threading.CompressedStack.h"
#include "System.NotImplementedException.h"
#include "System.Enum.h"
#include "System.Object.h"
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"
#include "System.Runtime.InteropServices.SafeHandle.h"
#include "System.Security.PermissionListSet.h"
#include "System.Threading.DomainCompressedStack.h"
#include "System.Threading.SafeCompressedStackHandle.h"
#include "System.Threading.StackCrawlMark.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		SafeCompressedStackHandle* CompressedStack::GetDelayedCompressedStack(StackCrawlMark& stackMark, int walkStack)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void CompressedStack::DestroyDelayedCompressedStack(int* unmanagedCompressedStack)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void CompressedStack::DestroyDCSList(SafeCompressedStackHandle* compressedStack)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t CompressedStack::GetDCSCount(SafeCompressedStackHandle* compressedStack)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int CompressedStack::IsImmediateCompletionCandidate(SafeCompressedStackHandle* compressedStack, CompressedStack** innerCS)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		DomainCompressedStack* CompressedStack::GetDomainCompressedStack(SafeCompressedStackHandle* compressedStack, int32_t index)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void CompressedStack::GetHomogeneousPLS(System::Security::PermissionListSet* hgPLS)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
