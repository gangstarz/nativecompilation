#include "System.Runtime.InteropServices.SafeBuffer.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.TypedReference.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			void SafeBuffer::PtrToStructureNative(uint8_t* ptr, System::TypedReference structure, uint32_t sizeofT)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void SafeBuffer::StructureToPtrNative(System::TypedReference structure, uint8_t* ptr, uint32_t sizeofT)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
