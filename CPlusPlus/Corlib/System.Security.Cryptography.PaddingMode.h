#ifndef __SYSTEM_SECURITY_CRYPTOGRAPHY_PADDINGMODE
#define __SYSTEM_SECURITY_CRYPTOGRAPHY_PADDINGMODE

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Security
	{
		namespace Cryptography
		{
			class PaddingMode
			{
			public:
				static const int32_t None = 1;
				static const int32_t PKCS7 = 2;
				static const int32_t Zeros = 3;
				static const int32_t ANSIX923 = 4;
				static const int32_t ISO10126 = 5;

			};
		}
	}
}

#endif
