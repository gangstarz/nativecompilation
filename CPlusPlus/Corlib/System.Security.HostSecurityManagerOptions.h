#ifndef __SYSTEM_SECURITY_HOSTSECURITYMANAGEROPTIONS
#define __SYSTEM_SECURITY_HOSTSECURITYMANAGEROPTIONS

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Security
	{
		class HostSecurityManagerOptions
		{
		public:
			static const int32_t None = 0;
			static const int32_t HostAppDomainEvidence = 1;
			static const int32_t HostPolicyLevel = 2;
			static const int32_t HostAssemblyEvidence = 4;
			static const int32_t HostDetermineApplicationTrust = 8;
			static const int32_t HostResolvePolicy = 16;
			static const int32_t AllFlags = 31;

		};
	}
}

#endif
