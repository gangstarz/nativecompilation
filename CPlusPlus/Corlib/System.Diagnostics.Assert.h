#ifndef __SYSTEM_DIAGNOSTICS_ASSERT
#define __SYSTEM_DIAGNOSTICS_ASSERT

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;

	namespace Diagnostics
	{
		class Assert : public System::Object
		{
		public:
			int32_t ShowDefaultAssertDialog(System::String* conditionString, System::String* message, System::String* stackTrace, System::String* windowTitle);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
