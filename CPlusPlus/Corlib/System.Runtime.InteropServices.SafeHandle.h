#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_SAFEHANDLE
#define __SYSTEM_RUNTIME_INTEROPSERVICES_SAFEHANDLE

#include "../gc.h"
#include <cstdint>
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"

namespace System
{
	class ValueType;

	namespace Runtime
	{
		namespace InteropServices
		{
			class SafeHandle : public System::Runtime::ConstrainedExecution::CriticalFinalizerObject
			{
			public:
				void InternalFinalize();
				void InternalDispose();
				void SetHandleAsInvalid();
				void DangerousAddRef(int& success);
				void DangerousRelease();

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
