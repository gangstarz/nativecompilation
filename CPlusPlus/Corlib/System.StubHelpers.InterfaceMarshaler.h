#ifndef __SYSTEM_STUBHELPERS_INTERFACEMARSHALER
#define __SYSTEM_STUBHELPERS_INTERFACEMARSHALER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace StubHelpers
	{
		class InterfaceMarshaler : public System::Object
		{
		public:
			int* ConvertToNative(System::Object* objSrc, int* itfMT, int* classMT, int32_t flags);
			System::Object* ConvertToManaged(int* pUnk, int* itfMT, int* classMT, int32_t flags);
			System::Object* ConvertToManagedWithoutUnboxing(int* pNative);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
