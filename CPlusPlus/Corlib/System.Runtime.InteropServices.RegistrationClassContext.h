#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_REGISTRATIONCLASSCONTEXT
#define __SYSTEM_RUNTIME_INTEROPSERVICES_REGISTRATIONCLASSCONTEXT

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			class RegistrationClassContext
			{
			public:
				static const int32_t InProcessServer = 1;
				static const int32_t InProcessHandler = 2;
				static const int32_t LocalServer = 4;
				static const int32_t InProcessServer16 = 8;
				static const int32_t RemoteServer = 16;
				static const int32_t InProcessHandler16 = 32;
				static const int32_t Reserved1 = 64;
				static const int32_t Reserved2 = 128;
				static const int32_t Reserved3 = 256;
				static const int32_t Reserved4 = 512;
				static const int32_t NoCodeDownload = 1024;
				static const int32_t Reserved5 = 2048;
				static const int32_t NoCustomMarshal = 4096;
				static const int32_t EnableCodeDownload = 8192;
				static const int32_t NoFailureLog = 16384;
				static const int32_t DisableActivateAsActivator = 32768;
				static const int32_t EnableActivateAsActivator = 65536;
				static const int32_t FromDefaultContext = 131072;

			};
		}
	}
}

#endif
