#ifndef __SYSTEM_RUNTIME_REMOTING_CHANNELS_PERF_CONTEXTS
#define __SYSTEM_RUNTIME_REMOTING_CHANNELS_PERF_CONTEXTS

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Runtime
	{
		namespace Remoting
		{
			namespace Channels
			{
				class Perf_Contexts : public System::ValueType
				{
				public:

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
