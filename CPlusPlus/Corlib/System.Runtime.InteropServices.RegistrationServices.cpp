#include "System.Runtime.InteropServices.RegistrationServices.h"
#include "System.NotImplementedException.h"
#include "System.Enum.h"
#include "System.Guid.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Runtime.InteropServices.RegistrationClassContext.h"
#include "System.Runtime.InteropServices.RegistrationConnectionType.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			void RegistrationServices::RegisterTypeForComClientsNative(System::Type* type, System::Guid& g)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int32_t RegistrationServices::RegisterTypeForComClientsExNative(System::Type* t, RegistrationClassContext clsContext, RegistrationConnectionType flags)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
