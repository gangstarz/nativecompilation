#ifndef __SYSTEM_MISSINGMEMBEREXCEPTION
#define __SYSTEM_MISSINGMEMBEREXCEPTION

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.MemberAccessException.h"

namespace System
{
	class String;
	class ValueType;

	class MissingMemberException : public MemberAccessException
	{
	public:
		System::String* FormatSignature(System::NormalArray<uint8_t> signature);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
