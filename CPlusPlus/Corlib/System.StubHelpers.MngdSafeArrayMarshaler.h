#ifndef __SYSTEM_STUBHELPERS_MNGDSAFEARRAYMARSHALER
#define __SYSTEM_STUBHELPERS_MNGDSAFEARRAYMARSHALER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace StubHelpers
	{
		class MngdSafeArrayMarshaler : public System::Object
		{
		public:
			void CreateMarshaler(int* pMarshalState, int* pMT, int32_t iRank, int32_t dwFlags);
			void ConvertSpaceToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ConvertContentsToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome, System::Object* pOriginalManaged);
			void ConvertSpaceToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ConvertContentsToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);
			void ClearNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
