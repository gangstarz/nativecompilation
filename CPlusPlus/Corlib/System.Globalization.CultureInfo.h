#ifndef __SYSTEM_GLOBALIZATION_CULTUREINFO
#define __SYSTEM_GLOBALIZATION_CULTUREINFO

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;

	namespace Globalization
	{
		class CultureInfo : public System::Object
		{
		public:
			System::String* nativeGetLocaleInfoEx(System::String* localeName, uint32_t field);
			int32_t nativeGetLocaleInfoExInt(System::String* localeName, uint32_t field);
			int nativeSetThreadLocale(System::String* localeName);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
