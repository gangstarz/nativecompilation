#ifndef __SYSTEM_TEXT_NORMALIZATION
#define __SYSTEM_TEXT_NORMALIZATION

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Enum;
	class String;
	class ValueType;
	namespace Text
	{
		class NormalizationForm;

		class Normalization : public System::Object
		{
		public:
			int32_t nativeNormalizationNormalizeString(NormalizationForm normForm, int32_t& iError, System::String* lpSrcString, int32_t cwSrcLength, System::NormalArray<wchar_t> lpDstString, int32_t cwDstLength);
			int nativeNormalizationIsNormalizedString(NormalizationForm normForm, int32_t& iError, System::String* lpString, int32_t cwLength);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
