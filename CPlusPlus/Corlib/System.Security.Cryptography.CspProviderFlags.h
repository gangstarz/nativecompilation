#ifndef __SYSTEM_SECURITY_CRYPTOGRAPHY_CSPPROVIDERFLAGS
#define __SYSTEM_SECURITY_CRYPTOGRAPHY_CSPPROVIDERFLAGS

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Security
	{
		namespace Cryptography
		{
			class CspProviderFlags
			{
			public:
				static const int32_t NoFlags = 0;
				static const int32_t UseMachineKeyStore = 1;
				static const int32_t UseDefaultKeyContainer = 2;
				static const int32_t UseNonExportableKey = 4;
				static const int32_t UseExistingKey = 8;
				static const int32_t UseArchivableKey = 16;
				static const int32_t UseUserProtectedKey = 32;
				static const int32_t NoPrompt = 64;
				static const int32_t CreateEphemeralKey = 128;

			};
		}
	}
}

#endif
