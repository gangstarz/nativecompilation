#ifndef __SYSTEM_APPDOMAIN
#define __SYSTEM_APPDOMAIN

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.MarshalByRefObject.h"

namespace System
{
	class AppDomainSetup;
	class Enum;
	class LoaderOptimization;
	class String;
	class ValueType;
	namespace Reflection
	{
		class Assembly;
		class AssemblyName;
		class RuntimeAssembly;
	}
	namespace Runtime
	{
		namespace Remoting
		{
			class ObjRef;
		}
	}
	namespace Security
	{
		class HostSecurityManagerOptions;
		namespace Policy
		{
			class Evidence;
		}
	}

	class AppDomain : public MarshalByRefObject
	{
	public:
		System::Array<System::Reflection::Assembly*>* nGetAssemblies(int forIntrospection);
		int IsUnloadingForcedFinalize();
		int IsFinalizingForUnload();
		void PublishAnonymouslyHostedDynamicMethodsAssembly(System::Reflection::RuntimeAssembly* assemblyHandle);
		int32_t _nExecuteAssembly(System::Reflection::RuntimeAssembly* assembly, System::NormalArray<System::String*>* args);
		System::String* nGetFriendlyName();
		int IsDomainIdValid(int32_t id);
		AppDomain* GetDefaultDomain();
		int* GetFusionContext();
		int* GetSecurityDescriptor();
		AppDomain* nCreateDomain(System::String* friendlyName, AppDomainSetup* setup, System::Security::Policy::Evidence* providedSecurityInfo, System::Security::Policy::Evidence* creatorsSecurityInfo, int* parentSecurityDescriptor);
		System::Runtime::Remoting::ObjRef* nCreateInstance(System::String* friendlyName, AppDomainSetup* setup, System::Security::Policy::Evidence* providedSecurityInfo, System::Security::Policy::Evidence* creatorsSecurityInfo, int* parentSecurityDescriptor);
		void UpdateLoaderOptimization(LoaderOptimization optimization);
		System::String* IsStringInterned(System::String* str);
		System::String* GetOrInternString(System::String* str);
		void nUnload(int32_t domainInternal);
		int32_t GetId();
		int nMonitoringIsEnabled();
		void nSetHostSecurityManagerFlags(System::Security::HostSecurityManagerOptions flags);
		System::String* nApplyPolicy(System::Reflection::AssemblyName* an);
		System::String* GetDynamicDir();
		int nIsDefaultAppDomainForEvidence();
		void nSetupFriendlyName(System::String* friendlyName);
		void nSetDisableInterfaceCache();
		void nChangeSecurityPolicy();
		void nEnableMonitoring();
		int64_t nGetTotalProcessorTime();
		int64_t nGetTotalAllocatedMemorySize();
		int64_t nGetLastSurvivedMemorySize();
		int64_t nGetLastSurvivedProcessMemorySize();

		// [place holder for auto-generated recompiled members]
	};
}

#endif
