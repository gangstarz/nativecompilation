#ifndef __SYSTEM_THREADING_REGISTEREDWAITHANDLESAFE
#define __SYSTEM_THREADING_REGISTEREDWAITHANDLESAFE

#include "../gc.h"
#include <cstdint>
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"

namespace System
{
	class ValueType;
	namespace Runtime
	{
		namespace InteropServices
		{
			class SafeHandle;
		}
	}

	namespace Threading
	{
		class RegisteredWaitHandleSafe : public System::Runtime::ConstrainedExecution::CriticalFinalizerObject
		{
		public:
			void WaitHandleCleanupNative(int* handle);
			int UnregisterWaitNative(int* handle, System::Runtime::InteropServices::SafeHandle* waitObject);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
