#ifndef __SYSTEM_BASECONFIGHANDLER
#define __SYSTEM_BASECONFIGHANDLER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;

	class BaseConfigHandler : public System::Object
	{
	public:
		void RunParser(System::String* fileName);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
