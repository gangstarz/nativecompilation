#include "System.Reflection.RuntimeAssembly.h"
#include "System.NotImplementedException.h"
#include "System.Enum.h"
#include "System.Object.h"
#include "System.Reflection.Assembly.h"
#include "System.Reflection.AssemblyName.h"
#include "System.Reflection.Module.h"
#include "System.Reflection.RuntimeModule.h"
#include "System.Security.Policy.Evidence.h"
#include "System.Security.SecurityContextSource.h"
#include "System.String.h"
#include "System.Threading.StackCrawlMark.h"
#include "System.ValueType.h"


namespace System
{
	namespace Reflection
	{
		RuntimeModule* RuntimeAssembly::GetManifestModule(RuntimeAssembly* assembly)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int RuntimeAssembly::AptcaCheck(RuntimeAssembly* targetAssembly, RuntimeAssembly* sourceAssembly)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t RuntimeAssembly::GetToken(RuntimeAssembly* assembly)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		RuntimeAssembly* RuntimeAssembly::_nLoad(AssemblyName* fileName, System::String* codeBase, System::Security::Policy::Evidence* assemblySecurity, RuntimeAssembly* locationHint, System::Threading::StackCrawlMark& stackMark, int* pPrivHostBinder, int throwOnFileNotFound, int forIntrospection, int suppressSecurityChecks)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int RuntimeAssembly::IsFrameworkAssembly(AssemblyName* assemblyName)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int RuntimeAssembly::IsReflectionOnly(RuntimeAssembly* assembly)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::NormalArray<System::String*>* RuntimeAssembly::GetManifestResourceNames(RuntimeAssembly* assembly)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::NormalArray<AssemblyName*>* RuntimeAssembly::GetReferencedAssemblies(RuntimeAssembly* assembly)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int RuntimeAssembly::IsGlobalAssemblyCache(RuntimeAssembly* assembly)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int RuntimeAssembly::FCallIsDynamic(RuntimeAssembly* assembly)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int RuntimeAssembly::IsNewPortableAssembly(AssemblyName* assemblyName)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		RuntimeAssembly* RuntimeAssembly::nLoadFile(System::String* path, System::Security::Policy::Evidence* evidence)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		RuntimeAssembly* RuntimeAssembly::nLoadImage(System::NormalArray<uint8_t> rawAssembly, System::NormalArray<uint8_t> rawSymbolStore, System::Security::Policy::Evidence* evidence, System::Threading::StackCrawlMark& stackMark, int fIntrospection, System::Security::SecurityContextSource securityContextSource)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
