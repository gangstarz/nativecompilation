#ifndef __SYSTEM_REFLECTION_EMIT_DYNAMICMETHOD
#define __SYSTEM_REFLECTION_EMIT_DYNAMICMETHOD

#include "../gc.h"
#include <cstdint>
#include "System.Reflection.MethodInfo.h"

namespace System
{
	namespace Reflection
	{
		namespace Emit
		{
			class DynamicMethod : public System::Reflection::MethodInfo
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
