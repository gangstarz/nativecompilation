#ifndef __SYSTEM_PARSENUMBERS
#define __SYSTEM_PARSENUMBERS

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;

	class ParseNumbers : public System::Object
	{
	public:
		int64_t StringToLong(System::String* s, int32_t radix, int32_t flags, int32_t* currPos);
		int32_t StringToInt(System::String* s, int32_t radix, int32_t flags, int32_t* currPos);
		System::String* IntToString(int32_t l, int32_t radix, int32_t width, wchar_t paddingChar, int32_t flags);
		System::String* LongToString(int64_t l, int32_t radix, int32_t width, wchar_t paddingChar, int32_t flags);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
