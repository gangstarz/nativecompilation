#ifndef __SYSTEM_TEXT_NORMALIZATIONFORM
#define __SYSTEM_TEXT_NORMALIZATIONFORM

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Text
	{
		class NormalizationForm
		{
		public:
			static const int32_t FormC = 1;
			static const int32_t FormD = 2;
			static const int32_t FormKC = 5;
			static const int32_t FormKD = 6;

		};
	}
}

#endif
