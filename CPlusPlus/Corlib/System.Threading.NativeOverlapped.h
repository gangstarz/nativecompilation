#ifndef __SYSTEM_THREADING_NATIVEOVERLAPPED
#define __SYSTEM_THREADING_NATIVEOVERLAPPED

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Threading
	{
		class NativeOverlapped : public System::ValueType
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
