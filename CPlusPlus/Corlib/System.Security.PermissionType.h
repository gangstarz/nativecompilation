#ifndef __SYSTEM_SECURITY_PERMISSIONTYPE
#define __SYSTEM_SECURITY_PERMISSIONTYPE

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Security
	{
		class PermissionType
		{
		public:
			static const int32_t SecurityUnmngdCodeAccess = 0;
			static const int32_t SecuritySkipVerification = 1;
			static const int32_t ReflectionTypeInfo = 2;
			static const int32_t SecurityAssert = 3;
			static const int32_t ReflectionMemberAccess = 4;
			static const int32_t SecuritySerialization = 5;
			static const int32_t ReflectionRestrictedMemberAccess = 6;
			static const int32_t FullTrust = 7;
			static const int32_t SecurityBindingRedirects = 8;
			static const int32_t UIPermission = 9;
			static const int32_t EnvironmentPermission = 10;
			static const int32_t FileDialogPermission = 11;
			static const int32_t FileIOPermission = 12;
			static const int32_t ReflectionPermission = 13;
			static const int32_t SecurityPermission = 14;
			static const int32_t SecurityControlEvidence = 16;
			static const int32_t SecurityControlPrincipal = 17;

		};
	}
}

#endif
