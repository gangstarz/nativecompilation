#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_WINDOWSRUNTIME_RUNTIMECLASS
#define __SYSTEM_RUNTIME_INTEROPSERVICES_WINDOWSRUNTIME_RUNTIMECLASS

#include "../gc.h"
#include <cstdint>
#include "System.__ComObject.h"

namespace System
{
	class String;
	class ValueType;

	namespace Runtime
	{
		namespace InteropServices
		{
			namespace WindowsRuntime
			{
				class RuntimeClass : public System::__ComObject
				{
				public:
					int* GetRedirectedGetHashCodeMD();
					int32_t RedirectGetHashCode(int* pMD);
					int* GetRedirectedToStringMD();
					System::String* RedirectToString(int* pMD);
					int* GetRedirectedEqualsMD();
					int RedirectEquals(System::Object* obj, int* pMD);

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
