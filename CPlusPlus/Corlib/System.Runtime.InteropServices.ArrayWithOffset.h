#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_ARRAYWITHOFFSET
#define __SYSTEM_RUNTIME_INTEROPSERVICES_ARRAYWITHOFFSET

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			class ArrayWithOffset : public System::ValueType
			{
			public:
				int32_t CalculateCount();

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
