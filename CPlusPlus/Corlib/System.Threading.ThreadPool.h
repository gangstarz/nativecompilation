#ifndef __SYSTEM_THREADING_THREADPOOL
#define __SYSTEM_THREADING_THREADPOOL

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Enum;
	class MarshalByRefObject;
	class ValueType;
	namespace Threading
	{
		class NativeOverlapped;
		class RegisteredWaitHandle;
		class StackCrawlMark;
		class WaitHandle;

		class ThreadPool : public System::Object
		{
		public:
			int PostQueuedCompletionStatus(NativeOverlapped* overlapped);
			int SetMinThreadsNative(int32_t workerThreads, int32_t completionPortThreads);
			int SetMaxThreadsNative(int32_t workerThreads, int32_t completionPortThreads);
			void GetMinThreadsNative(int32_t* workerThreads, int32_t* completionPortThreads);
			void GetMaxThreadsNative(int32_t* workerThreads, int32_t* completionPortThreads);
			void GetAvailableThreadsNative(int32_t* workerThreads, int32_t* completionPortThreads);
			int NotifyWorkItemComplete();
			void ReportThreadStatus(int isWorking);
			void NotifyWorkItemProgressNative();
			int IsThreadPoolHosted();
			int BindIOCompletionCallbackNative(int* fileHandle);
			int* RegisterWaitForSingleObjectNative(WaitHandle* waitHandle, System::Object* state, uint32_t timeOutInterval, int executeOnlyOnce, RegisteredWaitHandle* registeredWaitHandle, StackCrawlMark& stackMark, int compressStack);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
