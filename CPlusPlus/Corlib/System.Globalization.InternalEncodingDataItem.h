#ifndef __SYSTEM_GLOBALIZATION_INTERNALENCODINGDATAITEM
#define __SYSTEM_GLOBALIZATION_INTERNALENCODINGDATAITEM

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Globalization
	{
		class InternalEncodingDataItem : public System::ValueType
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
