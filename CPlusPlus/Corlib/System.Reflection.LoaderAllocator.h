#ifndef __SYSTEM_REFLECTION_LOADERALLOCATOR
#define __SYSTEM_REFLECTION_LOADERALLOCATOR

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	namespace Reflection
	{
		class LoaderAllocator : public System::Object
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
