#include "System.Runtime.Remoting.RemotingServices.h"
#include "System.NotImplementedException.h"
#include "System.ContextBoundObject.h"
#include "System.Guid.h"
#include "System.MarshalByRefObject.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.TypeInfo.h"
#include "System.Runtime.Remoting.Proxies.RealProxy.h"
#include "System.RuntimeType.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace Remoting
		{
			int RemotingServices::IsTransparentProxy(System::Object* proxy)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Runtime::Remoting::Proxies::RealProxy* RemotingServices::GetRealProxy(System::Object* proxy)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* RemotingServices::CreateTransparentProxy(System::Runtime::Remoting::Proxies::RealProxy* rp, System::RuntimeType* typeToProxy, int* stub, System::Object* stubData)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::MarshalByRefObject* RemotingServices::AllocateUninitializedObject(System::RuntimeType* objectType)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int RemotingServices::CORProfilerTrackRemoting()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RemotingServices::CallDefaultCtor(System::Object* o)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::MarshalByRefObject* RemotingServices::AllocateInitializedObject(System::RuntimeType* objectType)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* RemotingServices::Unwrap(System::ContextBoundObject* obj)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* RemotingServices::AlwaysUnwrap(System::ContextBoundObject* obj)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* RemotingServices::CheckCast(System::Object* objToExpand, System::RuntimeType* type)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int RemotingServices::CORProfilerTrackRemotingCookie()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int RemotingServices::CORProfilerTrackRemotingAsync()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RemotingServices::CORProfilerRemotingClientSendingMessage(System::Guid* id, int fIsAsync)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RemotingServices::CORProfilerRemotingClientReceivingReply(System::Guid id, int fIsAsync)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RemotingServices::CORProfilerRemotingServerReceivingMessage(System::Guid id, int fIsAsync)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RemotingServices::CORProfilerRemotingServerSendingReply(System::Guid* id, int fIsAsync)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RemotingServices::ResetInterfaceCache(System::Object* proxy)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void RemotingServices::nSetRemoteActivationConfigured()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
