#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_REGISTRATIONCONNECTIONTYPE
#define __SYSTEM_RUNTIME_INTEROPSERVICES_REGISTRATIONCONNECTIONTYPE

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			class RegistrationConnectionType
			{
			public:
				static const int32_t SingleUse = 0;
				static const int32_t MultipleUse = 1;
				static const int32_t MultiSeparate = 2;
				static const int32_t Suspended = 4;
				static const int32_t Surrogate = 8;

			};
		}
	}
}

#endif
