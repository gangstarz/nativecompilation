#include "System.ParseNumbers.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	int64_t ParseNumbers::StringToLong(System::String* s, int32_t radix, int32_t flags, int32_t* currPos)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t ParseNumbers::StringToInt(System::String* s, int32_t radix, int32_t flags, int32_t* currPos)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* ParseNumbers::IntToString(int32_t l, int32_t radix, int32_t width, wchar_t paddingChar, int32_t flags)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* ParseNumbers::LongToString(int64_t l, int32_t radix, int32_t width, wchar_t paddingChar, int32_t flags)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
