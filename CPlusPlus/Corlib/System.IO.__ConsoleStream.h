#ifndef __SYSTEM_IO___CONSOLESTREAM
#define __SYSTEM_IO___CONSOLESTREAM

#include "../gc.h"
#include <cstdint>
#include "System.IO.Stream.h"

namespace Microsoft
{
	namespace Win32
	{
		namespace SafeHandles
		{
			class SafeFileHandle;
			class SafeHandleZeroOrMinusOneIsInvalid;
		}
	}
}
namespace System
{
	class ValueType;
	namespace Runtime
	{
		namespace ConstrainedExecution
		{
			class CriticalFinalizerObject;
		}
		namespace InteropServices
		{
			class SafeHandle;
		}
	}

	namespace IO
	{
		class __ConsoleStream : public Stream
		{
		public:
			void WaitForAvailableConsoleInput(Microsoft::Win32::SafeHandles::SafeFileHandle* file, int isPipe);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
