#include "System.StubHelpers.WinRTTypeNameConverter.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.String.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		System::String* WinRTTypeNameConverter::ConvertToWinRTTypeName(System::Type* managedType, int* isPrimitive)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Type* WinRTTypeNameConverter::GetTypeFromWinRTTypeName(System::String* typeName, int* isPrimitive)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
