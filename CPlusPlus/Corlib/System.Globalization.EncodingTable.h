#ifndef __SYSTEM_GLOBALIZATION_ENCODINGTABLE
#define __SYSTEM_GLOBALIZATION_ENCODINGTABLE

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class String;
	class ValueType;
	namespace Globalization
	{
		class InternalCodePageDataItem;
		class InternalEncodingDataItem;

		class EncodingTable : public System::Object
		{
		public:
			uint8_t* nativeCreateOpenFileMapping(System::String* inSectionName, int32_t inBytesToAllocate, int** mappedFileHandle);
			InternalEncodingDataItem* GetEncodingData();
			int32_t GetNumEncodingItems();
			InternalCodePageDataItem* GetCodePageData();

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
