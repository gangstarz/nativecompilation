#ifndef __SYSTEM_SECURITY_PRINCIPAL_WINSECURITYCONTEXT
#define __SYSTEM_SECURITY_PRINCIPAL_WINSECURITYCONTEXT

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Security
	{
		namespace Principal
		{
			class WinSecurityContext
			{
			public:
				static const int32_t Thread = 1;
				static const int32_t Process = 2;
				static const int32_t Both = 3;

			};
		}
	}
}

#endif
