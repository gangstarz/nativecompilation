#include "System.Threading.DomainCompressedStack.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Reflection.Assembly.h"
#include "System.Security.FrameSecurityDescriptor.h"
#include "System.Security.PermissionSet.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		int32_t DomainCompressedStack::GetDescCount(int* dcs)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void DomainCompressedStack::GetDomainPermissionSets(int* dcs, System::Security::PermissionSet** granted, System::Security::PermissionSet** refused)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int DomainCompressedStack::GetDescriptorInfo(int* dcs, int32_t index, System::Security::PermissionSet** granted, System::Security::PermissionSet** refused, System::Reflection::Assembly** assembly, System::Security::FrameSecurityDescriptor** fsd)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int DomainCompressedStack::IgnoreDomain(int* dcs)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
