#include "System.Globalization.TextInfo.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Globalization
	{
		wchar_t TextInfo::InternalChangeCaseChar(int* handle, int* handleOrigin, System::String* localeName, wchar_t ch, int isToUpper)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::String* TextInfo::InternalChangeCaseString(int* handle, int* handleOrigin, System::String* localeName, System::String* str, int isToUpper)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t TextInfo::InternalGetCaseInsHash(int* handle, int* handleOrigin, System::String* localeName, System::String* str, int forceRandomizedHashing, int64_t additionalEntropy)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
