#ifndef __SYSTEM_SECURITY_ACCESSCONTROL_SECURITYINFOS
#define __SYSTEM_SECURITY_ACCESSCONTROL_SECURITYINFOS

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Security
	{
		namespace AccessControl
		{
			class SecurityInfos
			{
			public:
				static const int32_t Owner = 1;
				static const int32_t Group = 2;
				static const int32_t DiscretionaryAcl = 4;
				static const int32_t SystemAcl = 8;

			};
		}
	}
}

#endif
