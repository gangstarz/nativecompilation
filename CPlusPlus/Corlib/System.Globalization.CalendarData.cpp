#include "System.Globalization.CalendarData.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Globalization
	{
		int32_t CalendarData::nativeGetCalendars(System::String* localeName, int useUserOverride, System::NormalArray<int32_t>* calendars)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int CalendarData::nativeGetCalendarData(CalendarData* data, System::String* localeName, int32_t calendar)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t CalendarData::nativeGetTwoDigitYearMax(int32_t calID)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
