#ifndef __SYSTEM_TEXT_STRINGBUILDER
#define __SYSTEM_TEXT_STRINGBUILDER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace Text
	{
		class StringBuilder : public System::Object
		{
		public:
			void ReplaceBufferInternal(wchar_t* newBuffer, int32_t newLength);
			void ReplaceBufferAnsiInternal(int8_t* newBuffer, int32_t newLength);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
