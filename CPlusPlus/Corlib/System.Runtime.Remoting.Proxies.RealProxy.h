#ifndef __SYSTEM_RUNTIME_REMOTING_PROXIES_REALPROXY
#define __SYSTEM_RUNTIME_REMOTING_PROXIES_REALPROXY

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
	}

	namespace Runtime
	{
		namespace Remoting
		{
			namespace Proxies
			{
				class RealProxy : public System::Object
				{
				public:
					int* GetDefaultStub();
					void SetStubData(RealProxy* rp, System::Object* stubData);
					System::Object* GetStubData(RealProxy* rp);
					System::Type* GetProxiedType();
					int* GetStub();

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
