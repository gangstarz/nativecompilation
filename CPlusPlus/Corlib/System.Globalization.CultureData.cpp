#include "System.Globalization.CultureData.h"
#include "System.NotImplementedException.h"
#include "System.Globalization.NumberFormatInfo.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Globalization
	{
		System::String* CultureData::LCIDToLocaleName(int32_t lcid)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t CultureData::LocaleNameToLCID(System::String* localeName)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int CultureData::nativeInitCultureData(CultureData* cultureData)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int CultureData::nativeGetNumberFormatInfoValues(System::String* localeName, NumberFormatInfo* nfi, int useUserOverride)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::NormalArray<System::String*>* CultureData::nativeEnumTimeFormats(System::String* localeName, uint32_t dwFlags, int useUserOverride)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
