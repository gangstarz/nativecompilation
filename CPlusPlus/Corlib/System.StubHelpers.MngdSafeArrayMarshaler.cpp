#include "System.StubHelpers.MngdSafeArrayMarshaler.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		void MngdSafeArrayMarshaler::CreateMarshaler(int* pMarshalState, int* pMT, int32_t iRank, int32_t dwFlags)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdSafeArrayMarshaler::ConvertSpaceToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdSafeArrayMarshaler::ConvertContentsToNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome, System::Object* pOriginalManaged)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdSafeArrayMarshaler::ConvertSpaceToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdSafeArrayMarshaler::ConvertContentsToManaged(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void MngdSafeArrayMarshaler::ClearNative(int* pMarshalState, System::Object*& pManagedHome, int* pNativeHome)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
