#include "System.RuntimeTypeHandle.h"
#include "System.NotImplementedException.h"
#include "System.Enum.h"
#include "System.IRuntimeMethodInfo.h"
#include "System.Object.h"
#include "System.Reflection.Assembly.h"
#include "System.Reflection.CorElementType.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.Module.h"
#include "System.Reflection.RuntimeAssembly.h"
#include "System.Reflection.RuntimeModule.h"
#include "System.Reflection.TypeAttributes.h"
#include "System.Reflection.TypeInfo.h"
#include "System.RuntimeMethodHandleInternal.h"
#include "System.RuntimeType.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	int RuntimeTypeHandle::IsInstanceOfType(RuntimeType* type, System::Object* o)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int* RuntimeTypeHandle::GetValueInternal(RuntimeTypeHandle handle)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* RuntimeTypeHandle::CreateInstance(RuntimeType* type, int publicOnly, int noCheck, int& canBeCached, RuntimeMethodHandleInternal& ctor, int& bNeedSecurityCheck)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* RuntimeTypeHandle::CreateCaInstance(RuntimeType* type, IRuntimeMethodInfo* ctor)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* RuntimeTypeHandle::Allocate(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* RuntimeTypeHandle::CreateInstanceForAnotherGenericParameter(RuntimeType* type, RuntimeType* genericParameter)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Reflection::CorElementType RuntimeTypeHandle::GetCorElementType(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Reflection::RuntimeAssembly* RuntimeTypeHandle::GetAssembly(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Reflection::RuntimeModule* RuntimeTypeHandle::GetModule(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	RuntimeType* RuntimeTypeHandle::GetBaseType(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Reflection::TypeAttributes RuntimeTypeHandle::GetAttributes(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	RuntimeType* RuntimeTypeHandle::GetElementType(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t RuntimeTypeHandle::GetArrayRank(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t RuntimeTypeHandle::GetToken(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	RuntimeMethodHandleInternal RuntimeTypeHandle::GetMethodAt(RuntimeType* type, int32_t slot)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::GetFields(RuntimeType* type, int** result, int32_t* count)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::NormalArray<Type*>* RuntimeTypeHandle::GetInterfaces(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t RuntimeTypeHandle::GetNumVirtuals(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::IsComObject(RuntimeType* type, int isGenericCOM)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::IsContextful(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::IsInterface(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::HasProxyAttribute(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::IsValueType(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::CanCastTo(RuntimeType* type, RuntimeType* target)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	RuntimeType* RuntimeTypeHandle::GetDeclaringType(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	IRuntimeMethodInfo* RuntimeTypeHandle::GetDeclaringMethod(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::HasInstantiation(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::IsGenericTypeDefinition(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::IsGenericVariable(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::ContainsGenericVariables(RuntimeType* handle)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::IsEquivalentTo(RuntimeType* rtType1, RuntimeType* rtType2)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::IsEquivalentType(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	RuntimeMethodHandleInternal RuntimeTypeHandle::GetFirstIntroducedMethod(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void RuntimeTypeHandle::GetNextIntroducedMethod(RuntimeMethodHandleInternal& method)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void* RuntimeTypeHandle::_GetUtf8Name(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int32_t RuntimeTypeHandle::GetGenericVariableIndex(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int* RuntimeTypeHandle::_GetMetadataImport(RuntimeType* type)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::CompareCanonicalHandles(RuntimeType* left, RuntimeType* right)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int RuntimeTypeHandle::SatisfiesConstraints(RuntimeType* paramType, int** pTypeContext, int32_t typeContextLength, int** pMethodContext, int32_t methodContextLength, RuntimeType* toType)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
