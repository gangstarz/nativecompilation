#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_ITYPELIBIMPORTERNOTIFYSINK
#define __SYSTEM_RUNTIME_INTEROPSERVICES_ITYPELIBIMPORTERNOTIFYSINK

#include "../gc.h"
#include <cstdint>

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			class ITypeLibImporterNotifySink
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
