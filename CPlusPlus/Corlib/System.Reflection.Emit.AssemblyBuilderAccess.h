#ifndef __SYSTEM_REFLECTION_EMIT_ASSEMBLYBUILDERACCESS
#define __SYSTEM_REFLECTION_EMIT_ASSEMBLYBUILDERACCESS

#include "../gc.h"
#include <cstdint>
#include "System.Enum.h"

namespace System
{
	namespace Reflection
	{
		namespace Emit
		{
			class AssemblyBuilderAccess
			{
			public:
				static const int32_t Run = 1;
				static const int32_t Save = 2;
				static const int32_t RunAndSave = 3;
				static const int32_t ReflectionOnly = 6;
				static const int32_t RunAndCollect = 9;

			};
		}
	}
}

#endif
