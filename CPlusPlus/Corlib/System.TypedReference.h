#ifndef __SYSTEM_TYPEDREFERENCE
#define __SYSTEM_TYPEDREFERENCE

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class RuntimeType;
	class Type;
	namespace Reflection
	{
		class MemberInfo;
		class TypeInfo;
	}

	class TypedReference : public ValueType
	{
	public:
		System::Object* InternalToObject(void* value);
		void InternalSetTypedReference(void* target, System::Object* value);
		void InternalMakeTypedReference(void* result, System::Object* target, System::NormalArray<int*> flds, RuntimeType* lastFieldType);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
