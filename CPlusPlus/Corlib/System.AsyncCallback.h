#ifndef __SYSTEM_ASYNCCALLBACK
#define __SYSTEM_ASYNCCALLBACK

#include "../gc.h"
#include <cstdint>
#include "System.MulticastDelegate.h"

namespace System
{
	class AsyncCallback : public MulticastDelegate
	{
	public:

		// [place holder for auto-generated recompiled members]
	};
}

#endif
