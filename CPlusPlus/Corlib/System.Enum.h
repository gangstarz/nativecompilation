#ifndef __SYSTEM_ENUM
#define __SYSTEM_ENUM

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class RuntimeType;
	class Type;
	namespace Reflection
	{
		class MemberInfo;
		class TypeInfo;
	}

	class Enum : public ValueType
	{
	public:
		RuntimeType* InternalGetUnderlyingType(RuntimeType* enumType);
		System::Object* InternalGetValue();
		int Equals(System::Object* obj);
		int32_t InternalCompareTo(System::Object* o1, System::Object* o2);
		System::Object* InternalBoxEnum(RuntimeType* enumType, int64_t value);
		int InternalHasFlag(Enum* flags);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
