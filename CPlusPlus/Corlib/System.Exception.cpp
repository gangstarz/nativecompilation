#include "System.Exception.h"
#include "System.NotImplementedException.h"
#include "System.IRuntimeMethodInfo.h"
#include "System.Object.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	void Exception::GetStackTracesDeepCopy(Exception* exception, System::Object** currentStackTrace, System::Object** dynamicMethodArray)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Exception::SaveStackTracesFromDeepCopy(Exception* exception, System::Object* currentStackTrace, System::Object* dynamicMethodArray)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Exception::IsImmutableAgileException(Exception* e)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	IRuntimeMethodInfo* Exception::GetMethodFromStackTrace(System::Object* stackTrace)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void Exception::PrepareForForeignExceptionRaise()
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* Exception::CopyStackTrace(System::Object* currentStackTrace)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::Object* Exception::CopyDynamicMethods(System::Object* currentDynamicMethods)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	System::String* Exception::StripFileInfo(System::String* stackTrace, int isRemoteStackTrace)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	int Exception::nIsTransient(int32_t hr)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
