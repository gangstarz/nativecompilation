#ifndef __SYSTEM_STUBHELPERS_DATEMARSHALER
#define __SYSTEM_STUBHELPERS_DATEMARSHALER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class DateTime;
	class ValueType;

	namespace StubHelpers
	{
		class DateMarshaler : public System::Object
		{
		public:
			double ConvertToNative(System::DateTime managedDate);
			int64_t ConvertToManaged(double nativeDate);

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
