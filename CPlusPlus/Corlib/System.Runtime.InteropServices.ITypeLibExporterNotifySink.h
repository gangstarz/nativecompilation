#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_ITYPELIBEXPORTERNOTIFYSINK
#define __SYSTEM_RUNTIME_INTEROPSERVICES_ITYPELIBEXPORTERNOTIFYSINK

#include "../gc.h"
#include <cstdint>

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			class ITypeLibExporterNotifySink
			{
			public:

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
