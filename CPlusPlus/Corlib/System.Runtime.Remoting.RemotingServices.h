#ifndef __SYSTEM_RUNTIME_REMOTING_REMOTINGSERVICES
#define __SYSTEM_RUNTIME_REMOTING_REMOTINGSERVICES

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ContextBoundObject;
	class Guid;
	class MarshalByRefObject;
	class RuntimeType;
	class Type;
	class ValueType;
	namespace Reflection
	{
		class MemberInfo;
		class TypeInfo;
	}
	namespace Runtime
	{
		namespace Remoting
		{
			namespace Proxies
			{
				class RealProxy;
			}

			class RemotingServices : public System::Object
			{
			public:
				int IsTransparentProxy(System::Object* proxy);
				System::Runtime::Remoting::Proxies::RealProxy* GetRealProxy(System::Object* proxy);
				System::Object* CreateTransparentProxy(System::Runtime::Remoting::Proxies::RealProxy* rp, System::RuntimeType* typeToProxy, int* stub, System::Object* stubData);
				System::MarshalByRefObject* AllocateUninitializedObject(System::RuntimeType* objectType);
				int CORProfilerTrackRemoting();
				void CallDefaultCtor(System::Object* o);
				System::MarshalByRefObject* AllocateInitializedObject(System::RuntimeType* objectType);
				System::Object* Unwrap(System::ContextBoundObject* obj);
				System::Object* AlwaysUnwrap(System::ContextBoundObject* obj);
				System::Object* CheckCast(System::Object* objToExpand, System::RuntimeType* type);
				int CORProfilerTrackRemotingCookie();
				int CORProfilerTrackRemotingAsync();
				void CORProfilerRemotingClientSendingMessage(System::Guid* id, int fIsAsync);
				void CORProfilerRemotingClientReceivingReply(System::Guid id, int fIsAsync);
				void CORProfilerRemotingServerReceivingMessage(System::Guid id, int fIsAsync);
				void CORProfilerRemotingServerSendingReply(System::Guid* id, int fIsAsync);
				void ResetInterfaceCache(System::Object* proxy);
				void nSetRemoteActivationConfigured();

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
