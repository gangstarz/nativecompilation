#include "System.Runtime.Remoting.Proxies.RealProxy.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace Remoting
		{
			namespace Proxies
			{
				int* RealProxy::GetDefaultStub()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				void RealProxy::SetStubData(RealProxy* rp, System::Object* stubData)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::Object* RealProxy::GetStubData(RealProxy* rp)
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				System::Type* RealProxy::GetProxiedType()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

				int* RealProxy::GetStub()
				{
					// Implement this...
					throw new System::NotImplementedException();
				}

			}
		}
	}
}
