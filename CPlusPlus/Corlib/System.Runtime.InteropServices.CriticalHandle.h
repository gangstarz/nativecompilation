#ifndef __SYSTEM_RUNTIME_INTEROPSERVICES_CRITICALHANDLE
#define __SYSTEM_RUNTIME_INTEROPSERVICES_CRITICALHANDLE

#include "../gc.h"
#include <cstdint>
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"

namespace System
{
	class ValueType;

	namespace Runtime
	{
		namespace InteropServices
		{
			class CriticalHandle : public System::Runtime::ConstrainedExecution::CriticalFinalizerObject
			{
			public:
				void FireCustomerDebugProbe();

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
