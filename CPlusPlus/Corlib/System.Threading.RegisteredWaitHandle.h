#ifndef __SYSTEM_THREADING_REGISTEREDWAITHANDLE
#define __SYSTEM_THREADING_REGISTEREDWAITHANDLE

#include "../gc.h"
#include <cstdint>
#include "System.MarshalByRefObject.h"

namespace System
{
	namespace Threading
	{
		class RegisteredWaitHandle : public System::MarshalByRefObject
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
