#include "System.Threading.WaitHandle.h"
#include "System.NotImplementedException.h"
#include "Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid.h"
#include "Microsoft.Win32.SafeHandles.SafeWaitHandle.h"
#include "System.MarshalByRefObject.h"
#include "System.Object.h"
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"
#include "System.Runtime.InteropServices.SafeHandle.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		int32_t WaitHandle::WaitMultiple(System::NormalArray<WaitHandle*>* waitHandles, int32_t millisecondsTimeout, int exitContext, int WaitAll)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t WaitHandle::WaitOneNative(System::Runtime::InteropServices::SafeHandle* waitableSafeHandle, uint32_t millisecondsTimeout, int hasThreadAffinity, int exitContext)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t WaitHandle::SignalAndWaitOne(Microsoft::Win32::SafeHandles::SafeWaitHandle* waitHandleToSignal, Microsoft::Win32::SafeHandles::SafeWaitHandle* waitHandleToWaitOn, int32_t millisecondsTimeout, int hasThreadAffinity, int exitContext)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
