#ifndef __SYSTEM_SECURITY_CRYPTOGRAPHY_X509CERTIFICATES_SAFECERTCONTEXTHANDLE
#define __SYSTEM_SECURITY_CRYPTOGRAPHY_X509CERTIFICATES_SAFECERTCONTEXTHANDLE

#include "../gc.h"
#include <cstdint>
#include "Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid.h"

namespace System
{
	class ValueType;

	namespace Security
	{
		namespace Cryptography
		{
			namespace X509Certificates
			{
				class SafeCertContextHandle : public Microsoft::Win32::SafeHandles::SafeHandleZeroOrMinusOneIsInvalid
				{
				public:
					void _FreePCertContext(int* pCert);

					// [place holder for auto-generated recompiled members]
				};
			}
		}
	}
}

#endif
