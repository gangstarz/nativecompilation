#ifndef __MICROSOFT_WIN32_OSVERSIONINFOEX
#define __MICROSOFT_WIN32_OSVERSIONINFOEX

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace Microsoft
{
	namespace Win32
	{
		class OSVERSIONINFOEX : public System::Object
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
