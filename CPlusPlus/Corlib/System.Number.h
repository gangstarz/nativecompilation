#ifndef __SYSTEM_NUMBER
#define __SYSTEM_NUMBER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class Decimal;
	class String;
	class ValueType;
	namespace Globalization
	{
		class NumberFormatInfo;
	}

	class Number : public System::Object
	{
	public:
		System::String* FormatDecimal(System::Decimal value, System::String* format, System::Globalization::NumberFormatInfo* info);
		System::String* FormatDouble(double value, System::String* format, System::Globalization::NumberFormatInfo* info);
		System::String* FormatInt32(int32_t value, System::String* format, System::Globalization::NumberFormatInfo* info);
		System::String* FormatUInt32(uint32_t value, System::String* format, System::Globalization::NumberFormatInfo* info);
		System::String* FormatInt64(int64_t value, System::String* format, System::Globalization::NumberFormatInfo* info);
		System::String* FormatSingle(float value, System::String* format, System::Globalization::NumberFormatInfo* info);
		int NumberBufferToDecimal(uint8_t* number, System::Decimal& value);
		int NumberBufferToDouble(uint8_t* number, double& value);
		System::String* FormatUInt64(uint64_t value, System::String* format, System::Globalization::NumberFormatInfo* info);
		System::String* FormatNumberBuffer(uint8_t* number, System::String* format, System::Globalization::NumberFormatInfo* info, wchar_t* allDigits);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
