#include "System.Runtime.InteropServices.GCHandle.h"
#include "System.NotImplementedException.h"
#include "System.Enum.h"
#include "System.Object.h"
#include "System.Runtime.InteropServices.GCHandleType.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			int* GCHandle::InternalAlloc(System::Object* value, GCHandleType type)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void GCHandle::InternalFree(int* handle)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* GCHandle::InternalGet(int* handle)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void GCHandle::InternalSet(int* handle, System::Object* value, int isPinned)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* GCHandle::InternalCompareExchange(int* handle, System::Object* value, System::Object* oldValue, int isPinned)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			int* GCHandle::InternalAddrOfPinnedObject(int* handle)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			void GCHandle::InternalCheckDomain(int* handle)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			GCHandleType GCHandle::InternalGetHandleType(int* handle)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
