#ifndef __SYSTEM_RUNTIME_VERSIONING_VERSIONINGHELPER
#define __SYSTEM_RUNTIME_VERSIONING_VERSIONINGHELPER

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	namespace Runtime
	{
		namespace Versioning
		{
			class VersioningHelper : public System::Object
			{
			public:
				int32_t GetRuntimeId();

				// [place holder for auto-generated recompiled members]
			};
		}
	}
}

#endif
