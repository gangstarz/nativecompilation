#ifndef __SYSTEM_REFLECTION_METHODBODY
#define __SYSTEM_REFLECTION_METHODBODY

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	namespace Reflection
	{
		class MethodBody : public System::Object
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
