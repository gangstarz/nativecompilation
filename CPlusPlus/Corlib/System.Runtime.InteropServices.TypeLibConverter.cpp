#include "System.Runtime.InteropServices.TypeLibConverter.h"
#include "System.NotImplementedException.h"
#include "System.Collections.ArrayList.h"
#include "System.Enum.h"
#include "System.Object.h"
#include "System.Reflection.Assembly.h"
#include "System.Reflection.Module.h"
#include "System.Reflection.RuntimeAssembly.h"
#include "System.Reflection.RuntimeModule.h"
#include "System.Runtime.InteropServices.ITypeLibExporterNotifySink.h"
#include "System.Runtime.InteropServices.ITypeLibImporterNotifySink.h"
#include "System.Runtime.InteropServices.TypeLibExporterFlags.h"
#include "System.Runtime.InteropServices.TypeLibImporterFlags.h"
#include "System.String.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			void TypeLibConverter::nConvertTypeLibToMetadata(System::Object* typeLib, System::Reflection::RuntimeAssembly* asmBldr, System::Reflection::RuntimeModule* modBldr, System::String* nameSpace, TypeLibImporterFlags flags, ITypeLibImporterNotifySink* notifySink, System::Collections::ArrayList** eventItfInfoList)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

			System::Object* TypeLibConverter::nConvertAssemblyToTypeLib(System::Reflection::RuntimeAssembly* assembly, System::String* strTypeLibName, TypeLibExporterFlags flags, ITypeLibExporterNotifySink* notifySink)
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
