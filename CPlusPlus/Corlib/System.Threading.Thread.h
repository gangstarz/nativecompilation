#ifndef __SYSTEM_THREADING_THREAD
#define __SYSTEM_THREADING_THREAD

#include "../gc.h"
#include "../Array.h"
#include <cstdint>
#include "System.Runtime.ConstrainedExecution.CriticalFinalizerObject.h"

namespace System
{
	class AppDomain;
	class Delegate;
	class Enum;
	class MarshalByRefObject;
	class MulticastDelegate;
	class String;
	class ValueType;
	namespace Globalization
	{
		class CultureInfo;
	}
	namespace Runtime
	{
		namespace InteropServices
		{
			class SafeHandle;
		}
		namespace Remoting
		{
			namespace Contexts
			{
				class Context;
			}
		}
	}
	namespace Security
	{
		namespace Principal
		{
			class IPrincipal;
		}
	}
	namespace Threading
	{
		class InternalCrossContextDelegate;
		class SafeCompressedStackHandle;
		class StackCrawlMark;

		class Thread : public System::Runtime::ConstrainedExecution::CriticalFinalizerObject
		{
		public:
			int32_t get_ManagedThreadId();
			int* SetAppDomainStack(SafeCompressedStackHandle* csHandle);
			void RestoreAppDomainStack(int* appDomainStack);
			int* InternalGetCurrentThread();
			void AbortInternal();
			void SuspendInternal();
			void ResumeInternal();
			void InterruptInternal();
			int32_t GetPriorityNative();
			void SetPriorityNative(int32_t priority);
			int get_IsAlive();
			int get_IsThreadPoolThread();
			int JoinInternal(int32_t millisecondsTimeout);
			void SleepInternal(int32_t millisecondsTimeout);
			void SpinWaitInternal(int32_t iterations);
			int YieldInternal();
			Thread* GetCurrentThreadNative();
			void InternalFinalize();
			void DisableComObjectEagerCleanup();
			int IsBackgroundNative();
			void SetBackgroundNative(int isBackground);
			int32_t GetThreadStateNative();
			int32_t GetApartmentStateNative();
			System::Runtime::Remoting::Contexts::Context* GetContextInternal(int* id);
			System::Object* InternalCrossContextCallback(System::Runtime::Remoting::Contexts::Context* ctx, int* ctxID, int32_t appDomainID, InternalCrossContextDelegate* ftnToCall, System::NormalArray<System::Object*>* args);
			void BeginCriticalRegion();
			void EndCriticalRegion();
			void BeginThreadAffinity();
			void EndThreadAffinity();
			void MemoryBarrier();
			void SetAbortReason(System::Object* o);
			System::Object* GetAbortReason();
			void ClearAbortReason();
			void StartInternal(System::Security::Principal::IPrincipal* principal, StackCrawlMark& stackMark);
			void ResetAbortNative();
			void SetStart(System::Delegate* start, int32_t maxStackSize);
			int32_t SetApartmentStateNative(int32_t state, int fireMDAOnMismatch);
			void StartupSetApartmentStateInternal();
			int nativeGetSafeCulture(Thread* t, int32_t appDomainId, int isUI, System::Globalization::CultureInfo*& safeCulture);
			int nativeSetThreadUILocale(System::String* locale);
			System::AppDomain* GetDomainInternal();
			System::AppDomain* GetFastDomainInternal();

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
