#include "System.StubHelpers.ValueClassMarshaler.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.StubHelpers.CleanupWorkList.h"
#include "System.ValueType.h"


namespace System
{
	namespace StubHelpers
	{
		void ValueClassMarshaler::ConvertToNative(int* dst, int* src, int* pMT, CleanupWorkList*& pCleanupWorkList)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ValueClassMarshaler::ConvertToManaged(int* dst, int* src, int* pMT)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void ValueClassMarshaler::ClearNative(int* dst, int* pMT)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
