#include "System.Threading.Interlocked.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.TypedReference.h"
#include "System.ValueType.h"


namespace System
{
	namespace Threading
	{
		int32_t Interlocked::Exchange(int32_t& location1, int32_t value)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int64_t Interlocked::Exchange(int64_t& location1, int64_t value)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		float Interlocked::Exchange(float& location1, float value)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		double Interlocked::Exchange(double& location1, double value)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Object* Interlocked::Exchange(System::Object*& location1, System::Object* value)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* Interlocked::Exchange(int*& location1, int* value)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Interlocked::_Exchange(System::TypedReference location1, System::TypedReference value)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t Interlocked::CompareExchange(int32_t& location1, int32_t value, int32_t comparand)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int64_t Interlocked::CompareExchange(int64_t& location1, int64_t value, int64_t comparand)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		float Interlocked::CompareExchange(float& location1, float value, float comparand)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		double Interlocked::CompareExchange(double& location1, double value, double comparand)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		System::Object* Interlocked::CompareExchange(System::Object*& location1, System::Object* value, System::Object* comparand)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int* Interlocked::CompareExchange(int*& location1, int* value, int* comparand)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		void Interlocked::_CompareExchange(System::TypedReference location1, System::TypedReference value, System::Object* comparand)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t Interlocked::CompareExchange(int32_t& location1, int32_t value, int32_t comparand, int& succeeded)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int32_t Interlocked::ExchangeAdd(int32_t& location1, int32_t value)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

		int64_t Interlocked::ExchangeAdd(int64_t& location1, int64_t value)
		{
			// Implement this...
			throw new System::NotImplementedException();
		}

	}
}
