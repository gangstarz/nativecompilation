#ifndef __SYSTEM_THREADING_LOCKCOOKIE
#define __SYSTEM_THREADING_LOCKCOOKIE

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	namespace Threading
	{
		class LockCookie : public System::ValueType
		{
		public:

			// [place holder for auto-generated recompiled members]
		};
	}
}

#endif
