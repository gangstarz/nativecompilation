#include "System.TypedReference.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.Reflection.MemberInfo.h"
#include "System.Reflection.TypeInfo.h"
#include "System.RuntimeType.h"
#include "System.Type.h"
#include "System.ValueType.h"


namespace System
{
	System::Object* TypedReference::InternalToObject(void* value)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void TypedReference::InternalSetTypedReference(void* target, System::Object* value)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

	void TypedReference::InternalMakeTypedReference(void* result, System::Object* target, System::NormalArray<int*> flds, RuntimeType* lastFieldType)
	{
		// Implement this...
		throw new System::NotImplementedException();
	}

}
