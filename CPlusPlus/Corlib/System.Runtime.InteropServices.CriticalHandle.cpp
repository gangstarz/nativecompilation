#include "System.Runtime.InteropServices.CriticalHandle.h"
#include "System.NotImplementedException.h"
#include "System.Object.h"
#include "System.ValueType.h"


namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			void CriticalHandle::FireCustomerDebugProbe()
			{
				// Implement this...
				throw new System::NotImplementedException();
			}

		}
	}
}
