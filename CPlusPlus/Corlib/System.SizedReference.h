#ifndef __SYSTEM_SIZEDREFERENCE
#define __SYSTEM_SIZEDREFERENCE

#include "../gc.h"
#include <cstdint>
#include "System.Object.h"

namespace System
{
	class ValueType;

	class SizedReference : public System::Object
	{
	public:
		int* CreateSizedRef(System::Object* o);
		void FreeSizedRef(int* h);
		System::Object* GetTargetOfSizedRef(int* h);
		int64_t GetApproximateSizeOfSizedRef(int* h);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
