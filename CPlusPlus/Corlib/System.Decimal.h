#ifndef __SYSTEM_DECIMAL
#define __SYSTEM_DECIMAL

#include "../gc.h"
#include <cstdint>
#include "System.ValueType.h"

namespace System
{
	class Currency;

	class Decimal : public ValueType
	{
	public:
		int32_t FCallCompare(System::Decimal& d1, System::Decimal& d2);
		int32_t GetHashCode();
		double ToDouble(System::Decimal d);
		int32_t FCallToInt32(System::Decimal d);
		float ToSingle(System::Decimal d);
		void FCallAddSub(System::Decimal& d1, System::Decimal& d2, uint8_t bSign);
		void FCallAddSubOverflowed(System::Decimal& d1, System::Decimal& d2, uint8_t bSign, int& overflowed);
		void FCallDivide(System::Decimal& d1, System::Decimal& d2);
		void FCallDivideOverflowed(System::Decimal& d1, System::Decimal& d2, int& overflowed);
		void FCallFloor(System::Decimal& d);
		void FCallMultiply(System::Decimal& d1, System::Decimal& d2);
		void FCallMultiplyOverflowed(System::Decimal& d1, System::Decimal& d2, int& overflowed);
		void FCallRound(System::Decimal& d, int32_t decimals);
		void FCallToCurrency(Currency& result, System::Decimal d);
		void FCallTruncate(System::Decimal& d);

		// [place holder for auto-generated recompiled members]
	};
}

#endif
