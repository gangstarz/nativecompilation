#ifndef __ARRAY_H
#define __ARRAY_H

#include "Corlib/System.Array.h"

namespace System
{
	template <class T>
	struct NormalArray : Array {

	};

	template <class T, int N>
	struct MultiDimensionalArray : Array {

	};
}

#endif