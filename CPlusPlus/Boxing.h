template<typename T>
struct TypeHelper
{
	T value;

	static const bool IsValueType = true;

	static T* Box(const T& input) { return new T(input); }
	static void InitObject(T& value) 
	{
		T* startPtr = &value;
		T* endPtr = startPtr++;

		for (char* c = reinterpret_cast<char*>(startPtr); c != reinterpret_cast<char*>(endPtr); ++c)
		{
			*c = 0;
		}
	}
};

template<typename T>
struct TypeHelper < T* >
{
	static const bool IsValueType = false;

	static T* Box(T* input) { return input; }

	static void InitObject(T* value)
	{
		return 0; // null pointer
	}
};
