﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class InnerClassDefinition
    {
        public InnerClassDefinition(AccessFlags access, string name, IFieldType def, IFieldType owner)
        {
            this.Access = access;
            this.Name = name;
            this.Definition = def;
            this.Owner = owner;
        }
        public AccessFlags Access { get; set; }
        public string Name { get; set; }
        public IFieldType Definition { get; set; }
        public IFieldType Owner { get; set; }
    }
}
