﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class SourceDebugExtension : IAttributeInfo
    {
        public SourceDebugExtension() { }

        internal SourceDebugExtension(BigEndianReader reader, uint length)
        {
            var data = reader.ReadBytes((int)length);
            this.Value = ConstantUTF8.Parse(data);
        }

        public string Value { get; set; }

    }
}
