﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class RuntimeVisibleParameterAnnotations: IAttributeInfo
    {
        public RuntimeVisibleParameterAnnotations() { }

        internal RuntimeVisibleParameterAnnotations(BigEndianReader reader, ClassFile file)
        {
            int parameters = reader.ReadByte();
            ParameterAnnotations = new Annotation[parameters][];
            for (int i = 0; i < parameters; ++i)
            {
                var count = reader.ReadUInt16();
                var annotations = new Annotation[count];
                for (int j = 0; j < count; ++j)
                {
                    annotations[j] = new Annotation(reader, file);
                }

                ParameterAnnotations[i] = annotations;
            }
        }

        public Annotation[][] ParameterAnnotations;
    }
}