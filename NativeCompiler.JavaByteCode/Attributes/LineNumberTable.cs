﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class LineNumberTable :IAttributeInfo 
    {
        public LineNumberTable() { }

        internal LineNumberTable(BigEndianReader reader, ClassFile classFile)
        {
            var len = reader.ReadUInt16();
            LineNumberInfo[] info = new LineNumberInfo[len];
            for (int i = 0; i < len; ++i)
            {
                info[i] = new LineNumberInfo(reader.ReadUInt16(), reader.ReadUInt16());
            }
            this.Table = info;
        }

        public LineNumberInfo[] Table;

    }
}
