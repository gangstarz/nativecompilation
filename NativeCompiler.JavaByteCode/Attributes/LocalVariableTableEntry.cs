﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class LocalVariableTableEntry
    {
        public LocalVariableTableEntry(ushort pc, ushort len, string name, IFieldType descr, int index)
        {
            this.StartProgramCounter = pc;
            this.Length = len;
            this.Name = name;
            this.Descriptor = descr;
            this.Index = index;
        }

        public ushort StartProgramCounter;
        public ushort Length;
        public string Name;
        public IFieldType Descriptor;
        public int Index;
    }

}
