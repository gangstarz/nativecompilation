﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class RuntimeInvisibleAnnotations : IAttributeInfo
    {
        public RuntimeInvisibleAnnotations() { }

        internal RuntimeInvisibleAnnotations(BigEndianReader reader, ClassFile file)
        {
            var count = reader.ReadUInt16();
            Annotations = new Annotation[count];
            for (int i = 0; i < count; ++i)
            {
                Annotations[i] = new Annotation(reader, file);
            }
        }

        public Annotation[] Annotations;
    }
}