﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class LocalVariableTypeTableEntry
    {
        public LocalVariableTypeTableEntry(ushort pc, ushort len, string name, IFieldTypeSignature signature, int index)
        {
            this.StartProgramCounter = pc;
            this.Length = len;
            this.Name = name;
            this.Signature = signature;
            this.Index = index;
        }

        public ushort StartProgramCounter;
        public ushort Length;
        public string Name;
        public IFieldTypeSignature Signature;
        public int Index;
    }

    public class LocalVariableTypeTable : IAttributeInfo
    {
        public LocalVariableTypeTable() { }

        internal LocalVariableTypeTable(BigEndianReader reader, ClassFile classFile)
        {
            var numberEntries = reader.ReadUInt16();

            Table = new LocalVariableTypeTableEntry[numberEntries];

            for (int i = 0; i < numberEntries; ++i)
            {
                ushort pc = reader.ReadUInt16();
                ushort len = reader.ReadUInt16();
                ushort nameIdx = reader.ReadUInt16();
                ushort descrIdx = reader.ReadUInt16();
                ushort index = reader.ReadUInt16();

                string name = ((ConstantUTF8)classFile.ConstantPool[nameIdx]).Constant;
                string signature = ((ConstantUTF8)classFile.ConstantPool[descrIdx]).Constant;
                var sig = FieldTypeParser.ParseFieldTypeSignature(signature);

                Table[i] = new LocalVariableTypeTableEntry(pc, len, name, sig, index);
            }
        }

        public LocalVariableTypeTableEntry[] Table;
    }
}
