﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class RuntimeVisibleAnnotations : IAttributeInfo
    {
        public RuntimeVisibleAnnotations() { }

        internal RuntimeVisibleAnnotations(BigEndianReader reader, ClassFile file)
        {
            var count = reader.ReadUInt16();
            Annotations = new Annotation[count];
            for (int i = 0; i < count; ++i)
            {
                Annotations[i] = new Annotation(reader, file);
            }
        }

        public Annotation[] Annotations;
    }
}
