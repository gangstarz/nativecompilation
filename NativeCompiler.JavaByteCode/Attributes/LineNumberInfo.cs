﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class LineNumberInfo
    {
        public LineNumberInfo(int pc, int ln)
        {
            this.ProgramCounter = pc;
            this.LineNumber = ln;
        }

        public int ProgramCounter { get; private set; }
        public int LineNumber { get; private set; }
    }
}
