﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class SourceFile : IAttributeInfo
    {
        public SourceFile() { }

        internal SourceFile(BigEndianReader reader, ClassFile classFile)
        {
            var idx = reader.ReadUInt16();
            var cp = classFile.ConstantPool[idx] as ConstantUTF8;
            if (cp == null)
            {
                throw new IncorrectClassFileException("Expected string as source file");
            }

            this.Filename = cp.Constant;
        }

        public string Filename { get; set; }
    }
}
