﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class Signature : IAttributeInfo
    { 
        public Signature() { }

        internal Signature(BigEndianReader reader, ClassFile classFile)
        {
            var idx = reader.ReadUInt16();
            var cp = classFile.ConstantPool[idx] as ConstantUTF8;
            if (cp == null)
            {
                throw new IncorrectClassFileException("Expected string as signature");
            }

            this.Value = cp.Constant;
        }

        public string Value { get; set; }
    }
}
