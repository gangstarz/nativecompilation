﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class UnknownAttributeInfo : IAttributeInfo
    {
        public UnknownAttributeInfo(string name, byte[] data)
        {
            this.Name = name;
            this.Data = data;
        }

        public string Name { get; set; }
        public byte[] Data { get; set; }
    }
}
