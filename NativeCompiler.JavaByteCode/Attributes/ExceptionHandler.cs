﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class ExceptionHandler
    {
        public ExceptionHandler(int startTry, int endTry, int startHandler, IFieldType catchType = null)
        {
            this.StartTry = startTry;
            this.EndTry = endTry;
            this.StartHandler = startHandler;
            this.CatchType = catchType;
        }

        public int StartTry { get; private set; }
        public int EndTry { get; private set; }
        public int StartHandler { get; private set; }
        public IFieldType CatchType { get; private set; }
    }
}
