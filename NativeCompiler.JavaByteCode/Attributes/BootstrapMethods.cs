﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class BootstrapMethod
    {
        public BootstrapMethod(ConstantMethodHandle handle, ConstantPoolInfo[] args)
        {
            this.Handle = handle;
            this.Arguments = args;
        }

        public ConstantMethodHandle Handle { get; set; }
        public ConstantPoolInfo[] Arguments { get; set; }
    }

    public class BootstrapMethods : IAttributeInfo
    {
        public BootstrapMethods() { }

        internal BootstrapMethods(BigEndianReader reader, ClassFile file)
        {
            var count = reader.ReadUInt16();
            this.Methods = new BootstrapMethod[count];

            for (int i = 0; i < count; ++i)
            {
                ConstantMethodHandle cmh = file.ConstantPool[reader.ReadUInt16()] as ConstantMethodHandle;

                var numberArgs = reader.ReadUInt16();
                ConstantPoolInfo[] args = new ConstantPoolInfo[numberArgs];
                for (int j = 0; j < numberArgs; ++j)
                {
                    args[j] = file.ConstantPool[reader.ReadUInt16()];
                }

                Methods[i] = new BootstrapMethod(cmh, args);
            }
        }

        public BootstrapMethod[] Methods;
    }
}
