﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class InnerClasses : IAttributeInfo
    {
        public InnerClasses() { }

        internal InnerClasses(BigEndianReader reader, ClassFile classFile)
        {
            var numberClasses = reader.ReadUInt16();

            Classes = new InnerClassDefinition[numberClasses];

            for (int i = 0; i < numberClasses; ++i)
            {
                var innerClassInfo = reader.ReadUInt16();
                var outerClassInfo = reader.ReadUInt16();
                var innerClassName = reader.ReadUInt16();
                var innerClassAccess = (AccessFlags)reader.ReadUInt16();

                if (!(classFile.ConstantPool[innerClassInfo] is ConstantClass) ||
                    (outerClassInfo != 0 && !(classFile.ConstantPool[outerClassInfo] is ConstantClass)))
                {
                    throw new IncorrectClassFileException("Expected class definition in inner classes");
                }

                string icname = null;
                if (innerClassName != 0 && !(classFile.ConstantPool[innerClassName] is ConstantUTF8))
                {
                    throw new IncorrectClassFileException("Expected name as inner class name");
                }
                else if (innerClassName != 0)
                {
                    icname = ((ConstantUTF8)classFile.ConstantPool[innerClassName]).Constant;
                }

                var c1 = ((ConstantClass)classFile.ConstantPool[innerClassInfo]);
                var c2 = ((ConstantClass)classFile.ConstantPool[outerClassInfo]);

                Classes[i] = new InnerClassDefinition(innerClassAccess, icname, c1 == null ? null : c1.Name, c2 == null ? null : c2.Name);
            }
        }

        public InnerClassDefinition[] Classes;
    }
}
