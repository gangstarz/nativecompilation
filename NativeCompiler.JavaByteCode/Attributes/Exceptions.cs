﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class Exceptions : IAttributeInfo
    {
        public Exceptions() { }

        internal Exceptions(BigEndianReader reader, ClassFile classFile)
        {
            var numberExceptions = reader.ReadUInt16();
            ConstantClass[] classes = new ConstantClass[numberExceptions];
            for (int i = 0; i < numberExceptions; ++i)
            {
                var ui = reader.ReadUInt16();
                var cp = classFile.ConstantPool[ui] as ConstantClass;
                if (cp == null)
                {
                    throw new IncorrectClassFileException("Expected class definition");
                }
                classes[i] = cp;
            }

            this.Classes = classes.Select((a)=>a.Name).ToArray();
        }

        public IFieldType[] Classes { get; private set; }

    }
}
