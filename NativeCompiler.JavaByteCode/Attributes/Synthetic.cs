﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class Synthetic : IAttributeInfo
    {
        public Synthetic() { }

        internal Synthetic(BigEndianReader reader, ClassFile classFile)
        { }
    }
}
