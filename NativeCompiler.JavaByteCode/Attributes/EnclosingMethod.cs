﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class EnclosingMethod : IAttributeInfo
    {
        public EnclosingMethod() { }

        internal EnclosingMethod(BigEndianReader reader, ClassFile classFile)
        {
            var classIndex = reader.ReadUInt16();
            var methodIndex = reader.ReadUInt16();

            var cp = classFile.ConstantPool[classIndex] as ConstantClass;
            var cp2 = classFile.ConstantPool[methodIndex] as ConstantNameAndType;

            if (cp == null || cp2 == null)
            {
                throw new IncorrectClassFileException("Incorrect enclosing method definition");
            }

            this.Class = cp.Name;
            this.NameAndType = cp2;
        }

        public IFieldType Class { get; set; }
        public ConstantNameAndType NameAndType { get; set; }
    }
}
