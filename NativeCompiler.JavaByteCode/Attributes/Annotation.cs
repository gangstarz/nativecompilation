﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NativeCompiler.JavaByteCode.ConstantPool;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class Annotation
    {
        public Annotation() { }

        internal Annotation(BigEndianReader reader, ClassFile file)
        {
            var typeIndex = file.ConstantPool[reader.ReadUInt16()] as ConstantUTF8;
            if (typeIndex == null)
            {
                throw new IncorrectClassFileException("Expected string as annotation type");
            }

            var numberValues = reader.ReadUInt16();
            KeyValues = new KeyValuePair<string, object>[numberValues];

            for (int i = 0; i < numberValues; ++i)
            {
                var nameIndex = file.ConstantPool[reader.ReadUInt16()] as ConstantUTF8;

                if (nameIndex == null)
                {
                    throw new IncorrectClassFileException("Expected string as annotation key");
                }

                object value = ReadElementValue(reader, file);

                KeyValues[i] = new KeyValuePair<string, object>(nameIndex.Constant, value);
            }
        }

        internal static object ReadElementValue(BigEndianReader reader, ClassFile file)
        {
            object value = null;

            var tag = (char)reader.ReadByte();
            switch (tag)
            {
                // NOTE: The standard isn't very clear about this regarding to 'B','C','S','Z' and 's'.
                case 'B':
                    value = (byte)((ConstantInteger)file.ConstantPool[reader.ReadUInt16()]).Constant;
                    break;
                case 'C':
                    value = (char)((ConstantInteger)file.ConstantPool[reader.ReadUInt16()]).Constant;
                    break;
                case 'D':
                    value = ((ConstantDouble)file.ConstantPool[reader.ReadUInt16()]).Constant;
                    break;
                case 'F':
                    value = ((ConstantFloat)file.ConstantPool[reader.ReadUInt16()]).Constant;
                    break;
                case 'I':
                    value = ((ConstantInteger)file.ConstantPool[reader.ReadUInt16()]).Constant;
                    break;
                case 'J':
                    value = ((ConstantLong)file.ConstantPool[reader.ReadUInt16()]).Constant;
                    break;
                case 'S':
                    value = ((ConstantString)file.ConstantPool[reader.ReadUInt16()]).Constant;
                    break;
                case 'Z':
                    value = ((ConstantInteger)file.ConstantPool[reader.ReadUInt16()]).Constant != 0;
                    break;
                case 's':
                    value = ((ConstantUTF8)file.ConstantPool[reader.ReadUInt16()]).Constant;
                    break;

                case 'e':
                    {
                        var type = ((ConstantUTF8)file.ConstantPool[reader.ReadUInt16()]).Constant;
                        var val = ((ConstantUTF8)file.ConstantPool[reader.ReadUInt16()]).Constant;
                        var ftype = new ClassType(FieldTypeParser.ParseMultiPartIdentifier(type));

                        value = new Tuple<IFieldType, string>(ftype, val);
                    }
                    break;

                case 'c':
                    {
                        var type = ((ConstantUTF8)file.ConstantPool[reader.ReadUInt16()]).Constant;
                        value = FieldTypeParser.ParseDescriptorSignature(type);
                    }
                    break;

                case '@':
                    value = new Annotation(reader, file);
                    break;

                case '[':
                    {
                        int count = reader.ReadUInt16();
                        object[] values = new object[count];
                        for (int i = 0; i < count; ++i)
                        {
                            values[i] = ReadElementValue(reader, file);
                        }
                        value = values;
                    }
                    break;
                default:
                    throw new IncorrectClassFileException("Incorrect tag name encountered");
            }
            return value;
        }

        public KeyValuePair<string, object>[] KeyValues { get; set; }
    }
}
