﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public static class AttributeInfoBuilder
    {
        public static IAttributeInfo Read(BigEndianReader reader, ClassFile classFile)
        {
            ushort attributeNameIndex = reader.ReadUInt16();

            var utf8 = classFile.ConstantPool[attributeNameIndex] as ConstantUTF8;
            if (utf8 == null)
            {
                throw new IncorrectClassFileException("Expected UTF8 attribute name");
            }

            var length = reader.ReadUInt32();

            var name = utf8.Constant;

            var pos = reader.BaseStream.Position;

            var attr = Create(reader, classFile, name, length);

            if (reader.BaseStream.Position != pos + length)
            {
                throw new IncorrectClassFileException("Reading attribute " + attr.GetType().Name + " resulted in an incorrect stream offset.");
            }
            return attr;
        }

        private static IAttributeInfo Create(BigEndianReader reader, ClassFile classFile, string name, uint length)
        {
            switch (name)
            {
                case "ConstantValue":
                    return CreateConstantValue(classFile.ConstantPool[reader.ReadUInt16()]);

                case "Code":
                    return new Code(reader, classFile);

                case "StackMapTable":
                    return new StackMapTable(reader, classFile);

                case "Exceptions":
                    return new Exceptions(reader, classFile);

                case "InnerClasses":
                    return new InnerClasses(reader, classFile);

                case "EnclosingMethod":
                    return new EnclosingMethod(reader, classFile);

                case "Synthetic":
                    return new Synthetic(reader, classFile);

                case "Signature":
                    return new Signature(reader, classFile);

                case "SourceFile":
                    return new SourceFile(reader, classFile);

                case "SourceDebugExtension":
                    return new SourceDebugExtension(reader, length);

                case "LineNumberTable":
                    return new LineNumberTable(reader, classFile);

                case "LocalVariableTable":
                    return new LocalVariableTable(reader, classFile);

                case "LocalVariableTypeTable":
                    return new LocalVariableTypeTable(reader, classFile);

                case "Deprecated":
                    return new Deprecated(reader, classFile);

                case "RuntimeVisibleAnnotations":
                    return new RuntimeVisibleAnnotations(reader, classFile);

                case "RuntimeInvisibleAnnotations":
                    return new RuntimeInvisibleAnnotations(reader, classFile);

                case "RuntimeVisibleParameterAnnotations":
                    return new RuntimeVisibleParameterAnnotations(reader, classFile);

                case "RuntimeInvisibleParameterAnnotations":
                    return new RuntimeInvisibleParameterAnnotations(reader, classFile);

                case "AnnotationDefault":
                    return new AnnotationDefault(reader, classFile);

                case "BootstrapMethods":
                    return new BootstrapMethods(reader, classFile);
                    
                default:
                    var data = reader.ReadBytes((int)length);
                    return new UnknownAttributeInfo(name, data);

            }

        }

        private static IAttributeInfo CreateConstantValue(ConstantPoolInfo info)
        {
            if (info is ConstantDouble)
            {
                return new ConstantValue<double>(((ConstantDouble)info).Constant);
            }
            else if (info is ConstantLong)
            {
                return new ConstantValue<ulong>(((ConstantLong)info).Constant);
            }
            else if (info is ConstantFloat)
            {
                return new ConstantValue<float>(((ConstantFloat)info).Constant);
            }
            else if (info is ConstantInteger)
            {
                return new ConstantValue<uint>(((ConstantInteger)info).Constant);
            }
            else if (info is ConstantString)
            {
                return new ConstantValue<string>(((ConstantString)info).Constant);
            }
            else
            {
                throw new IncorrectClassFileException("Expected constant value");
            }
        }

        private static void AssertLength(uint length, int expected)
        {
            if (length != expected)
            {
                throw new IncorrectClassFileException("Expected attribute to have length " + expected);
            }
        }
    }
}
