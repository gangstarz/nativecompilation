﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class AnnotationDefault : IAttributeInfo
    {
        public AnnotationDefault() { }

        internal AnnotationDefault(BigEndianReader reader, ClassFile classFile)
        {
            this.Defaultvalue = Annotation.ReadElementValue(reader, classFile);
        }

        public object Defaultvalue { get; set; }
    }
}
