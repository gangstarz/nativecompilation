﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class LocalVariableTable : IAttributeInfo
    {
        public LocalVariableTable() { }

        internal LocalVariableTable(BigEndianReader reader, ClassFile classFile)
        {
            var numberEntries = reader.ReadUInt16();

            Table = new LocalVariableTableEntry[numberEntries];

            for (int i = 0; i < numberEntries; ++i)
            {
                ushort pc = reader.ReadUInt16();
                ushort len = reader.ReadUInt16();
                ushort nameIdx = reader.ReadUInt16();
                ushort descrIdx = reader.ReadUInt16();
                ushort index = reader.ReadUInt16();

                string name = ((ConstantUTF8)classFile.ConstantPool[nameIdx]).Constant;
                string descriptor = ((ConstantUTF8)classFile.ConstantPool[descrIdx]).Constant;
                var descr = FieldTypeParser.ParseDescriptorSignature(descriptor);

                Table[i] = new LocalVariableTableEntry(pc, len, name, descr, index);
            }
        }

        public LocalVariableTableEntry[] Table;
    }
}
