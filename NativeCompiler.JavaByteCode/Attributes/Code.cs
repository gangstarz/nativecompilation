﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class Code : IAttributeInfo
    {
        public Code() { }

        internal Code(BigEndianReader reader, ClassFile classFile)
        {
            this.MaxStack = reader.ReadUInt16();
            this.MaxLocals = reader.ReadUInt16();
            var codeLength = reader.ReadUInt32();
            
            var data = reader.ReadBytes((int)codeLength);

            var exceptionTableLength = reader.ReadUInt16();

            ExceptionHandler[] handlers = new ExceptionHandler[exceptionTableLength];
            for (int i = 0; i < exceptionTableLength; ++i)
            {
                var s1 = reader.ReadUInt16();
                var e1 = reader.ReadUInt16();
                var s2 = reader.ReadUInt16();
                IFieldType type = null;

                var catchType = reader.ReadUInt16();
                if (catchType != 0) // Finally
                {
                    type = ((ConstantClass)classFile.ConstantPool[catchType]).Name;
                }

                handlers[i] = new ExceptionHandler(s1, e1, s2, type);
            }

            this.Handlers = handlers;

            int attrCount = reader.ReadUInt16();
            Attributes = new IAttributeInfo[attrCount];
            for (int i = 0; i < attrCount; ++i)
            {
                Attributes[i] = AttributeInfoBuilder.Read(reader, classFile);
            }

            this.ProgramCode = new Instructions.Instructions(data, classFile);
        }

        public ushort MaxStack;
        public ushort MaxLocals;
        public Instructions.Instructions ProgramCode;
        public ExceptionHandler[] Handlers;
        public IAttributeInfo[] Attributes;
    }
}
