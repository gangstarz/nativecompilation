﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Attributes
{
    public class ConstantValue<T> : IAttributeInfo where T : IEquatable<T>
    {
        public ConstantValue(T value)
        {
            this.Value = value;
        }

        public T Value { get; set; }
    }
}
