﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NativeCompiler.JavaByteCode
{
    public class IncorrectClassFileException : Exception
    {
        public IncorrectClassFileException(string msg) : base(msg) { }
    }
}
