﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace NativeCompiler.JavaByteCode
{
    class Program
    {
        static void Main(string[] args)
        {
            string file = @"C:\Users\Stefan\Desktop\HAPI\lib\hapi-base-2.1\ca\uhn\hl7v2\DefaultHapiContext.class";

            ClassFile cfr = new ClassFile(File.OpenRead(file));
            cfr.Parse();
        }
    }
}
