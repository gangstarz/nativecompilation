﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode
{
    public class BigEndianReader
    {
        public BigEndianReader(Stream stream)
        {
            this.stream = stream;
        }

        private Stream stream;

        public Stream BaseStream { get { return stream; } }

        public ulong ReadUInt64()
        {
            return
                (((ulong)stream.ReadByte()) << 56) |
                (((ulong)stream.ReadByte()) << 48) |
                (((ulong)stream.ReadByte()) << 40) |
                (((ulong)stream.ReadByte()) << 32) |
                (((ulong)stream.ReadByte()) << 24) |
                (((ulong)stream.ReadByte()) << 16) |
                (((ulong)stream.ReadByte()) << 8) |
                ((ulong)stream.ReadByte());
        }

        public long ReadInt64()
        {
            return (long)ReadUInt64();
        }

        public uint ReadUInt32()
        {
            return
                (((uint)stream.ReadByte()) << 24) |
                (((uint)stream.ReadByte()) << 16) |
                (((uint)stream.ReadByte()) << 8) |
                ((uint)stream.ReadByte());
        }

        public int ReadInt32()
        {
            return
                (((int)stream.ReadByte()) << 24) |
                (((int)stream.ReadByte()) << 16) |
                (((int)stream.ReadByte()) << 8) |
                ((int)stream.ReadByte());
        }

        public ushort ReadUInt16()
        {
            return (ushort)((stream.ReadByte() << 8) | stream.ReadByte());
        }

        public short ReadInt16()
        {
            return (short)((stream.ReadByte() << 8) | stream.ReadByte());
        }

        public byte ReadByte()
        {
            return (byte)stream.ReadByte();
        }

        public float ReadSingle()
        {
            byte[] data = new byte[4];
            stream.Read(data, 0, 4);
            
            var tmp = data[0];
            data[0] = data[3];
            data[3] = tmp;

            tmp = data[1];
            data[1] = data[2];
            data[2] = tmp;

            return BitConverter.ToSingle(data, 0);
        }

        public double ReadDouble()
        {
            byte[] data = new byte[8];
            stream.Read(data, 0, 8);

            var tmp = data[0];
            data[0] = data[7];
            data[7] = tmp;

            tmp = data[1];
            data[1] = data[6];
            data[6] = tmp;
            
            tmp = data[2];
            data[2] = data[5];
            data[5] = tmp;

            tmp = data[3];
            data[3] = data[4];
            data[4] = tmp;

            return BitConverter.ToDouble(data, 0);
        }

        public byte[] ReadBytes(int length)
        {
            byte[] buf = new byte[length];
            stream.Read(buf, 0, length);
            return buf;
        }
    }
}
