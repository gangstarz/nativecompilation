﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class ClassTypeSignatureComponent : IEquatable<ClassTypeSignatureComponent>
    {
        public ClassTypeSignatureComponent(string name, List<TypeArgument> args = null)
        {
            this.Name = name;
            this.TypeArguments = args;
        }

        public string Name { get; private set; }
        public List<TypeArgument> TypeArguments { get; private set; }

        public void Write(StringBuilder sb)
        {
            sb.Append(Name);
            if (TypeArguments != null)
            {
                sb.Append('<');

                bool first = true;
                foreach (var item in TypeArguments)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        sb.Append(',');
                    }

                    item.Write(sb);
                }

                sb.Append('>');
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() * 31 +
                   TypeArguments.ListHash();
        }

        public override bool Equals(object other)
        {
            return other is ClassTypeSignatureComponent && Equals((ClassTypeSignatureComponent)other);
        }

        public bool Equals(ClassTypeSignatureComponent other)
        {
            return Name.Equals(other.Name) && TypeArguments.ListEquals(other.TypeArguments);
        }
    }
}
