﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class ClassSignature : IEquatable<ClassSignature>
    {
        public ClassSignature(List<FormalTypeParameter> formalTypeParameters, ClassTypeSignature superClass, List<ClassTypeSignature> superInterface)
        {
            this.FormalTypeParameters = formalTypeParameters;
            this.SuperClassSignature = superClass;
            this.SuperInterfaceSignature = superInterface;
        }

        public List<FormalTypeParameter> FormalTypeParameters = null;
        public ClassTypeSignature SuperClassSignature;
        public List<ClassTypeSignature> SuperInterfaceSignature = new List<ClassTypeSignature>();

        public void Write(StringBuilder sb)
        {
            if (FormalTypeParameters != null)
            {
                sb.Append('<');
                foreach (var item in FormalTypeParameters)
                {
                    item.Write(sb);
                }
                sb.Append('>');
            }
            SuperClassSignature.Write(sb);
            foreach (var item in SuperInterfaceSignature)
            {
                item.Write(sb);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return FormalTypeParameters.ListHash() * 7577 +
                SuperClassSignature.GetHashCode() * 31 +
                SuperInterfaceSignature.ListHash();
        }

        public override bool Equals(object other)
        {
            return other is ClassSignature && Equals((ClassSignature)other);
        }

        public bool Equals(ClassSignature other)
        {
            return
                FormalTypeParameters.ListEquals(other.FormalTypeParameters) &&
                SuperClassSignature.Equals(other.SuperClassSignature) &&
                SuperInterfaceSignature.ListEquals(other.SuperInterfaceSignature);
        }
    }
}
