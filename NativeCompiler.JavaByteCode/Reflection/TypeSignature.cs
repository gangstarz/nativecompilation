﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class TypeSignature : IEquatable<TypeSignature>
    {
        public TypeSignature(IFieldTypeSignature sig, BasicType bt)
        {
            this.FieldType = sig;
            this.BaseType = bt;
        }

        public IFieldTypeSignature FieldType { get; private set; }
        public BasicType BaseType { get; private set; }

        public void Write(StringBuilder sb)
        {
            FieldType.Write(sb);
            BaseType.Write(sb);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return FieldType.GetHashCode() * 31 + BaseType.GetHashCode();
        }

        public override bool Equals(object other)
        {
            return other is TypeSignature && Equals((TypeSignature)other);
        }

        public bool Equals(TypeSignature other)
        {
            return FieldType.Equals(other.FieldType) && BaseType.Equals(other.BaseType);
        }
    }
}
