﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class TypeVariableSignature : IFieldTypeSignature, IEquatable<TypeVariableSignature>
    {
        public TypeVariableSignature(string identifier)
        {
            this.Identifier = identifier;
        }

        public string Identifier { get; private set; }

        public void Write(StringBuilder sb)
        {
            sb.Append('V');
            sb.Append(Identifier);
            sb.Append(';');
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return Identifier.GetHashCode();
        }

        public override bool Equals(object other)
        {
            return other is TypeVariableSignature && Equals((TypeVariableSignature)other);
        }

        public bool Equals(IFieldTypeSignature other)
        {
            return other is TypeVariableSignature && Equals((TypeVariableSignature)other);
        }

        public bool Equals(TypeVariableSignature other)
        {
            return Identifier.Equals(other.Identifier);
        }
    }
}
