﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class BasicType : IFieldType , IEquatable<BasicType>
    {
        public BasicType(BasicTypeKind t)
        {
            this.Type = t;
        }

        public BasicTypeKind Type { get; private set; }

        public void Write(StringBuilder sb)
        {
            switch (Type)
            {
                case BasicTypeKind.Boolean: sb.Append('Z'); break;
                case BasicTypeKind.Byte: sb.Append('B'); break;
                case BasicTypeKind.Char: sb.Append('C'); break;
                case BasicTypeKind.Double: sb.Append('D'); break;
                case BasicTypeKind.Float: sb.Append('F'); break;
                case BasicTypeKind.Int: sb.Append('I'); break;
                case BasicTypeKind.Long: sb.Append('J'); break;
                case BasicTypeKind.Short: sb.Append('S'); break;
                case BasicTypeKind.Void: sb.Append('V'); break;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode();
        }

        public override bool Equals(object other)
        {
            return other is BasicType && Equals((BasicType)other);
        }

        public bool Equals(IFieldType other)
        {
            return other is BasicType && Equals((BasicType)other);
        }

        public bool Equals(BasicType other)
        {
            return other.Type == Type;
        }
    }
}
