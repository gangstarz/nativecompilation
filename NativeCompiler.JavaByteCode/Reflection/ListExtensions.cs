﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public static class ListExtensions
    {
        public static bool ListEquals<T>(this List<T> lhs, List<T> rhs) where T : IEquatable<T>
        {
            if (lhs == null && rhs == null)
            {
                return true;
            }
            else if (lhs != null && rhs != null && lhs.Count == rhs.Count)
            {
                for (int i = 0; i < lhs.Count; ++i)
                {
                    if (!lhs[i].Equals(rhs[i]))
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        public static int ListHash<T>(this List<T> list)
        {
            if (list == null)
            {
                return 0x389;
            }
            else
            {
                var id = 7577;
                foreach (var item in list)
                {
                    id = id * 31 + item.GetHashCode();
                }
                return id;
            }
        }
    }
}
