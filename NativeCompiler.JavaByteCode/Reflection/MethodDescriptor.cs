﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class MethodDescriptor : IFieldType, IEquatable<MethodDescriptor>
    {
        public MethodDescriptor(List<IFieldType> parameters, IFieldType returnType)
        {
            this.Parameters = parameters;
            this.ReturnType = returnType;
        }

        public List<IFieldType> Parameters { get; private set; }
        public IFieldType ReturnType { get; private set; }

        public void Write(StringBuilder sb)
        {
            sb.Append('(');
            bool first = true;
            foreach (var item in Parameters)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    sb.Append(',');
                }

                item.Write(sb);
            }
            sb.Append(')');
            ReturnType.Write(sb);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return Parameters.ListHash() + ReturnType.GetHashCode() * 31;
        }

        public override bool Equals(object other)
        {
            return other is MethodDescriptor && Equals((MethodDescriptor)other);
        }

        public bool Equals(IFieldType other)
        {
            return other is MethodDescriptor && Equals((MethodDescriptor)other);
        }

        public bool Equals(MethodDescriptor other)
        {
            return other.Parameters.ListEquals(Parameters) && ReturnType.Equals(ReturnType);
        }
    }
}
