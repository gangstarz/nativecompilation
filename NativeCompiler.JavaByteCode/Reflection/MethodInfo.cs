﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.Attributes;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class MethodInfo
    {
        public AccessFlags AccessFlags;
        public string Name;
        public IFieldType Method;
        public IAttributeInfo[] Attributes;

        public void Read(BigEndianReader reader, ClassFile classfile)
        {
            AccessFlags = (AccessFlags)reader.ReadUInt16();
            var nameIndex = reader.ReadUInt16();
            var descriptorIndex = reader.ReadUInt16();

            var cp = classfile.ConstantPool[nameIndex] as ConstantPool.ConstantUTF8;
            var cp2 = classfile.ConstantPool[descriptorIndex] as ConstantPool.ConstantUTF8;

            if (cp == null || cp2 == null)
            {
                throw new IncorrectClassFileException("Expected method name and descriptor");
            }

            this.Name = cp.Constant;
            this.Method = FieldTypeParser.ParseMethodName(cp2.Constant);

            var attributesCount = reader.ReadUInt16();
            this.Attributes = new IAttributeInfo[attributesCount];
            for (int i = 0; i < attributesCount; ++i)
            {
                Attributes[i] = AttributeInfoBuilder.Read(reader, classfile);
            }
        }
    }
}
