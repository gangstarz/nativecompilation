﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.Attributes;
using NativeCompiler.JavaByteCode.ConstantPool;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class FieldInfo : IEquatable<FieldInfo>
    {
        public AccessFlags AccessFlags;
        public string Name;
        public IFieldType Descriptor;

        public IAttributeInfo[] Attributes;

        public void Read(BigEndianReader reader, ClassFile classFile)
        {
            AccessFlags = (AccessFlags)reader.ReadUInt16();
            var nameIndex = reader.ReadUInt16();
            var cp = classFile.ConstantPool[nameIndex] as ConstantUTF8;
            if (cp == null)
            {
                throw new IncorrectClassFileException("Expected UTF8 field name");
            }
            this.Name = cp.Constant;

            var descriptorIndex = reader.ReadUInt16();
            cp = classFile.ConstantPool[descriptorIndex] as ConstantUTF8;
            if (cp == null)
            {
                throw new IncorrectClassFileException("Expected UTF8 field descriptor");
            }
            this.Descriptor = FieldTypeParser.ParseDescriptorSignature(cp.Constant);

            var attributesCount = reader.ReadUInt16();
            this.Attributes = new IAttributeInfo[attributesCount];
            for (int i = 0; i < attributesCount; ++i)
            {
                Attributes[i] = AttributeInfoBuilder.Read(reader, classFile);
            }
        }

        public override string ToString()
        {
            return "Field " + Name + "{" + Descriptor.ToString() + "}";
        }

        public override int GetHashCode()
        {
            return ((int)AccessFlags) * 7577 +
                   Name.GetHashCode() * 31 +
                   Descriptor.GetHashCode();
        }

        public override bool Equals(object other)
        {
            return other is FieldInfo && Equals((FieldInfo)other);
        }

        public bool Equals(FieldInfo other)
        {
            return AccessFlags == other.AccessFlags &&
                   Name == other.Name &&
                   Descriptor.Equals(other.Descriptor);
        }
    }
}
