﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public interface IFieldTypeSignature : IEquatable<IFieldTypeSignature>
    {
        void Write(StringBuilder sb);
    }
}
