﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public interface IFieldType : IEquatable<IFieldType>
    {
        void Write(StringBuilder sb);
    }
}
