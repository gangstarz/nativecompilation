﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class TypeArgument : IEquatable<TypeArgument>
    {
        public TypeArgument(char indicator, IFieldTypeSignature signature)
        {
            if (indicator != '-' && indicator != '+' && indicator != '\0')
            {
                throw new IncorrectClassFileException("Expected '+' or '-' as indicator");
            }
            this.WildcardIndicator = indicator;
            this.FieldType = signature;
        }

        public char WildcardIndicator { get; private set; }
        public IFieldTypeSignature FieldType { get; private set; }

        public static TypeArgument All
        {
            get
            {
                return new TypeArgument('-', null) { WildcardIndicator = '*' };
            }
        }

        public void Write(StringBuilder sb)
        {
            if (WildcardIndicator != '\0')
            {
                sb.Append(WildcardIndicator);
            }
            if (WildcardIndicator != '*')
            {
                FieldType.Write(sb);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return WildcardIndicator * 31 + FieldType.GetHashCode();
        }

        public override bool Equals(object other)
        {
            return other is TypeArgument && Equals((TypeArgument)other);
        }

        public bool Equals(TypeArgument other)
        {
            return WildcardIndicator == other.WildcardIndicator && FieldType.Equals(other.FieldType);
        }
    }
}
