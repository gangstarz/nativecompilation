﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class ArrayType : IFieldType, IEquatable<ArrayType>
    {
        public ArrayType(IFieldType elementType)
        {
            this.ElementType = elementType;
        }

        public IFieldType ElementType { get; private set; }

        public void Write(StringBuilder sb)
        {
            sb.Append('[');
            ElementType.Write(sb);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return ElementType.GetHashCode() * 31;
        }

        public override bool Equals(object other)
        {
            return other is ArrayType && Equals((ArrayType)other);
        }

        public bool Equals(IFieldType other)
        {
            return other is ArrayType && Equals((ArrayType)other);
        }

        public bool Equals(ArrayType other)
        {
            return other.ElementType.Equals(ElementType);
        }
    }
}
