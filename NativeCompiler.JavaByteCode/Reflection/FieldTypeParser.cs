﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public static class FieldTypeParser
    {
        public static IFieldType ParseDescriptorSignature(string name)
        {
            int ptr = 0;
            var result = ParseDescriptorSignature(name, ref ptr);
            if (ptr != name.Length)
            {
                throw new IncorrectClassFileException("Incorrect type name found");
            }
            return result;
        }

        public static IFieldType ParseDescriptorSignature(string name, ref int ptr)
        {
            if (name.Length > 0)
            {
                IFieldType result;
                switch (name[ptr])
                {
                    case 'B': result = new BasicType(BasicTypeKind.Byte); ++ptr; break;
                    case 'C': result = new BasicType(BasicTypeKind.Char); ++ptr; break;
                    case 'D': result = new BasicType(BasicTypeKind.Double); ++ptr; break;
                    case 'F': result = new BasicType(BasicTypeKind.Float); ++ptr; break;
                    case 'I': result = new BasicType(BasicTypeKind.Int); ++ptr; break;
                    case 'J': result = new BasicType(BasicTypeKind.Long); ++ptr; break;
                    case 'S': result = new BasicType(BasicTypeKind.Short); ++ptr; break;
                    case 'Z': result = new BasicType(BasicTypeKind.Boolean); ++ptr; break;
                    case 'V': result = new BasicType(BasicTypeKind.Void); ++ptr; break;

                    case 'L':
                        ++ptr;
                        result = new ClassType(ParseClassNameComponents(name, ref ptr));
                        break;
                    case '[':
                        ++ptr;
                        result = new ArrayType(ParseDescriptorSignature(name, ref ptr));
                        break;
                    default:
                        throw new IncorrectClassFileException("Incorrect type name found");
                }

                return result;
            }
            else
            {
                throw new IncorrectClassFileException("Incorrect type name found");
            }
        }

        private static string[] ParseClassNameComponents(string name, ref int ptr)
        {
            List<string> ll = new List<string>();

            ll.Add(ParseIdentifier(name, ref ptr));
            while (name[ptr] == '/')
            {
                ++ptr;
                ll.Add(ParseIdentifier(name, ref ptr));
            }

            if (name[ptr++] != ';')
            {
                throw new IncorrectClassFileException("Incorrect type name found");
            }

            return ll.ToArray();
        }

        public static MethodDescriptor ParseMethodName(string name)
        {
            int ptr = 0;
            List<IFieldType> parameters = new List<IFieldType>();

            if (name[ptr++] != '(')
            {
                throw new IncorrectClassFileException("Expected '(' at start of parameters");
            }

            if (name[ptr] != ')')
            {
                do
                {
                    parameters.Add(ParseDescriptorSignature(name, ref ptr));
                }
                while (ptr < name.Length && name[ptr] != ')');
            }

            if (name[ptr++] != ')')
            {
                throw new IncorrectClassFileException("Expected '(' at start of parameters");
            }

            var returnType = ParseDescriptorSignature(name, ref ptr);

            if (name.Length != ptr)
            {
                throw new IncorrectClassFileException("Incorrect method specification: trailing characters");
            }

            return new MethodDescriptor(parameters, returnType);
        }

        public static string ParseIdentifier(string name, ref int ptr)
        {
            int a = ptr;
            while (ptr < name.Length && !".;[</".Contains(name[ptr]))
            {
                ++ptr;
            }
            return name.Substring(a, ptr - a);
        }

        public static ClassSignature ParseClassSignature(string name, ref int ptr)
        {
            List<FormalTypeParameter> typeParameters = null;

            if (name[ptr] == '<')
            {
                ++ptr;
                typeParameters = new List<FormalTypeParameter>();

                while (name[ptr] != '>')
                {
                    typeParameters.Add(ParseFormalTypeParameter(name, ref ptr));
                }
            }

            var superClass = ParseClassTypeSignature(name, ref ptr);

            List<ClassTypeSignature> superInterfaces = new List<ClassTypeSignature>();
            while (name[ptr] == 'L')
            {
                superInterfaces.Add(ParseClassTypeSignature(name, ref ptr));
            }

            return new ClassSignature(typeParameters, superClass, superInterfaces);
        }

        public static ClassTypeSignature ParseClassTypeSignature(string name, ref int ptr)
        {
            if (name[ptr] != 'L')
            {
                throw new IncorrectClassFileException("Expected 'L'");
            }

            List<string> package = new List<string>();
            string className = ParseIdentifier(name, ref ptr);

            while (name[ptr] == '/')
            {
                ++ptr;
                package.Add(className);
                className = ParseIdentifier(name, ref ptr);
            }

            List<TypeArgument> typeArguments = null;
            if (name[ptr] == '<')
            {
                ptr++;

                typeArguments = new List<TypeArgument>();
                do
                {
                    typeArguments.Add(ParseTypeArgument(name, ref ptr));
                }
                while (name[ptr] != '>');

                ++ptr;
            }

            List<ClassTypeSignatureComponent> suffix = new List<ClassTypeSignatureComponent>();
            while (name[ptr] == '.')
            {
                ++ptr;
                string suffixName = ParseIdentifier(name, ref ptr);

                List<TypeArgument> suffixTypeArguments = null;
                if (name[ptr] == '<')
                {
                    ptr++;

                    suffixTypeArguments = new List<TypeArgument>();
                    do
                    {
                        suffixTypeArguments.Add(ParseTypeArgument(name, ref ptr));
                    }
                    while (name[ptr] != '>');

                    ++ptr;
                }

                suffix.Add(new ClassTypeSignatureComponent(suffixName, suffixTypeArguments));
            }

            if (name[ptr++] != ';')
            {
                throw new IncorrectClassFileException("Expected ';' after class type");
            }

            return new ClassTypeSignature(package.ToArray(), new ClassTypeSignatureComponent(className, typeArguments), suffix);
        }

        public static TypeArgument ParseTypeArgument(string name, ref int ptr)
        {
            IFieldTypeSignature signature;

            switch (name[ptr])
            {
                case '+':
                case '-':
                    char ch = name[ptr++];
                    signature = ParseFieldTypeSignature(name, ref ptr);
                    return new TypeArgument(ch, signature);
                case '*':
                    return TypeArgument.All;
                default:
                    signature = ParseFieldTypeSignature(name, ref ptr);
                    return new TypeArgument('\0', signature);
            }
        }

        public static IFieldTypeSignature ParseFieldTypeSignature(string name)
        {
            int ptr = 0;
            var result = ParseFieldTypeSignature(name, ref ptr);
            if (ptr != name.Length)
            {
                throw new IncorrectClassFileException("Expected end of field type signature");
            }
            return result;
        }

        private static IFieldTypeSignature ParseFieldTypeSignature(string name, ref int ptr)
        {
            switch (name[ptr])
            {
                case 'L':
                    return ParseClassTypeSignature(name, ref ptr);
                case '[':
                    var typeSignature = ParseTypeSignature(name, ref ptr);
                    return new ArrayTypeSignature(typeSignature);
                case 'T':
                    return ParseTypeVariableSignature(name, ref ptr);

                default:
                    throw new IncorrectClassFileException("Expected field type signature");
            }
        }

        private static IFieldTypeSignature ParseTypeVariableSignature(string name, ref int ptr)
        {
            if (name[ptr++] != 'T')
            {
                throw new IncorrectClassFileException("Expected 'T'");
            }

            string identifier = ParseIdentifier(name, ref ptr);

            if (name[ptr++] != ';')
            {
                throw new IncorrectClassFileException("Expected ';' after type identifier");
            }

            return new TypeVariableSignature(identifier);
        }

        private static TypeSignature ParseTypeSignature(string name, ref int ptr)
        {
            IFieldTypeSignature fieldType = ParseFieldTypeSignature(name, ref ptr);
            var baseType = ParseBaseType(name, ref ptr);

            return new TypeSignature(fieldType, baseType);
        }

        private static BasicType ParseBaseType(string name, ref int ptr)
        {
            BasicType result;
            switch (name[ptr])
            {
                case 'B': result = new BasicType(BasicTypeKind.Byte); ++ptr; break;
                case 'C': result = new BasicType(BasicTypeKind.Char); ++ptr; break;
                case 'D': result = new BasicType(BasicTypeKind.Double); ++ptr; break;
                case 'F': result = new BasicType(BasicTypeKind.Float); ++ptr; break;
                case 'I': result = new BasicType(BasicTypeKind.Int); ++ptr; break;
                case 'J': result = new BasicType(BasicTypeKind.Long); ++ptr; break;
                case 'S': result = new BasicType(BasicTypeKind.Short); ++ptr; break;
                case 'Z': result = new BasicType(BasicTypeKind.Boolean); ++ptr; break;

                default:
                    throw new IncorrectClassFileException("Incorrect base type name found");
            }

            return result;
        }

        public static FormalTypeParameter ParseFormalTypeParameter(string name, ref int ptr)
        {
            var identifier = ParseIdentifier(name, ref ptr);
            IFieldTypeSignature classBound = null;
            List<IFieldTypeSignature> interfaceBound = null;

            if (name[ptr] == ':')
            {
                ++ptr;
                classBound = ParseFieldTypeSignature(name, ref ptr);
                if (name[ptr] == ':')
                {
                    interfaceBound = new List<IFieldTypeSignature>();
                    while (name[ptr] == ':')
                    {
                        ++ptr;
                        interfaceBound.Add(ParseFieldTypeSignature(name, ref ptr));
                    }
                }
            }

            return new FormalTypeParameter(identifier, classBound, interfaceBound);
        }

        public static string[] ParseMultiPartIdentifier(string name)
        {
            List<string> ll = new List<string>();
            int ptr = 0;
            while (ptr < name.Length)
            {
                string ident = ParseIdentifier(name, ref ptr);
                ll.Add(ident);
                ++ptr;
            }
            return ll.ToArray();
        }
    }
}
