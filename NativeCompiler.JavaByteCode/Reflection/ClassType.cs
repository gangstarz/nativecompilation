﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class ClassType : IFieldType, IEquatable<ClassType>
    {
        public ClassType(string[] name)
        {
            this.Name = name;
        }

        public string[] Name { get; private set; }

        public void Write(StringBuilder sb)
        {
            sb.Append('L');
            bool first = true;
            foreach (var item in Name)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    sb.Append('/');
                }
                sb.Append(item);
            }
            sb.Append(';');
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return Name.Sum((a) => a.GetHashCode());
        }

        public override bool Equals(object other)
        {
            return other is ClassType && Equals((ClassType)other);
        }

        public bool Equals(IFieldType other)
        {
            return other is ClassType && Equals((ClassType)other);
        }

        public bool Equals(ClassType other)
        {
            if (Name.Length == other.Name.Length)
            {
                for (int i = 0; i < Name.Length; ++i)
                {
                    if (Name[i] != other.Name[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }
}
