﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class ArrayTypeSignature : IFieldTypeSignature, IEquatable<ArrayTypeSignature>
    {
        public ArrayTypeSignature(TypeSignature elementType)
        {
            this.ElementType = elementType;
        }

        public TypeSignature ElementType { get; private set; }

        public void Write(StringBuilder sb)
        {
            sb.Append('[');
            ElementType.Write(sb);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return ElementType.GetHashCode() * 31;
        }

        public override bool Equals(object other)
        {
            return other is ArrayTypeSignature && Equals((ArrayTypeSignature)other);
        }

        public bool Equals(IFieldTypeSignature other)
        {
            return other is ArrayTypeSignature && Equals((ArrayTypeSignature)other);
        }

        public bool Equals(ArrayTypeSignature other)
        {
            return other.ElementType.Equals(ElementType);
        }
    }
}
