﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class ClassTypeSignature : IFieldTypeSignature, IEquatable<ClassTypeSignature>
    {
        public ClassTypeSignature(string[] package, ClassTypeSignatureComponent name, List<ClassTypeSignatureComponent> suffix = null)
        {
            this.Package = package;
            this.Name = name;
            this.Suffix = suffix;
        }

        public string[] Package { get; private set; }
        public ClassTypeSignatureComponent Name { get; private set; }
        public List<ClassTypeSignatureComponent> Suffix { get; private set; }

        public void Write(StringBuilder sb)
        {
            sb.Append('L');
            Name.Write(sb);
            if (Suffix != null)
            {
                foreach (var item in Suffix)
                {
                    sb.Append('.');
                    item.Write(sb);
                }
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return Package.Sum((a)=>a.GetHashCode()) * 7577 +
                   Name.GetHashCode() * 31 +
                   Suffix.ListHash();
        }

        public override bool Equals(object other)
        {
            return other is ClassTypeSignature && Equals((ClassTypeSignature)other);
        }

        public bool Equals(IFieldTypeSignature other)
        {
            return other is ClassTypeSignature && Equals((ClassTypeSignature)other);
        }

        public bool Equals(ClassTypeSignature other)
        {
            if (Package.Length == other.Package.Length)
            {
                for (int i = 0; i < Package.Length; ++i)
                {
                    if (Package[i] != other.Package[i])
                    {
                        return false;
                    }
                }
                return Name.Equals(other.Name) && Suffix.ListEquals(other.Suffix);
            }
            return false;
        }
    }
}
