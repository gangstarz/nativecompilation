﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Reflection
{
    public class FormalTypeParameter : IEquatable<FormalTypeParameter>
    {
        public FormalTypeParameter(string identifier, IFieldTypeSignature classBound, List<IFieldTypeSignature> interfaceBound)
        {
            this.Identifier = identifier;
            this.ClassBound = classBound;
            this.InterfaceBound = interfaceBound;
        }

        public string Identifier;
        public IFieldTypeSignature ClassBound;
        public List<IFieldTypeSignature> InterfaceBound;

        public void Write(StringBuilder sb)
        {
            sb.Append(Identifier);
            sb.Append(':');
            if (ClassBound != null)
            {
                ClassBound.Write(sb);
            }
            foreach (var intf in InterfaceBound)
            {
                sb.Append(':');
                intf.Write(sb);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb);
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return Identifier.GetHashCode() * 7577 +
                ClassBound.GetHashCode() * 31 +
                InterfaceBound.ListHash();
        }

        public override bool Equals(object other)
        {
            return other is FormalTypeParameter && Equals((FormalTypeParameter)other);
        }

        public bool Equals(FormalTypeParameter other)
        {
            return
                Identifier == other.Identifier &&
                ClassBound.Equals(other.ClassBound) &&
                InterfaceBound.ListEquals(other.InterfaceBound);
        }
    }
}
