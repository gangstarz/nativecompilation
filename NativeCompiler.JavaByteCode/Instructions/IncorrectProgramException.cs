﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Instructions
{
    public class IncorrectProgramException : Exception
    {
        public IncorrectProgramException(string msg) : base(msg) { }
    }
}
