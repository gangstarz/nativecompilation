﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.Instructions
{
    public class Instruction
    {
        public Instruction(Opcode code, object operand1 = null, object operand2 = null)
        {
            this.Code = code;
            this.Operand1 = operand1;
            this.Operand2 = operand2;
        }

        public Opcode Code { get; private set; }
        public object Operand1 { get; private set; }
        public object Operand2 { get; private set; }
    }
}
