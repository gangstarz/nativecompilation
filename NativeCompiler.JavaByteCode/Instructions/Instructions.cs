﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.ConstantPool;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.Instructions
{
    public class Instructions
    {
        public Instructions(byte[] program, ClassFile file)
        {
            int ptr = 0;

            var code = new List<Instruction>();
            while (ptr < program.Length)
            {
                code.Add(ParseInstruction(file, program, ref ptr));
            }

            this.Code = code;
        }

        public List<Instruction> Code;

        private static Instruction ParseInstruction(ClassFile file, byte[] data, ref int ptr)
        {
            // NOTE: 'wide' is not here; it's passed as 'wide' boolean.

            var code = (Opcode)data[ptr++];

            switch (code)
            {
                case Opcode.aaload:
                case Opcode.aastore:
                case Opcode.aconst_null:
                case Opcode.aload_0:
                case Opcode.aload_1:
                case Opcode.aload_2:
                case Opcode.aload_3:
                case Opcode.areturn:
                case Opcode.arraylength:
                case Opcode.astore_0:
                case Opcode.astore_1:
                case Opcode.astore_2:
                case Opcode.astore_3:
                case Opcode.athrow:
                case Opcode.baload:
                case Opcode.bastore:
                case Opcode.caload:
                case Opcode.castore:
                case Opcode.d2f:
                case Opcode.d2i:
                case Opcode.d2l:
                case Opcode.dadd:
                case Opcode.daload:
                case Opcode.dastore:
                case Opcode.dcmpg:
                case Opcode.dcmpl:
                case Opcode.dconst_0:
                case Opcode.dconst_1:
                case Opcode.ddiv:
                case Opcode.dload_0:
                case Opcode.dload_1:
                case Opcode.dload_2:
                case Opcode.dload_3:
                case Opcode.dmul:
                case Opcode.dneg:
                case Opcode.drem:
                case Opcode.dreturn:
                case Opcode.dstore_0:
                case Opcode.dstore_1:
                case Opcode.dstore_2:
                case Opcode.dstore_3:
                case Opcode.dsub:
                case Opcode.dup:
                case Opcode.dup_x1:
                case Opcode.dup_x2:
                case Opcode.dup2:
                case Opcode.dup2_x1:
                case Opcode.dup2_x2:
                case Opcode.f2d:
                case Opcode.f2i:
                case Opcode.f2l:
                case Opcode.fadd:
                case Opcode.faload:
                case Opcode.fastore:
                case Opcode.fcmpg:
                case Opcode.fcmpl:
                case Opcode.fconst_0:
                case Opcode.fconst_1:
                case Opcode.fconst_2:
                case Opcode.fdiv:
                case Opcode.fload_0:
                case Opcode.fload_1:
                case Opcode.fload_2:
                case Opcode.fload_3:
                case Opcode.fmul:
                case Opcode.fneg:
                case Opcode.frem:
                case Opcode.freturn:
                case Opcode.fstore_0:
                case Opcode.fstore_1:
                case Opcode.fstore_2:
                case Opcode.fstore_3:
                case Opcode.fsub:
                case Opcode.i2b:
                case Opcode.i2c:
                case Opcode.i2d:
                case Opcode.i2f:
                case Opcode.i2l:
                case Opcode.i2s:
                case Opcode.iadd:
                case Opcode.iaload:
                case Opcode.iand:
                case Opcode.iastore:
                case Opcode.iconst_m1:
                case Opcode.iconst_0:
                case Opcode.iconst_1:
                case Opcode.iconst_2:
                case Opcode.iconst_3:
                case Opcode.iconst_4:
                case Opcode.iconst_5:
                case Opcode.idiv:
                case Opcode.iload_0:
                case Opcode.iload_1:
                case Opcode.iload_2:
                case Opcode.iload_3:
                case Opcode.imul:
                case Opcode.ineg:
                case Opcode.ior:
                case Opcode.irem:
                case Opcode.ireturn:
                case Opcode.ishl:
                case Opcode.ishr:
                case Opcode.istore_0:
                case Opcode.istore_1:
                case Opcode.istore_2:
                case Opcode.istore_3:
                case Opcode.isub:
                case Opcode.iushr:
                case Opcode.ixor:
                case Opcode.l2d:
                case Opcode.l2f:
                case Opcode.l2i:
                case Opcode.ladd:
                case Opcode.laload:
                case Opcode.land:
                case Opcode.lastore:
                case Opcode.lcmp:
                case Opcode.lconst_0:
                case Opcode.lconst_1:
                case Opcode.ldiv:
                case Opcode.lload_0:
                case Opcode.lload_1:
                case Opcode.lload_2:
                case Opcode.lload_3:
                case Opcode.lmul:
                case Opcode.lneg:
                case Opcode.lor:
                case Opcode.lrem:
                case Opcode.lreturn:
                case Opcode.lshl:
                case Opcode.lshr:
                case Opcode.lstore_0:
                case Opcode.lstore_1:
                case Opcode.lstore_2:
                case Opcode.lstore_3:
                case Opcode.lsub:
                case Opcode.lushr:
                case Opcode.lxor:
                case Opcode.monitorenter:
                case Opcode.monitorexit:
                case Opcode.nop:
                case Opcode.pop:
                case Opcode.pop2:
                case Opcode.returnvoid:
                case Opcode.saload:
                case Opcode.sastore:
                case Opcode.swap:
                    // No operands
                    return new Instruction(code);

                case Opcode.aload:
                case Opcode.astore:
                case Opcode.bipush:
                case Opcode.dload:
                case Opcode.dstore:
                case Opcode.fload:
                case Opcode.fstore:
                case Opcode.iload:
                case Opcode.istore:
                case Opcode.lload:
                case Opcode.lstore:
                case Opcode.ret:
                    // Single byte operand
                    return new Instruction(code, (int)data[ptr++]);

                case Opcode.goto_short:
                case Opcode.if_acmpeq:
                case Opcode.if_acmpne:
                case Opcode.if_icmpeq:
                case Opcode.if_icmpge:
                case Opcode.if_icmpgt:
                case Opcode.if_icmple:
                case Opcode.if_icmplt:
                case Opcode.if_icmpne:
                case Opcode.ifeq:
                case Opcode.ifge:
                case Opcode.ifgt:
                case Opcode.ifle:
                case Opcode.iflt:
                case Opcode.ifne:
                case Opcode.ifnonnull:
                case Opcode.ifnull:
                case Opcode.jsr:
                case Opcode.sipush:
                    return new Instruction(code, (data[ptr++] << 8) | data[ptr++]);

                case Opcode.goto_wide:
                case Opcode.jsr_w:
                    return new Instruction(code,  (data[ptr++] << 24) | (data[ptr++] << 16) | (data[ptr++] << 8) | data[ptr++]);

                case Opcode.anewarray:
                case Opcode.checkcast:
                case Opcode.instanceof:
                    {
                        // Type operand
                        int idx = (data[ptr++] << 8) | data[ptr++];
                        var type = ((ConstantPool.ConstantClass)file.ConstantPool[idx]).Name;
                        return new Instruction(code, type);
                    }

                case Opcode.getfield:
                case Opcode.getstatic:
                case Opcode.putfield:
                case Opcode.putstatic:
                    {
                        // Field
                        int idx = (data[ptr++] << 8) | data[ptr++];
                        var type = ((ConstantPool.ConstantFieldRef)file.ConstantPool[idx]);
                        return new Instruction(code, type);
                    }

                case Opcode.invokedynamic:
                    {
                        // Dynamic method
                        int idx = (data[ptr++] << 8) | data[ptr++];

                        if (data[ptr++] != 0 || data[ptr++] != 0)
                        {
                            // Apparently we have to skip 2 '0' bytes
                            throw new IncorrectClassFileException("Expected 2x '0' bytes after dynamic invoke");
                        }
                        
                        var type = ((ConstantPool.ConstantInvokeDynamic)file.ConstantPool[idx]);
                        return new Instruction(code, type);
                    }

                case Opcode.invokeinterface:
                    {
                        // Invoke on interface
                        int idx = (data[ptr++] << 8) | data[ptr++];
                        int count = data[ptr++];

                        if (data[ptr++] != 0)
                        {
                            // Apparently we have to skip a '0' bytes
                            throw new IncorrectClassFileException("Expected a '0' bytes after invoke interface");
                        }
                        
                        var type = ((ConstantPool.ConstantInterfaceMethodRef)file.ConstantPool[idx]);
                        return new Instruction(code, type, count);
                    }

                case Opcode.invokespecial:
                case Opcode.invokestatic:
                case Opcode.invokevirtual:
                    {
                        // Dynamic method
                        int idx = (data[ptr++] << 8) | data[ptr++];
                        var type = ((ConstantPool.ConstantMethodRef)file.ConstantPool[idx]);
                        return new Instruction(code,  type);
                    }

                case Opcode.iinc: // local[idx] += value;
                    {
                        int idx = data[ptr++]; // Local index
                        int value= data[ptr++]; // Value
                        return new Instruction(code, idx, value);
                    }

                case Opcode.multianewarray:
                    {
                        int idx = (data[ptr++] << 8) | data[ptr++];
                        var type = ((ConstantPool.ConstantClass)file.ConstantPool[idx]).Name;
                        int value = data[ptr++]; // Value
                        return new Instruction(code, type, value);
                    }

                case Opcode.newobject:
                    {
                        int idx = (data[ptr++] << 8) | data[ptr++];
                        var type = ((ConstantPool.ConstantClass)file.ConstantPool[idx]).Name;
                        return new Instruction(code, type);
                    }

                case Opcode.newarray:
                    {
                        IFieldType type;
                        var atype = data[ptr++];
                        switch (atype)
                        {
                            case 4: type = new BasicType(BasicTypeKind.Boolean); break;
                            case 5: type = new BasicType(BasicTypeKind.Char); break;
                            case 6: type = new BasicType(BasicTypeKind.Float); break;
                            case 7: type = new BasicType(BasicTypeKind.Double); break;
                            case 8: type = new BasicType(BasicTypeKind.Byte); break;
                            case 9: type = new BasicType(BasicTypeKind.Short); break;
                            case 10: type = new BasicType(BasicTypeKind.Int); break;
                            case 11: type = new BasicType(BasicTypeKind.Long); break;
                            default:
                                throw new IncorrectClassFileException("Unexpected array type found");
                        } 
                        return new Instruction(code, type);
                    }

                case Opcode.ldc:
                    return new Instruction(code, ((IConstantValue)file.ConstantPool[data[ptr++]]).Value);

                case Opcode.ldc_w:
                case Opcode.ldc2_w:
                    return new Instruction(code, ((IConstantValue)file.ConstantPool[(data[ptr++] << 8) | data[ptr++]]).Value);


                case Opcode.lookupswitch:
                    {
                        while ((ptr % 4) != 0) { ++ptr; } // padding

                        var defaultValue = (int)(data[ptr++] << 24) | (int)(data[ptr++] << 16) | (int)(data[ptr++] << 8) | (int)data[ptr++];
                        var nrPairs = (int)(data[ptr++] << 24) | (int)(data[ptr++] << 16) | (int)(data[ptr++] << 8) | (int)data[ptr++];

                        KeyValuePair<int, int>[] jumpTable = new KeyValuePair<int, int>[nrPairs];
                        for (int i = 0; i < nrPairs; ++i)
                        {
                            var match = (int)(data[ptr++] << 24) | (int)(data[ptr++] << 16) | (int)(data[ptr++] << 8) | (int)data[ptr++];
                            var offset = (int)(data[ptr++] << 24) | (int)(data[ptr++] << 16) | (int)(data[ptr++] << 8) | (int)data[ptr++];
                            
                            jumpTable[i] = new KeyValuePair<int, int>(match, offset);
                        }

                        return new Instruction(code, defaultValue, jumpTable);
                    }
                    
                case Opcode.tableswitch:
                    {
                        while ((ptr % 4) != 0) { ++ptr; } // padding

                        var defaultValue = (int)(data[ptr++] << 24) | (int)(data[ptr++] << 16) | (int)(data[ptr++] << 8) | (int)data[ptr++];
                        var lowValue = (int)(data[ptr++] << 24) | (int)(data[ptr++] << 16) | (int)(data[ptr++] << 8) | (int)data[ptr++];
                        var highValue = (int)(data[ptr++] << 24) | (int)(data[ptr++] << 16) | (int)(data[ptr++] << 8) | (int)data[ptr++];

                        KeyValuePair<int, int>[] jumpTable = new KeyValuePair<int, int>[highValue - lowValue + 1];
                        for (int i = 0; i < jumpTable.Length; ++i)
                        {
                            var offset = (int)(data[ptr++] << 24) | (int)(data[ptr++] << 16) | (int)(data[ptr++] << 8) | (int)data[ptr++];
                            jumpTable[i] = new KeyValuePair<int, int>(lowValue + i, offset);
                        }

                        return new Instruction(code, defaultValue, jumpTable);
                    }

                case Opcode.wide:
                    var opcode2 = (Opcode)data[ptr++];
                    switch (opcode2)
                    {
                        case Opcode.iload:
                        case Opcode.fload:
                        case Opcode.aload:
                        case Opcode.lload:
                        case Opcode.dload:
                        case Opcode.astore:
                        case Opcode.dstore:
                        case Opcode.fstore:
                        case Opcode.istore:
                        case Opcode.lstore:
                        case Opcode.ret:
                            // Single byte operand -> 2 bytes.
                            return new Instruction(code, (int)((data[ptr++] << 8) | data[ptr++]));
                        
                        case Opcode.iinc:
                            {
                                int idx = (data[ptr++] << 8) | data[ptr++]; // Local index
                                int value = (data[ptr++] << 8) | data[ptr++]; // Value
                                return new Instruction(code, idx, value);
                            }

                        default:
                            throw new IncorrectProgramException("Incorrect 'wide' opcode");
                    }

                default:
                    throw new IncorrectProgramException("Incorrect 'wide' opcode");
            }
        }
    }
}
