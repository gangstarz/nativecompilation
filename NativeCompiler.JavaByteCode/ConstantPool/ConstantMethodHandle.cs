﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public enum MethodHandleKind : byte
    {
        GetField = 1,
        GetStatic = 2,
        PutField = 3,
        PutStatic = 4,
        InvokeVirtual = 5,
        InvokeStatic = 6,
        InvokeSpecial = 7,
        NewInvokeSpecial = 8,
        InvokeInterface = 9
    }

    public class ConstantMethodHandle : ConstantPoolInfo
    {
        public ConstantPoolInfo Info;

        public MethodHandleKind ReferenceKind;
        private ushort ReferenceIndex;

        public override void Read(BigEndianReader reader)
        {
            ReferenceKind = (MethodHandleKind)reader.ReadByte();
            ReferenceIndex = reader.ReadUInt16();
        }

        public override void Load(ClassFile file)
        {
            // TODO FIXME
            Info = file.ConstantPool[ReferenceIndex];
        }
    }
}
