﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public class ConstantDouble : ConstantPoolInfo, IConstantValue
    {
        public double Constant;

        public override void Read(BigEndianReader reader)
        {
            Constant = reader.ReadDouble();
        }

        public override void Load(ClassFile file)
        {
        }

        public object Value
        {
            get { return Constant; }
        }
    }
}
