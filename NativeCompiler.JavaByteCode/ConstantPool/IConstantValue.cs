﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    // TODO FIXME: Types, methods? See LDC: http://docs.oracle.com/javase/specs/jvms/se7/html/jvms-6.html#jvms-6.5
    public interface IConstantValue
    {
        object Value { get; }
    }
}
