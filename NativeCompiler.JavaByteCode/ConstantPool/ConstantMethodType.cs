﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public class ConstantMethodType : ConstantPoolInfo
    {
        public MethodDescriptor MethodDescriptor { get; set; }

        private ushort descriptorIndex;

        public override void Read(BigEndianReader reader)
        {
            descriptorIndex = reader.ReadUInt16();
        }

        public override void Load(ClassFile file)
        {
            var cp = file.ConstantPool[descriptorIndex] as ConstantUTF8;
            if (cp != null)
            {
                this.MethodDescriptor = FieldTypeParser.ParseMethodName(cp.Constant);
            }
            else
            {
                throw new IncorrectClassFileException("Expected UTF8 string");
            }
        }
    }
}
