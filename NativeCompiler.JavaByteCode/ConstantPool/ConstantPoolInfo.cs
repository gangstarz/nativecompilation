﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public abstract class ConstantPoolInfo
    {
        public abstract void Read(BigEndianReader reader);
        public abstract void Load(ClassFile file);
    }
}
