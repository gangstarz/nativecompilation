﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public class ConstantNameAndType : ConstantPoolInfo
    {
        public IFieldType Name { get; set; }
        public IFieldType Descriptor { get; set; }

        private ushort nameIndex;
        private ushort descriptorIndex;

        public override void Read(BigEndianReader reader)
        {
            nameIndex = reader.ReadUInt16();
            descriptorIndex = reader.ReadUInt16();
        }

        public override void Load(ClassFile file)
        {
            var cp = file.ConstantPool[nameIndex] as ConstantUTF8;
            if (cp !=null)
            {
                this.Name = new ClassType(FieldTypeParser.ParseMultiPartIdentifier(cp.Constant));
            }
            else
            {
                throw new IncorrectClassFileException("Expected name");
            }

            var cp2 = file.ConstantPool[descriptorIndex] as ConstantUTF8;
            if (cp2 != null)
            {
                if (cp2.Constant.StartsWith("("))
                {
                    this.Descriptor = FieldTypeParser.ParseMethodName(cp2.Constant);
                }
                else
                {
                    this.Descriptor = FieldTypeParser.ParseDescriptorSignature(cp2.Constant);
                }
            }
            else
            {
                throw new IncorrectClassFileException("Expected name and type");
            }
        }
    }
}
