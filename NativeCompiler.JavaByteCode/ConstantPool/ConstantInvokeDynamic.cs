﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public class ConstantInvokeDynamic : ConstantPoolInfo
    {
        private ushort bootstrapMethodAttributeIndex;
        private ushort nameAndIndexType;

        public override void Read(BigEndianReader reader)
        {
            bootstrapMethodAttributeIndex = reader.ReadUInt16();
            nameAndIndexType = reader.ReadUInt16();
        }

        public override void Load(ClassFile file)
        {
        }
    }
}
