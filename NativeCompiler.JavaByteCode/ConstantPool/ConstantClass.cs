﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public class ConstantClass : ConstantPoolInfo
    {
        public IFieldType Name { get; set; }

        private ushort nameIndex;

        public override void Read(BigEndianReader reader)
        {
            this.nameIndex = reader.ReadUInt16();
        }

        public override void Load(ClassFile file)
        {
            var cp = file.ConstantPool[nameIndex] as ConstantUTF8;

            if (cp == null)
            {
                throw new IncorrectClassFileException("Expected class name");
            }

            // Can be an array or a multi-part object identifier
            var name = cp.Constant;
            if (name.StartsWith("["))
            {
                this.Name = FieldTypeParser.ParseDescriptorSignature(name);
            }
            else
            {
                this.Name = new ClassType(FieldTypeParser.ParseMultiPartIdentifier(name));
            }
        }
    }
}
