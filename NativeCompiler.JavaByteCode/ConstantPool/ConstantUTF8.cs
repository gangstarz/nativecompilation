﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public class ConstantUTF8 : ConstantPoolInfo
    {
        public string Constant;

        public override void Read(BigEndianReader reader)
        {
            var length = reader.ReadUInt16();
            var buf = reader.ReadBytes(length);

            this.Constant = Parse(buf);
        }

        public override void Load(ClassFile file)
        {
        }

        internal static string Parse(byte[] buf)
        {
            // Obviously it's not standard UTF8... that would just makes things easy.
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < buf.Length; ++i)
            {
                byte b1 = buf[i];
                if ((b1 & 0x80) == 0)
                {
                    sb.Append((char)(b1 & 0x7F));
                }
                else if ((b1 & 0xE0) == 0xC0)
                {
                    var b2 = buf[++i];
                    if ((b2 & 0xC0) != 0x80) { throw new NotSupportedException("Incorrect modified-UTF8 token"); }

                    sb.Append((char)(((b1 & 0x1f) << 6) | (b2 & 0x3f)));
                }
                else if ((b1 & 0xF0) == 0xE0)
                {
                    var b2 = buf[++i];
                    if ((b2 & 0xC0) != 0x80) { throw new NotSupportedException("Incorrect modified-UTF8 token"); }
                    var b3 = buf[++i];
                    if ((b3 & 0xC0) != 0x80) { throw new NotSupportedException("Incorrect modified-UTF8 token"); }

                    sb.Append((char)(((b1 & 0xf) << 12) | ((b2 & 0x3f) << 6) | (b3 & 0x3f)));
                }
                else
                {
                    throw new NotSupportedException("Incorrect modified-UTF8 token");
                }
            }

            return sb.ToString();
        }
    }
}
