﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public class ConstantFloat : ConstantPoolInfo, IConstantValue
    {
        public float Constant;

        public override void Read(BigEndianReader reader)
        {
            Constant = reader.ReadSingle();
        }

        public override void Load(ClassFile file)
        {
        }

        public object Value
        {
            get { return Constant; }
        }
    }
}
