﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public class ConstantLong : ConstantPoolInfo, IConstantValue
    {
        public ulong Constant;

        public override void Read(BigEndianReader reader)
        {
            Constant = reader.ReadUInt64();
        }

        public override void Load(ClassFile file)
        {
        }

        public object Value
        {
            get { return Constant; }
        }
    }
}
