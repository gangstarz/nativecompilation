﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public class ConstantInterfaceMethodRef : ConstantPoolInfo
    {
        public ConstantClass ClassType { get; private set; }
        public ConstantNameAndType NameAndType { get; private set; }

        private ushort classIndex;
        private ushort nameAndTypeIndex;

        public override void Read(BigEndianReader reader)
        {
            this.classIndex = reader.ReadUInt16();
            this.nameAndTypeIndex = reader.ReadUInt16();
        }

        public override void Load(ClassFile file)
        {
            var cp = file.ConstantPool[classIndex];
            if (cp is ConstantClass)
            {
                this.ClassType = (ConstantClass)cp;
            }
            else
            {
                throw new IncorrectClassFileException("Expected class name");
            }

            var cp2 = file.ConstantPool[nameAndTypeIndex];
            if (cp2 is ConstantNameAndType)
            {
                this.NameAndType = (ConstantNameAndType)cp2;
            }
            else
            {
                throw new IncorrectClassFileException("Expected name and type");
            }
        }
    }
}
