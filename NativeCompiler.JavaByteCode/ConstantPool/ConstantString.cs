﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.JavaByteCode.ConstantPool
{
    public class ConstantString : ConstantPoolInfo, IConstantValue
    {
        public string Constant { get; set; }

        private ushort stringIndex;

        public override void Read(BigEndianReader reader)
        {
            this.stringIndex = reader.ReadUInt16();
        }

        public override void Load(ClassFile file)
        {
            var cp = file.ConstantPool[this.stringIndex] as ConstantUTF8;
            if (cp !=null)
            {
                Constant = cp.Constant;
            }
            else
            {
                throw new IncorrectClassFileException("Constant pool should have a string");
            }
        }

        public object Value
        {
            get { return Constant; }
        }
    }
}
