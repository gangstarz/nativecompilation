﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.JavaByteCode.Attributes;
using NativeCompiler.JavaByteCode.ConstantPool;
using NativeCompiler.JavaByteCode.Reflection;

namespace NativeCompiler.JavaByteCode
{
    public class ClassFile
    {
        public ClassFile(Stream fileStream)
        {
            this.reader = new BigEndianReader(fileStream);
        }

        public BigEndianReader reader;

        public List<ConstantPoolInfo> ConstantPool;
        public ushort AccessFlags;
        public ushort ThisClass;
        public ushort SuperClass;
        public ushort[] Interfaces;
        public FieldInfo[] Fields;
        public MethodInfo[] Methods;
        public IAttributeInfo[] Attributes;

        public void Parse()
        {
            uint magic = reader.ReadUInt32();
            if (magic != 0xCAFEBABE)
            {
                throw new NotSupportedException("Expected class file magic number 0xCAFEBABE");
            }

            var minor = reader.ReadInt16();
            var major = reader.ReadInt16();

            var constantPoolCount = reader.ReadInt16() - 1;
            ConstantPool = new List<ConstantPoolInfo>();
            
            ConstantPool.Add(null); // Apparently this is because of legacy reasons
            for (int i = 0; i < constantPoolCount; ++i)
            {
                ConstantPoolInfo cpi = null;

                byte id = reader.ReadByte();
                switch (id)
                {
                    case 7: cpi = new ConstantClass(); break;
                    case 9: cpi = new ConstantFieldRef(); break;
                    case 10: cpi = new ConstantMethodRef(); break;
                    case 11: cpi = new ConstantInterfaceMethodRef(); break;
                    case 8: cpi = new ConstantString(); break;
                    case 3: cpi = new ConstantInteger(); break;
                    case 4: cpi = new ConstantFloat(); break;
                    case 5: cpi = new ConstantLong(); break;
                    case 6: cpi = new ConstantDouble(); break;
                    case 12: cpi = new ConstantNameAndType(); break;
                    case 1: cpi = new ConstantUTF8(); break;
                    case 15: cpi = new ConstantMethodHandle(); break;
                    case 16: cpi = new ConstantMethodType(); break;
                    case 18: cpi = new ConstantInvokeDynamic(); break;
                    default:
                        throw new NotSupportedException("Unknown constant type " + id);
                }

                cpi.Read(reader);

                ConstantPool.Add(cpi);
            }

            for (int i = 0; i < constantPoolCount; ++i)
            {
                ConstantPool[i+1].Load(this);
            }
        
            this.AccessFlags = reader.ReadUInt16();
            this.ThisClass = reader.ReadUInt16();
            this.SuperClass = reader.ReadUInt16();

            var interfacesCount = reader.ReadUInt16();
            this.Interfaces = new ushort[interfacesCount];
            for (int i = 0; i < interfacesCount; ++i)
            {
                Interfaces[i] = reader.ReadUInt16();
            }

            var fieldsCount = reader.ReadUInt16();
            this.Fields = new FieldInfo[fieldsCount];
            for (int i = 0; i < fieldsCount; ++i)
            {
                Fields[i] = new FieldInfo();
                Fields[i].Read(reader, this);
            }

            var methodsCount = reader.ReadUInt16();
            this.Methods = new MethodInfo[methodsCount];
            for (int i = 0; i < methodsCount; ++i)
            {
                Methods[i] = new MethodInfo();
                Methods[i].Read(reader, this);
            }

            var attributesCount = reader.ReadUInt16();
            this.Attributes = new IAttributeInfo[attributesCount];
            for (int i = 0; i < attributesCount; ++i)
            {
                Attributes[i] = AttributeInfoBuilder.Read(reader, this);
            }

            if (reader.BaseStream.Position != reader.BaseStream.Length)
            {
                throw new IncorrectClassFileException("Expected to be at EOF; trailing data in class file found");
            }
        }
    }
}
