# Recompilation #

Implementing a recompiler means you need to understand how some of the things work in .NET and the decompiler.

All types and methods you need for recompilation of your application are available after the (flow) analysis phase of the decompiler. The decompiler normalizes the stack to zero by introducing temporary variables, which basically means that each block you will be recompiling has a stack depth of 0 on both entry and all exit points of the method. In other words, you will never encounter statements like 'a=b=c=2;', because they have been rewritten to something like 'tmp=2; a=tmp; b=tmp; c=tmp;'. Especially in combination with branches, this makes life much easier.

## Type system ##

Unlike for example Java, not everything is an object in .NET. There are both value types and reference types. Reference types are objects that live on the heap and are destroyed by the Garbage Collector (GC). Value types live on the stack and are copied-on-write. Copy on write never throws an exception; the 'assign' operator cannot be overwritten. Basically all value types in the value type are copied, and pointers are kept the way they are. (The C++ equivalent would be putting an object on the stack).

Basic types (all value types) are:
- byte, sbyte (8 bit) 
- short, ushort (16 bit)
- int, uint (32 bit)
- long, ulong (64 bit)
- float, double (32/64 bit floating point)
- decimal (128 bit fixed point)

Special types:
- string (UTF-16 encoded strings, reference type)
- Type (RTTI, reference type)
- Object (base object, reference type)
- ValueType (value type base object, value type)

Types can be altered in some ways:
- native pointer (void *); used for unmanaged interop
- reference of value-type (to pass an object by reference; note this is different from boxing, it's similar to C++ pass-by-reference 'foo &')
- 1-dimensional fixed-size arrays (foo[]). Arrays are always reference types.
- multi-dimensional fixed-size arrays (foo[dim1, dim2, dim3]). Multi-dimensional arrays are always reference types.

All reference types derive from Object, all value types from ValueType (which strangely inherits from Object). Casting a value type to Object is possible, and does a 'boxing' operation. Boxing means wrapping a value type inside an object container and putting it on the heap. Unboxing happens when this object is 'cast' back to a value type, which means the wrap is removed again, changing the contents of the box back to a value type. For example:

- object[] is an array of reference types. If you put an 'int' in here, it is first boxed, then put in the object array.
- int[] is an array of value types. In other words, it's represented in memory as a single list of bytes.

In the recompiler scope, value / reference types is important for assignments, method passing behavior (copy or pass by reference).

## Calling and executing methods ##

The first argument on the stack (argument 0) is usually 'this', unless the method is static. 'callvirt' is used more frequently than 'call' in .NET, for the simple reason that 'callvirt' does an implicit null check and a virtual function call.

## Exception handling ##

The first step in the recompilation process is processing the exception handling clauses. There are 5 types of exception handler blocks in .NET that are recognized.

- Try blocks. Basically what 'can go wrong'.
- Catch blocks (pushes an exception object on the stack). Catches all objects of the specified type.
- Finally blocks. Handles all exceptions and exits normally.
- Filter blocks (push an exception object on the stack). Enters the handler if the filter succeeds. (VB.NET 'catch Exception when ...')
- Fault blocks. Handle all exceptions but do a normal exit.

Try/catch/finally is fairly standard behavior, so we won't describe these. Filter and fault blocks are somewhat special.

The exact behavior is described by the ECMA-335 standard (Note: C# has different behavior sometimes!). 

All Exception objects inherit from Object. On other words, all exceptions that are thrown are Objects and if no catch variable is specified, the object is pushed on the stack.

### Instructions and exceptions ###

Most instructions have boundary conditions in which they throw. These are the exceptions:

- OverflowException. Arithmetics instructions IF they are the checked version. For the first version the recompiler, this is ignored; this doesn't really happen in practice.
- NullReferenceException (if array is null, unbox, call, ldlen, ldind, stind)
- IndexOutOfRangeException (ldlen, ldind, stind)
- InvalidCastException (castclass, unbox with incorrect type as rhs)

Relevant links, see: http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.add_ovf(v=vs.110).aspx (and other operations).

for example:

	int id = foo[12];

is the same as:

	if (foo == null) { throw new NullReferenceException(); }
	if (foo.Length < 12) { throw new IndexOutOfRangeException(); }
	int id = foo[12];

Static flow analysis can determine in most cases if you need these checks or not; for example, if all paths initialize the variable, the null reference exception will never occur. This is not implemented yet.

### Stack behavior ###

There are numerous ways to enter an exception handler block, always from the start. This start offset determines the initial stack, and when an exception is thrown, the stack is rolled back to this point. The only way to exit an exception block is by invoking a 'leave' / 'endfilter' / 'endfinally' operation, which rolls back the operation to the initial stack size before executing a jump. In other words, all stuff on the stack is removed before issuing the jump.

Note that 'leave' invokes the 'finally' block(s) prior to executing the jump.

### Preprocessing fault blocks ###

While 'fault' blocks seem to map on 'catch [object] { }', the best way to describe this is by handling it in a finally:

	bool hasError = true;
	try 
	{ 
		[...] 
		hasError = false;
	}
	catch (ArgumentException e1)
	{ 
		[...]
  	}
	catch (NullReferenceException e2) 
	{
		[...]
	}
	// a catch { } here would not handle exceptions that are already handled, so that's not correct
	finally 
	{
		if (hasError) 
		{
			// [... Handle fault block ...]
		}
		[... rest of the finally ...]
	}

### Preprocessing filter blocks ###

Filter blocks are especially hard to map to something else. The best way would probably be to introduce a new exception block:

	// The extra 'try' is necessary to ensure other exception handlers can handle the re-throw. Each filter introduces a 'try'/'catch'.
	try
	{
		try 
		{ 
			[...] 
		}
		catch (ArgumentException e1)
		{ 
			[...]
	  	}
		catch (Exception e) // the filter
		{
			if (e.Foo == bar) 
			{
				// handle exception: the filter part.
			}
			else
			{
				throw;
			}
		}
	} 
	catch (NullReferenceException e2) 
	{
		[...]
	}
	// a catch { } here would not handle exceptions that are already handled, so that's not correct
	finally 
	{
		[... finally block ...]
	}

## Recompiling statements ##

The analysis phase already figured out which blocks, statements and methods have to be recompiled. If they aren't there, they are probably not used. Also, if a statement is invoked, exception behavior is ordered as it should be. 

You might want to give all types a new name that doesn't conflict with the naming rules of the target language. All generics are already expanded (e.g. you won't encounter 'Dictionary<T,U>' , but you will encounter multiple types of that dictionary like 'Dictionary<string, int>').

Most languages have the same syntax for expressions. The "ExpressionRewriter" handles this syntax, mapping the IL of expressions to something that's easy to handle. 
