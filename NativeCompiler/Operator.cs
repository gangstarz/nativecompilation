﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler
{
    public class Operator : StackOperationBase
    {
        public Operator(IStackOperation orig, string format, int precedence)
        {
            this.orig = orig;
            this.format = format;
            this.precedence = precedence;
        }

        private IStackOperation orig;
        private string format;
        private int precedence;

        public IStackOperation Original { get { return orig;} }

        public override Type ReturnType { get { return orig.ReturnType; } set { orig.ReturnType = value; } }

        public override void Execute(ILStack stack)
        {
        }

        public override IEnumerable<IStackOperation> Children { get { return orig.Children; } }

        public override IStackOperation Rewrite(StackRewriterBase rewriter)
        {
            return this;
        }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return default(T); }

        public override int DeltaStackDepth
        {
            get { return orig.DeltaStackDepth; }
        }

        public override void Write(System.IO.TextWriter sb)
        {
            var ar = orig.Children.Reverse().ToArray();
            int idx = 0;
            foreach (var ch in format)
            {
                if (ch == 'X')
                {
                    Operator op = ar[idx] as Operator;
                    if (op != null)
                    {
                        if (op.precedence >= precedence)
                        {
                            sb.Write('(');
                            op.Write(sb);
                            sb.Write(')');
                        }
                        else
                        {
                            op.Write(sb);
                        }
                    }
                    else
                    {
                        sb.Write(ar[idx].ToString());
                    }
                    ++idx;
                }
                else
                {
                    sb.Write(ch);
                }
            }
        }

        public override string ToString()
        {
            System.IO.StringWriter sb = new System.IO.StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }

}
