﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.PortableExecutable
{
    [Flags]
    public enum DllCharacteristics : ushort
    {
        DYNAMIC_BASE = 0x0040,		    // DLL can be relocated at load time.
        FORCE_INTEGRITY = 0x0080,		// Code Integrity checks are enforced.
        NX_COMPAT = 0x0100,		        // Image is NX compatible.
        NO_ISOLATION = 0x0200,		    // Isolation aware, but do not isolate the image.
        NO_SEH = 0x0400,		        // Does not use structured exception (SE) handling. No SE handler may be called in this image.
        NO_BIND = 0x0800,		        // Do not bind the image.
        WDM_DRIVER = 0x2000,		    // A WDM driver.
        TERMINAL_SERVER_AWARE = 0x8000	// Terminal Server aware.
    }

}
