﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.PortableExecutable
{
    [Flags]
    public enum ImageFileCharacteristics : ushort
    {
        RELOCS_STRIPPED = 0x0001,           // File doesn't contain relocations
        EXECUTABLE_IMAGE = 0x0002,          // File is executable and can be run
        LINE_NUMS_STRIPPED = 0x0004,        // COFF line numbers removed
        LOCAL_SYMS_STRIPPED = 0x0008,       // COFF symbol table entries for local symbols have been removed; deprecated; should be 0
        AGGRESSIVE_WS_TRIM = 0x0010,        // Obsolete; aggressively trim working set
        LARGE_ADDRESS_AWARE = 0x0020,       // Application can handle > 2 GB addresses.
        BYTES_REVERSED_LO = 0x0080,         // Big Endian; deprecated; should be 0
        MACHINE_32BIT = 0x0100,             // Machine is based on a 32-bit-word architecture.
        DEBUG_STRIPPED = 0x0200,            // Debugging information is removed from the image file.
        REMOVABLE_RUN_FROM_SWAP = 0x0400,   // If the image is on removable media, fully load it and copy it to the swap file.
        NET_RUN_FROM_SWAP = 0x0800,         // If the image is on network media, fully load it and copy it to the swap file.
        SYSTEM = 0x1000,                    // System file
        DLL = 0x2000,                       // File is a DLL
        UP_SYSTEM_ONLY = 0x4000,            // The file should be run only on a uniprocessor machine.
        BYTES_REVERSED_HI = 0x8000           // Big Endian; deprecated; should be 0
    }
}
