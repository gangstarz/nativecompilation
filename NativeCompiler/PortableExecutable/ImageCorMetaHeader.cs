﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.PortableExecutable
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ImageCorMetaHeader
    {
        public UInt32 cb;
        public UInt16 MajorRuntimeVersion;
        public UInt16 MinorRuntimeVersion;
        public ImageDataDirectory MetaData;
        public ImageCorMetaHeaderFlags Flags;
        public UInt32 EntryPointToken; // If NATIVE_ENTRYPOINT is set -> this is an RVA token
        public ImageDataDirectory Resources;
        public ImageDataDirectory StrongNameSignature;
        public ImageDataDirectory CodeManagerTable;
        public ImageDataDirectory VTableFixups;
        public ImageDataDirectory ExportAddressTableJumps;
        public ImageDataDirectory ManagedNativeHeader;
    }

}
