﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.PortableExecutable
{
    [Flags]
    public enum ImageCorMetaHeaderFlags : uint
    {
        // COM+ Header entry point flags.
        ILONLY = 0x00000001,
        BIT32_REQUIRED = 0x00000002,
        IL_LIBRARY = 0x00000004,
        STRONGNAMESIGNED = 0x00000008,
        NATIVE_ENTRYPOINT = 0x00000010,
        TRACKDEBUGDATA = 0x00010000,

    };
}
