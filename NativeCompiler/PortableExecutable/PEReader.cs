﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.PortableExecutable
{
    public class PEReader
    {
        private ImageDosHeader dosHeader;
        private ImageFileHeader fileHeader;
        private ImageOptionalHeader optionalHeader;
        private ImageOptionalHeader32 optionalHeader32; // only filled if we have a 32-bit DLL/EXE
        private ImageOptionalHeader64 optionalHeader64; // only filled if we have a 64-bit DLL/EXE
        private ImageSectionHeader[] imageSectionHeaders;

        public PEReader(string filePath)
        {
            // Read in the DLL or EXE and get the timestamp
            using (FileStream stream = new FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                BinaryReader reader = new BinaryReader(stream);
                dosHeader = FromBinaryReader<ImageDosHeader>(reader);

                // Add 4 bytes to the offset
                stream.Seek(dosHeader.e_lfanew, SeekOrigin.Begin);
                UInt32 ntHeadersSignature = reader.ReadUInt32();

                if (ntHeadersSignature != 0x4550)
                {
                    throw new Exception("Expected 'PE' signature in header of PE header file.");
                }

                fileHeader = FromBinaryReader<ImageFileHeader>(reader);

                var startPos = reader.BaseStream.Position;

                ImageDataDirectory CLRRuntimeHeader = new ImageDataDirectory();
                long ImageBase;

                if (fileHeader.SizeOfOptionalHeader > 0)
                {
                    optionalHeader = FromBinaryReader<ImageOptionalHeader>(reader);

                    if (this.Is32BitHeader)
                    {
                        if (optionalHeader.Magic != 0x10b)
                        {
                            throw new Exception("Optional header has incorrect PE32 signature");
                        }

                        optionalHeader32 = FromBinaryReader<ImageOptionalHeader32>(reader);

                        CLRRuntimeHeader = optionalHeader32.CLRRuntimeHeader;
                        ImageBase = optionalHeader32.ImageBase; 
                    }
                    else
                    {
                        if (optionalHeader.Magic != 0x20b)
                        {
                            throw new Exception("Optional header has incorrect PE32+ signature");
                        }
                        optionalHeader64 = FromBinaryReader<ImageOptionalHeader64>(reader);

                        CLRRuntimeHeader = optionalHeader64.CLRRuntimeHeader;
                        ImageBase = optionalHeader32.ImageBase;
                    }
                }

                long position = (long)startPos + (long)fileHeader.SizeOfOptionalHeader;
                stream.Seek(position, SeekOrigin.Begin);

                imageSectionHeaders = new ImageSectionHeader[fileHeader.NumberOfSections];
                for (int headerNo = 0; headerNo < imageSectionHeaders.Length; ++headerNo)
                {
                    imageSectionHeaders[headerNo] = FromBinaryReader<ImageSectionHeader>(reader);
                }

                // Get CLR address
                for (int i = 0; i < imageSectionHeaders.Length; ++i)
                {
                    if (imageSectionHeaders[i].Section == ".text")
                    {
                        // The .text section contains the cormeta header and all CLR data

                        var addr = imageSectionHeaders[i].PointerToRawData + CLRRuntimeHeader.VirtualAddress - imageSectionHeaders[i].VirtualAddress;

                        stream.Seek(addr, SeekOrigin.Begin);
                        var clrHeader = FromBinaryReader<ImageCorMetaHeader>(reader);
                        Console.WriteLine("Version: {0}.{1}", clrHeader.MajorRuntimeVersion, clrHeader.MinorRuntimeVersion);

                        // TODO FIXME: Implement parser of CLR / COR
                        throw new NotImplementedException();
                    }
                }

            }
        }

        private static T FromBinaryReader<T>(BinaryReader reader)
        {
            // Read in a byte array
            byte[] bytes = reader.ReadBytes(Marshal.SizeOf(typeof(T)));

            // Pin the managed memory while, copy it out the data, then unpin it
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T theStructure = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            handle.Free();

            return theStructure;
        }

        public bool Is32BitHeader
        {
            get
            {
                return fileHeader.Characteristics.HasFlag(ImageFileCharacteristics.MACHINE_32BIT);
            }
        }

        public DateTime TimeStamp
        {
            get
            {
                // Timestamp is a date offset from 1970
                DateTime returnValue = new DateTime(1970, 1, 1, 0, 0, 0);

                // Add in the number of seconds since 1970/1/1
                returnValue = returnValue.AddSeconds(fileHeader.TimeDateStamp);
                // Adjust to local timezone
                returnValue += TimeZone.CurrentTimeZone.GetUtcOffset(returnValue);

                return returnValue;
            }
        }
    }
}
