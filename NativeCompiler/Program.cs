﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Decompiler;
using NativeCompiler.Decompiler.Blocks;
using NativeCompiler.Dependencies;
using NativeCompiler.Language;
using NativeCompiler.Language.AST;

namespace NativeCompiler
{
    class Program
    {
        static void Main(string[] args)
        {
            // We're going to analyze the test application:
            string loc = typeof(TestApplication.Program).Assembly.Location;

            // Method dependency scanner:
            Console.WriteLine("Step 1: Analyzing dependencies of all types and methods in the application...");

            var analyzer = new Analyzer(loc, true);
            // analyzer.AnalyzeDependencies();
            analyzer.AnalyzeSingleAssembly(typeof(TestApplication.Program).Assembly);

            // Code blocker:
            Console.WriteLine("Step 2: Generating code blocks for methods...");
            foreach (var val in analyzer.Results.Values)
            {
                foreach (var method in val.Methods.Values)
                {
                    method.GenerateBlocks();
                }
            }

            // Code analysis / IL block generator:
            Console.WriteLine("Step 3: Analyzing blocks; constructing statements...");
            foreach (var val in analyzer.Results.Values)
            {
                foreach (var method in val.Methods.Values)
                {
                    method.AnalyzeBlocks();
                }
            }

            Console.WriteLine("Step 4: 0-aligning blocks...");
            foreach (var val in analyzer.Results.Values)
            {
                foreach (var method in val.Methods.Values)
                {
                    method.PrepareDecompile();
                }
            }

            Console.WriteLine("Step 5: Emitting output in target languages...");

            // Generate files
            var outputPath = Path.GetDirectoryName(loc).TrimEnd('\\','/');
            while (outputPath.Contains("bin"))
            {
                outputPath = outputPath.Substring(0, outputPath.LastIndexOf("\\"));
            }
            outputPath = Path.GetFullPath(Path.Combine(outputPath, @"..\CPlusPlusTest\generated\"));
            try { Directory.CreateDirectory(outputPath); }
            catch { }

            Console.WriteLine("- Writing C++ to folder: {0}", outputPath);

            ASTBuilder builder = new ASTBuilder();
            var astTree = builder.Build(analyzer.Results.Values);

            Language.CPlusPlus.FileEmitter fe = new Language.CPlusPlus.FileEmitter(astTree, outputPath);
            fe.Process();
           
            Console.WriteLine("All done.");

            Console.ReadLine();
        }
    }
}
