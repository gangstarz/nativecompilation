﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler
{
    public class Code : StackOperationBase
    {
        public Code(IStackOperation orig, string format, bool doformat = true)
        {
            this.orig = orig;
            this.format = format;
            this.doformat = doformat;

            this.StackDepth = orig.StackDepth;
            this.Offset = orig.Offset;
        }

        private IStackOperation orig;
        private string format;
        private bool doformat;

        public override Type ReturnType { get { return orig.ReturnType; } set { orig.ReturnType = value; } }

        public override void Execute(ILStack stack)
        {
        }

        public override IEnumerable<IStackOperation> Children { get { return orig.Children; } }

        public override IStackOperation Rewrite(StackRewriterBase rewriter)
        {
            return this;
        }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return default(T); }
        
        public override int DeltaStackDepth
        {
            get { return orig.DeltaStackDepth; }
        }

        public override void Write(TextWriter sb)
        {
            var children = orig.Children.ToArray();

            if (doformat)
            {
                int state = 0;
                int idx = 0;
                foreach (var ch in format)
                {
                    if (ch == '{')
                    {
                        if (state == 0) { state = 1; idx = 0; }
                        else { state = 0; sb.Write('{'); }
                    }
                    else if (state == 1)
                    {
                        if (ch == '}')
                        {
                            children[idx].Write(sb);
                            state = 0;
                        }
                        else
                        {
                            idx = idx * 10 + ch - '0';
                        }
                    }
                    else
                    {
                        if (state == 2)
                        {
                            if (ch == '}')
                            {
                                sb.Write(ch);
                                state = 0;
                            }
                            else
                            {
                                throw new FormatException("Incorrect format specified");
                            }
                        }
                        else
                        {
                            if (ch == '}')
                            {
                                state = 2;
                            }
                            else
                            {
                                sb.Write(ch);
                            }
                        }
                    }
                }
                if (state != 0)
                {
                    throw new Exception();
                }
            }
            else
            {
                sb.Write(format);
            }
        }

        public override string ToString()
        {
            StringWriter sb = new StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }
}
