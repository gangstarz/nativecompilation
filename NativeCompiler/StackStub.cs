﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler
{
    public class StackStub : NoOperandsOperation
    {
        public StackStub(int idx)
        {
            this.index = idx;
        }

        private int index;

        public override void Execute(ILStack stack)
        {
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return -1; }
        }

        public override void Write(System.IO.TextWriter sb)
        {
            sb.Write("[[stack @ -{0}]]", index);
        }
    }

}
