﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler
{
    public static class ReflectionExtensions
    {
        /// <summary>
        /// Returns a friendly name for the method, for example: string Class.Foo(Tuple&lt;int, int&gt; par);.
        /// </summary>
        /// <param name="method">The method to get a friendly name for</param>
        /// <returns>A friendly name for the given method</returns>
        public static string FriendlyName(this MethodBase method, bool asDeclaration = true)
        {
            StringBuilder sb = new StringBuilder();
            var mi = method as MethodInfo;
            if (mi != null && asDeclaration)
            {
                if (mi.ReturnType == typeof(void))
                {
                    sb.Append("void ");
                }
                else
                {
                    sb.Append(mi.ReturnType.FriendlyName());
                    sb.Append(' ');
                }
            }

            sb.Append(method.DeclaringType.FriendlyName());
            sb.Append(".");
            sb.Append(method.Name);

            if (asDeclaration)
            {
                sb.Append('(');
                bool first = true;
                foreach (var param in method.GetParameters())
                {
                    if (param.IsOut)
                    {
                        sb.Append("out ");
                    }
                    if (first) { first = false; } else { sb.Append(", "); }
                    sb.Append(param.ParameterType.FriendlyName());
                }

                sb.Append(')');
            }
            return sb.ToString();
        }

        /// <summary>
        /// Returns a friendly name for the type, for example: string, int, float?, MyNamespace.Foo, MyNamespace.Bar&lt;Foo&gt;.
        /// </summary>
        /// <param name="type">The type to get a friendly name for</param>
        /// <returns>A friendly name for the given type</returns>
        public static string FriendlyName(this Type type)
        {
            if (type.IsArray)
            {
                int rank = type.GetArrayRank() - 1;
                return string.Format("{0}[{1}]", type.GetElementType().FriendlyName(), new string(',', rank));
            }
            else if (type.IsGenericTypeDefinition)
            {
                StringBuilder fn = new StringBuilder();
                if (type.Namespace != "System" && type.Namespace != "System.Collections.Generic" && type.Namespace != "System.Text" &&
                    !string.IsNullOrEmpty(type.Namespace))
                {
                    fn.Append(type.Namespace);
                    fn.Append('.');
                }
                int idx = type.Name.IndexOf('`');
                if (idx >= 0)
                {
                    fn.Append(type.Name.Substring(0, idx));
                }
                else
                {
                    fn.Append(type.Name);
                }

                idx = type.Name.IndexOf('`');
                if (idx >= 0)
                {
                    fn.Append('<');
                    // Not pretty but this seems to be the way to do it...

                    int numberParams = int.Parse(type.Name.Substring(type.Name.IndexOf('`') + 1));
                    for (int i = 1; i < numberParams; ++i)
                    {
                        fn.Append(',');
                    }
                    fn.Append('>');
                }

                return fn.ToString();
            }
            else if (type.IsGenericType)
            {
                var baseType = type.GetGenericTypeDefinition();
                var args = type.GetGenericArguments();
                if (baseType == typeof(Nullable<>))
                {
                    return args[0].FriendlyName() + "?";
                }
                else
                {
                    StringBuilder fn = new StringBuilder();
                    if (type.Namespace != "System" && type.Namespace != "System.Collections.Generic" && type.Namespace != "System.Text" &&
                        !string.IsNullOrEmpty(type.Namespace))
                    {
                        fn.Append(type.Namespace);
                        fn.Append('.');
                    }
                    int idx = type.Name.IndexOf('`');
                    if (idx >= 0)
                    {
                        fn.Append(type.Name.Substring(0, idx));

                        fn.Append('<');
                        fn.Append(string.Join(", ", args.Select((a) => (a.FriendlyName()))));
                        fn.Append('>');
                    }
                    else
                    {
                        fn.Append(type.Name);
                    }

                    return fn.ToString();
                }
            }
            else
            {
                if (type.Namespace != "System" && type.Namespace != "System.Collections.Generic" && type.Namespace != "System.Text")
                {
                    StringBuilder fn = new StringBuilder();
                    if (!string.IsNullOrEmpty(type.Namespace))
                    {
                        fn.Append(type.Namespace);
                        fn.Append('.');
                    }
                    fn.Append(type.Name);
                    return fn.ToString();
                }
                else
                {
                    switch (type.Name)
                    {
                        case "String": return "string";
                        case "Boolean": return "bool";
                        case "Byte": return "byte";
                        case "SByte": return "sbyte";
                        case "Char": return "char";
                        case "UInt16": return "ushort";
                        case "Int16": return "short";
                        case "UInt32": return "uint";
                        case "Int32": return "int";
                        case "UInt64": return "ulong";
                        case "Int64": return "long";
                        case "Decimal": return "decimal";
                        case "Single": return "float";
                        case "Double": return "float";
                        default: return type.Name;
                    }
                }
            }
        }

    }
}
