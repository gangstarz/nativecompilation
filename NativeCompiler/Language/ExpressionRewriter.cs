﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Language
{
    public class ExpressionRewriter : StackRewriterBase
    {
        private static CultureInfo enUS = CultureInfo.GetCultureInfo("en-US");

        public override IStackOperation Rewrite(Add item)
        {
            return new Operator(item, "X + X", 4);
        }

        public override IStackOperation Rewrite(And item)
        {
            return new Operator(item, "X & X", 8);
        }

        public override IStackOperation Rewrite(Ceq item)
        {
            return new Operator(item, "X == X", 7);
        }

        public override IStackOperation Rewrite(Cgt item)
        {
            return new Operator(item, "X > X", 6);
        }

        public override IStackOperation Rewrite(Clt item)
        {
            return new Operator(item, "X < X", 6);
        }

        public override IStackOperation Rewrite(Div item)
        {
            return new Operator(item, "X / X", 3);
        }

        public override IStackOperation Rewrite(LdcI4N item)
        {
            return new Operator(item, item.N.ToString(), 0);
        }

        public override IStackOperation Rewrite(LdcI8 item)
        {
            return new Operator(item, item.N.ToString(), 0);
        }

        public override IStackOperation Rewrite(LdcR4 item)
        {
            return new Operator(item, item.N.ToString(enUS), 0);
        }

        public override IStackOperation Rewrite(LdcR8 item)
        {
            return new Operator(item, item.N.ToString(enUS), 0);
        }

        public override IStackOperation Rewrite(LdElem item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(LdElemI1 item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(LdElemI2 item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(LdElemI4 item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(LdElemI8 item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(LdElemR4 item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(LdElemR8 item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(LdElemRef item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(LdElemU1 item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(LdElemU2 item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(LdElemU4 item)
        {
            return new Operator(item, "X[X]", 1);
        }

        public override IStackOperation Rewrite(Mul item)
        {
            return new Operator(item, "X * X", 3);
        }

        public override IStackOperation Rewrite(Neg item)
        {
            return new Operator(item, "-X", 2);
        }

        public override IStackOperation Rewrite(Not item)
        {
            return new Operator(item, "~X", 2);
        }

        public override IStackOperation Rewrite(Or item)
        {
            return new Operator(item, "X | X", 10);
        }

        public override IStackOperation Rewrite(Rem item)
        {
            return new Operator(item, "X % X", 3);
        }

        public override IStackOperation Rewrite(Shl item)
        {
            return new Operator(item, "X << X", 5);
        }

        public override IStackOperation Rewrite(Shr item)
        {
            return new Operator(item, "X >> X", 5);
        }

        public override IStackOperation Rewrite(ShrUn item)
        {
            return new Operator(item, "X >> X", 5);
        }

        public override IStackOperation Rewrite(Sub item)
        {
            return new Operator(item, "X - X", 4);
        }

        public override IStackOperation Rewrite(Xor item)
        {
            return new Operator(item, "X ^ X", 9);
        }
    }
}
