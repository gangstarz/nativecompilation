﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Dependencies;

namespace NativeCompiler.Language
{
    public class LanguageFactory
    {
        public void Write(string targetLanguage, string filename, MethodBase startupMethod, IEnumerable<DependentType> types)
        {
            ILanguageEmitter emitter = BuildEmitter(targetLanguage);
            emitter.Write(filename, startupMethod, types);
        }

        public IEnumerable<string> GetLanguages()
        {
            yield return "cs";
        }

        public ILanguageEmitter BuildEmitter(string targetLanguage)
        {
            switch (targetLanguage.ToLower())
            {
                case "cs": return new CSharp.CSEmitter();
                default:
                    throw new NotSupportedException("Target language is not supported.");
            }
        }
    }
}
