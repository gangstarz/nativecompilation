﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.CPlusPlus
{
    public static class ClassHelpers
    {
        public static bool IsDelegate(this Class clas)
        {
            if (clas != null)
            {
                var mcd = clas.BaseClasses.Select((a) => a.BaseDefinition).FirstOrDefault((a) => a != null && a.Name == "MulticastDelegate");
                if (mcd != null)
                {
                    var ns = mcd.Container as Namespace;
                    if (ns.Name == "System" && ns.Container != null && ((Namespace)ns.Container).Name == null)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool IsDelegate(this Callable method)
        {
            if (method is Method)
            {
                var container = method.Container;
                if (container.IsDelegate() &&
                    ((Method)method).Name == "Invoke")
                {
                    return true;
                }
            }

            return false;
        }
    }
}
