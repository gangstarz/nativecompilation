﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.CPlusPlus
{
    public class CppFileEmitter : IDisposable
    {
        public CppFileEmitter(string filename, Class clas)
        {
            this.sw = new CppStreamWriter(new System.IO.StreamWriter(filename, false));
            this.clas = clas;
        }

        private CppStreamWriter sw;
        private Class clas;

        public void Generate(CppStaticInitializers staticInitialization, CppRTTIEmitter rtti)
        {
            // Include header file
            var cur = NameBuilder.GetFilename(clas);
            sw.WriteLine("#include \"{0}.h\"", cur);

            if (!clas.InterfaceClass)
            {
                // Include files
                List<Tuple<Class, string>> classNames = clas.CollectDependencies().Select((a) => new Tuple<Class, string>(a, NameBuilder.GetFilename(a))).ToList();
                classNames.Sort((a, b) => a.Item2.CompareTo(b.Item2));
                foreach (var item in classNames)
                {
                    if (!(item.Item1 is GenericArgument) && item.Item2 != cur)
                    {
                        sw.WriteLine("#include \"{0}.h\"", item.Item2);
                    }
                }
            }

            // Go to namespace of class
            var curns = new List<string>();
            string currentClassName = NameBuilder.GetFilename(clas);
            int jdx = currentClassName.LastIndexOf('.');
            string ns2 = (jdx >= 0) ? currentClassName.Substring(0, jdx) : "";
            CppNamespaces.GotoNamespace(sw, curns, ns2);

            // Determine template prefix and class name; we need this later
            string className;

            if (clas.GenericArguments != null)
            {
                className = string.Format("{0}_Generic_{1}", clas.Name, clas.GenericArguments.Count);
            }
            else
            {
                className = clas.Name;
            }

            if (!clas.InterfaceClass)
            {
                // First handle static initialization:
                staticInitialization.WriteCppFile(sw, clas);

                // Emit default constructor
                sw.Write(className);
                sw.Write("::");
                sw.Write(className);
                sw.Write("(");
                if (clas.GenericArguments != null)
                {
                    for (int i = 0; i < clas.GenericArguments.Count; ++i)
                    {
                        if (i != 0)
                        {
                            sw.Write(", ");
                        }
                        sw.Write("System::Type *t");
                        sw.Write(i);
                    }
                }
                sw.WriteLine(")");
                sw.WriteLine("{");
                if (clas.GenericArguments != null)
                {
                    for (int i = 0; i < clas.GenericArguments.Count; ++i)
                    {
                        sw.WriteLine("this->_generic{0} = t{0};", i);
                    }
                }
                sw.WriteLine("}");
                sw.WriteLine();

                // Emit class constructors
                foreach (var ctor in clas.Constructors)
                {
                    if (ctor.Body != null)
                    {
                        sw.Write(className);
                        sw.Write("::");
                        sw.Write(className);
                        sw.Write("(int _shouldInit");
                        if (clas.GenericArguments != null)
                        {
                            for (int i = 0; i < clas.GenericArguments.Count; ++i)
                            {
                                sw.Write(", System::Type *t");
                                sw.Write(i);
                            }
                        }
                        for (int i = 0; i < ctor.Parameters.Count; ++i)
                        {
                            sw.Write(", ");
                            var type = NameBuilder.GetTypeName(ctor.Parameters[i].Type, clas);
                            sw.Write("{0} {1}", type, ctor.Parameters[i].Name);
                        }
                        sw.WriteLine(")");
                        sw.WriteLine("{");
                        
                        if (clas.GenericArguments != null)
                        {
                            for (int i = 0; i < clas.GenericArguments.Count; ++i)
                            {
                                sw.WriteLine("this->_generic{0} = t{0};", i);
                            }
                        }

                        sw.Write("_constructor(");
                        if (clas.GenericArguments != null)
                        {
                            for (int i = 0; i < clas.GenericArguments.Count; ++i)
                            {
                                if (i != 0)
                                {
                                    sw.Write(", ");
                                }
                                sw.Write("t");
                                sw.Write(i);
                            }
                        }
                        for (int i = 0; i < ctor.Parameters.Count; ++i)
                        {
                            if (i != 0 || clas.GenericArguments != null)
                            {
                                sw.Write(", ");
                            }

                            sw.Write("{0}", ctor.Parameters[i].Name);
                        }
                        sw.WriteLine(");");

                        sw.WriteLine("}");

                        sw.WriteLine();
                    }
                }

                // Emit constructor initialization methods
                // emit body
                foreach (var ctor in clas.Constructors)
                {
                    if (ctor.Body != null)
                    {
                        sw.Write("void ");
                        sw.Write(className);
                        sw.Write("::_constructor");
                        sw.Write("(");
                        if (clas.GenericArguments != null)
                        {
                            for (int i = 0; i < clas.GenericArguments.Count; ++i)
                            {
                                if (i != 0)
                                {
                                    sw.Write(", ");
                                }
                                sw.Write("System::Type *t");
                                sw.Write(i);
                            }
                        }
                        for (int i = 0; i < ctor.Parameters.Count; ++i)
                        {
                            if (i != 0 || clas.GenericArguments != null)
                            {
                                sw.Write(", ");
                            }

                            var type = NameBuilder.GetTypeName(ctor.Parameters[i].Type, clas);
                            sw.Write("{0} {1}", type, ctor.Parameters[i].Name);
                        }
                        sw.WriteLine(")");

                        CppBodyEmitter body = new CppBodyEmitter(sw, rtti, clas);
                        body.Emit(ctor.MainScope);

                        sw.WriteLine();
                    }
                }

                foreach (var meth in clas.Methods)
                {
                    if (meth.Body != null)
                    {
                        sw.Write(NameBuilder.GetTypeName(meth.ReturnType, clas));
                        sw.Write(" ");
                        sw.Write(className);
                        sw.Write("::");
                        sw.Write(meth.Name);
                        sw.Write("(");
                        for (int i = 0; i < meth.Parameters.Count; ++i)
                        {
                            if (i != 0)
                            {
                                sw.Write(", ");
                            }

                            var type = NameBuilder.GetTypeName(meth.Parameters[i].Type, clas);
                            sw.Write("{0} {1}", type, meth.Parameters[i].Name);
                        }
                        sw.WriteLine(")");

                        // emit body
                        CppBodyEmitter body = new CppBodyEmitter(sw, rtti, clas);
                        body.Emit(meth.MainScope);

                        sw.WriteLine();
                    }
                }

                if (!clas.ValueType)
                {
                    sw.WriteLine("void {0}::GC_MarkChildren(int flag)", className);
                    sw.WriteLine("{");
                    foreach (var field in clas.Fields)
                    {
                        if (field.Type.StorageType == MemoryType.ReferenceType)
                        {
                            sw.WriteLine("{0}->GC_Mark(flag);", field.Name);
                        }
                    }
                    sw.WriteLine("}");
                    sw.WriteLine();
                }

                sw.WriteLine("System::Type *{0}::GetType()", className);
                sw.WriteLine("{");
                sw.WriteLine("return ::RTTIMetaInfo::GetTypeById({0});", rtti.GetClassId(clas));
                sw.WriteLine("}");
                sw.WriteLine();
            }

            // Implement RTTI class
            string rttiClassName = string.Format("_{0}_RTTI", className);
            sw.Write("{0}::{0}() : ", rttiClassName);
            sw.WriteLine("RuntimeType({0}, {1}, RTTIMetaInfo::Instance().modules[{2}])", EscapeString(clas.Name), EscapeString(GetNamespace(clas)), rtti.GetModuleId(clas.Module));
            sw.WriteLine("{");
            sw.WriteLine("}");
            sw.WriteLine();
            sw.WriteLine("System::Object *{0}::CreateNewWithDefaultConstructor()", rttiClassName);
            sw.WriteLine("{");
            var defaultCtor = clas.Constructors.FirstOrDefault((a) => a.Parameters.Count == 0 && !a.IsStatic);
            if (defaultCtor != null && clas.GenericArguments == null && !clas.InterfaceClass && !clas.Methods.Any((a) => a.Body == null))
            {
                
                sw.WriteLine("return new {0}(1);", className);
            }
            else
            {
                sw.WriteLine("throw System::MissingMemberException();");
            }
            sw.WriteLine("}");
            sw.WriteLine();
            sw.WriteLine("System::Object *{0}::CreateNew(System::Array<Object*> parameters)", rttiClassName);
            sw.WriteLine("{");
            sw.WriteLine("throw System::NotImplementedException();");
            sw.WriteLine("}");
            sw.WriteLine();
            sw.WriteLine("{0}::~{0}()", rttiClassName);
            sw.WriteLine("{");
            sw.WriteLine("}");

            // Close namespaces
            for (int i = 0; i < curns.Count; ++i)
            {
                sw.WriteLine("}");
            }
            sw.WriteLine();
        }

        public void Dispose()
        {
            if (sw != null)
            {
                sw.Dispose();
                sw = null;
            }
        }

        private string GetNamespace(Class cl)
        {
            while (cl.Container != null && cl.Container is Class) { cl = (Class)cl.Container; }

            List<Namespace> ll = new List<Namespace>();
            Namespace last = cl.Container as Namespace;
            while (last != null)
            {
                if (last.Name != null)
                {
                    ll.Add(last);
                }
                last = last.Container as Namespace;
            }
            StringBuilder sb = new StringBuilder();
            for (int i = ll.Count - 1; i >= 0; --i)
            {
                sb.Append(ll[i].Name);
                if (i != 0)
                {
                    sb.Append(".");
                }
            }
            return sb.ToString();
        }

        private string EscapeString(string s)
        {
            if (s == null)
            {
                return "0";
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("L\"");
                foreach (var ch in s)
                {
                    switch (ch)
                    {
                        case '\'':
                            sb.Append("\\'");
                            break;
                        case '\"':
                            sb.Append("\\\"");
                            break;
                        case '\\':
                            sb.Append("\\\\");
                            break;
                        case '\a':
                            sb.Append("\\a");
                            break;
                        case '\b':
                            sb.Append("\\b");
                            break;
                        case '\f':
                            sb.Append("\\f");
                            break;
                        case '\r':
                            sb.Append("\\r");
                            break;
                        case '\n':
                            sb.Append("\\n");
                            break;
                        case '\t':
                            sb.Append("\\t");
                            break;
                        case '\v':
                            sb.Append("\\v");
                            break;
                        default:
                            if (ch < 255 && ch >= 32)
                            {
                                sb.Append(ch);
                            }
                            else
                            {
                                sb.Append("\\u");
                                sb.Append(((int)ch).ToString("x4"));
                            }
                            break;
                    }
                }
                sb.Append("\"");
                return sb.ToString();
            }
        }

    }
}
