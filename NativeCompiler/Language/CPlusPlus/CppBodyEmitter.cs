﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NativeCompiler.Language.AST;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Structure;
using System.Globalization;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.CPlusPlus
{
    public class CppBodyEmitter
    {
        public CppBodyEmitter(CppStreamWriter sw, CppRTTIEmitter rtti, Class clas)
        {
            this.sw = sw;
            this.rtti = rtti;
            this.clas = clas;
        }

        private CppStreamWriter sw;
        private CppRTTIEmitter rtti;
        private Class clas;

        private static CultureInfo enUs = CultureInfo.GetCultureInfo("en-US");

        private void Accept(ASTNode node)
        {
            this.Visit((dynamic)node);
        }

        public void Emit(Statement stmt)
        {
            Accept(stmt);
        }

        public void Visit(Break stmt)
        {
            sw.WriteLine("break;");
        }

        public void Visit(Case stmt)
        {
            throw new NotSupportedException("Case is only valid in a switch statement");
        }

        public void Visit(Catch stmt)
        {
            sw.Write("catch (");
            sw.Write(NameBuilder.GetTypeName(stmt.Variable.Type, clas));
            sw.Write(' ');
            sw.Write(stmt.Variable.Name);
            sw.WriteLine(")");
            sw.WriteLine("{");
            HandleScope(stmt.Body);
            sw.WriteLine("}");
        }

        public void Visit(Continue stmt)
        {
            sw.WriteLine("continue;");
        }

        public void Visit(DoWhile stmt)
        {
            sw.WriteLine("do");
            Accept(stmt.Body);
            sw.Write("while (");
            Accept(stmt.Condition);
            sw.WriteLine(");");
        }

        public void Visit(ExceptionBlock stmt)
        {
            foreach (var child in stmt.Children)
            {
                Accept(child);
            }
        }

        public void Visit(ExpressionStatement stmt)
        {
            Accept(stmt.Expression);
            sw.WriteLine(";");
        }

        public void Visit(Finally stmt)
        {
            sw.WriteLine("finally");
            sw.WriteLine("{");
            HandleScope(stmt.Body);
            sw.WriteLine("}");
        }
        
        public void Visit(For stmt)
        {
            sw.Write("for (");
            if (stmt.Variable != null)
            {
                sw.Write(NameBuilder.GetTypeName(stmt.Variable.Type, clas));
                sw.Write(' ');
            }
            if (stmt.PreCondition != null)
            {
                Accept(stmt.PreCondition);
            }
            sw.Write("; ");
            if (stmt.Condition != null)
            {
                Accept(stmt.Condition);
            }
            sw.Write("; ");
            if (stmt.PostCondition != null)
            {
                Accept(stmt.PostCondition);
            }
            sw.WriteLine(")");
            Accept(stmt.Body);
        }

        public void Visit(Goto stmt)
        {
            sw.WriteLine("goto {0};", stmt.Target.Name);
        }

        public void Visit(IfThenElse stmt)
        {
            if (stmt.ThenBody == null)
            {
                if (stmt.ElseBody != null)
                {
                    var condition = Operators.LogicalNot(stmt.Condition);

                    sw.Write("if (");
                    Accept(condition);
                    sw.WriteLine(")");
                    Accept(stmt.ElseBody);
                }
                else
                {
                    throw new Exception("Weird case found while emitting if-then-else");
                }
            }
            else
            {
                sw.Write("if (");
                Accept(stmt.Condition);
                sw.WriteLine(")");
                Accept(stmt.ThenBody);
                if (stmt.ElseBody != null)
                {
                    sw.WriteLine("else");
                    Accept(stmt.ElseBody);
                }
            }
        }

        public virtual void Visit(InitializeVariable stmt)
        {
            sw.Write(NameBuilder.GetTypeName(stmt.Variable.Type, clas, true, true));
            sw.Write(' ');
            sw.Write(stmt.Variable.Name);
            if (stmt.Value != null)
            {
                sw.Write(" = ");
                Accept(stmt.Value);
                sw.WriteLine(";");
            }
            else if (stmt.Variable.Type is ArrayType)
            {
                sw.WriteLine(";"); // trigger default ctor
            }
            else
            {
                sw.WriteLine(" = 0;"); // set default value
            }
        }

        public void Visit(Label stmt)
        {
            sw.WriteLine("{0}:", stmt.Name);
        }

        public void Visit(Rethrow stmt)
        {
            sw.WriteLine("throw;");
        }

        public void Visit(Return stmt)
        {
            if (stmt.Result != null)
            {
                sw.Write("return ");
                Accept(stmt.Result);
                sw.WriteLine(";");
            }
            else
            {
                sw.WriteLine("return;");
            }
        }

        public void Visit(Scope stmt)
        {
            sw.WriteLine("{");
            HandleScope(stmt);
            sw.WriteLine("}");
        }

        private void HandleScope(Scope stmt)
        {
            bool lastIsScoped = false;
            foreach (var child in stmt.Children.Cast<Statement>())
            {
                bool isScoped = child is IfThenElse || child is For || child is While || child is DoWhile;
                if (lastIsScoped)
                {
                    sw.WriteLine();
                }
                lastIsScoped = isScoped;

                Accept(child);
            }
        }

        public void Visit(Switch stmt)
        {
            sw.Write("switch (");
            Accept(stmt.Condition);
            sw.WriteLine(")");
            sw.WriteLine("{");
            foreach (var item in stmt.Cases)
            {
                foreach (var expr in item.Expressions)
                {
                    sw.Write("case ");
                    Accept(expr);
                    sw.WriteLine(":");
                }
                if (item.HasDefault)
                {
                    sw.WriteLine("default:");
                }

                // If there's an 'initialize variable', we start a new scope - otherwise we simply emit.
                var scope = item.Body;
                while (scope.NumberOfChildren == 1 && scope[0] is Scope)
                {
                    scope = (Scope)scope[0];
                }

                if (scope.Children.Any((a) => a is InitializeVariable))
                {
                    Accept(scope);
                }
                else
                {
                    HandleScope(scope);
                }

                // TODO FIXME: Check if we need to add a 'break' or not. Doesn't hurt to add one, so this will do for now:
                sw.WriteLine("break;");
            }

            sw.WriteLine("}");
        }

        public void Visit(Try stmt)
        {
            sw.WriteLine("try");
            sw.WriteLine("{");
            HandleScope(stmt.Body);
            sw.WriteLine("}");
        }

        public void Visit(Throw stmt)
        {
            sw.Write("throw *");
            Accept(stmt.Argument);
            sw.WriteLine(";");
        }

        public void Visit(While stmt)
        {
            sw.Write("while (");
            Accept(stmt.Condition);
            sw.WriteLine(")");
            Accept(stmt.Body);
        }

        public void Visit(ArrayLength expr)
        {
            Accept(expr.Child);
            sw.Write(".get_Length()");
        }

        public void Visit(BasicTypeCast expr)
        {
            sw.Write("static_cast<");
            sw.Write(NameBuilder.GetTypeName(expr.Target, clas));
            sw.Write(">(");
            Accept(expr.Child);
            sw.Write(")");
        }

        public void Visit(Box expr)
        {
            if (expr.Type.StorageType == MemoryType.ValueType)
            {
                // make object from value type (use copy constructor)

                sw.Write("(new ");
                // FIXME: Generics
                sw.Write(NameBuilder.GetTypeName(expr.Type, clas, false));
                sw.Write("(");
                Accept(expr.Target);
                sw.Write("))");
            }
            else
            {
                Accept(expr.Target);
            }
        }

        public void Visit(BinaryOperator expr)
        {
            if (expr.RequiresParentheses(expr.Lhs) ||
                (expr.Lhs is BinaryOperator && new string[] { "&&", "||" }.Contains(((BinaryOperator)expr.Lhs).Operator) &&
                 new string[] { "&&", "||" }.Contains(expr.Operator)))
            {
                sw.Write('(');
                Accept(expr.Lhs);
                sw.Write(')');
            }
            else
            {
                Accept(expr.Lhs);
            }

            sw.Write(' ');
            sw.Write(expr.Operator);
            sw.Write(' ');

            if (expr.RequiresParentheses(expr.Rhs) ||
                (expr.Rhs is BinaryOperator && new string[] { "&&", "||" }.Contains(((BinaryOperator)expr.Rhs).Operator) &&
                 new string[] { "&&", "||" }.Contains(expr.Operator)))
            {
                sw.Write('(');
                Accept(expr.Rhs);
                sw.Write(')');
            }
            else
            {
                Accept(expr.Rhs);
            }
        }

        public void Visit(Call expr)
        {
            // Check if it's a delegate
            bool isdelegate = expr.Callable.IsDelegate();
            if (expr.Object != null)
            {
                Variable var = expr.Object as Variable;
                if (var != null && var.Declaration.Type.BaseDefinition is GenericArgument)
                {
                    sw.Write("dynamic_cast<"); // Can this be one of the faster casts?
                    sw.Write(NameBuilder.GetTypeName(new ClassType(expr.Callable.Container), clas, false));
                    sw.Write("*>(");
                    Accept(expr.Object);
                    sw.Write(")");
                }
                else
                {
                    Accept(expr.Object);
                }

                if (!isdelegate)
                {
                    sw.Write("->");
                }
            }
            if (expr.Callable is Method)
            {
                Method m = (Method)expr.Callable;

                if (!isdelegate)
                {
                    if (!expr.VirtualCall)
                    {
                        sw.Write(NameBuilder.GetTypeName(new ClassType(expr.Callable.Container), clas, false));
                        sw.Write("::");
                    }

                    sw.Write(m.Name);
                }

                sw.Write("(");
                bool first = true;
                if (m.IsStatic)
                {
                    if (expr.Callable.Container.GenericArguments != null)
                    {
                        foreach (var gen in expr.Callable.Container.GenericArguments)
                        {
                            if (first)
                            {
                                first = false;
                            }
                            else
                            {
                                sw.Write(", ");
                            }

                            sw.Write("System::Type::GetTypeFromHandle({0})", rtti.GetClassId(gen));
                        }
                    }
                }
                if (m.GenericArguments != null)
                {
                    foreach (var gen in m.GenericArguments)
                    {
                        if (first)
                        {
                            first = false;
                        }
                        else
                        {
                            sw.Write(", ");
                        }

                        if (gen is GenericArgument)
                        {
                            bool found = false;
                            if (m.GenericArguments != null)
                            {
                                // TODO FIXME: Method generics
                            }

                            var cl = this.clas;
                            if (cl.GenericArguments != null)
                            {
                                int idx = 0;
                                for (; idx < cl.GenericArguments.Count && !object.ReferenceEquals(cl.GenericArguments[idx], gen); ++idx) { }

                                if (idx < cl.GenericArguments.Count)
                                {
                                    sw.Write("_generic{0}", idx);
                                    found = true;
                                }
                            }

                            if (!found)
                            {
                                throw new Exception("Error: generic argument not found in current scope.");
                            }

                        }
                        else
                        {
                            sw.Write("System::Type::GetTypeFromHandle({0})", rtti.GetClassId(gen));
                        }
                    }
                }

                foreach (var par in expr.Parameters)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        sw.Write(", ");
                    }
                    Accept(par);
                }
                sw.Write(")");
            }
            else
            {
                Constructor ctor = (Constructor)expr.Callable;

                // FIXME: What to do with base ctors of generics?
                sw.Write(NameBuilder.GetTypeName(new ClassType(ctor.Container), clas, false));
                sw.Write("::");
                sw.Write("_constructor(");
                bool first = true;
                foreach (var par in expr.Parameters)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        sw.Write(", ");
                    }
                    Accept(par);
                }
                sw.Write(")");
            }
        }

        public void Visit(Constant<int> expr)
        {
            if (expr.Value < 1000)
            {
                sw.Write(expr.Value);
            }
            else
            {
                sw.Write("0x");
                sw.Write(expr.Value.ToString("x"));
            }
        }

        public void Visit(Constant<long> expr)
        {
            if (expr.Value < 1000)
            {
                sw.Write(expr.Value);
            }
            else
            {
                sw.Write("0x");
                sw.Write(expr.Value.ToString("x"));
            }
            sw.Write("ll");
        }

        public void Visit(Constant<float> expr)
        {
            sw.Write(expr.Value.ToString(enUs));
            sw.Write("f");
        }

        public void Visit(Constant<double> expr)
        {
            sw.Write(expr.Value.ToString(enUs));
        }

        public void Visit(Constant<string> expr)
        {
            // NOTE: Constant strings are 'pooled' in the CppStaticInitializers. This method isn't normally called.
            sw.Write("new string(L\"");
            foreach (var ch in expr.Value)
            {
                switch (ch)
                {
                    case '\'':
                        sw.Write("\\'");
                        break;
                    case '\"':
                        sw.Write("\\\"");
                        break;
                    case '\\':
                        sw.Write("\\\\");
                        break;
                    case '\a':
                        sw.Write("\\a");
                        break;
                    case '\b':
                        sw.Write("\\b");
                        break;
                    case '\f':
                        sw.Write("\\f");
                        break;
                    case '\r':
                        sw.Write("\\r");
                        break;
                    case '\n':
                        sw.Write("\\n");
                        break;
                    case '\t':
                        sw.Write("\\t");
                        break;
                    case '\v':
                        sw.Write("\\v");
                        break;
                    default:
                        if (ch < 255 && ch >= 32)
                        {
                            sw.Write(ch);
                        }
                        else
                        {
                            sw.Write("\\u");
                            sw.Write(((int)ch).ToString("x4"));
                        }
                        break;
                }
            }
            sw.Write("\")");
        }

        public void Visit(Constant<object> nullexpr)
        {
            sw.Write("0");
        }

        public void Visit(Constant<IType> type)
        {
            if (type.Value is ClassType)
            {
                var id = rtti.GetClassId(((ClassType)type.Value).Definition);
                sw.Write("{0}", id);
            }
            else
            {
                throw new NotImplementedException(); // use RTTI? Compose?
            }
        }

        public void Visit(DynamicCast expr)
        {
            if (expr.ThrowOnInvalidCast)
            {
                sw.Write("dynamic_cast_or_throw<");
            }
            else
            {
                sw.Write("dynamic_cast<");
            }

            sw.Write(NameBuilder.GetTypeName(expr.Type, clas));
            sw.Write(">(");
            Accept(expr.Target);
            sw.Write(")");
        }

        public void Visit(FunctionPointer fcn)
        {
            if (fcn.Target is Constructor)
            {
                throw new NotSupportedException("A delegate shouldn't be a constructor");
            }

            Method m = (Method)fcn.Target;

            // 1. Check if the method is overloaded:
            bool overloaded = (fcn.Target.Container.Methods.Where((a) => a.Name == m.Name).Count() > 1);

            //std::bind(
            sw.Write("std::bind(");

            string className = NameBuilder.GetTypeName(new ClassType(fcn.Target.Container), clas, false);

            // Overloaded -> 
            if (overloaded)
            {
                sw.Write("static_cast<");
                sw.Write(NameBuilder.GetTypeName(m.ReturnType, clas));
                sw.Write("(");
                sw.Write(className);
                sw.Write("::*)(");
                bool first = true;
                if (m.GenericArguments != null)
                {
                    for (int i = 0; i < m.GenericArguments.Count; ++i)
                    {
                        if (first)
                        {
                            first = false;
                        }
                        else
                        {
                            sw.Write(", ");
                        }
                        sw.Write("System::Type *");
                    }
                }
                for (int i = 0; i < m.Parameters.Count; ++i)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        sw.Write(", ");
                    }
                    sw.Write(NameBuilder.GetTypeName(m.Parameters[i].Type, clas));
                }
                sw.Write(")>(");
            }
            
            //    static_cast<
            //        int(DelegateTest::*)(int)		// this is the signature of the function we want to bind to; because it is overloaded you need to be very specific here
            //    >(&DelegateTest::Test2),
            //    this, 
            //    std::placeholders::_1
            //);

            
            sw.Write("&");
            sw.Write(className);
            sw.Write("::");
            sw.Write(m.Name);

            if (overloaded)
            {
                sw.Write(")");
            }

            if (!fcn.Target.IsStatic)
            {
                // del = std::bind(&DelegateTest::STest1, std::placeholders::_1);
                sw.Write(", this");
            }

            // del = std::bind(&DelegateTest::Test1, this, std::placeholders::_1);
            if (m.GenericArguments != null)
            {
                foreach (var gen in m.GenericArguments)
                {
                    sw.Write(", ");

                    if (gen is GenericArgument)
                    {
                        bool found = false;

                        // TODO FIXME: Generic parameters of this method (passed as arguments)

                        var cl = this.clas;
                        if (cl.GenericArguments != null)
                        {
                            int idx = 0;
                            for (; idx < cl.GenericArguments.Count && !object.ReferenceEquals(cl.GenericArguments[idx], gen); ++idx) { }

                            if (idx < cl.GenericArguments.Count)
                            {
                                sw.Write("_generic{0}", idx);
                                found = true;
                            }
                        }

                        if (!found)
                        {
                            throw new Exception("Error: generic argument not found in current scope.");
                        }

                    }
                    else
                    {
                        sw.Write("System::Type::GetTypeFromHandle({0})", rtti.GetClassId(gen));
                    }
                }
            }

            for (int i = 0; i < m.Parameters.Count; ++i)
            {
                sw.Write(", std::placeholders::_");
                sw.Write((i + 1).ToString());
            }
            sw.Write(")");
        }

        public void Visit(Index expr)
        {
            Accept(expr.Container);
            sw.Write('[');
            Accept(expr.Value);
            sw.Write(']');
        }

        public void Visit(IsFinite expr)
        {
            sw.Write("std::isinf(");
            Accept(expr.Child);
            sw.Write(")");
        }

        public void Visit(InitializeArray expr)
        {
            if (expr.NumberOfChildren < 16)
            {
                sw.Write("{ ");
                bool first = true;
                foreach (var item in expr.InitializerList)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        sw.Write(", ");
                    }
                    Accept(item);
                }
                sw.Write(" }");
            }
            else
            {
                sw.WriteLine("{");
                int ctr = 0;
                foreach (var item in expr.InitializerList)
                {
                    if (ctr != 0)
                    {
                        sw.Write(", ");
                        if ((ctr % 20) == 0)
                        {
                            sw.WriteLine();
                        }
                    }
                    Accept(item);
                    ++ctr;
                }
                sw.WriteLine();
                sw.WriteLine(" }");

            }
        }

        public void Visit(NewArray expr)
        {
            sw.Write("System::Array<");
            sw.Write(NameBuilder.GetTypeName(expr.ElementType, clas));
            sw.Write(">(");
            Accept(expr.Length);
            sw.Write(")");
        }

        public void Visit(NewObject expr)
        {
            if (expr.Constructor.Container.IsDelegate())
            {
                Accept(expr.Parameters.ToArray()[1]);
            }
            else
            {
                sw.Write("(new ");
                // FIXME: Generics
                sw.Write(NameBuilder.GetTypeName(new ClassType(expr.Constructor.Container), clas, false));
                sw.Write("(1"); // shouldInit
                bool first = true;
                if (expr.Constructor.Container.GenericArguments != null)
                {
                    foreach (var gen in expr.Constructor.Container.GenericArguments)
                    {
                        sw.Write(", ");
                        sw.Write("System::Type::GetTypeFromHandle({0})", rtti.GetClassId(gen));
                    }
                }
                foreach (var item in expr.Parameters)
                {
                    sw.Write(", ");
                    Accept(item);
                }
                sw.Write("))");
            }
        }

        public void Visit(PrefixUnaryOperator expr)
        {
            sw.Write(expr.Operator);

            if (expr.RequiresParentheses(expr.Child))
            {
                sw.Write('(');
                Accept(expr.Child);
                sw.Write(')');
            }
            else
            {
                Accept(expr.Child);
            }
        }

        public void Visit(Store expr)
        {
            if (expr.Object != null)
            {
                Accept(expr.Object);
                sw.Write("->");
            }

            sw.Write(expr.Target.Name);
            sw.Write(" = ");
            Accept(expr.Value);
        }

        public void Visit(StoreIndirect expr)
        {
            Accept(expr.Destination);
            sw.Write(" = ");
            Accept(expr.Value);
        }

        public void Visit(SuffixUnaryOperator expr)
        {
            if (expr.RequiresParentheses(expr.Child))
            {
                sw.Write('(');
                Accept(expr.Child);
                sw.Write(')');
            }
            else
            {
                Accept(expr.Child);
            }

            sw.Write(expr.Operator);
        }

        public void Visit(This expr)
        {
            sw.Write("this");
        }

        public void Visit(Unbox expr)
        {
            if (expr.Type.StorageType == MemoryType.ValueType)
            {
                // make value type from object (use deref / copy constructor)

                sw.Write("(*");
                Accept(expr.Target);
                sw.Write(")");
            }
            else
            {
                Accept(expr.Target);
            }
        }

        public void Visit(Variable variable)
        {
            sw.Write(variable.Declaration.Name);
        }

    }
}
