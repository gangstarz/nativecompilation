﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.CPlusPlus
{
    public class CppRTTIEmitter : IDisposable
    {
        public CppRTTIEmitter(string filename, Namespace rootNamespace)
        {
            this.sw = new CppStreamWriter(new System.IO.StreamWriter(filename + ".h", false));
            this.cppsw = new CppStreamWriter(new System.IO.StreamWriter(filename + ".cpp", false));
            this.root = rootNamespace;
            this.filename = Path.GetFileName(filename);
        }

        private string filename;
        private CppStreamWriter sw;
        private CppStreamWriter cppsw;
        private Namespace root;
        private Dictionary<Class, int> classIdMapping;
        private HashSet<Assembly> assemblies;
        private HashSet<Module> modules;
        private List<Class> classes;
        private Dictionary<Module, int> moduleMapping;
        private Dictionary<Assembly, int> assemblyMapping;

        public int GetClassId(Class cl)
        {
            int id;
            if (!classIdMapping.TryGetValue(cl, out id))
            {
                id = -1;
            }
            return id;
        }

        public int GetModuleId(Module module)
        {
            return moduleMapping[module];
        }

        public int GetAssemblyId(Assembly ass)
        {
            return assemblyMapping[ass];
        }

        public void GenerateHeaderFile()
        {
            // List of assemblies, list of modules, list of classes
            assemblies = new HashSet<Assembly>();
            modules = new HashSet<Module>();
            classes = new List<Class>();
            this.classIdMapping = new Dictionary<Class, int>();

            Stack<Namespace> ns = new Stack<Namespace>();
            ns.Push(root);
            while (ns.Count > 0)
            {
                var cur = ns.Pop();
                foreach (var clas in cur.Classes)
                {
                    modules.Add(clas.Module);
                    assemblies.Add(clas.Module.Container);

                    if (clas.IsDefined &&
                        (clas.Container != null && !(clas.Container is Namespace && ((Namespace)clas.Container).Name == "" && clas.Name.IndexOfAny(new char[] { '=', '{', '}' }) >= 0)))
                    {
                        classIdMapping.Add(clas, classes.Count);
                        classes.Add(clas);
                    }
                }
                foreach (var n in cur.Namespaces)
                {
                    ns.Push(n);
                }
            }

            sw.WriteLine("#ifndef RTTI_META_INFO");
            sw.WriteLine("#define RTTI_META_INFO");
            sw.WriteLine();
            sw.WriteLine("#include \"../LanguageFeatures.h\"");
            sw.WriteLine("#include <vector>");
            sw.WriteLine("#include <string>");
            sw.WriteLine();
            sw.WriteLine("class RTTIMetaInfo");
            sw.WriteLine("{");
            sw.WriteLine("RTTIMetaInfo();");
            sw.WriteLine();
            sw.WriteLine("public:");
            sw.WriteLine("System::RuntimeType** types;");
            sw.WriteLine("System::RuntimeModule** modules;");
            sw.WriteLine("System::RuntimeAssembly** assemblies;");
            sw.WriteLine();
            sw.WriteLine("inline static RTTIMetaInfo& Instance()");
            sw.WriteLine("{");
            sw.WriteLine("static RTTIMetaInfo instance;");
            sw.WriteLine("return instance;");
            sw.WriteLine("}");
            sw.WriteLine();
            sw.WriteLine("static inline System::RuntimeType *GetTypeById(int id)");
            sw.WriteLine("{");
            sw.WriteLine("return Instance().types[id];");
            sw.WriteLine("}");
            sw.WriteLine("};");
            sw.WriteLine();
            sw.WriteLine("#endif");
        }

        public void GenerateCppFile()
        {
            var sw = cppsw;

            sw.WriteLine("#include \"{0}.h\"", filename);

            // Include header files for all classes; we need this to get to the RTTI class:
            foreach (var clas in classes)
            {
                if (clas.IsDefined &&
                    (clas.Container != null && !(clas.Container is Namespace && ((Namespace)clas.Container).Name == "" && clas.Name.IndexOfAny(new char[] { '=', '{', '}' }) >= 0)))
                {
                    var cur = NameBuilder.GetFilename(clas);
                    sw.WriteLine("#include \"{0}.h\"", cur);
                }
            }

            sw.WriteLine();
            sw.WriteLine("using namespace System;");
            sw.WriteLine();
            
            sw.WriteLine("Type* Type::GetTypeFromHandle(int handle)");
            sw.WriteLine("{");
            sw.WriteLine("return ::RTTIMetaInfo::GetTypeById(handle);");
            sw.WriteLine("}");
            sw.WriteLine();

            sw.WriteLine("RTTIMetaInfo::RTTIMetaInfo()");
            sw.WriteLine("{");
            sw.WriteLine("types = new RuntimeType*[{0}];", this.classes.Count);
            sw.WriteLine("modules = new RuntimeModule*[{0}];", this.modules.Count);
            sw.WriteLine("assemblies = new RuntimeAssembly*[{0}];", this.assemblies.Count);

            // Emit assemblies
            sw.UpdateScope = false;
            sw.WriteLine();
            sw.WriteLine("// Assemblies");
            int n = 0;
            assemblyMapping = new Dictionary<Assembly, int>();
            foreach (var ass in assemblies)
            {
                assemblyMapping[ass] = n;
                sw.WriteLine("assemblies[{0}] = new RuntimeAssembly({1}, {2}, {3}, {4});", n, EscapeString(ass.CodeBase), EscapeString(ass.FullName), ass.InGAC?"1":"0", EscapeString(ass.RuntimeVersion));
                ++n;
            }

            // Emit modules
            sw.WriteLine();
            sw.WriteLine("// Modules");
            n = 0;
            moduleMapping = new Dictionary<Module, int>();
            foreach (var mod in modules)
            {
                moduleMapping[mod] = n;
                sw.WriteLine("modules[{0}] = new RuntimeModule({1}, {2}, assemblies[{3}]);", n, EscapeString(mod.FullyQualifiedName), EscapeString(mod.Name), assemblyMapping[mod.Container]);
                sw.WriteLine("assemblies[{0}]->modules.push_back(modules[{1}]);", assemblyMapping[mod.Container], n);
                ++n;
            }

            // Emit classes
            sw.WriteLine();
            sw.WriteLine("// Types");
            n = 0;
            foreach (var clas in classes)
            {
                if (clas.IsDefined &&
                    (clas.Container != null && !(clas.Container is Namespace && ((Namespace)clas.Container).Name == "" && clas.Name.IndexOfAny(new char[] { '=', '{', '}' }) >= 0)))
                {
                    string className;

                    if (clas.GenericArguments != null)
                    {
                        className = string.Format("{0}_Generic_{1}", clas.Name, clas.GenericArguments.Count);
                    }
                    else
                    {
                        className = clas.Name;
                    }
                    className = GetNamespace(clas).Replace(".", "::") + "::_" + className + "_RTTI";

                    sw.WriteLine("types[{0}] = {1}::Instance();", n, className);
                    sw.WriteLine("modules[{0}]->types.push_back(types[{1}]);", moduleMapping[clas.Module], n);
                    sw.WriteLine("assemblies[{0}]->types.push_back(types[{1}]);", assemblyMapping[clas.Module.Container], n);
                    ++n;
                }
            }
            sw.UpdateScope = true;

            sw.WriteLine("}");
        }

        private string GetNamespace(Class cl)
        {
            while (cl.Container != null && cl.Container is Class) { cl = (Class)cl.Container; }

            List<Namespace> ll = new List<Namespace>();
            Namespace last = cl.Container as Namespace;
            while (last != null)
            {
                if (last.Name != null)
                {
                    ll.Add(last);
                }
                last = last.Container as Namespace;
            }
            StringBuilder sb = new StringBuilder();
            for (int i = ll.Count - 1; i >= 0; --i)
            {
                sb.Append(ll[i].Name);
                if (i != 0)
                {
                    sb.Append(".");
                }
            }
            return sb.ToString();
        }

        private string EscapeString(string s)
        {
            if (s == null)
            {
                return "0";
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("L\"");
                foreach (var ch in s)
                {
                    switch (ch)
                    {
                        case '\'':
                            sb.Append("\\'");
                            break;
                        case '\"':
                            sb.Append("\\\"");
                            break;
                        case '\\':
                            sb.Append("\\\\");
                            break;
                        case '\a':
                            sb.Append("\\a");
                            break;
                        case '\b':
                            sb.Append("\\b");
                            break;
                        case '\f':
                            sb.Append("\\f");
                            break;
                        case '\r':
                            sb.Append("\\r");
                            break;
                        case '\n':
                            sb.Append("\\n");
                            break;
                        case '\t':
                            sb.Append("\\t");
                            break;
                        case '\v':
                            sb.Append("\\v");
                            break;
                        default:
                            if (ch < 255 && ch >= 32)
                            {
                                sb.Append(ch);
                            }
                            else
                            {
                                sb.Append("\\u");
                                sb.Append(((int)ch).ToString("x4"));
                            }
                            break;
                    }
                }
                sb.Append("\"");
                return sb.ToString();
            }
        }

        public void Close()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (this.sw != null)
            {
                this.sw.Close();
                this.sw.Dispose();
                this.sw = null;
            }

            if (this.cppsw != null)
            {
                this.cppsw.Close();
                this.cppsw.Dispose();
                this.cppsw = null;
            }
        }

    }
}
