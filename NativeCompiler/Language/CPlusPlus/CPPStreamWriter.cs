﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.CPlusPlus
{
    public class CppStreamWriter : TextWriter
    {
        public CppStreamWriter(TextWriter baseWriter)
        {
            this.baseWriter = baseWriter;
            UpdateScope = true;
        }

        private int tabs = 0;
        private TextWriter baseWriter;
        private StringBuilder buffer = new StringBuilder();
        public bool UpdateScope { get; set; }

        public override void Write(char value)
        {
            buffer.Append(value);

            if (value == '\n')
            {
                FlushLine();
            }
        }

        private void FlushLine()
        {
            string str = buffer.ToString();
            buffer.Clear();

            int lastChar = str.Length - 1;
            while (lastChar > 0 && !"{}".Contains(str[lastChar]))
            {
                --lastChar;
            }

            int delta = 0;
            if (lastChar >= 0 && UpdateScope)
            {
                if (str[lastChar] == '{')
                {
                    delta = 1;
                }
                else if (str[lastChar] == '}')
                {
                    delta = -1;
                }
            }

            if (delta < 0)
            {
                tabs += delta;
                if (tabs < 0)
                {
                    //throw new Exception("Tab underflow; you have too many '}' in your code.");
                }
            }

            string trimmed = str.TrimEnd();
            if (trimmed == "public:" || trimmed == "private:" || trimmed == "protected:" || trimmed.EndsWith(":"))
            {
                baseWriter.WriteLine(((tabs >= 1) ? new string('\t', tabs - 1) : "") + trimmed);
            }
            else
            {
                baseWriter.WriteLine(((tabs>=0)?new string('\t', tabs):"") + trimmed);
            }

            if (delta > 0)
            {
                tabs += delta;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (tabs != 0)
            {
                //throw new ArgumentException("Expected indentation tabs to be at 0 after close file.");
            }

            if (baseWriter != null)
            {
                FlushLine();

                baseWriter.Close();
                baseWriter = null;
            }

            base.Dispose(disposing);
        }

        public override Encoding Encoding
        {
            get { return baseWriter.Encoding; }
        }

        public void WriteLiteralLine(string str)
        {
            if (buffer.Length > 0)
            {
                FlushLine();
            }
            baseWriter.Write(new string('\t', tabs));
            baseWriter.WriteLine(str);
        }
    }
}
