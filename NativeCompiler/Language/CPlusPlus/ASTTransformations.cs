﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST;
using NativeCompiler.Language.Transformations;

namespace NativeCompiler.Language.CPlusPlus
{
    public class ASTTransformations
    {
        public ASTTransformations()
        {
            this.transformations = new ICodeTransformation[]
            {
                new ScopeFolding(),
                new VariableInlining(),
                new ClosedBlockElimination(),
                new EqualsOptimizer(),
                new ForVariables(),
                new ImplicitLastReturn(),
                new IncrementTransformation(),
                new LogicalNotOptimizer(),
                new LogicalOperators(),
                new LoopTransform(),
                new ScopeFolding(),
                new SwitchBranchElimination(),
            };
        }

        private ICodeTransformation[] transformations;

        public void Transform(ASTNode rootnode)
        {
            int code = rootnode.GetHashCode();
            int code2 = code;
            do
            {
                code = code2;

                foreach (var trans in transformations)
                {
                    trans.Transform(rootnode);
                }

                code2 = rootnode.GetHashCode();
            }
            while (code != code2);
        }
    }
}
