﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.CPlusPlus
{
    public class CppStaticInitializers
    {
        public void Transform(Statement scope)
        {
            foreach (var init in scope.AllChildrenOf<InitializeArray>())
            {
                Rewrite(init);
            }

            foreach (var str in scope.AllChildrenOf<Constant<string>>())
            {
                Rewrite(str);
            }
        }

        // Static initializers are:
        //
        // - Array initializers.
        public List<Tuple<InitializeArray, Field>> ArrayInitialization = new List<Tuple<InitializeArray, Field>>();

        // - String constants
        public Dictionary<string, Field> StringConstants = new Dictionary<string, Field>();

        // Both are rewritten to static fields:
        public void Rewrite(InitializeArray ar)
        {
            // Create the static field
            string name = "_init" + ArrayInitialization.Count;
            var field = new Field(new ArrayType(ar.ElementType), name, true);
            ArrayInitialization.Add(new Tuple<InitializeArray, Field>(ar, field));

            // Substitute the array by the field.
            ar.SubstituteBy(new Variable(field));
        }

        public void Rewrite(Constant<string> str)
        {
            // Ensure string literals are unique.
            Field f;
            if (!StringConstants.TryGetValue(str.Value, out f))
            {
                f = new Field(new BasicType("System.String"), "_str" + StringConstants.Count, true);
                StringConstants.Add(str.Value, f);
            }
            str.SubstituteBy(new Variable(f));
        }

        public void WriteHeaderFile(TextWriter sw, Class clas)
        {
            // Pimpl'd away.
        }

        public void WriteCppFile(CppStreamWriter sw, Class clas)
        {
            // Determine template prefix and class name; we need this later
            var bodyEmitter = new CppBodyEmitter(sw, null, clas);
            sw.UpdateScope = false;

            // Emit array initializers
            foreach (var item in ArrayInitialization)
            {
                sw.Write("static ");
                sw.Write(NameBuilder.GetTypeName(item.Item1.ElementType, clas));
                sw.Write(' ');
                sw.Write(item.Item2.Name);
                sw.Write("[");
                sw.Write(item.Item1.InitializerList.Count());
                sw.Write("] = ");
                bodyEmitter.Visit(item.Item1);
                sw.WriteLine(';');
            }

            // Emit string initializers

            // Emit array initializers
            foreach (var item in StringConstants)
            {
                sw.Write("static string* ");
                sw.Write(item.Value.Name);
                sw.Write(" = ");
                bodyEmitter.Visit(new Constant<string>(item.Key));
                sw.WriteLine(';');
            }

            sw.UpdateScope = true;
            if (ArrayInitialization.Count > 0 || StringConstants.Count > 0)
            {
                sw.WriteLine();
            }
        }
    }
}
