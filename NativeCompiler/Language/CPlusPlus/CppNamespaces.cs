﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.CPlusPlus
{
    public static class CppNamespaces
    {
        // Helpers for namespaces
        public static void GotoNamespace(TextWriter sw, List<string> curns, string namesp)
        {
            bool first = true;
            string[] ns = namesp.Split('.');
            while (ShouldPopNS(curns, ns))
            {
                sw.WriteLine("}");
                curns.RemoveAt(curns.Count - 1);
            }
            while (curns.Count < ns.Length)
            {
                if (first)
                {
                    sw.WriteLine();
                    first = false;
                }
                sw.WriteLine("namespace {0}", ns[curns.Count]);
                sw.WriteLine("{");
                curns.Add(ns[curns.Count]);
            }
        }

        private static bool ShouldPopNS(List<string> curns, string[] nsItems)
        {
            if (nsItems.Length < curns.Count)
            {
                return true;
            }
            for (int i = 0; i < curns.Count; ++i)
            {
                if (curns[i] != nsItems[i])
                {
                    return true;
                }
            }
            return false;
        }


    }
}
