﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.CPlusPlus
{
    public class FileEmitter : AST.ASTStructureVisitorBase
    {
        public FileEmitter(Namespace rootNamespace, string rootFolder)
        {
            this.rootNamespace = rootNamespace;
            this.rootFolder = rootFolder;
        }

        private Namespace rootNamespace;
        private string rootFolder;
        private CppRTTIEmitter rttiEmitter;

        public void Process()
        {
            this.rttiEmitter = new CppRTTIEmitter(Path.Combine(rootFolder, "RTTIMetaInfo"), rootNamespace);
            rttiEmitter.GenerateHeaderFile();
            rttiEmitter.GenerateCppFile();
            rttiEmitter.Close();

            rootNamespace.Accept(this);

            var entryPoint = rootNamespace.FindEntryPoint();

            if (entryPoint != null)
            {
                GenerateEntryPoint(Path.Combine(rootFolder, "main.cpp"), entryPoint);
            }
        }

        public override void Visit(Class clas)
        {
            // If the class container = '' we usually have a reserved, 'compiler generated' helper with strange data etc., which should be optimized away somewhere.
            // An example is array initializers, which are fields with arbitrary length, managed by RuntimeHelpers.
            if (clas.IsDefined &&
                (clas.Container != null && !(clas.Container is Namespace && ((Namespace)clas.Container).Name == "" && clas.Name.IndexOfAny(new char[] { '=', '{', '}' }) >= 0)))
            {
                CppStaticInitializers staticInitialization = new CppStaticInitializers();

                foreach (var ctor in clas.Constructors)
                {
                    if (ctor.Body != null)
                    {
                        VariableInitializationVisitor varinit = new VariableInitializationVisitor();
                        varinit.InjectInBestScope(ctor);

                        new ASTTransformations().Transform(ctor.MainScope);

                        staticInitialization.Transform(ctor.MainScope); // will never rewrite scope, so we can ignore the return value.
                    }
                }

                foreach (var meth in clas.Methods)
                {
                    if (meth.Body != null)
                    {
                        VariableInitializationVisitor varinit = new VariableInitializationVisitor();
                        varinit.InjectInBestScope(meth);

                        new ASTTransformations().Transform(meth.MainScope);

                        staticInitialization.Transform(meth.MainScope); // will never rewrite scope, so we can ignore the return value.
                    }
                }

                using (HeaderFileEmitter headerFile = new HeaderFileEmitter(Path.Combine(rootFolder, NameBuilder.GetFilename(clas) + ".h"), clas))
                {
                    headerFile.Generate(staticInitialization);
                }

                if (!clas.IsDelegate())
                {
                    using (CppFileEmitter cppFile = new CppFileEmitter(Path.Combine(rootFolder, NameBuilder.GetFilename(clas) + ".cpp"), clas))
                    {
                        cppFile.Generate(staticInitialization, rttiEmitter);
                    }
                }
            }
        }

        private void GenerateEntryPoint(string filename, Method entryPoint)
        {
            using (var writer = new CppStreamWriter(new StreamWriter(filename)))
            {
                var className = NameBuilder.GetFilename(entryPoint.Container);

                // Include files
                writer.WriteLine("#include \"../LanguageFeatures.h\"");
                writer.WriteLine("#include \"{0}.h\"", className);
                writer.WriteLine("#include <string>");
                writer.WriteLine("#include <iostream>");
                writer.WriteLine();
                writer.WriteLine("using namespace System;");
                writer.WriteLine();
                writer.WriteLine("int main(int argc, char** argv)");
                writer.WriteLine("{");
                writer.WriteLine("try");
                writer.WriteLine("{");

                bool returnsInt = (entryPoint.ReturnType is BasicType) && ((BasicType)entryPoint.ReturnType).Name == "System.Int32";

                if (returnsInt)
                {
                    writer.WriteLine("int result;");
                }

                // Call Program(...)
                if (entryPoint.Parameters.Count == 0)
                {
                    // If it returns a return value, return it
                    if (returnsInt)
                    {
                        writer.Write("result = ");
                    }

                    // Simple case, just call the method
                    writer.Write(className.Replace(".", "::"));
                    writer.Write("::");
                    writer.Write(entryPoint.Name);
                    writer.WriteLine("();");
                }
                else
                {
                    // Build the array with argc/argv
                    writer.WriteLine("Array<string*> ar(argc);");
                    writer.WriteLine("for (int i=0; i<argc; ++i)");
                    writer.WriteLine("{");
                    writer.WriteLine("ar[i] = new string(argv[i]);");
                    writer.WriteLine("}");

                    // If it returns a return value, return it
                    if (returnsInt)
                    {
                        writer.Write("result = ");
                    }

                    // Simple case, just call the method
                    writer.Write(className.Replace(".", "::"));
                    writer.Write("::");
                    writer.Write(entryPoint.Name);
                    writer.WriteLine("(ar);");

                }

                // Trailer, finish main

                // Wait for a key:

                writer.WriteLine();
                writer.WriteLine("std::string s;");
                writer.WriteLine("std::getline(std::cin, s);");
                writer.WriteLine();

                if (!returnsInt)
                {
                    writer.WriteLine("return 0;");
                }
                else
                {
                    writer.WriteLine("return result;");
                }

                writer.WriteLine("}");
                writer.WriteLine("catch (...)");
                writer.WriteLine("{");
                writer.WriteLine("return 1;"); // error
                writer.WriteLine("}");
                writer.WriteLine("}");
            }
        }
    }
}
