﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.CPlusPlus
{
    public class HeaderFileEmitter : IDisposable
    {
        public HeaderFileEmitter(string filename, Class clas)
        {
            this.sw = new CppStreamWriter(new System.IO.StreamWriter(filename, false));
            this.clas = clas;
        }

        private CppStreamWriter sw;
        private Class clas;

        public void Generate(CppStaticInitializers staticInitialization)
        {
            string ndefName = NameBuilder.GetDefineName(clas);

            sw.WriteLine("#ifndef {0}", ndefName);
            sw.WriteLine("#define {0}", ndefName);
            sw.WriteLine();

            // Standard includes
            sw.WriteLine("#include <cstdint>");

            // Avoid circular reference of array
            sw.WriteLine("#include \"../LanguageFeatures.h\"");
            sw.WriteLine("#include \"RTTIMetaInfo.h\"");

            // Include all base classes
            foreach (var baseClass in clas.BaseClasses)
            {
                var classDefinition = baseClass.BaseDefinition;
                if (classDefinition != null && classDefinition.IsDefined)
                {
                    string baseClassName = NameBuilder.GetFilename(classDefinition);
                    sw.WriteLine("#include \"{0}.h\"", baseClassName);
                }
            }

            sw.WriteLine();

            // Generate forward definitions (in the correct namespaces)
            List<Tuple<Class, string>> classNames = clas.CollectDependencies().Select((a) => new Tuple<Class, string>(a, NameBuilder.GetFilename(a))).ToList();
            classNames.Sort((a, b) => a.Item2.CompareTo(b.Item2));

            List<string> curns = new List<string>();
            foreach (var fwd in classNames)
            {
                if (!object.ReferenceEquals(fwd.Item1, clas) && !(fwd.Item1 is GenericArgument) && !clas.BaseClasses.Any((a) => a.BaseDefinition == fwd.Item1))
                {
                    int idx = fwd.Item2.LastIndexOf('.');
                    string ns = (idx >= 0) ? fwd.Item2.Substring(0, idx) : "";
                    CppNamespaces.GotoNamespace(sw, curns, ns);

                    // Emit forward definition
                    if (fwd.Item1.GenericArguments != null)
                    {
                        sw.WriteLine("class {0}_Generic_{1};", fwd.Item1.Name, fwd.Item1.GenericArguments.Count);
                    }
                    else
                    {
                        sw.WriteLine("class {0};", fwd.Item1.Name);
                    }
                }
            }

            // Go to namespace of class
            string currentClassName = NameBuilder.GetFilename(clas);
            int jdx = currentClassName.LastIndexOf('.');
            string ns2 = (jdx >= 0) ? currentClassName.Substring(0, jdx) : "";
            CppNamespaces.GotoNamespace(sw, curns, ns2);

            string className;

            if (clas.IsDelegate())
            {
                if (clas.GenericArguments != null)
                {
                    className = string.Format("{0}_Generic_{1}", clas.Name, clas.GenericArguments.Count);
                }
                else
                {
                    className = clas.Name;
                }

                // TODO FIXME: Generics.

                // Find the 'Invoke' method; this has the signature of the method.
                var method = clas.Methods.FirstOrDefault((a) => a.Name == "Invoke");

                // typedef std::function<int(int)> TestDelegate;
                sw.Write("typedef std::function<");
                sw.Write(NameBuilder.GetTypeName(method.ReturnType, clas, false));
                sw.Write("(");
                for (int i = 0; i < method.Parameters.Count; ++i)
                {
                    if (i != 0)
                    {
                        sw.Write(", ");
                    }
                    sw.Write(NameBuilder.GetTypeName(method.Parameters[i].Type, clas, false));
                }
                sw.Write(")> ");
                sw.Write(className);
                sw.WriteLine(";");
            }
            else
            {
                // Open class definition
                if (clas.GenericArguments != null)
                {
                    className = string.Format("{0}_Generic_{1}", clas.Name, clas.GenericArguments.Count);
                    sw.Write("class {0}_Generic_{1}", clas.Name, clas.GenericArguments.Count);
                }
                else
                {
                    className = clas.Name;
                    sw.Write("class {0}", clas.Name);
                }

                int additionalIndent = className.Length + 6 + 3;

                // Interfaces and base classes
                for (int i = 0; i < clas.BaseClasses.Count; ++i)
                {
                    if (i != 0)
                    {
                        sw.WriteLine(", ");
                        sw.Write(new string(' ', additionalIndent));
                    }
                    else
                    {
                        sw.Write(" : ");
                    }
                    sw.Write("public virtual ");
                    string fqn = NameBuilder.GetTypeName(clas.BaseClasses[i], clas, false);
                    sw.Write(fqn);
                }

                sw.WriteLine();

                // Open class
                sw.WriteLine("{");

                // Generic arguments -> private members
                if (clas.GenericArguments != null)
                {
                    sw.WriteLine("private:");
                    for (int i = 0; i < clas.GenericArguments.Count; ++i)
                    {
                        sw.Write("System::Type *_generic");
                        sw.Write(i);
                        sw.WriteLine(";");
                    }
                    sw.WriteLine();
                }


                sw.WriteLine("public:");

                // Static initialization?
                staticInitialization.WriteHeaderFile(sw, clas);

                // Create empty default ctor to ensure we always can construct the base class. Constructor initialization is in the '_constructor' methods.
                // If we have a generic, we should only be able to construct it if we pass (all) the type arguments.
                if (!clas.InterfaceClass)
                {
                    sw.Write("{0}(", className);
                    if (clas.GenericArguments != null)
                    {
                        for (int i = 0; i < clas.GenericArguments.Count; ++i)
                        {
                            if (i != 0)
                            {
                                sw.Write(", ");
                            }
                            sw.Write("System::Type *t");
                            sw.Write(i);
                        }
                    }
                    sw.WriteLine(");");
                }

                foreach (var ctor in clas.Constructors)
                {
                    sw.Write(className);
                    sw.Write("(int _shouldInit"); // always 'true'. Base ctor calls always evaluate to 'false' (default ctor).
                    if (clas.GenericArguments != null)
                    {
                        for (int i = 0; i < clas.GenericArguments.Count; ++i)
                        {
                            sw.Write(", System::Type *t");
                            sw.Write(i);
                        }
                    }
                    for (int i = 0; i < ctor.Parameters.Count; ++i)
                    {
                        sw.Write(", ");
                        var type = NameBuilder.GetTypeName(ctor.Parameters[i].Type, clas);
                        sw.Write("{0} {1}", type, ctor.Parameters[i].Name);
                    }
                    sw.WriteLine(");");
                }

                foreach (var ctor in clas.Constructors)
                {
                    sw.Write("void _constructor(");
                    if (clas.GenericArguments != null)
                    {
                        for (int i = 0; i < clas.GenericArguments.Count; ++i)
                        {
                            if (i != 0)
                            {
                                sw.Write(", ");
                            }
                            sw.Write("System::Type *t");
                            sw.Write(i);
                        }
                    }
                    for (int i = 0; i < ctor.Parameters.Count; ++i)
                    {
                        if (i != 0 || clas.GenericArguments != null)
                        {
                            sw.Write(", ");
                        }
                        var type = NameBuilder.GetTypeName(ctor.Parameters[i].Type, clas);
                        sw.Write("{0} {1}", type, ctor.Parameters[i].Name);
                    }
                    sw.WriteLine(");");
                }

                if (clas.Constructors.Count > 0)
                {
                    sw.WriteLine();
                }

                foreach (var field in clas.Fields)
                {
                    if (field.IsStatic)
                    {
                        sw.Write("static ");
                    }
                    var type = NameBuilder.GetTypeName(field.Type, clas);
                    sw.WriteLine("{0} {1};", type, field.Name);
                }

                if (clas.Fields.Count > 0)
                {
                    sw.WriteLine();
                }

                foreach (var meth in clas.Methods)
                {
                    if (meth.IsStatic)
                    {
                        sw.Write("static ");
                    }
                    else if (meth.IsVirtual || meth.Body == null)
                    {
                        sw.Write("virtual ");
                    }

                    sw.Write(NameBuilder.GetTypeName(meth.ReturnType, clas, false));
                    sw.Write(" ");
                    sw.Write(meth.Name);
                    sw.Write("(");
                    for (int i = 0; i < meth.Parameters.Count; ++i)
                    {
                        if (i != 0)
                        {
                            sw.Write(", ");
                        }

                        var type = NameBuilder.GetTypeName(meth.Parameters[i].Type, clas);
                        sw.Write("{0} {1}", type, meth.Parameters[i].Name);
                    }
                    sw.Write(")");

                    if (meth.Body == null)
                    {
                        sw.WriteLine(" = 0;");
                    }
                    else
                    {
                        sw.WriteLine(";");
                    }
                }

                // Make destructor protected if we have an interface class
                if (clas.InterfaceClass)
                {
                    sw.WriteLine();
                    sw.WriteLine("protected:");
                    sw.WriteLine("~{0}()", className);
                    sw.WriteLine("{");
                    sw.WriteLine("}");
                }
                else
                {
                    sw.WriteLine("virtual System::Type* GetType();");
                    sw.WriteLine();
                    if (!clas.ValueType)
                    {
                        sw.WriteLine();
                        sw.WriteLine("protected:");
                        sw.WriteLine("virtual void GC_MarkChildren(int flag);");
                    }
                }

                // Close class
                sw.WriteLine("};");
            }

            // RTTI class
            sw.WriteLine();
            sw.WriteLine("class _{0}_RTTI : public System::RuntimeType", className);
            sw.WriteLine("{");
            sw.WriteLine("_{0}_RTTI();", className);
            sw.WriteLine("public:");
            sw.WriteLine("inline static _{0}_RTTI* Instance()", className);
            sw.WriteLine("{");
            sw.WriteLine("static _{0}_RTTI instance;", className);
            sw.WriteLine("return &instance;");
            sw.WriteLine("}");
            sw.WriteLine();
            sw.WriteLine("virtual System::Object *CreateNewWithDefaultConstructor();");
            sw.WriteLine("virtual System::Object *CreateNew(System::Array<System::Object*> parameters);");
            sw.WriteLine("virtual ~_{0}_RTTI();", className);
            sw.WriteLine("};");
            // Close namespaces
            for (int i = 0; i < curns.Count; ++i)
            {
                sw.WriteLine("}");
            }
            sw.WriteLine();

            sw.WriteLine("#endif");
        }

        public void Dispose()
        {
            if (sw != null)
            {
                sw.Dispose();
                sw = null;
            }
        }
    }
}
