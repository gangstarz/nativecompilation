﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Dependencies;

namespace NativeCompiler.Language
{
    public interface ILanguageEmitter
    {
        void Write(string baseFilename, MethodBase main, IEnumerable<DependentType> types);
    }
}
