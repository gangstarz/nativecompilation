﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.Transformations
{
    public class ClosedBlockElimination : ICodeTransformation
    {
        public void Transform(AST.ASTNode body)
        {
            // Rewrite [.. goto x, label x..] -> goto is not useful.
            // Label can be removed if the goto is empty
            foreach (var branche in body.ChildrenOf<Goto>().ToList())
            {
                var label = branche.NextSibling() as Label;
                if (label != null && branche.Target == label)
                {
                    branche.Target = null; // also removes the 'usedBy'
                    if (label.UsedBy.Count == 0)
                    {
                        label.Remove();
                    }
                    branche.Remove();
                }
            }

            foreach (var stmt in body.AllChildrenOf<Goto>())
            {
                TryEliminate(stmt);
            }
        }

        private bool IsEndOfScope(ASTNode node)
        {
            Statement curNode = node as Statement;
            if (curNode != null)
            {
                if (curNode is Goto || curNode is Return || curNode is Throw || curNode is Rethrow)
                {
                    return true;
                }
                else if (curNode is IfThenElse)
                {
                    IfThenElse ite = (IfThenElse)curNode;
                    return (IsEndOfScope(ite.ThenBody.Last) && (ite.ElseBody == null || IsEndOfScope(ite.ElseBody.Last)));
                }
                else if (curNode is Switch)
                {
                    Switch sw = (Switch)curNode;
                    return sw.Cases.All((a) => IsEndOfScope(a.Body.Last));
                }
            }
            return false;
        }

        private void TryEliminate(Goto stmt)
        {
            if (object.ReferenceEquals(stmt.NextSibling(), stmt.Target))
            {
                // Not a useful goto I'd say.
                stmt.Remove(true);
            }
            else if (stmt.Target.UsedBy.Count == 1 && stmt.Target > stmt)
            {
                if (!(stmt.PathTo(stmt.Target).Any((a) => a is InitializeVariable))) // this means pain and horror...
                {
                    var node = stmt.Target;

                    var prev = node.PreviousSibling();
                    Statement firstStatement = null;

                    if (IsEndOfScope(prev))
                    {
                        var curNode = node.NextSibling();
                        firstStatement = (Statement)curNode;
                        ASTNode insertPosition = stmt;
                        while (curNode != null && !(IsEndOfScope(curNode)) && !(curNode is Label))
                        {
                            var tmp = curNode;
                            curNode = curNode.NextSibling();

                            tmp.Remove();
                            tmp.InsertAfter(insertPosition);
                            insertPosition = tmp;
                        }
                        if (curNode is Label)
                        {
                            var newGoto = new Goto((Label)curNode);
                            newGoto.InsertAfter(insertPosition);
                        }
                        else
                        {
                            curNode.Remove();
                            curNode.InsertAfter(insertPosition);
                        }

                        node.Remove();
                        stmt.Remove();
                    }
                }
            }
        }

    }
}
