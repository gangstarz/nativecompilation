﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;

namespace NativeCompiler.Language.Transformations
{
    public class ForVariables : ICodeTransformation
    {
        public void Transform(AST.ASTNode body)
        {
            foreach (var forloop in body.AllChildrenOf<For>())
            {
                TryIntroduceVariables(forloop);
            }
        }

        private void TryIntroduceVariables(For stmt)
        {
            var prev = stmt.PreviousSibling() as InitializeVariable;
            if (prev != null && prev.Value != null &&
                stmt.PreCondition == null && stmt.Variable == null)
            {
                var variable = prev.Variable;
                if (stmt.PostCondition.AllChildrenOf<Variable>().Where((a) => a.Declaration == variable).Any())
                {
                    // Check if we can inline the variable into the 'for'. We can do this iff the variable is not used outside of the for loop.

                    var node = stmt.NextSibling();
                    bool valid = true;
                    while (node != null && valid)
                    {
                        valid = !node.Uses(variable);
                        node = node.NextSibling();
                    }

                    if (valid)
                    {
                        // inline the variable
                        stmt.Variable = variable;
                        var tmp = prev.Value;
                        tmp.Remove();
                        stmt.PreCondition = new Store(null, variable, tmp);

                        prev.Remove();
                    }
                }
            }

        }
    }
}
