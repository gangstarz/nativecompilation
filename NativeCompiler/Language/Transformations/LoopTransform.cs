﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;

namespace NativeCompiler.Language.Transformations
{
    public class LoopTransform : ICodeTransformation
    {
        public void Transform(AST.ASTNode body)
        {
            foreach (var item in body.AllChildrenOf<Scope>())
            {
                CreateWhileFor(item);
                CreateDoWhile(item);
            }
        }

        private void CreateWhileFor(Scope scope)
        {
            for (int i = 0; i < scope.Children.Count - 1; ++i)
            {
                var continueGoto = scope[i] as Goto;
                var startLoopLabel = scope[i + 1] as Label;
                var continueLoop = continueGoto == null ? null : continueGoto.Target;

                // goto [continue loop]
                // label [start loop]
                // [body]
                // label [continue loop]
                // if (condition)
                // {
                //   goto [start loop]; // and only this
                // }
                // // no else

                if (continueGoto != null && startLoopLabel != null && continueLoop != null &&
                    continueLoop > startLoopLabel &&
                    startLoopLabel.UsedBy.Count == 1 && startLoopLabel.UsedBy[0] > continueLoop)
                {
                    var startLoopGoto = startLoopLabel.UsedBy[0];
                    if (startLoopGoto.Parent is Scope && startLoopGoto.Parent.NumberOfChildren == 1 &&
                        startLoopGoto.Parent.Parent is IfThenElse && ((IfThenElse)startLoopGoto.Parent.Parent).ElseBody == null)
                    {
                        var ite = (IfThenElse)startLoopGoto.Parent.Parent;

                        // everything between continueLoop..ite has to be initialization and in the same scope
                        if (ite.Parent == continueLoop.Parent)
                        {
                            var node = continueLoop.NextSibling();
                            if (node == ite)
                            {
                                // Nice, we have a loop.

                                var lastStatement = continueLoop.PreviousSibling() as ExpressionStatement;

                                // 'for' loop check:
                                bool isWhileLoop = true;
                                if (lastStatement != null)
                                {
                                    var es = ((ExpressionStatement)lastStatement).Expression;

                                    if ((es is PrefixUnaryOperator && new[] { "++", "--" }.Contains(((PrefixUnaryOperator)es).Operator)) ||
                                        (es is SuffixUnaryOperator && new[] { "++", "--" }.Contains(((SuffixUnaryOperator)es).Operator)))
                                    {
                                        isWhileLoop = false;
                                    }
                                }

                                if (isWhileLoop)
                                {
                                    // Construct AST tree for while node
                                    var condition = ite.Condition;
                                    condition.Remove();
                                    var whileLoop = new While(condition);
                                    whileLoop.Body.MoveFrom(startLoopLabel.NextSibling(), continueLoop);

                                    whileLoop.InsertBefore(continueGoto);

                                    // remove the labels and goto's etc
                                    ite.Remove();
                                    continueLoop.Remove();
                                    startLoopLabel.Remove();
                                    continueGoto.Remove();
                                }
                                else
                                {
                                    // Construct AST tree for 'for' node
                                    var condition = ite.Condition;
                                    condition.Remove();

                                    var postCondition = ((ExpressionStatement)lastStatement).Expression;
                                    postCondition.Remove();
                                    lastStatement.Remove();

                                    var forLoop = new For(true);
                                    forLoop.PostCondition = postCondition;
                                    forLoop.Condition = condition;
                                    forLoop.Body.MoveFrom(startLoopLabel.NextSibling(), continueLoop);
                                    forLoop.InsertBefore(continueGoto);

                                    // remove the labels and goto's etc
                                    ite.Remove();
                                    continueLoop.Remove();
                                    startLoopLabel.Remove();
                                    continueGoto.Remove();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void CreateDoWhile(Scope scope)
        {
            for (int i = 0; i < scope.Children.Count; ++i)
            {
                var startLabel = scope.Children[i] as Label;
                if (startLabel != null && startLabel.UsedBy.Count == 1)
                {
                    var startJump = startLabel.UsedBy[0];

                    if (startJump > startLabel && startJump.Parent != null && startJump.Parent.Children.Count == 1 &&
                        (startJump.Parent.Parent as IfThenElse) != null)
                    {
                        var ite = (IfThenElse)startJump.Parent.Parent;
                        if (ite.Parent == startLabel.Parent)
                        {
                            // We have a do..while loop

                            // Move the 'else' part outside of the if-then-else branche.
                            if (ite.ElseBody != null)
                            {
                                ASTNode node;
                                while ((node = ite.ElseBody.Last) != null)
                                {
                                    node.Remove();
                                    node.InsertAfter(ite);
                                }
                            }

                            // Get the condition
                            var cond = ite.Condition;

                            // Check if the condition depends on the body. This is the case if the condition 
                            // uses any of the variables that was initialized in the body.
                            bool hasDependency = false;
                            foreach (var item in startLabel.NextSibling().PathTo(ite))
                            {
                                if (item is InitializeVariable && cond.Uses(((InitializeVariable)item).Variable))
                                {
                                    hasDependency = true;
                                    break;
                                }
                                else if (item is Goto)
                                {
                                    Goto g = (Goto)item;
                                    if (g.Target < startLabel || g.Target > startJump)
                                    {
                                        hasDependency = true;
                                        break;
                                    }
                                }
                                else if (item is Label)
                                {
                                    Label lbl = (Label)item;
                                    if (lbl.UsedBy.Any((a) => a < startLabel || a > startJump))
                                    {
                                        hasDependency = true;
                                        break;
                                    }
                                }
                            }

                            if (!hasDependency)
                            {
                                // Construct the do-while AST node
                                cond.Remove();
                                var doWhile = new DoWhile(cond);
                                doWhile.Body.MoveFrom(startLabel.NextSibling(), ite);

                                // Insert the do-while into the AST tree and clean up the garbage
                                doWhile.InsertBefore(startLabel);
                                startLabel.Remove();
                                ite.Remove();
                            }
                        }
                    }
                }
            }
        }
    }
}
