﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.Transformations
{
    public class LogicalOperators : ICodeTransformation
    {
        public void Transform(AST.ASTNode body)
        {
            foreach (var ite in body.AllChildrenOf<IfThenElse>())
            {
                Analyze(ite);
            }
        }

        public void Analyze(IfThenElse stmt)
        {
            VariableDeclaration temporary = null;

            // Find temporary variable (if it exists...)
            var node = stmt;
            while (node is IfThenElse && node.ThenBody.NumberOfChildren == 1 && node.ThenBody.Children[0] is IfThenElse)
            {
                node = (IfThenElse)node.ThenBody.Children[0];
            }
            if (node != null && node.ThenBody.NumberOfChildren == 1 && node.ThenBody.Children[0] is ExpressionStatement)
            {
                Store st = ((ExpressionStatement)node.ThenBody.Children[0]).Expression as Store;
                if (st != null && st.Object == null)
                {
                    temporary = st.Target;
                }
            }

            if (temporary != null && stmt.ThenBody.NumberOfChildren == 1)
            {
                // Check for initialization to 0
                var init = stmt.PreviousSibling() as InitializeVariable;
                bool canAssumeFalse = init != null && init.Variable == temporary && (init.Value == null || (init.Value is Constant<int> && ((Constant<int>)init.Value).Value == 0));

                var expr = BuildLogicOperatorsRecursive(stmt, temporary, canAssumeFalse);

                if (expr != null)
                {
                    var store = new ExpressionStatement(new Store(null, temporary, expr));
                    stmt.SubstituteBy(store);

                    return;
                }
            }

            // Build logic operators || and &&
            // if (x) {
            //   if (y) {
            //     ...
            //   }
            // }
            //
            // ->
            //
            // if (x && y) {
            //   ..
            // }

            if (stmt.ElseBody == null && stmt.ThenBody.NumberOfChildren == 1)
            {
                var ite2 = stmt.ThenBody.First as IfThenElse;
                if (ite2 != null && ite2.ElseBody == null)
                {
                    var cond1 = stmt.Condition;
                    var cond2 = ite2.Condition;
                    var body = ite2.ThenBody;

                    // Remove nodes from AST tree
                    stmt.ThenBody.Remove();
                    cond1.Remove();
                    cond2.Remove();
                    body.Remove();

                    // x && y
                    var newCondition = Operators.LogicalAnd(cond1, cond2);
                    stmt.Add(newCondition);

                    // add body
                    stmt.Add(body);
                }
            }
        }

        private Expression BuildLogicOperatorsRecursive(IfThenElse ite, VariableDeclaration temporary, bool canAssumeFalse)
        {
            ExpressionStatement es;
            if (ite.ThenBody != null && ite.ElseBody == null && canAssumeFalse && ite.ThenBody.NumberOfChildren == 1)
            {
                if ((es = ite.ThenBody[0] as ExpressionStatement) != null && es.Expression is Store)
                {
                    var store = (Store)es.Expression;

                    if (store.Target == temporary && store.Object == null)
                    {
                        // if (.1.) { my_bool = [something]; }
                        //
                        // ..and we can assume a 'false'
                        // ->
                        // my_bool = (.1.) & [something];

                        var cond1 = ite.Condition;
                        var cond2 = store.Value;

                        cond1.Remove();
                        cond2.Remove();

                        return Operators.LogicalAnd(cond1, cond2);
                    }
                }
                else if (ite.ThenBody[0] is IfThenElse)
                {
                    // Nested 'if'; combine conditions as 'AND'.
                    var expression = BuildLogicOperatorsRecursive((IfThenElse)ite.ThenBody[0], temporary, canAssumeFalse);

                    if (expression != null)
                    {
                        var cond1 = ite.Condition;
                        cond1.Remove();

                        return Operators.LogicalAnd(cond1, expression);
                    }
                }
            }

            if (ite.ElseBody != null && ite.ThenBody.NumberOfChildren == 1 && ite.ElseBody.NumberOfChildren == 1)
            {
                var itelhs = ite.ThenBody[0] as IfThenElse;
                var stlhs = (ite.ThenBody[0] is ExpressionStatement) ? ((ExpressionStatement)ite.ThenBody[0]).Expression as Store : null;
                if (stlhs != null && stlhs.Target != temporary)
                {
                    stlhs = null;
                }

                var iterhs = ite.ElseBody[0] as IfThenElse;
                var strhs = (ite.ElseBody[0] is ExpressionStatement) ? ((ExpressionStatement)ite.ElseBody[0]).Expression as Store : null;
                if (strhs != null && strhs.Target != temporary)
                {
                    strhs = null;
                }

                if ((itelhs != null || stlhs != null) && (iterhs != null || strhs != null))
                {
                    Expression lhsExpr;
                    if (itelhs != null)
                    {
                        lhsExpr = BuildLogicOperatorsRecursive(itelhs, temporary, canAssumeFalse);
                    }
                    else
                    {
                        lhsExpr = stlhs.Value;
                    }

                    Expression rhsExpr;
                    if (iterhs != null)
                    {
                        rhsExpr = BuildLogicOperatorsRecursive(iterhs, temporary, canAssumeFalse);
                    }
                    else
                    {
                        rhsExpr = strhs.Value;
                    }

                    if (rhsExpr != null && lhsExpr is Constant<int> && ((Constant<int>)lhsExpr).Value == 0)
                    {
                        // if (.1.) { my_bool = 0 (false); } else { my_bool = (.2.); }
                        // ->
                        // my_bool = !(.1.) && (.2.)

                        var cond1 = ite.Condition;
                        cond1.Remove();
                        if (rhsExpr.Parent != null) { rhsExpr.Remove(); }

                        return Operators.LogicalAnd(Operators.LogicalNot(cond1), rhsExpr);
                    }
                    else if (lhsExpr != null && rhsExpr is Constant<int> && ((Constant<int>)rhsExpr).Value == 0)
                    {
                        // if (.1.) { my_bool = (.2.); } else { my_bool = 0 (false); }
                        // ->
                        // my_bool = (.1.) && (.2.)

                        var cond1 = ite.Condition;
                        cond1.Remove();
                        if (lhsExpr.Parent != null) { lhsExpr.Remove(); }

                        return Operators.LogicalAnd(cond1, lhsExpr);
                    }
                    else if (rhsExpr != null && lhsExpr is Constant<int> && ((Constant<int>)lhsExpr).Value == 1)
                    {
                        // if (.1.) { my_bool = 1 (true); } else { my_bool = (.2.); }
                        // ->
                        // my_bool = (.1.) || (.2.)

                        var cond1 = ite.Condition;
                        cond1.Remove();
                        if (rhsExpr.Parent != null) { rhsExpr.Remove(); }

                        return Operators.LogicalOr(cond1, rhsExpr);
                    }
                    else
                    {
                        // unfortunately we cannot fix this... see if we broke any if-then-elses:
                        if (lhsExpr != null && itelhs != null)
                        {
                            itelhs.SubstituteBy(new ExpressionStatement(new Store(null, temporary, lhsExpr)));
                        }

                        if (rhsExpr != null && iterhs != null)
                        {
                            iterhs.SubstituteBy(new ExpressionStatement(new Store(null, temporary, rhsExpr)));
                        }
                    }
                }
            }

            return null;
        }


    }
}
