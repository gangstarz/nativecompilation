﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST;
using NativeCompiler.Language.AST.Statements;

namespace NativeCompiler.Language.Transformations
{
    public class ScopeFolding : ICodeTransformation
    {
        public void Transform(AST.ASTNode body)
        {
            // Do this recursive: we really don't want to rewrite something we've just rewritten.

            if (body is Scope)
            {
                var stmt = (Scope)body;

                // First thing we do is collect all the statements in the same sub-tree. 
                // Rewrite all scope[..scope[*]..] to scope[..*..]
                foreach (var scope in stmt.ChildrenOf<Scope>().ToList())
                {
                    stmt.MoveFromBefore(scope, 0, scope.NumberOfChildren, scope);
                    scope.Remove();
                }
            }

            for (int i = 0; i < body.Children.Count; ++i)
            {
                Transform(body.Children[i]);
            }
        }
    }
}
