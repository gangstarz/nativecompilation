﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;

namespace NativeCompiler.Language.Transformations
{
    public class EqualsOptimizer : ICodeTransformation
    {
        public void Transform(AST.ASTNode body)
        {
            foreach (var bop in body.AllChildrenOf<BinaryOperator>())
            {
                Optimize(bop);
            }
        }

        private void Optimize(BinaryOperator expr)
        {
            // .NET uses a trick with binary operators: ([boolean]) == 0 is actually !([boolean])
            if (expr.Operator == "==")
            {
                if (expr.Lhs is BinaryOperator && expr.Rhs is Constant<int> && ((Constant<int>)expr.Rhs).Value == 0)
                {
                    var target = expr.Lhs;
                    target.Remove();
                    expr.SubstituteBy(Operators.LogicalNot(target));
                }
                else if (expr.Rhs is BinaryOperator && expr.Lhs is Constant<int> && ((Constant<int>)expr.Lhs).Value == 0)
                {
                    var target = expr.Rhs;
                    target.Remove();
                    expr.SubstituteBy(Operators.LogicalNot(target));
                }
            }
        }
    }
}
