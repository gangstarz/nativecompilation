﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;

namespace NativeCompiler.Language.Transformations
{
    public class LogicalNotOptimizer : ICodeTransformation
    {
        public void Transform(AST.ASTNode body)
        {
            foreach (var puo in body.AllChildrenOf<PrefixUnaryOperator>())
            {
                OptimizeLogicalNot(puo);
            }
        }

        private void OptimizeLogicalNot(PrefixUnaryOperator stmt)
        {
            if (stmt.Operator == "!") // logical not
            {
                var bop = stmt.Child as BinaryOperator;
                if (bop != null)
                {
                    // remove nodes from the current AST tree: we're going to create another subtree using these
                    var lhs = bop.Lhs;
                    var rhs = bop.Rhs;
                    lhs.Remove();
                    rhs.Remove();

                    // Create new expression
                    Expression expr = null;
                    switch (bop.Operator)
                    {
                        case "<": expr = Operators.LargerEquals(lhs, rhs); break;
                        case "<=": expr = Operators.Larger(lhs, rhs); break;
                        case ">": expr = Operators.SmallerEquals(lhs, rhs); break;
                        case ">=": expr = Operators.Smaller(lhs, rhs); break;
                        case "==": expr = Operators.NotEquals(lhs, rhs); break;
                        case "!=": expr = Operators.Equals(lhs, rhs); break;
                        case "&&": expr = DeMorgan(lhs, rhs); break;
                    }

                    if (expr == null)
                    {
                        // Not sure, restore it the way it was.
                        expr = stmt;
                        bop.Add(lhs);
                        bop.Add(rhs);
                    }

                    if (!object.ReferenceEquals(expr, stmt))
                    {
                        stmt.SubstituteBy(expr);
                    }
                }

                var puo = stmt.Child as PrefixUnaryOperator;
                if (puo != null && puo.Operator == "!")
                {
                    var repl = puo.Child;
                    repl.Remove();
                    stmt.SubstituteBy(repl);
                }
            }
        }

        private Expression DeMorgan(Expression lhs, Expression rhs)
        {
            PrefixUnaryOperator puo = lhs as PrefixUnaryOperator;
            if (puo != null && puo.Operator == "!")
            {
                // not(not(x)) | rhs => x | !rhs
                var lhsval = puo.Child;
                lhsval.Remove();

                return Operators.LogicalOr(lhsval, Operators.LogicalNot(rhs));
            }

            // !(a && b) = !a || !b
            BinaryOperator bop = lhs as BinaryOperator;
            if (bop != null && bop.Operator == "!=")
            {
                return Operators.LogicalOr(Operators.LogicalNot(lhs), Operators.LogicalNot(rhs));
            }
            else
            {
                return null;
            }
        }

    }
}
