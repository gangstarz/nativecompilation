﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Statements;

namespace NativeCompiler.Language.Transformations
{
    public class ImplicitLastReturn : ICodeTransformation
    {
        public void Transform(AST.ASTNode body)
        {
            // If the last statement is a 'return' and we have no return value, remove it.
            var last = body.Last as Return;
            if (last != null && last.Result == null)
            {
                last.Remove(true);
            }

            // If it's an if-then-else with a blank return as last statement, remove it.
            var ite = body.Last as IfThenElse;
            if (ite != null)
            {
                RemoveReturnIfThenElse(ite);
            }
        }

        private static void RemoveReturnIfThenElse(IfThenElse ite)
        {
            // if (...)
            // {
            //   ...
            //   return;
            // }
            // else {
            //   ...
            //   return;
            // }
            //
            // Both return statements can be eliminated.
            foreach (var child in ite.Children)
            {
                var last = child.Last as Return;
                if (last != null && last.Result == null)
                {
                    last.Remove(true);
                }
                var iteChild = child.Last as IfThenElse;
                if (iteChild != null)
                {
                    RemoveReturnIfThenElse(iteChild);
                }
            }
        }

    }
}
