﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST;

namespace NativeCompiler.Language.Transformations
{
    public interface ICodeTransformation
    {
        void Transform(ASTNode body);
    }
}
