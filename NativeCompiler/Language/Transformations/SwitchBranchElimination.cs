﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;

namespace NativeCompiler.Language.Transformations
{
    public class SwitchBranchElimination : ICodeTransformation
    {
        public void Transform(ASTNode body)
        {
            foreach (var sw in body.AllChildrenOf<Switch>())
            {
                Visit(sw);
            }
        }

        public void Visit(Switch stmt)
        {
            var node = stmt.NextSibling();

            // Construct / update the default case.
            //
            // First check if it's allowed:
            if (!stmt.Cases.Any((a) => a.HasDefault) &&
                stmt.Cases.All((a) => a.Body.Last is Goto))
            {
                var defaultCase = new Case(true, new Expression[0]);
                while (node != null && !node.HasChildOf<Label>() && !(node is Label))
                {
                    node.Remove();
                    defaultCase.Body.Add(node);
                    node = node.NextSibling();
                }

                stmt.Add(defaultCase);
            }

            // If we have multiple cases with the same body -> merge them.
            bool mustUpdate = false;
            Dictionary<Scope, List<Case>> scopes = new Dictionary<Scope, List<Case>>();
            foreach (var cas in stmt.Cases)
            {
                List<Case> ll;
                if (!scopes.TryGetValue(cas.Body, out ll))
                {
                    ll = new List<Case>();
                    scopes.Add(cas.Body, ll);
                }
                else
                {
                    mustUpdate = true;
                }
                ll.Add(cas);
            }

            if (mustUpdate)
            {
                // Invert back to switch
                foreach (var kv in scopes)
                {
                    Case newCase = new Case(kv.Value.Any((a) => a.HasDefault));

                    // Add a scope
                    var scope = new Scope();
                    scope.MoveAllFrom(kv.Key);
                    newCase.Add(scope);
                    for (int i = 0; i < kv.Value.Count; ++i)
                    {
                        foreach (var expr in kv.Value[i].Expressions.ToList())
                        {
                            expr.Remove();
                            newCase.Add(expr);
                        }

                        kv.Value[i].Remove(true);
                    }

                    stmt.Add(newCase);
                }
            }

            // Check if we can remove labels
            var afterStatement = stmt.NextSibling();
            if (afterStatement is Label)
            {
                foreach (var cas in stmt.Cases)
                {
                    var last = cas.Body.Last as Goto;
                    if (last != null && object.ReferenceEquals(last.Target, afterStatement))
                    {
                        last.Remove(true);
                    }
                }

                if (((Label)afterStatement).UsedBy.Count == 0)
                {
                    afterStatement.Remove(true);
                }
            }
        }
    }
}
