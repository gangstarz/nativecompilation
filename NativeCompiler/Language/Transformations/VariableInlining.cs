﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.Transformations
{
    public class VariableInlining : ICodeTransformation
    {
        public void Transform(AST.ASTNode body)
        {
            // If we have [.. init_var x, x = y ..], we might as well assign x=y immediately at variable initialization
            foreach (var init in body.ChildrenOf<InitializeVariable>().Where((a) => a.Value == null).ToList())
            {
                var expr = init.NextSibling() as ExpressionStatement;
                if (expr != null && expr.Expression is Store && ((Store)expr.Expression).Target == init.Variable)
                {
                    var tmp = ((Store)expr.Expression).Value;
                    tmp.Remove();
                    init.Add(tmp);

                    expr.Remove();
                }
            }

            foreach (var stmt in body.AllChildrenOf<Scope>())
            {
                InlineVariables(stmt);
            }

            foreach (var stmt in body.AllChildrenOf<InitializeVariable>())
            {
                InlineTemporaryVariables(stmt);
            }

            foreach (var stmt in body.AllChildrenOf<ExpressionStatement>())
            {
                InlineStoreVariables(stmt);
            }
        }

        private void InlineVariables(Scope stmt)
        {
            // If we have a variable that's only used once, we might as well inline it IF it doesn't require order.
            Dictionary<VariableDeclaration, List<ASTNode>> variableInlines = new Dictionary<VariableDeclaration, List<ASTNode>>();

            foreach (var variable in stmt.AllChildrenOf<Variable>())
            {
                List<ASTNode> ll;
                if (!variableInlines.TryGetValue(variable.Declaration, out ll))
                {
                    ll = new List<ASTNode>();
                    variableInlines.Add(variable.Declaration, ll);
                }
                ll.Add(variable);
            }

            foreach (var variable in stmt.AllChildrenOf<InitializeVariable>())
            {
                List<ASTNode> ll;
                if (!variableInlines.TryGetValue(variable.Variable, out ll))
                {
                    ll = new List<ASTNode>();
                    variableInlines.Add(variable.Variable, ll);
               }
                ll.Add(variable);
            }

            foreach (var variable in stmt.AllChildrenOf<Store>())
            {
                List<ASTNode> ll;
                if (!variableInlines.TryGetValue(variable.Target, out ll))
                {
                    ll = new List<ASTNode>();
                    variableInlines.Add(variable.Target, ll);
                }
                ll.Add(variable);
            }

            foreach (var variable in stmt.AllChildrenOf<For>())
            {
                if (variable.Variable != null)
                {
                    List<ASTNode> ll;
                    if (!variableInlines.TryGetValue(variable.Variable, out ll))
                    {
                        ll = new List<ASTNode>();
                        variableInlines.Add(variable.Variable, ll);
                    }
                    ll.Add(variable);
                }
            }

            foreach (var kv in variableInlines)
            {
                if (kv.Value.Count == 1 && kv.Value[0] is InitializeVariable)
                {
                    InitializeVariable iv = (InitializeVariable)kv.Value[0];
                    if (iv.Value == null) // well that's easy... :-)
                    {
                        iv.Remove(true);
                    }
                    else
                    {
                        var val = iv.Value;
                        val.Remove();
                        ExpressionStatement es = new ExpressionStatement(val);
                        es.InsertBefore(iv);
                        iv.Remove(true);
                    }
                }
                else if (kv.Value.Count == 2)
                {
                    // Check if we *can* inline it.
                    var v1 = kv.Value[0];
                    var v2 = kv.Value[1];
                    if (v1 > v2)
                    {
                        var tmp = v1;
                        v1 = v2;
                        v2 = tmp;
                    }

                    bool dependsOnUndefinedFlow = false;
                    HashSet<VariableDeclaration> dependentVariables = new HashSet<VariableDeclaration>(ObjectEquality<VariableDeclaration>.Instance);
                    foreach (var node in v1.AllChildren())
                    {
                        if (node is Call || node is NewObject || node is NewArray)
                        {
                            dependsOnUndefinedFlow = true;
                        }
                        else if (node is Store)
                        {
                            dependentVariables.Add(((Store)node).Target);
                        }
                        else if (node is Variable)
                        {
                            dependentVariables.Add(((Variable)node).Declaration);
                        }
                    }

                    // If the v1 value expression contains a call, newobj, newarray, store, variable or storeindirect, we have to
                    // be extra careful. These expressions mean that the result is non-deterministic if another call or 
                    // assignment takes place, which means we cannot move them.

                    if (v1 is InitializeVariable)
                    {
                        // Check if we can inline the variable in v2
                        // This is basically the case if there is no 'border' between v1 and v2. These borders are:
                        // - calls
                        // - loops

                        bool valid = true;
                        foreach (var node in v1.PathTo(v2))
                        {
                            if (dependsOnUndefinedFlow && (node is Call || node is NewObject || node is NewArray))
                            {
                                valid = false;
                                break;
                            }
                            else if ((node is Store && dependentVariables.Contains(((Store)node).Target)) ||
                                     (node is Variable && dependentVariables.Contains(((Variable)node).Declaration)) ||
                                     (node is InitializeVariable && dependentVariables.Contains(((InitializeVariable)node).Variable)))
                            {
                                valid = false;
                                break;
                            }
                            else if (node is DoWhile || node is For || node is While)
                            {
                                valid = false;
                                break;
                            }
                        }

                        if (valid)
                        {
                            var value = ((InitializeVariable)v1).Value;
                            if (value != null)
                            {
                                v1.Remove();
                                v2.SubstituteBy(((InitializeVariable)v1).Value);
                            }
                        }
                    }
                }
            }
        }

        private void InlineTemporaryVariables(InitializeVariable stmt)
        {
            // Trivial inlining case.
            if (stmt.Value == null || CanBeIgnored(stmt.Value))
            {
                var next = stmt.NextSibling() as ExpressionStatement;
                if (next != null && next.Expression is Store)
                {
                    var store = (Store)next.Expression;
                    if (store.Target == stmt.Variable)
                    {
                        if (stmt.Value != null)
                        {
                            stmt.Value.Remove(true);
                        }
                        var tmp = store.Value;
                        tmp.Remove();
                        stmt.Add(tmp);

                        next.Remove(true);
                    }
                }
            }

            // Basic inlining:
            // 
            // If we use a variable right after initialization, and we don't use it anymore or only as store, we can inline it. E.g.:
            // int foo = Blah() + 2; // init
            // if (foo > 4) // use
            // ..
            // [ foo is not used anymore, or is part of store - independent on the path ]

            if (stmt.NextSibling() != null)// Should always be the case
            {
                var used = stmt.NextSibling().AllChildrenOf<Variable>().Where((a) => a.Declaration == stmt.Variable).ToArray();
                if (used.Length == 1)
                {
                    if (CheckAllPathsStore(stmt.NextSibling().NextSibling(), stmt.Variable))
                    {
                        // Inline
                        var value = stmt.Value;
                        if (value != null)
                        {
                            value.Remove();
                            used[0].SubstituteBy(value);
                        }
                    }
                }
            }
        }

        private void InlineStoreVariables(ExpressionStatement stmt)
        {
            // Inlining (2); see InitializeVariable

            var store = stmt.Expression as Store;
            if (store != null && stmt.NextSibling() != null)// Should always be the case
            {
                var used = stmt.NextSibling().AllChildrenOf<Variable>().Where((a) => a.Declaration == store.Target).ToArray();
                if (used.Length == 1)
                {
                    if (CheckAllPathsStore(stmt.NextSibling().NextSibling(), store.Target))
                    {
                        // Inline
                        var value = store.Value;
                        stmt.Remove();
                        value.Remove();
                        used[0].SubstituteBy(value);
                        stmt = null;
                    }
                }
            }
        }

        private bool CheckAllPathsStore(ASTNode node, VariableDeclaration variableDeclaration)
        {
            // Evaluates to 'true' if all code paths lead to a 'store' operation or 'end of code'.
            while (node != null)
            {
                if (node.AllChildrenOf<Variable>().Where((a) => a.Declaration == variableDeclaration).Any())
                {
                    return false;
                }

                if (node is ExpressionStatement)
                {
                    var expr = ((ExpressionStatement)node).Expression;

                    var store = expr as Store;
                    if (store != null && store.Target == variableDeclaration)
                    {
                        return true;
                    }
                }
                else
                {
                    var firstChild = node.Children.OfType<Statement>().FirstOrDefault();
                    if (!CheckAllPathsStore(firstChild, variableDeclaration))
                    {
                        return false;
                    }
                    // else continue to the next statement.
                }

                do
                {
                    node = node.NextSibling();
                }
                while (node != null && !(node is Statement));
            }

            return true;
        }

        private bool CanBeIgnored(Expression expression)
        {
            return !(expression.HasChildOf<Call>() || expression.HasChildOf<NewObject>());
        }
    }
}
