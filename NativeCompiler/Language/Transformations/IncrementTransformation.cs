﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;

namespace NativeCompiler.Language.Transformations
{
    public class IncrementTransformation : ICodeTransformation
    {
        public void Transform(AST.ASTNode body)
        {
            foreach (var stmt in body.AllChildrenOf<ExpressionStatement>())
            {
                PrePostIncrement(stmt);
            }
        }

        private void PrePostIncrement(ExpressionStatement stmt)
        {
            // Pre/Post increment works using a temporary (introduced from 'dup'):
            // i = i + 1;

            var store = stmt.Expression as Store;
            if (store != null && store.Value is BinaryOperator)
            {
                var bop = (BinaryOperator)store.Value;
                if (bop.Operator == "+" &&
                    ((bop.Lhs is Constant<int> && ((Constant<int>)bop.Lhs).Value == 1 && bop.Rhs is Variable && ((Variable)bop.Rhs).Declaration == store.Target) ||
                     (bop.Rhs is Constant<int> && ((Constant<int>)bop.Rhs).Value == 1 && bop.Lhs is Variable && ((Variable)bop.Lhs).Declaration == store.Target)))
                {
                    // i = 1 + i; --> ++i;
                    store.SubstituteBy(Operators.PreIncrement(new Variable(store.Target)));
                }
                else if (bop.Operator == "-" &&
                         bop.Rhs is Constant<int> && ((Constant<int>)bop.Rhs).Value == 1 && bop.Lhs is Variable && ((Variable)bop.Lhs).Declaration == store.Target)
                {
                    // i = i - 1; --> --i;
                    store.SubstituteBy(Operators.PreDecrement(new Variable(store.Target)));
                }
            }
        }
    }
}
