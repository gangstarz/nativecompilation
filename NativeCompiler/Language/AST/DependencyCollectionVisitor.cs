﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST
{
    internal class DependencyCollectionVisitor
    {
        public static void CollectDependencies(ASTNode statement, HashSet<Class> classes, HashSet<object> visited)
        {
            foreach (var iv in statement.AllChildrenOf<InitializeVariable>())
            {
                iv.Variable.Type.CollectDependencies(classes, visited);
            }

            foreach (var st in statement.AllChildrenOf<Store>())
            {
                st.Target.Type.CollectDependencies(classes, visited);
            }

            foreach (var v in statement.AllChildrenOf<Variable>())
            {
                v.Declaration.Type.CollectDependencies(classes, visited);
            }
            
            foreach (var fl in statement.AllChildrenOf<For>())
            {
                if (fl.Variable != null)
                {
                    fl.Variable.Type.CollectDependencies(classes, visited);
                }
            }
        }
    }
}
