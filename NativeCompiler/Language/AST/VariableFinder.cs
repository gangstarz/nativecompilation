﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST
{
    public static class VariableFinderExtensions
    {
        public static bool Uses(this ASTNode node, VariableDeclaration var)
        {
            if (node is Statement)
            {
                return Uses((Statement)node, var);
            }
            else if (node is Expression)
            {
                return Uses((Expression)node, var);
            }
            else
            {
                throw new NotSupportedException("Unsupported AST node");
            }
        }

        public static bool Uses(this Statement scope, VariableDeclaration var)
        {
            return NumberTimesUsed(scope, var) > 0;
        }

        public static bool Uses(this Expression scope, VariableDeclaration var)
        {
            return NumberTimesUsed(scope, var) > 0;
        }

        public static int NumberOfTimesUsed(this ASTNode node, VariableDeclaration var)
        {
            if (node is Statement)
            {
                return NumberOfTimesUsed((Statement)node, var);
            }
            else if (node is Expression)
            {
                return NumberOfTimesUsed((Expression)node, var);
            }
            else
            {
                throw new NotSupportedException("Unsupported AST node");
            }
        }

        public static int NumberOfTimesUsed(this Statement scope, VariableDeclaration var)
        {
            return NumberTimesUsed(scope, var);
        }

        public static int NumberOfTimesUsed(this Expression scope, VariableDeclaration var)
        {
            return NumberTimesUsed(scope, var);
        }

        private static int NumberTimesUsed(ASTNode node, VariableDeclaration decl)
        {
            int total = 0;
            total += node.AllChildrenOf<Variable>().Where((a) => object.ReferenceEquals(a.Declaration, decl)).Count();
            total += node.AllChildrenOf<Store>().Where((a) => object.ReferenceEquals(a.Target, decl)).Count();
            total += node.AllChildrenOf<For>().Where((a) => a.Variable!=null && object.ReferenceEquals(a.Variable, decl)).Count();
            return total;
        }
    }
}
