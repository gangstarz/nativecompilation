﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Types
{
    public class Reference : IType
    {
        public Reference(IType target)
        {
            this.Target = target;
        }

        public IType Target { get; private set; }

        public Structure.Class BaseDefinition { get { return Target.BaseDefinition; } }

        public void CollectDependencies(HashSet<Structure.Class> classes, HashSet<object> visited)
        {
            if (visited.Add(this))
            {
                Target.CollectDependencies(classes, visited);
            }
        }

        public MemoryType StorageType { get { return MemoryType.ByReference; } }

        public void Write(System.IO.TextWriter tw)
        {
            Target.Write(tw);
            tw.Write("&");
        }
    }
}
