﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Types
{
    public class ArrayType : IType
    {
        public ArrayType(IType element)
        {
            this.Element = element;
        }

        public IType Element { get; private set; }

        public Structure.Class BaseDefinition { get { return Element.BaseDefinition; } }

        public void CollectDependencies(HashSet<Structure.Class> classes, HashSet<object> visited)
        {
            if (visited.Add(this))
            {
                Element.CollectDependencies(classes, visited);
            }
        }

        public MemoryType StorageType { get { return MemoryType.ReferenceType; } }

        public void Write(System.IO.TextWriter tw)
        {
            Element.Write(tw);
            tw.Write("[]");
        }
    }
}
