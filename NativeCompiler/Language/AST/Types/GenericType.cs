﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Types
{
    public class GenericType : IType
    {
        public GenericType(IType genericDefinition, List<IType> arguments)
        {
            this.GenericDefinition = genericDefinition;
            this.Arguments = arguments;
        }

        public IType GenericDefinition;
        public List<IType> Arguments;

        public Structure.Class BaseDefinition { get { return GenericDefinition.BaseDefinition; } }

        public bool ValueType
        {
            get
            {
                if (GenericDefinition is ClassType)
                {
                    return ((ClassType)GenericDefinition).Definition.ValueType;
                }
                else if (GenericDefinition is GenericType)
                {
                    return ((GenericType)GenericDefinition).ValueType;
                }
                else
                {
                    throw new NotSupportedException("Incorrect generic type");
                }
            }
        }

        public void CollectDependencies(HashSet<Structure.Class> classes, HashSet<object> visited)
        {
            if (visited.Add(this))
            {
                GenericDefinition.CollectDependencies(classes, visited);
                foreach (var arg in Arguments)
                {
                    arg.CollectDependencies(classes, visited);
                }
            }
        }

        public MemoryType StorageType { get { return ValueType?MemoryType.ValueType:MemoryType.ReferenceType; } }

        public void Write(System.IO.TextWriter tw)
        {
            GenericDefinition.Write(tw);
            tw.Write("<");
            for (int i = 0; i < Arguments.Count; ++i)
            {
                if (i != 0)
                {
                    tw.Write(", ");
                }
                Arguments[i].Write(tw);
            }
            tw.Write(">");
        }
    }
}
