﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Types
{
    public class BasicType : IType
    {
        public BasicType(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }

        public Structure.Class BaseDefinition { get { return null; } }

        public static bool IsBasicType(Type t)
        {
            return t == typeof(bool) || t == typeof(byte) || t == typeof(sbyte) || t == typeof(ushort) || t == typeof(short) || t == typeof(char) || 
                   t == typeof(int) || t == typeof(uint) || t == typeof(long) || t == typeof(ulong) || t == typeof(string) || t == typeof(object) ||
                   t == typeof(double) || t == typeof(float) || t == typeof(decimal) || 
                   t == typeof(ValueType) || t == typeof(Exception) || t == typeof(Array) || t == typeof(void) || t == typeof(Type);
        }

        public bool ValueType
        {
            get
            {
                return !(Name == "System.String" || Name == "System.Object" || Name == "System.Array" || Name == "System.Type" || Name == "System.Exception");
            }
        }

        public void CollectDependencies(HashSet<Structure.Class> classes, HashSet<object> visited)
        {
        }

        public MemoryType StorageType { get { return ValueType ? MemoryType.ValueType : MemoryType.ReferenceType; } }

        public void Write(System.IO.TextWriter tw)
        {
            string n = Name;
            if (n.StartsWith("System.")) { n = n.Substring("System.".Length); }
            tw.Write(n);
        }

    }
}
