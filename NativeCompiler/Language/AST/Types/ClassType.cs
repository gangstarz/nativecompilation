﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST.Types
{
    public class ClassType : IType
    {
        public ClassType(Class definition)
        {
            this.Definition = definition;
        }

        public Structure.Class BaseDefinition { get { return Definition; } }

        public Class Definition { get; private set; }

        public void CollectDependencies(HashSet<Structure.Class> classes, HashSet<object> visited)
        {
            classes.Add(Definition);
        }

        public MemoryType StorageType { get { return Definition.ValueType ? MemoryType.ValueType : MemoryType.ReferenceType; } }

        public void Write(System.IO.TextWriter tw)
        {
            tw.Write(Definition.Name);
        }
    }
}
