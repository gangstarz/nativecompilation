﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NativeCompiler.Language.AST.Types
{
    public interface IType
    {
        Structure.Class BaseDefinition { get; }
        void CollectDependencies(HashSet<Structure.Class> classes, HashSet<object> visited);

        MemoryType StorageType { get; }

        void Write(System.IO.TextWriter tw);
    }
}
