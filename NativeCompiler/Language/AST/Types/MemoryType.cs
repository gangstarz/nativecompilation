﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Types
{
    public enum MemoryType
    {
        ValueType, // lives on the stack
        ReferenceType, // lives on the heap
        Pointer, // lives unmanaged
        ByReference // lives as reference to either value or reference type
    }
}
