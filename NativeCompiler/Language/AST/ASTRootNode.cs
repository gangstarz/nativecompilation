﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST
{
    public class ASTRootNode : ASTNode
    {
        protected override ASTNode ShallowClone(object context)
        {
            throw new NotSupportedException("Cloning a root node is not supported");
        }

        protected override int CalculateHashcode()
        {
            return 1;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return o is ASTRootNode;
        }

        public override void Write(System.IO.TextWriter tw)
        {
        }
    }
}
