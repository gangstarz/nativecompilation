﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST
{
    public static class EntryPointFinderExtensions
    {
        public static Method FindEntryPoint(this Namespace ns)
        {
            var visitor = new EntryPointFinder();
            ns.Accept(visitor);
            return visitor.EntryPoint;
        }
    }

    public class EntryPointFinder : AST.ASTStructureVisitorBase
    {
        public Method EntryPoint { get; private set; }

        public override void Visit(Structure.Method method)
        {
            if (method.IsStatic && method.Name == "Main" && method.Container.GenericArguments == null)
            {
                // Check for: 'void Main(string[])', 'void Main()', 'int Main(string[])' and 'int Main()'
                //
                if (method.ReturnType is BasicType && 
                    (((BasicType)method.ReturnType).Name == "System.Void" || ((BasicType)method.ReturnType).Name == "System.Int32") &&
                    ((method.Parameters.Count == 1 &&
                      method.Parameters[0].Type is ArrayType &&
                      ((ArrayType)method.Parameters[0].Type).Element is BasicType && 
                      ((BasicType)(((ArrayType)method.Parameters[0].Type).Element)).Name == "System.String") || 
                     method.Parameters.Count == 0))
                {
                    this.EntryPoint = method;
                }
            }
        }
    }
}
