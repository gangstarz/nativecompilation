﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Decompiler.Blocks;
using NativeCompiler.Instructions.IL;
using NativeCompiler.Instructions.Meta;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST
{
    public class BlockBuilder : IBlockVisitor<Statement>
    {
        public BlockBuilder(Scope scope, Class context, 
                            StatementBuilder statementBuilder, ExpressionBuilder expressionBuilder, LabelBuilder labelBuilder,
                            VariableMap currentMap, ASTBuilder astBuilder)
        {
            this.root = scope;
            this.context = context;
            this.expressionBuilder = expressionBuilder;
            this.statementBuilder = statementBuilder;
            this.labelBuilder = labelBuilder;
            this.currentMap = currentMap;
            this.astBuilder = astBuilder;
        }

        private Scope root;

        private ExpressionBuilder expressionBuilder;
        private StatementBuilder statementBuilder;
        private Class context;
        private LabelBuilder labelBuilder;
        private VariableMap currentMap;
        private ASTBuilder astBuilder;

        public Statement Visit(MethodBlock block)
        {
            block.SubBlocks.ForEach((a) => root.Add(a.Visit(this)));
            return root;
        }

        public Statement Visit(NativeCompiler.Decompiler.Blocks.ExceptionBlock block)
        {
            NativeCompiler.Language.AST.Statements.ExceptionBlock eb = new Statements.ExceptionBlock();
            foreach (var sb in block.SubBlocks.Where((a) => a.SubBlocks.Count > 0))
            {
                var result = sb.Visit(this);
                if (result != null)
                {
                    eb.Add(result);
                }
            }
            return eb;
        }

        public Statement Visit(StatementBlock block)
        {
            var newscope = new Scope();

            foreach (var bl in block.SubBlocks)
            {
                var result = bl.Visit(this);
                if (result != null)
                {
                    if (result is Scope)
                    {
                        newscope.MoveAllFrom((Scope)result);
                    }
                    else
                    {
                        newscope.Add(result);
                    }
                }
            }

            return newscope;
        }

        public Statement Visit(IfThenElseBlock block)
        {
            var expr = expressionBuilder.Build(block.Condition);

            IfThenElse ite;
            if (block.Then == null)
            {
                ite = new IfThenElse(Operators.LogicalNot(expr));

                if (block.Else != null)
                {
                    ite.ThenBody.SubstituteBy(block.Else.Visit(this));
                }
            }
            else
            {
                ite = new IfThenElse(expr);

                if (block.Then != null)
                {
                    ite.ThenBody.SubstituteBy(block.Then.Visit(this));
                }

                if (block.Else != null)
                {
                    ite.Add(block.Else.Visit(this));
                }
            }
            return ite;
        }

        public Statement Visit(DoWhileBlock block)
        {
            var expr = expressionBuilder.Build(block.Condition);

            var stmt = new DoWhile(expr);

            if (block.Body != null)
            {
                stmt.Body.SubstituteBy(block.Body.Visit(this));
            }

            return stmt;
        }

        public Statement Visit(WhileBlock block)
        {
            var expr = expressionBuilder.Build(block.Condition);

            var stmt = new While(expr);

            if (block.Body != null)
            {
                stmt.Body.SubstituteBy(block.Body.Visit(this));
            }

            return stmt;
        }

        public Statement Visit(ForBlock block)
        {
            var stmt = new For(true);
            if (block.Condition != null)
            {
                stmt.Condition = expressionBuilder.Build(block.Condition);
            }
            if (block.Precondition != null)
            {
                stmt.PreCondition = expressionBuilder.Build(block.Precondition);
            }
            if (block.Postcondition != null)
            {
                stmt.PostCondition = expressionBuilder.Build(block.Postcondition);
            }

            if (block.Body != null)
            {
                stmt.Body.SubstituteBy(block.Body.Visit(this));
            }

            return stmt;
        }

        public Statement Visit(TryBlock block)
        {
            Try bl = new Try();
            foreach (var sb in block.SubBlocks)
            {
                bl.Body.Add(sb.Visit(this));
            }
            return bl;
        }

        public Statement Visit(CatchBlock block)
        {
            Catch bl = new Catch(this.astBuilder.FindOrCreateType(block.Type), this.currentMap.GetDeclaration(block));
            foreach (var sb in block.SubBlocks)
            {
                bl.Body.Add(sb.Visit(this));
            }
            return bl;
        }

        public Statement Visit(FinallyBlock block)
        {
            Finally bl = new Finally();
            foreach (var sb in block.SubBlocks)
            {
                bl.Body.Add(sb.Visit(this));
            }
            return bl;
        }

        public Statement Visit(FaultBlock block)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(FilterBlock block)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(CodeBlock block)
        {
            var newscope = new Scope();

            foreach (var bl in block.SubBlocks)
            {
                var result = bl.Visit(this);
                if (result != null)
                {
                    if (result is Scope)
                    {
                        Scope s = (Scope)result;
                        newscope.MoveAllFrom(s);
                    }
                    else
                    {
                        newscope.Add(result);
                    }
                }
            }

            return newscope;
        }

        public Statement Visit(ILBlock block)
        {
            var newscope = new Scope();

            if (block.RequiresLabel)
            {
                var lbl = labelBuilder.GetOrDefineLabel(block.MinOffset);
                newscope.Add(lbl);
            }

            foreach (var op in block.StackOperations)
            {
                var stmt = statementBuilder.Build(op);
                if (stmt != null)
                {
                    newscope.Add(stmt);
                }
            }

            return newscope;
        }
    }
}
