﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST
{
    public class GotoElimination
    {
        // Implementation of the goto-elimination algorithm, described in 
        // "Taming control flow: A structured approach to eliminating goto statements"

        public void EliminateGotos(Scope body)
        {
            // Get list of labels and gotos for method
            var labelList = body.AllChildrenOf<Label>().ToList();
            var gotoList = body.AllChildrenOf<Goto>().ToList();

            // Introduce and initialize the logical variables
            int n = 0;
            foreach (var lbl in labelList)
            {
                // Introduce a variable for each label
                var variable = new VariableDeclaration(VariableType.Local, new BasicType("System.Boolean"), "label" + (++n));

                // Introduce a statement that initializes the label to 'false', just after the label
                var init = new InitializeVariable(variable, new Constant<int>(0));
                init.InsertAfter(lbl);
            }

            // Change unconditional goto's to conditional goto's.
            foreach (var br in gotoList)
            {
                IfThenElse ite;
                if (!(br.Parent is Scope) || (ite = br.Parent.Parent as IfThenElse) == null || ite.ElseBody != null || ite.ThenBody.NumberOfChildren != 1)
                {
                    var condition = new Constant<int>(1);
                    
                    // Make it a conditional branche
                    var conditionalBranche = new IfThenElse(condition);
                    conditionalBranche.InsertBefore(br);
                    br.Remove();
                    conditionalBranche.ThenBody.Add(br);
                }
            }

            // Eliminate gotos
            while (gotoList.Count > 0)
            {
                var g = gotoList[gotoList.Count - 1];
                var l = g.Target;

                // Force l and g to be directly related
                if (IsIndirectlyRelated(g, l))
                {
                    if (DifferentStatements(g, l))
                    {
                        // Move g out using outward-movement transformation, until it is directly related to l
                    }
                    else
                    {
                        // Different branches of same 'if' or 'switch'
                        // 
                        // Move g out using outward-movement transformation until it becomes directly related 
                        // to an 'if' of 'switch' containing l
                    }
                }

                // Force g and l to be siblings
                if (DirectlyRelated(g, l))
                {
                    if (g.Depth > l.Depth)
                    {
                        // Move g out to level l using outward-movement transformation
                    }
                    else // if (g.Depth < l.Depth) // NOTE TODO FIXME: This is incorrect! Can also be ==
                    {
                        if (g > l)
                        {
                            // Lift g to above statement containing l using goto-lifting transformation
                        }
                        // Move g in to level l using inward-movement transformation
                    }
                }

                // 'g' and 'l' are guaranteed to be siblings. We can eliminate 'g' now.
                if (g < l)
                {
                    // Eliminate g with a conditional
                    //
                    // if (condition) { goto label; }
                    // ...
                    // label:
                    // ...
                    //
                    // -> 
                    // if (condition) {
                    // ...
                    // }
                    // label:
                    // ...

                    var ite = (IfThenElse)g.Parent.Parent;
                    g.Remove(true);
                    ite.ThenBody.MoveFrom(ite.NextSibling(), l);
                }
                else
                {
                    // Eliminate g with a do-loop
                    //
                    // label:
                    // ...
                    // if (condition) { goto label; }
                    //
                    // ->
                    // do {
                    //   label:
                    //   ...
                    // }
                    // while (condition);
                    
                    var ite = (IfThenElse)g.Parent.Parent;
                    var condition = ite.Condition;
                    ite.Condition.Remove();

                    var dowhile = new DoWhile(condition);
                    dowhile.Body.MoveFrom(l, ite);
                    dowhile.InsertBefore(ite);
                    
                    ite.Remove(true);
                }
            }

            // Eliminate labels
            foreach (var label in labelList)
            {
                label.Remove(true);
            }
        }

        private bool DirectlyRelated(Goto g, Label l)
        {
            var n = g.Parent.Parent; // we need the 'conditional goto'.
            if (!(n is IfThenElse))
            {
                throw new Exception("Expected a conditional branche.");
            }

            var common = n.CommonAncestor(l);
            return (common == n.Parent || common == l.Parent) && !(common == n.Parent && common == l.Parent);
        }

        private bool DifferentStatements(Goto g, Label l)
        {
            var n = g.Parent.Parent; // we need the 'conditional goto'.
            if (!(n is IfThenElse))
            {
                throw new Exception("Expected a conditional branche.");
            }

            ASTNode n1 = n;
            while (n1 != null && !(n1 is DoWhile || n1 is For || n1 is IfThenElse || n1 is Switch || n1 is While))
            {
                n1 = n1.Parent;
            }

            ASTNode n2 = l;
            while (n2 != null && !(n2 is DoWhile || n2 is For || n2 is IfThenElse || n2 is Switch || n2 is While))
            {
                n2 = n2.Parent;
            }

            return n1 != n2;
        }

        private bool IsIndirectlyRelated(Goto g, Label l)
        {
            var n = g.Parent.Parent; // we need the 'conditional goto'.
            if (!(n is IfThenElse))
            {
                throw new Exception("Expected a conditional branche.");
            }

            var common = n.CommonAncestor(l);
            return (common != n.Parent && common != l.Parent);
        }
    }
}
