﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Structure
{
    public class Constructor : Callable
    {
        public Constructor(Class container, bool isStatic)
            : base(container, new List<VariableDeclaration>(), isStatic)
        {
            var tmp = MainScope;
        }

        public void Accept(ASTStructureVisitorBase visitor)
        {
            visitor.Visit(this);
        }
    }
}
