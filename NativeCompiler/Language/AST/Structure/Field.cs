﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Structure
{
    public class Field : VariableDeclaration
    {
        public Field(IType type, string name, bool isStatic)
            : base(VariableType.Field, type, name)
        {
            this.IsStatic = isStatic;
        }

        public bool IsStatic;

        public void Accept(ASTStructureVisitorBase visitor)
        {
            visitor.Visit(this);
        }
    }
}
