﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;

namespace NativeCompiler.Language.AST.Structure
{
    public class Callable
    {
        public Callable(Class container, List<VariableDeclaration> parameters, bool isStatic)
        {
            this.Container = container;
            this.Parameters = parameters;
            this.IsStatic = isStatic;
        }

        public Class Container;
        public bool IsStatic;
        public List<VariableDeclaration> Parameters = new List<VariableDeclaration>();

        public List<VariableDeclaration> LocalVariables = null;
        public ASTTree Body = null;

        public Scope MainScope
        {
            get
            {
                if (Body == null)
                {
                    Body = new ASTTree();
                    Body.Root.Add(new Scope(Body));
                }
                return (Scope)Body.Root[0];
            }
        }
    }
}
