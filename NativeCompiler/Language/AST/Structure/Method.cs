﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Structure
{
    public class Method : Callable, IGenericContainer
    {
        public Method(Class container, string name, bool isStatic, bool isVirtual)
            : base(container, new List<VariableDeclaration>(), isStatic)
        {
            this.Name = name;
            this.IsVirtual = isVirtual;
        }

        public string Name;
        public IType ReturnType = null;
        public bool IsVirtual = false;
        public List<Class> GenericArguments { get; set; }

        public void Accept(ASTStructureVisitorBase visitor)
        {
            visitor.Visit(this);
        }
    }
}
