﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Structure
{
    public class Module
    {
        public Module(Assembly assembly, string name, string fqn)
        {
            this.Container = assembly;
            this.Name = name;
            this.FullyQualifiedName = fqn;
        }

        public List<Class> DefinedTypes = new List<Class>();

        //public List<Attribute> CustomAttributes = new List<Attribute>();

        public Assembly Container { get; private set; }
        public string Name { get; private set; }
        public string FullyQualifiedName { get; private set; }
    }
}
