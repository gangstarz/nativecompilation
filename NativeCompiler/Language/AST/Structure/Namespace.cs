﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Structure
{
    public class Namespace
    {
        public Namespace()
        {
            Name = null;// root
        }

        public Namespace(Namespace container, string name)
        {
            this.Name = name;
            this.Container = container;
        }

        private Dictionary<string, Namespace> childNamespaces = new Dictionary<string, Namespace>();
        private Dictionary<Type, Class> childClasses = new Dictionary<Type, Class>();

        public Namespace Container { get; private set; }
        public string Name { get; private set; }

        public IEnumerable<Namespace> Namespaces { get { return childNamespaces.Values; } }
        public IEnumerable<Class> Classes { get { return childClasses.Values; } }

        public Namespace GotoNamespace(string name)
        {
            Namespace ns;
            if (!childNamespaces.TryGetValue(name, out ns))
            {
                ns = new Namespace(this, name);
                childNamespaces.Add(name, ns);
            }
            return ns;
        }

        public void Accept(ASTStructureVisitorBase visitor)
        {
            visitor.Visit(this);
        }

        public bool IsDefined(Type t)
        {
            return childClasses.ContainsKey(t);
        }

        public Class FindOrCreateClass(Type t, ASTBuilder astbuilder)
        {
            Class cl;
            if (!childClasses.TryGetValue(t, out cl))
            {
                var module = FindOrCreateModule(t.Module, astbuilder);

                cl = new Class(module, this, t);
                childClasses.Add(t, cl);
                cl.InitFields(t, astbuilder);

                var genarg = t.GetGenericArguments();
                if (genarg.Length > 0)
                {
                    cl.GenericArguments = new List<Class>();
                    foreach (var arg in genarg)
                    {
                        cl.GenericArguments.Add(astbuilder.FindOrCreateClass(arg));
                    }
                }

                if (t.IsGenericParameter)
                {
                    foreach (var constraint in t.GetGenericParameterConstraints())
                    {
                        if (constraint.IsInterface)
                        {
                            cl.BaseClasses.Add(astbuilder.FindOrCreateType(constraint));
                        }
                        else
                        {
                            var bt = astbuilder.FindOrCreateType(constraint);
                            if (bt.BaseDefinition != null)
                            {
                                cl.BaseClasses.Add(bt);
                            }
                            else if (t.BaseType == typeof(object) || t.BaseType == typeof(Exception) || t.BaseType == typeof(ValueType))
                            {
                                cl.BaseClasses.Add(bt);
                            }
                        }
                    }
                }

                if (t.BaseType != null)
                {
                    var bt = astbuilder.FindOrCreateType(t.BaseType);
                    if (bt.BaseDefinition != null)
                    {
                        cl.BaseClasses.Add(bt);
                    }
                    else if (t.BaseType == typeof(object) || t.BaseType == typeof(Exception) || t.BaseType == typeof(ValueType))
                    {
                        cl.BaseClasses.Add(bt);
                    }
                }
                foreach (var intf in t.GetInterfaces())
                {
                    cl.BaseClasses.Add(astbuilder.FindOrCreateType(intf));
                }
            }

            return cl;
        }

        private Module FindOrCreateModule(System.Reflection.Module module, ASTBuilder astbuilder)
        {
            return astbuilder.FindOrCreateModule(module);
        }
    }
}
