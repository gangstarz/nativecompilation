﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Structure
{
    public class GenericArgument : Class
    {
        public GenericArgument(Type t) : base(null, (Namespace)null, t)
        {
        }

        public override HashSet<Class> CollectDependencies()
        {
            HashSet<Class> classes = new HashSet<Class>();
            CollectDependencies(classes, new HashSet<object>());
            return classes;
        }

        public override void CollectDependencies(HashSet<Class> classes, HashSet<object> visited)
        {
            if (visited.Add(this))
            {
                foreach (var cl in BaseClasses)
                {
                    cl.CollectDependencies(classes, visited);
                }
            }
        }
    }
}
