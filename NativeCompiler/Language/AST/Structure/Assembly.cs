﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Structure
{
    public class Assembly
    {
        public Assembly(System.Reflection.Assembly basedOn)
        {
            this.CodeBase = basedOn.CodeBase;
            this.FullName = basedOn.FullName;
            this.InGAC = basedOn.GlobalAssemblyCache;
            this.RuntimeVersion = basedOn.ImageRuntimeVersion;
        }

        public string CodeBase { get; private set; }
        public string FullName { get; private set; }
        public bool InGAC { get; private set; }
        public string RuntimeVersion { get; private set; }

        //public List<Attribute> CustomAttributes = new List<Attribute>();
        
        public Method EntryPoint = null;

        public List<Module> Modules = new List<Module>();

    }
}
