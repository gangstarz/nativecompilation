﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Structure
{
    public class VariableDeclaration
    {
        public VariableDeclaration(VariableType varType, IType type, string name)
        {
            this.Type = type;
            this.Name = name;
            this.VariableType = varType;
        }

        private string name;

        public IType Type { get; private set; }
        public VariableType VariableType { get; private set; }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != this.name)
                {
                    this.name = value;
                }
            }
        }

        public void Write(TextWriter tw)
        {
            tw.Write(Name);
        }
    }
}
