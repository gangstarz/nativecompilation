﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Structure
{
    public class Class : IGenericContainer
    {
        public Class(Module module, Namespace container, Type basedOn)
        {
            this.Container = container;
            this.Module = module;

            Init(basedOn);
        }

        public Class(Module module, Class container, Type basedOn)
        {
            this.Container = container;
            this.Module = module;

            Init(basedOn);
        }

        private void Init(Type basedOn)
        {
            this.Name = basedOn.Name;
            int idx = Name.IndexOf('`');
            if (idx >= 0)
            {
                Name = Name.Substring(0, idx);
            }

            this.InterfaceClass = (basedOn.IsInterface);

            if (basedOn.IsGenericType && !basedOn.IsGenericTypeDefinition)
            {
                basedOn = basedOn.GetGenericTypeDefinition();
            }

            this.ValueType = basedOn.IsValueType;
        }

        // Separate method to avoid circular calls.
        public void InitFields(Type basedOn, ASTBuilder astBuilder)
        {
            foreach (var field in basedOn.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance))
            {
                var name = field.Name;
                var type = astBuilder.FindOrCreateType(field.FieldType);
                Fields.Add(new Field(type, name, field.IsStatic));
            }
        }

        // This flag tells us if the class is decompiled and analyzed.
        public bool IsDefined { get; set; }

        public string Name { get; private set; }
        public bool InterfaceClass { get; private set; }

        public object Container { get; set; } // class or namespace
        public List<Class> GenericArguments { get; set; }
        public Module Module { get; private set; }

        public List<Constructor> Constructors = new List<Constructor>();
        public List<Field> Fields = new List<Field>();
        public List<Method> Methods = new List<Method>();

        public List<IType> BaseClasses = new List<IType>();

        public bool ValueType { get; set; }

        public void Accept(ASTStructureVisitorBase visitor)
        {
            visitor.Visit(this);
        }

        public virtual HashSet<Class> CollectDependencies()
        {
            HashSet<Class> classes = new HashSet<Class>();
            CollectDependencies(classes, new HashSet<object>());
            return classes;
        }

        public virtual void CollectDependencies(HashSet<Class> classes, HashSet<object> visited)
        {
            if (visited.Add(this))
            {
                classes.Add(this);
                foreach (var cl in BaseClasses)
                {
                    cl.CollectDependencies(classes, visited);
                }
                if (GenericArguments != null)
                {
                    foreach (var arg in GenericArguments)
                    {
                        arg.CollectDependencies(classes, visited);
                    }
                }

                foreach (var ctor in Constructors)
                {
                    foreach (var par in ctor.Parameters)
                    {
                        par.Type.CollectDependencies(classes, visited);
                    }

                    DependencyCollectionVisitor.CollectDependencies(ctor.MainScope, classes, visited);
                }
                foreach (var field in Fields)
                {
                    field.Type.CollectDependencies(classes, visited);
                }
                foreach (var meth in Methods)
                {
                    meth.ReturnType.CollectDependencies(classes, visited);
                    foreach (var par in meth.Parameters)
                    {
                        par.Type.CollectDependencies(classes, visited);
                    }

                    if (meth.Body != null)
                    {
                        DependencyCollectionVisitor.CollectDependencies(meth.MainScope, classes, visited);
                    }
                }
            }
        }
    }
}
