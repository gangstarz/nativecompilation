﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NativeCompiler.Language.AST.Structure
{
    public interface IGenericContainer
    {
        List<Class> GenericArguments { get; set; }
    }
}
