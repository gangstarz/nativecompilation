﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Structure
{
    public enum VariableType
    {
        Field,
        Parameter,
        Local,
        Exception
    }
}
