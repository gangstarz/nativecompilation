﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST
{
    public abstract class ASTNode : IEquatable<ASTNode>
    {
        protected ASTNode() : this(null) { }

        internal ASTNode(ASTTree hierarchy)
        {
            this.hierarchy = hierarchy;
            this.size = 1;
        }

        private ASTTree hierarchy;

        private int index;      // index in the parent
        private int size;       // total number of children
        private int hashcode;   // hashcode of the contents of this and all child nodes

        private ASTNode parent = null;
        protected List<ASTNode> children = new List<ASTNode>();

        public virtual void Add(ASTNode child)
        {
            if (child.parent != null)
            {
                throw new Exception("You cannot add a node that is already part of a hierarchy");
            }

            var node = this;
            while (node != null)
            {
                node.hashcode = -1;
                node.size += child.size;
                node = node.parent;
            }

            this.children.Add(child);

            child.parent = this;
            child.index = this.children.Count - 1;

            if (this.hierarchy != null)
            {
                child.AddToHierarchyRecursive(this.hierarchy);
            }

            child.ResetHashcode();
        }

        public virtual void MoveAllFrom(ASTNode src)
        {
            bool removeFromHierarchy = (src.hierarchy != this.hierarchy && src.hierarchy != null);
            bool addToHierarchy = (src.hierarchy != this.hierarchy && this.hierarchy != null);

            foreach (var item in src.children)
            {
                // Remove from old
                if (removeFromHierarchy)
                {
                    item.RemoveFromHierarchyRecursive();
                }

                // Add here
                this.children.Add(item);

                item.parent = this;
                item.index = this.children.Count - 1;
                item.hashcode = -1;

                if (addToHierarchy)
                {
                    item.AddToHierarchyRecursive(this.hierarchy);
                }
            }

            src.children.Clear();

            // Update size and hashcode of src node
            int deltaSize = src.size - 1;
            var node = src;
            while (node != null)
            {
                node.hashcode = -1;
                node.size -= deltaSize;
                node = node.parent;
            }

            // Update size and hashcode of this node
            node = this;
            while (node != null)
            {
                node.hashcode = -1;
                node.size += deltaSize;
                node = node.parent;
            }
        }

        public virtual void MoveFrom(ASTNode src, int startIndex, int endIndex)
        {
            bool removeFromHierarchy = (src.hierarchy != this.hierarchy && src.hierarchy != null);
            bool addToHierarchy = (src.hierarchy != this.hierarchy && this.hierarchy != null);

            int deltaSize = 0;
            for (int i = startIndex; i < endIndex; ++i)
            {
                deltaSize += src.children[i].size;
                var item = src.children[i];

                // Remove from old
                if (removeFromHierarchy)
                {
                    item.RemoveFromHierarchyRecursive();
                }

                // Add here
                this.children.Add(item);

                item.parent = this;
                item.index = this.children.Count - 1;
                item.hashcode = -1;

                if (addToHierarchy)
                {
                    item.AddToHierarchyRecursive(this.hierarchy);
                }
            }

            src.children.RemoveRange(startIndex, endIndex - startIndex);

            // Update size and hashcode of src node
            var node = src;
            while (node != null)
            {
                node.hashcode = -1;
                node.size -= deltaSize;
                node = node.parent;
            }

            // Update indexes of src node
            for (int i = startIndex; i < src.children.Count; ++i)
            {
                src.children[i].index = i;
            }

            // Update size and hashcode of this node
            node = this;
            while (node != null)
            {
                node.hashcode = -1;
                node.size += deltaSize;
                node = node.parent;
            }
        }

        public virtual void MoveFrom(ASTNode start, ASTNode end)
        {
            if (start == null && end == null)
            {
                throw new Exception("Either start or end node shouldn't be null");
            }
            else if (start != null && end != null && start.parent != end.parent)
            {
                throw new Exception("Start and end node must have the same parent node");
            }
            var src = start == null ? end.parent : start.parent;

            int startIndex = start == null ? 0 : start.index;
            int endIndex = end == null ? children.Count : end.index;
            MoveFrom(src, startIndex, endIndex);
        }

        public virtual void MoveFromBefore(ASTNode src, int startIndex, int endIndex, ASTNode before)
        {
            if (before == null || before.parent == null)
            {
                throw new Exception("Destination node cannot be missing or a root node");
            }
            else if (!object.ReferenceEquals(before.parent, this))
            {
                throw new Exception("Destination node must be a child of the current node");
            }

            MoveFromBefore(src, startIndex, endIndex, before.index);
        }

        public virtual void MoveFromBefore(ASTNode src, int startIndex, int endIndex, int beforeIndex)
        {
            bool removeFromHierarchy = (src.hierarchy != this.hierarchy && src.hierarchy != null);
            bool addToHierarchy = (src.hierarchy != this.hierarchy && this.hierarchy != null);

            List<ASTNode> tmp = new List<ASTNode>();
            int deltaSize = 0;
            for (int i = startIndex; i < endIndex; ++i)
            {
                deltaSize += src.children[i].size;
                var item = src.children[i];

                // Remove from old
                if (removeFromHierarchy)
                {
                    item.RemoveFromHierarchyRecursive();
                }

                // Add here
                tmp.Add(item);

                item.parent = this;
                item.hashcode = -1;

                if (addToHierarchy)
                {
                    item.AddToHierarchyRecursive(this.hierarchy);
                }
            }

            this.children.InsertRange(beforeIndex, tmp);
            src.children.RemoveRange(startIndex, endIndex - startIndex);

            // Update size and hashcode of src node
            var node = src;
            while (node != null)
            {
                node.hashcode = -1;
                node.size -= deltaSize;
                node = node.parent;
            }

            // Update indexes of src node
            for (int i = startIndex; i < src.children.Count; ++i)
            {
                src.children[i].index = i;
            }

            // Update indexes of this node
            for (int i = beforeIndex; i < children.Count; ++i)
            {
                children[i].index = i;
            }

            // Update size and hashcode of this node
            node = this;
            while (node != null)
            {
                node.hashcode = -1;
                node.size += deltaSize;
                node = node.parent;
            }
        }

        public virtual void MoveFromBefore(ASTNode start, ASTNode end, ASTNode before)
        {
            if (start == null && end == null)
            {
                throw new Exception("Either start or end node shouldn't be null");
            }
            else if (start != null && end != null && start.parent != end.parent)
            {
                throw new Exception("Start and end node must have the same parent node");
            }
            else if (before == null || before.parent == null)
            {
                throw new Exception("Destination node cannot be missing or a root node");
            }
            else if (!object.ReferenceEquals(before.parent, this))
            {
                throw new Exception("Destination node must be a child of the current node");
            }

            var src = start == null ? end.parent : start.parent;

            int startIndex = start == null ? 0 : start.index;
            int endIndex = end == null ? children.Count : end.index;

            MoveFromBefore(src, startIndex, endIndex, before.index);
        }

        public virtual void InsertBefore(ASTNode other) // insert this before other
        {
            if (this.parent != null)
            {
                throw new Exception("You cannot add a node that is already part of a hierarchy");
            }

            if (other.parent == null)
            {
                throw new Exception("You cannot add a node before a disconnected or root node");
            }

            var node = other.parent;
            while (node != null)
            {
                node.hashcode = -1;
                node.size += this.size;
                node = node.parent;
            }

            var ch = other.parent.children;
            ch.Insert(other.index, this);

            for (int i = other.index; i < ch.Count; ++i)
            {
                ch[i].index = i;
            }

            this.parent = other.parent;

            if (other.hierarchy != null)
            {
                this.AddToHierarchyRecursive(other.hierarchy);
            }

            this.ResetHashcode();
        }

        public virtual void InsertAfter(ASTNode other) // insert this after other
        {
            if (this.parent != null)
            {
                throw new Exception("You cannot add a node that is already part of a hierarchy");
            }

            if (other.parent == null)
            {
                throw new Exception("You cannot add a node before a disconnected or root node");
            }

            var node = other.parent;
            while (node != null)
            {
                node.hashcode = -1;
                node.size += this.size;
                node = node.parent;
            }

            var ch = other.parent.children;
            ch.Insert(other.index + 1, this);

            for (int i = other.index + 1; i < ch.Count; ++i)
            {
                ch[i].index = i;
            }

            this.parent = other.parent;

            if (other.hierarchy != null)
            {
                this.AddToHierarchyRecursive(other.hierarchy);
            }

            this.ResetHashcode();
        }

        public virtual void Remove(ASTNode child)
        {
            if (child.parent == null)
            {
                throw new Exception("Cannot remove a node that is not part of a hierarchy");
            }

            int idx = child.index;
            children.RemoveAt(idx);
            for (int i = idx; i < children.Count; ++i)
            {
                children[i].index--;
            }

            var node = this;
            while (node != null)
            {
                node.hashcode = -1;
                node.size -= child.size;
                node = node.parent;
            }

            child.parent = null;
            child.index = 0;

            child.RemoveFromHierarchyRecursive();

            this.ResetHashcode();
        }

        public void Remove(bool delete = false)
        {
            if (parent == null)
            {
                throw new Exception("Cannot remove a node that is not part of a hierarchy");
            }

            parent.Remove(this);

            if (delete)
            {
                Delete();
            }
        }

        public virtual void Delete()
        {
            foreach (var child in children)
            {
                child.Delete();
            }
        }

        public void SubstituteBy(ASTNode other)
        {
            if (object.ReferenceEquals(other, this)) { return; } // trivial border case.

            if (this.parent == null)
            {
                throw new Exception("Cannot remove a node that is not part of a hierarchy");
            }

            int idx = this.index;
            var node = this.parent;

            node.children[idx] = other;

            int deltaSize = -this.size + other.size;
            while (node != null)
            {
                node.hashcode = -1;
                node.size += deltaSize;
                node = node.parent;
            }

            other.parent = this.parent;
            other.index = this.index;
            other.ResetHashcode();

            if (this.hierarchy != null)
            {
                other.AddToHierarchyRecursive(this.hierarchy);
            }

            this.parent = null;
            this.index = 0;
            this.RemoveFromHierarchyRecursive();
            this.ResetHashcode();
        }

        public void AddRangeFrom(ASTNode src, int inclFirstIndex, int exclLastIndex)
        {
            if (inclFirstIndex == exclLastIndex)
            {
                return;
            }

            int index = src[inclFirstIndex].index;
            int totalSize = 0;
            for (int i = inclFirstIndex; i < exclLastIndex; ++i)
            {
                var ch = src.children[i];

                totalSize += ch.size;

                // Detach child
                ch.parent = null;
                ch.index = 0;
                ch.RemoveFromHierarchyRecursive();

                // Add here
                Add(ch);
            }

            src.ResetHashcode();


            this.ResetHashcode();

        }

        public int NumberOfChildren { get { return children.Count; } }

        public ASTNode this[int index]
        {
            get { return children[index]; }
            set { children[index].SubstituteBy(value); }
        }

        public bool IsDescendentOf(ASTNode other)
        {
            var cur = this;
            while (cur != null && !object.ReferenceEquals(other, cur))
            {
                cur = cur.parent;
            }
            return cur != null;
        }

        public bool IsAncesterOf(ASTNode other)
        {
            var cur = other;
            while (cur != null && !object.ReferenceEquals(this, cur))
            {
                cur = cur.parent;
            }
            return cur != null;
        }

        public bool IsSiblingOf(ASTNode other)
        {
            return this.parent != null && !object.ReferenceEquals(this, other) && object.ReferenceEquals(other.parent, this.parent);
        }

        public ASTNode Parent { get { return parent; } }

        public List<ASTNode> Children { get { return children; } }

        public IEnumerable<ASTNode> AllChildren()
        {
            Stack<IEnumerator<ASTNode>> stack = new Stack<IEnumerator<ASTNode>>();
            stack.Push(this.children.GetEnumerator());
            while (stack.Count > 0)
            {
                IEnumerator<ASTNode> item = stack.Peek();
                if (item.MoveNext())
                {
                    yield return item.Current;
                    var enumerator = item.Current.children;
                    if (enumerator.Count > 0)
                    {
                        stack.Push(enumerator.GetEnumerator());
                    }
                }
                else
                {
                    item.Dispose();
                    stack.Pop();
                }
            }
        }

        public IEnumerable<ASTNode> AllChildren(Func<ASTNode, bool> fcn)
        {
            Stack<IEnumerator<ASTNode>> stack = new Stack<IEnumerator<ASTNode>>();
            stack.Push(this.children.GetEnumerator());
            while (stack.Count > 0)
            {
                IEnumerator<ASTNode> item = stack.Peek();
                if (item.MoveNext())
                {
                    if (fcn(item.Current))
                    {
                        yield return item.Current;
                    }

                    var enumerator = item.Current.children;
                    if (enumerator.Count > 0)
                    {
                        stack.Push(enumerator.GetEnumerator());
                    }
                }
                else
                {
                    item.Dispose();
                    stack.Pop();
                }
            }
        }

        public IEnumerable<T> AllChildrenOf<T>() where T : ASTNode
        {
            var nodes = hierarchy.FindNodesOfType(typeof(T));
            foreach (var item in nodes)
            {
                if (item.IsDescendentOf(this))
                {
                    yield return (T)item;
                }
            }
        }

        public IEnumerable<T> ChildrenOf<T>() where T : ASTNode
        {
            return children.OfType<T>();
        }

        public ASTNode NextSibling()
        {
            if (this.parent == null || this.index + 1 >= this.parent.children.Count)
            {
                return null;
            }
            return this.parent.children[this.index + 1];
        }

        public ASTNode PreviousSibling()
        {
            if (this.parent == null || this.index - 1 < 0)
            {
                return null;
            }
            return this.parent.children[this.index - 1];
        }

        public ASTNode First
        {
            get
            {
                return children.Count > 0 ? children[0] : null;
            }
        }

        public ASTNode Last
        {
            get
            {
                return children.Count > 0 ? children[children.Count - 1] : null;
            }
        }

        public bool HasChildOf<T>()
        {
            var nodes = hierarchy.FindNodesOfType(typeof(T));
            foreach (var item in nodes)
            {
                if (item.IsDescendentOf(this))
                {
                    return true;
                }
            }
            return false;
        }

        public bool HasChild(Func<ASTNode, bool> fcn)
        {
            Stack<ASTNode> st = new Stack<ASTNode>();
            st.Push(this);
            while (st.Count > 0)
            {
                var cur = st.Pop();

                if (fcn(cur))
                {
                    return true;
                }

                foreach (var child in cur.children)
                {
                    st.Push(child);
                }
            }
            return false;
        }

        public T Find<T>() where T : ASTNode
        {
            foreach (var node in children)
            {
                if (node is T)
                {
                    return (T)node;
                }
            }
            return null;
        }

        public ASTNode Find(Func<ASTNode, bool> fcn)
        {
            foreach (var node in children)
            {
                if (fcn(node))
                {
                    return node;
                }
            }
            return null;
        }

        public ASTNode Find<T>(ASTNode prev) where T : ASTNode
        {
            if (prev == null)
            {
                return Find<T>();
            }
            else if (prev != null && !object.ReferenceEquals(prev.parent, this))
            {
                throw new Exception("Previous node is not part of the same parent node");
            }

            for (int i = prev.index + 1; i < children.Count; ++i)
            {
                var node = children[i] as T;
                if (node != null)
                {
                    return node;
                }
            }

            return null;
        }

        public ASTNode Find(Func<ASTNode, bool> fcn, ASTNode prev)
        {
            if (prev == null)
            {
                return Find(fcn);
            }
            else if (prev != null && !object.ReferenceEquals(prev.parent, this))
            {
                throw new Exception("Previous node is not part of the same parent node");
            }

            for (int i = prev.index + 1; i < children.Count; ++i)
            {
                var node = children[i];
                if (fcn(node))
                {
                    return node;
                }
            }

            return null;
        }

        public int Depth
        {
            get
            {
                int d = 0;
                var node = this;
                while (node.parent != null)
                {
                    node = node.parent;
                    ++d;
                }
                return d;
            }
        }

        private static int CalculateAbsoluteIndex(ASTNode node)
        {
            int idx = 0;
            while (node != null && node.parent != null)
            {
                ++idx;
                var p = node.parent;
                for (int i = 0; i < node.index; ++i)
                {
                    idx += p.children[i].size;
                }
                node = p;
            }
            return idx;
        }

        public static bool operator <(ASTNode lhs, ASTNode rhs)
        {
            // Calculate the index of this and other node
            int index1 = CalculateAbsoluteIndex(lhs);
            int index2 = CalculateAbsoluteIndex(rhs);
            return index1 < index2;
        }

        public static bool operator >(ASTNode lhs, ASTNode rhs)
        {
            // Calculate the index of this and other node
            int index1 = CalculateAbsoluteIndex(lhs);
            int index2 = CalculateAbsoluteIndex(rhs);
            return index1 > index2;
        }

        public static bool operator <=(ASTNode lhs, ASTNode rhs)
        {
            // Calculate the index of this and other node
            int index1 = CalculateAbsoluteIndex(lhs);
            int index2 = CalculateAbsoluteIndex(rhs);
            return index1 <= index2;
        }

        public static bool operator >=(ASTNode lhs, ASTNode rhs)
        {
            // Calculate the index of this and other node
            int index1 = CalculateAbsoluteIndex(lhs);
            int index2 = CalculateAbsoluteIndex(rhs);
            return index1 >= index2;
        }

        public static bool operator ==(ASTNode lhs, ASTNode rhs)
        {
            return object.ReferenceEquals(lhs, null) ? object.ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(ASTNode lhs, ASTNode rhs)
        {
            return object.ReferenceEquals(lhs, null) ? !object.ReferenceEquals(rhs, null) : !lhs.Equals(rhs);
        }

        protected virtual ASTNode Clone(object context)
        {
            var node = ShallowClone(context);
            foreach (var item in children)
            {
                node.Add(item.Clone(context));
            }
            return node;
        }

        public virtual ASTNode Clone()
        {
            return Clone(null);
        }

        protected void ResetHashcode()
        {
            var node = this;
            while (node != null)
            {
                hashcode = -1;
                node = node.parent;
            }
        }

        public override int GetHashCode()
        {
            if (hashcode == -1)
            {
                this.hashcode = CalculateHashcode() + this.GetType().GetHashCode();
                foreach (var item in children)
                {
                    hashcode = hashcode * 7577 + item.GetHashCode();
                }
            }
            return hashcode;
        }

        public bool Equals(ASTNode other)
        {
            if (object.ReferenceEquals(this, other))
            {
                return true;
            }
            else if (other != null && this.size == other.size && this.GetType() == other.GetType() && this.children.Count == other.children.Count &&
                     GetHashCode() == other.GetHashCode() && NodeEquals(other))
            {
                for (int i = 0; i < children.Count; ++i)
                {
                    if (!children[i].Equals(other.children[i]))
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        public override bool Equals(object obj)
        {
            ASTNode n = obj as ASTNode;
            return n != null && this.Equals(n);
        }


        // Hierarchy operators
        public ASTNode CommonAncestor(ASTNode rhs)
        {
            ASTNode lhs = this;

            int lhsDepth = lhs.Depth;
            int rhsDepth = rhs.Depth;
            while (lhsDepth > rhsDepth)
            {
                lhs = lhs.parent;
                --lhsDepth;
            }
            while (rhsDepth > lhsDepth)
            {
                rhs = rhs.parent;
                --rhsDepth;
            }
            while (lhs != null && rhs != null && !object.ReferenceEquals(lhs, rhs))
            {
                lhs = lhs.parent;
                rhs = rhs.parent;
            }
            return lhs;
        }

        public IEnumerable<ASTNode> PathTo(ASTNode o)
        {
            var node = this;
            yield return node;

            while (node != null && !object.ReferenceEquals(node, o))
            {
                int idx = node.index;
                var parent = node.parent;
                if (parent == null)
                {
                    break;
                }

                if (parent.children.Count > idx + 1)
                {
                    node = parent.children[idx + 1];

                    // move to top-most node
                    while (node.children.Count > 0)
                    {
                        node = node.children[0];
                    }

                    yield return node;
                }
                else
                {
                    node = parent;
                    yield return node;
                }
            }
        }

        // Node-specific implementation details:

        protected virtual void AddToHierarchyRecursive(ASTTree h)
        {
            if (hierarchy != null)
            {
                throw new Exception("Cannot add to hierarchy if it's already added");
            }
            hierarchy = h;
            hierarchy.AddToHierarchy(this);
            foreach (var child in children)
            {
                child.AddToHierarchyRecursive(h);
            }
        }

        protected virtual void RemoveFromHierarchyRecursive()
        {
            if (hierarchy != null)
            {
                hierarchy.RemoveFromHierarchy(this);
                foreach (var child in children)
                {
                    child.RemoveFromHierarchyRecursive();
                }
                hierarchy = null;
            }
        }

        protected abstract ASTNode ShallowClone(object context);

        protected abstract int CalculateHashcode();

        protected abstract bool NodeEquals(ASTNode o);

        public abstract void Write(TextWriter tw);

        public override string ToString()
        {
            StringWriter sw = new StringWriter();
            Write(sw);
            return sw.ToString();
        }
    }
}
