﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST
{
    /*
    public static class VariableSubstitutionExtensions
    {
        public static Expression Substitute(this Expression expr, VariableDeclaration variable, Expression substitute)
        {
            VariableSubstitution subs = new VariableSubstitution(variable, substitute);
            foreach (var item in expr.ChildrenOf<Variable>)
            return expr.Rewrite(subs);
        }

        public static Statement Substitute(this Statement stmt, VariableDeclaration variable, Expression substitute)
        {
            VariableSubstitution subs = new VariableSubstitution(variable, substitute);
            return stmt.Rewrite(subs);
        }
    } 

    public class VariableSubstitution : ASTCodeRewriterBase
    {
        public VariableSubstitution(VariableDeclaration var, Expression substitute)
        {
            this.variable = var;
            this.substitute = substitute;
        }

        private VariableDeclaration variable;
        private Expression substitute;

        public override Expression Rewrite(Variable variable)
        {
            if (variable.Declaration == this.variable)
            {
                return substitute;
            }
            else
            {
                return base.Rewrite(variable);
            }
        }
    }
     */
}
