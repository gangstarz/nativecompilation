﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST.Statements
{
    public abstract class Statement : ASTNode
    {
        public Statement() : base() { }
        internal Statement(ASTTree tree) : base(tree) { }

        public abstract void ClonePhase1(Dictionary<Label, Label> innerLabelMappings);

        public override ASTNode Clone()
        {
            var context = new Dictionary<Label, Label>(ObjectEquality<Label>.Instance);
            return base.Clone(context);
        }

    }
}
