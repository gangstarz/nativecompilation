﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST.Statements
{
    public class InitializeVariable : Statement
    {
        public InitializeVariable(VariableDeclaration var = null, Expression value = null)
        {
            this.Variable = var;
            if (value != null)
            {
                Add(value);
            }
        }

        public VariableDeclaration Variable { get; private set;}

        public override void Add(ASTNode child)
        {
            if (NumberOfChildren > 1 || !(child is Expression))
            {
                throw new Exception("Incorrect AST node; expected expression.");
            }
            base.Add(child);
        }

        public Expression Value
        {
            get { return children.Count > 0 ? (Expression)children[0] : null; }
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new InitializeVariable();
        }

        protected override int CalculateHashcode()
        {
            return 101;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            Variable.Type.Write(tw);
            tw.Write(' ');
            Variable.Write(tw);
            if (Value != null)
            {
                tw.Write(" = ");
                Value.Write(tw);
            }
            tw.WriteLine(";");
        }
    }
}
