﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Statements
{
    public class Finally : Statement
    {
        public Finally()
        {
            Add(new Scope());
        }

        private Finally(bool init) { }

        protected override ASTNode ShallowClone(object context)
        {
            return new Finally(false);
        }

        public Scope Body
        {
            get { return (children.Count == 1) ? (Scope)children[0] : null; }
        }

        public override void Add(ASTNode child)
        {
            base.Add(child);
        }

        protected override int CalculateHashcode()
        {
            return 203;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.WriteLine("finally");
            foreach (var child in children)
            {
                child.Write(tw);
            }
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
            foreach (var child in children.Cast<Statement>())
            {
                child.ClonePhase1(innerLabelMappings);
            }
        }
    }
}
