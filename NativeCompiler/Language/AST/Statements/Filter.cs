﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;

namespace NativeCompiler.Language.AST.Statements
{
    public class Filter : Statement
    {
        public Filter(Expression condition = null)
        {
            if (condition != null)
            {
                Add(condition);
                Add(new Scope());
            }
        }

        public Expression Condition
        {
            get { return (children.Count == 2) ? (Expression)children[0] : null; }
        }

        public Scope Body
        {
            get { return (children.Count == 2) ? (Scope)children[1] : null; }
        }

        public override void Add(ASTNode child)
        {
            if (NumberOfChildren == 0 && !(child is Expression))
            {
                throw new NotSupportedException("First child must always be an expression");
            }
            if (NumberOfChildren == 1 && !(child is Scope))
            {
                throw new NotSupportedException("Second child must always be a scope");
            }
            if (NumberOfChildren == 2)
            {
                throw new NotSupportedException("Filter does not support more than 2 children.");
            }
            base.Add(child);
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
            Body.ClonePhase1(innerLabelMappings);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new While();
        }

        protected override int CalculateHashcode()
        {
            return 0;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.Write("filter (");
            Condition.Write(tw);
            tw.WriteLine(")");
            tw.WriteLine("{");
            Body.Write(tw);
            tw.WriteLine("}");
        }
    }
}
