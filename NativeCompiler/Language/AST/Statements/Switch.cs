﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;

namespace NativeCompiler.Language.AST.Statements
{
    public class Switch : Statement
    {
        public Switch(Expression condition = null)
        {
            if (condition != null)
            {
                Add(condition);
            }
        }

        public Expression Condition
        {
            get { return (children.Count == 0) ? null : (Expression)children[0]; }
        }

        public IEnumerable<Case> Cases
        {
            get
            {
                for (int i = 1; i < children.Count; ++i)
                {
                    yield return ((Case)children[i]);
                }
            }
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
            for (int i = 1; i < children.Count; ++i)
            {
                ((Case)children[i]).ClonePhase1(innerLabelMappings);
            }
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Switch();
        }

        protected override int CalculateHashcode()
        {
            return 106;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.Write("switch (");
            Condition.Write(tw);
            tw.WriteLine(")");
            tw.WriteLine("{");
            for (int i = 1; i < children.Count; ++i)
            {
                ((Case)children[i]).Write(tw);
            }
            tw.WriteLine("}");
        }

    }
}
