﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;

namespace NativeCompiler.Language.AST.Statements
{
    public class Return : Statement
    {
        public Return(Expression result = null)
        {
            if (result != null)
            {
                Add(result);
            }
        }

        public Expression Result
        {
            get { return children.Count == 0 ? null : (Expression)children[0]; }
        }

        public override void Add(ASTNode child)
        {
            if (children.Count == 1 || !(child is Expression))
            {
                throw new Exception("Return statement can only have 1 (expression) child.");
            }

            base.Add(child);
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Return();
        }

        protected override int CalculateHashcode()
        {
            return 105;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            if (Result == null)
            {
                tw.WriteLine("return;");
            }
            else
            {
                tw.Write("return ");
                Result.Write(tw);
                tw.WriteLine(";");
            }
        }
    }
}
