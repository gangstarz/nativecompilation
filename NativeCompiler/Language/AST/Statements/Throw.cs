﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;

namespace NativeCompiler.Language.AST.Statements
{
    public class Throw : Statement
    {
        public Throw(Expression argument = null)
        {
            if (argument != null)
            {
                Add(argument);
            }
        }

        public Expression Argument
        {
            get { return (children.Count == 1) ? (Expression)children[0] : null; }
        }

        public override void Add(ASTNode child)
        {
            if (NumberOfChildren == 0 && !(child is Expression))
            {
                throw new NotSupportedException("First child must always be an expression");
            }

            base.Add(child);
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Throw();
        }

        protected override int CalculateHashcode()
        {
            return 107;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.Write("throw ");
            Argument.Write(tw);
            tw.WriteLine(";");
        }
    }
}
