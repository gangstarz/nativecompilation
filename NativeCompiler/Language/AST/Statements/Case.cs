﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;

namespace NativeCompiler.Language.AST.Statements
{
    public class Case : Statement
    {
        public Case(bool hasDefault, IEnumerable<Expression> expressions = null)
        {
            this.HasDefault = hasDefault;

            if (expressions != null)
            {
                Add(new Scope());
                foreach (var expr in expressions)
                {
                    Add(expr);
                }
            }
        }

        public bool HasDefault { get; private set; }

        public Scope Body
        {
            get { return children.Count > 0 ? (Scope)children[0] : null; }
        }

        public IEnumerable<Expression> Expressions
        {
            get
            {
                for (int i = 1; i < children.Count; ++i)
                {
                    yield return (Expression)children[i];
                }
            }
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
            ((Scope)children[0]).ClonePhase1(innerLabelMappings);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Case(HasDefault);
        }

        protected override int CalculateHashcode()
        {
            return 107;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            if (HasDefault)
            {
                tw.WriteLine("default:");
            }
            for (int i = 1; i < children.Count; ++i)
            {
                tw.Write("case ");
                ((Expression)children[i]).Write(tw);
                tw.WriteLine(":");
            }

            children[0].Write(tw);
            tw.WriteLine("break;");
        }
    }
}
