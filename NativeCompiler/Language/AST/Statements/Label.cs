﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Statements
{
    public class Label : Statement
    {
        public Label(int offset) : this("label_" + offset.ToString("x4")) 
        { }

        public Label(string name)
        {
            this.Name = name;
        }

        public string Name { get; private set; }

        public List<Goto> UsedBy = new List<Goto>();

        public override void Add(ASTNode child)
        {
            throw new NotSupportedException("Labels cannot have any child nodes");
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
            innerLabelMappings.Add(this, new Label(this.Name + "_cl"));
        }

        protected override ASTNode ShallowClone(object context)
        {
            var innerLabelMappings = (Dictionary<Label, Label>)context;
            return innerLabelMappings[this];
        }

        protected override int CalculateHashcode()
        {
            return this.Name.GetHashCode() + 103;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return Name == ((Label)o).Name;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.Write(Name);
            tw.WriteLine(":");
        }
    }
}
