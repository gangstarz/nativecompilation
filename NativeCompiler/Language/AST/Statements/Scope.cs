﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Statements
{
    public class Scope : Statement
    {
        public Scope() { }

        internal Scope(ASTTree tree) : base(tree) { }

        protected override ASTNode ShallowClone(object context)
        {
            return new Scope();
        }

        protected override int CalculateHashcode()
        {
            return 106;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.WriteLine("{");
            foreach (var child in children)
            {
                child.Write(tw);
            }
            tw.WriteLine("}");
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
            foreach (var child in children.Cast<Statement>())
            {
                child.ClonePhase1(innerLabelMappings);
            }
        }

    }
}
