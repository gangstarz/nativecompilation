﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Statements
{
    public class ExceptionBlock : Statement
    {
        public ExceptionBlock() { }

        protected override ASTNode ShallowClone(object context)
        {
            return new ExceptionBlock();
        }

        protected override int CalculateHashcode()
        {
            return 205;
        }

        public override void Add(ASTNode child)
        {
            if (child is Try || child is Catch || child is Finally || child is Filter)
            {
                base.Add(child);
            }
            else
            {
                throw new Exception("Child is not supported in an exception block.");
            }
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            foreach (var child in children)
            {
                child.Write(tw);
            }
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
            foreach (var child in children.Cast<Statement>())
            {
                child.ClonePhase1(innerLabelMappings);
            }
        }
    }
}
