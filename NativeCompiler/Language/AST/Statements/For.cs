﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST.Statements
{
    public class For : Statement
    {
        public For(bool initialize)
        {
            if (initialize)
            {
                Add(new Scope());
            }
        }

        private int preConditionIndex = -1;
        private int conditionIndex = -1;
        private int postConditionIndex = -1;

        private void RemoveItem(int index)
        {
            if (index >= 0)
            {
                children[index].Remove();

                if (index < preConditionIndex)
                {
                    --preConditionIndex;
                }
                if (index < conditionIndex)
                {
                    --conditionIndex;
                }
                if (index < postConditionIndex)
                {
                    --postConditionIndex;
                }
            }
        }

        public VariableDeclaration Variable { get; set; }

        public Expression PreCondition
        {
            get
            {
                if (preConditionIndex != -1)
                {
                    return (Expression)base.children[preConditionIndex];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                RemoveItem(preConditionIndex);
                if (value != null)
                {
                    Add(value);
                    preConditionIndex = children.Count - 1;
                }
            }
        }

        public Expression Condition
        {
            get
            {
                if (conditionIndex != -1)
                {
                    return (Expression)base.children[conditionIndex];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                RemoveItem(conditionIndex);
                if (value != null)
                {
                    Add(value);
                    conditionIndex = children.Count - 1;
                }
            }
        }

        public Expression PostCondition
        {
            get
            {
                if (postConditionIndex != -1)
                {
                    return (Expression)base.children[postConditionIndex];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                RemoveItem(postConditionIndex);
                if (value != null)
                {
                    Add(value);
                    postConditionIndex = children.Count - 1;
                }
            }
        }

        public Scope Body
        {
            get { return (Scope)this.children[0]; }
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
            Body.ClonePhase1(innerLabelMappings);
        }

        protected override ASTNode ShallowClone(object context)
        {
            var result = new For(false);

            result.conditionIndex = conditionIndex;
            result.preConditionIndex = preConditionIndex;
            result.postConditionIndex = postConditionIndex;

            return result;
        }

        protected override int CalculateHashcode()
        {
            return 0;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.Write("for (");
            if (Variable != null)
            {
                Variable.Type.Write(tw);
                tw.Write(' ');
            }
            if (PreCondition != null)
            {
                PreCondition.Write(tw);
            }
            tw.Write("; ");
            if (Condition != null)
            {
                Condition.Write(tw);
            }
            tw.Write("; ");
            if (PostCondition != null)
            {
                PostCondition.Write(tw);
            }
            tw.WriteLine(")");
            Body.Write(tw);
        }
    }
}
