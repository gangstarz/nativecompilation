﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;

namespace NativeCompiler.Language.AST.Statements
{
    public class IfThenElse : Statement
    {
        public IfThenElse(Expression condition = null)
        {
            if (condition != null)
            {
                Add(condition);
                Add(new Scope()); // Then.
            }
        }

        public Expression Condition
        {
            get { return children.Count > 0 ? (Expression)children[0] : null; }
        }

        public Scope ThenBody
        {
            get { return children.Count > 1 ? (Scope)children[1] : null; }
        }
        
        public Scope ElseBody
        {
            get { return children.Count > 2 ? (Scope)children[2] : null; }
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
            if (this.ThenBody != null) { ThenBody.ClonePhase1(innerLabelMappings); }
            if (this.ElseBody != null) { ElseBody.ClonePhase1(innerLabelMappings); }
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new IfThenElse();
        }

        protected override int CalculateHashcode()
        {
            return 100;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.Write("if (");
            Condition.Write(tw);
            tw.WriteLine(")");
            if (ThenBody != null)
            {
                ThenBody.Write(tw);

                if (ElseBody != null)
                {
                    ElseBody.Write(tw);
                }
            }
        }
    }
}
