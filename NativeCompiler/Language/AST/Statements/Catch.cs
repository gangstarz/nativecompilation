﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Statements
{
    public class Catch : Statement
    {
        public Catch(IType type, VariableDeclaration decl)
        {
            if (type != null && decl != null)
            {
                this.Variable = decl;
                this.Type = type;
                Add(new Scope());
            }
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Catch(null, null)
            {
                Type = this.Type,
                Variable = this.Variable
            };
        }

        public VariableDeclaration Variable { get; private set; }
        public IType Type { get; private set; }

        public Scope Body
        {
            get { return (children.Count == 1) ? (Scope)children[0] : null; }
        }

        public override void Add(ASTNode child)
        {
            base.Add(child);
        }

        protected override int CalculateHashcode()
        {
            return 201;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.WriteLine("catch");
            foreach (var child in children)
            {
                child.Write(tw);
            }
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
            foreach (var child in children.Cast<Statement>())
            {
                child.ClonePhase1(innerLabelMappings);
            }
        }

    }
}
