﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;

namespace NativeCompiler.Language.AST.Statements
{
    public class ExpressionStatement : Statement
    {
        public ExpressionStatement(Expression expr = null)
        {
            if (expr != null)
            {
                Add(expr);
            }
        }

        public Expression Expression
        {
            get { return (children.Count == 1) ? (Expression)children[0] : null; }
        }

        public override void Add(ASTNode child)
        {
            if (NumberOfChildren == 0 && !(child is Expression))
            {
                throw new NotSupportedException("Child must always be an expression");
            }
            if (NumberOfChildren == 1)
            {
                throw new NotSupportedException("Statement does not support more than 1 child.");
            }

            base.Add(child);
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new ExpressionStatement();
        }

        protected override int CalculateHashcode()
        {
            return 0;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            Expression.Write(tw);
            tw.WriteLine(";");
        }
    }
}
