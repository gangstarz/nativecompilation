﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Statements
{
    public class Rethrow : Statement
    {
        public override void Add(ASTNode child)
        {
            throw new NotSupportedException("Re-throw cannot have any child nodes");
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Rethrow();
        }

        protected override int CalculateHashcode()
        {
            return 104;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.WriteLine("throw;");
        }
    }
}
