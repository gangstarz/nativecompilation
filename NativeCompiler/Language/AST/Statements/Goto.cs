﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Statements
{
    public class Goto : Statement
    {
        public Goto(Label label)
        {
            this.Target = label;
        }

        private Label target = null;

        public Label Target
        {
            get
            {
                return target;
            }
            set
            {
                if (target != null)
                {
                    bool found = true;
                    for (int i = 0; i < target.UsedBy.Count; ++i)
                    {
                        if (object.ReferenceEquals(target.UsedBy[i], this))
                        {
                            found = true;
                            target.UsedBy.RemoveAt(i);
                            break;
                        }
                    }
                    if (!found)
                    {
                        throw new Exception("Goto cannot be removed from target: goto missing");
                    }
                }
                this.target = value;
                if (value != null)
                {
                    value.UsedBy.Add(this);
                }
            }
        }

        public override void Add(ASTNode child)
        {
            throw new NotSupportedException("Statement does not support children.");
        }

        public override void ClonePhase1(Dictionary<Label, Label> innerLabelMappings)
        {
        }

        protected override ASTNode ShallowClone(object context)
        {
            var mapping = (Dictionary<Label, Label>)context;
            Label newLabel;
            if (mapping.TryGetValue(this.target, out newLabel))
            {
                return new Goto(newLabel);
            }
            else
            {
                return new Goto(this.target);
            }
        }

        protected override int CalculateHashcode()
        {
            return target.Name.GetHashCode();
        }

        public override void Delete()
        {
            Target = null;
            base.Delete();
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return target != null && target.Equals(((Goto)o).Target);
        }

        public override void Write(System.IO.TextWriter tw)
        {
            tw.Write("goto ");
            if (target == null)
            {
                tw.Write("[unknown]");
            }
            else
            {
                tw.Write(target.Name);
            }
            tw.Write(";");
        }
    }
}
