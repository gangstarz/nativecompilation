﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NativeCompiler.Instructions;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;

namespace NativeCompiler.Language.AST
{
    public class StatementBuilder : IInstructionVisitor<Statement>
    {
        public StatementBuilder(ExpressionBuilder exprBuilder, LabelBuilder labelBuilder, ASTBuilder astBuilder)
        {
            this.expressionBuilder = exprBuilder;
            this.labelBuilder = labelBuilder;
            this.astBuilder = astBuilder;
        }

        private ExpressionBuilder expressionBuilder;
        private LabelBuilder labelBuilder;
        private ASTBuilder astBuilder;

        public Statement Build(IStackOperation instr)
        {
            if (instr is IStatementOperation)
            {
                return instr.Visit(this);
            }
            else
            {
                return new ExpressionStatement(expressionBuilder.Build(instr));
            }
        }

        public Statement Visit(Instructions.IL.And item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Add item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Beq item)
        {
            var ite = new IfThenElse(Operators.Equals(expressionBuilder.Build(item.lhs), expressionBuilder.Build(item.rhs)));
            ite.ThenBody.Add(new Goto(labelBuilder.GetOrDefineLabel(item.TargetOffset)));
            return ite;
        }

        public Statement Visit(Instructions.IL.Bge item)
        {
            var ite = new IfThenElse(Operators.LargerEquals(expressionBuilder.Build(item.lhs), expressionBuilder.Build(item.rhs)));
            ite.ThenBody.Add(new Goto(labelBuilder.GetOrDefineLabel(item.TargetOffset)));
            return ite;
        }

        public Statement Visit(Instructions.IL.Bgt item)
        {
            var ite = new IfThenElse(Operators.Larger(expressionBuilder.Build(item.lhs), expressionBuilder.Build(item.rhs)));
            ite.ThenBody.Add(new Goto(labelBuilder.GetOrDefineLabel(item.TargetOffset)));
            return ite;
        }

        public Statement Visit(Instructions.IL.Ble item)
        {
            var ite = new IfThenElse(Operators.SmallerEquals(expressionBuilder.Build(item.lhs), expressionBuilder.Build(item.rhs)));
            ite.ThenBody.Add(new Goto(labelBuilder.GetOrDefineLabel(item.TargetOffset)));
            return ite;
        }

        public Statement Visit(Instructions.IL.Blt item)
        {
            var ite = new IfThenElse(Operators.Smaller(expressionBuilder.Build(item.lhs), expressionBuilder.Build(item.rhs)));
            ite.ThenBody.Add(new Goto(labelBuilder.GetOrDefineLabel(item.TargetOffset)));
            return ite;
        }

        public Statement Visit(Instructions.IL.Bne_Un item)
        {
            var ite = new IfThenElse(Operators.NotEquals(expressionBuilder.Build(item.lhs), expressionBuilder.Build(item.rhs)));
            ite.ThenBody.Add(new Goto(labelBuilder.GetOrDefineLabel(item.TargetOffset)));
            return ite;
        }

        public Statement Visit(Instructions.IL.Box item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Br item)
        {
            return new Goto(labelBuilder.GetOrDefineLabel(item.TargetOffset));
        }

        public Statement Visit(Instructions.IL.BrFalse item)
        {
            var ite = new IfThenElse(Operators.LogicalNot(expressionBuilder.Build(item.child)));
            ite.ThenBody.Add(new Goto(labelBuilder.GetOrDefineLabel(item.TargetOffset)));
            return ite;
        }

        public Statement Visit(Instructions.IL.BrTrue item)
        {
            var ite = new IfThenElse(expressionBuilder.Build(item.child));
            ite.ThenBody.Add(new Goto(labelBuilder.GetOrDefineLabel(item.TargetOffset)));
            return ite;
        }

        public Statement Visit(Instructions.IL.Call item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.CallVirt item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.CastClass item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Ceq item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Cgt item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.CkFinite item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Clt item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvI item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvI1 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvI2 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvI4 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvI8 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvR item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvR4 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvR8 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvU item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvU1 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvU2 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvU4 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ConvU8 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Div item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Dup item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.EndFinally item)
        {
            return new Scope();
            throw new NotImplementedException();// FIXME?

        }

        public Statement Visit(Instructions.IL.EndFilter item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.InitObj item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.IsInst item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdArgN item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdArgA item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdcI4N item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdcI8 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdcR4 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdcR8 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElem item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemA item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemI item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemI1 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemI2 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemI4 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemI8 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemR4 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemR8 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemRef item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemU1 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemU2 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdElemU4 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdFld item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdFldA item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdFtn item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndI item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndI1 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndI2 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndI4 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndI8 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndR4 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndR8 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndRef item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndU1 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndU2 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdIndU4 item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdLen item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdLocN item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdLocA item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdNull item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdObj item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdSFld item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdSFldA item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdStr item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdToken item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LdVirtFtn item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.LocAlloc item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Leave item)
        {
            return new Goto(labelBuilder.GetOrDefineLabel(item.TargetOffset));
        }

        public Statement Visit(Instructions.IL.MkRefAny item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Mul item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Neg item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.NewArr item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.NewObj item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Nop item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Not item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Or item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Pop item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item.child));
        }

        public Statement Visit(Instructions.IL.ReadOnly item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Rem item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Ret item)
        {
            if (item.Argument == null)
            {
                return new Return();
            }
            else
            {
                return new Return(expressionBuilder.Build(item.Argument));
            }
        }

        public Statement Visit(Instructions.IL.Rethrow item)
        {
            return new Rethrow();
        }

        public Statement Visit(Instructions.IL.Shl item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Shr item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.ShrUn item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.SizeOf item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.StArg item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StElem item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StElemI item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StElemI1 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StElemI2 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StElemI4 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StElemI8 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StElemR4 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StElemR8 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StElemRef item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StFld item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StIndI item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StIndI1 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StIndI2 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StIndI4 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StIndI8 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StIndR4 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StIndR8 item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StIndRef item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StLocN item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StObj item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.StSFld item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }

        public Statement Visit(Instructions.IL.Sub item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Switch item)
        {
            Switch sw = new Switch(expressionBuilder.Build(item.child));

            for (int i = 0; i < item.JumpTable.Length; ++i)
            {
                var cas = new Case(false, new Expression[] { new Constant<int>(i) });
                cas.Body.Add(new Goto(labelBuilder.GetOrDefineLabel(item.JumpTable[i])));
                sw.Add(cas);
            }
            return sw;
        }

        public Statement Visit(Instructions.IL.TailCall item)
        {
            return null;
        }

        public Statement Visit(Instructions.IL.Throw item)
        {
            return new Throw(expressionBuilder.Build(item.child));
        }

        public Statement Visit(Instructions.IL.Unaligned item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Unbox item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Unbox_Any item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Volatile item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.IL.Xor item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(StackStub item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.Meta.LoadExceptionObject item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.Meta.LoadInitialStack item)
        {
            throw new NotImplementedException();
        }

        public Statement Visit(Instructions.Meta.StoreAdditionalStack item)
        {
            return new ExpressionStatement(expressionBuilder.Build(item));
        }
    }
}
