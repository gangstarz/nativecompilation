﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST
{
    public class VariableInitializationVisitor 
    {
        private HashSet<string> usedNames = new HashSet<string>();

        private Dictionary<VariableDeclaration, List<ASTNode>> CollectVariableDeclarations(Callable method)
        {
            // Temporary variables are normally saved as 'int' in IL, but are actually booleans. We fix that here.
            HashSet<VariableDeclaration> temporaryVariablesThatAreBool = new HashSet<VariableDeclaration>();
            Dictionary<VariableDeclaration, List<VariableDeclaration>> directlyDependentVariables = new Dictionary<VariableDeclaration, List<VariableDeclaration>>(); // i = j;

            Dictionary<VariableDeclaration, List<ASTNode>> nodes = new Dictionary<VariableDeclaration, List<ASTNode>>();

            foreach (var item in method.MainScope.AllChildrenOf<Catch>())
            {
                if (item.Variable.Name != null)
                {
                    usedNames.Add(item.Variable.Name);
                }
                else
                {
                    GenerateName(item.Variable);
                }
            }

            foreach (var item in method.MainScope.AllChildrenOf<Variable>())
            {
                if (item.Declaration.VariableType == VariableType.Local)
                {
                    List<ASTNode> ll;
                    if (!nodes.TryGetValue(item.Declaration, out ll))
                    {
                        ll = new List<ASTNode>();
                        nodes.Add(item.Declaration, ll);
                    }
                    ll.Add(item);
                }

                if (item.Declaration.Name != null)
                {
                    usedNames.Add(item.Declaration.Name);
                }

                var parent = item.Parent;
                
                // if parent is part of boolean logic, go to the parent. We only support 'not' at this point; logical 
                // conditions are create later in the AST optimizer.
                while (parent!=null && parent is PrefixUnaryOperator && ((PrefixUnaryOperator)parent).Operator == "!")
                {
                    parent = parent.Parent;
                }
                // If it's a condition in a statement, it's definitely a boolean
                if (parent is Statement && !(parent is ExpressionStatement || parent is Throw || parent is Return)) 
                {
                    temporaryVariablesThatAreBool.Add(item.Declaration);
                }
            }

            foreach (var item in method.MainScope.AllChildrenOf<Store>())
            {
                if (item.Target.VariableType == VariableType.Local)
                {
                    List<ASTNode> ll;
                    if (!nodes.TryGetValue(item.Target, out ll))
                    {
                        ll = new List<ASTNode>();
                        nodes.Add(item.Target, ll);
                    }
                    ll.Add(item);
                }

                if (item.Target.Name != null)
                {
                    usedNames.Add(item.Target.Name);
                }

                if (item.Value.EvaluatesToBoolean())
                {
                    temporaryVariablesThatAreBool.Add(item.Target);
                }

                if (item.Value is Variable) // operators are already handled
                {
                    List<VariableDeclaration> ll;
                    if (!directlyDependentVariables.TryGetValue(((Variable)item.Value).Declaration, out ll))
                    {
                        ll = new List<VariableDeclaration>();
                        directlyDependentVariables.Add(((Variable)item.Value).Declaration, ll);
                    }
                    ll.Add(item.Target);
                }
            }

            // Generate names if necessary
            foreach (var item in nodes)
            {
                if (item.Key.Name == null)
                {
                    GenerateName(item.Key);
                }
            }

            // Check booleans
            foreach (var item in nodes.Keys)
            {
                if (temporaryVariablesThatAreBool.Contains(item))
                {
                    Stack<VariableDeclaration> decl = new Stack<VariableDeclaration>();
                    HashSet<VariableDeclaration> handled = new HashSet<VariableDeclaration>();
                    decl.Push(item);
                    handled.Add(item);

                    while (decl.Count > 0) 
                    {
                        var cur = decl.Pop();

                        if (cur.Type is BasicType && ((BasicType)cur.Type).Name == "System.Int32")
                        {
                            ((BasicType)cur.Type).Name = "System.Boolean";

                            List<VariableDeclaration> dep;
                            if (directlyDependentVariables.TryGetValue(cur, out dep))
                            {
                                foreach (var dependency in dep)
                                {
                                    if (handled.Add(dependency))
                                    {
                                        decl.Push(dependency);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return nodes;
        }

        public void InjectInTopScope(Callable method)
        {
            var nodes = CollectVariableDeclarations(method);

            var toplevel = method.MainScope;
            if (toplevel.NumberOfChildren != 0)
            {
                var first = toplevel.First;

                // Inject initialization code
                foreach (var kv in nodes)
                {
                    (new InitializeVariable(kv.Key)).InsertBefore(first);
                }
            }
        }

        public void InjectInBestScope(Callable method)
        {
            var nodes = CollectVariableDeclarations(method);

            // Inject initialization code
            foreach (var kv in nodes)
            {
                // We need to know the first point at which we can insert the variabel initialization:
                if (kv.Value.Count == 1)
                {
                    ASTNode insertPoint = kv.Value[0];
                    while (insertPoint != null && !(insertPoint is Statement))
                    {
                        insertPoint = insertPoint.Parent;
                    }
                    // Trivial exception case:
                    (new InitializeVariable(kv.Key)).InsertBefore(insertPoint);
                }
                else
                {
                    // Calculate the common anchestor of all nodes
                    var common = kv.Value[0];
                    for (int i = 1; i < kv.Value.Count; ++i)
                    {
                        common = common.CommonAncestor(kv.Value[i]);
                    }

                    if (common is Scope)
                    {
                        // Find out where to insert it into the scope
                        Scope sc = (Scope)common;
                        ASTNode before = null;
                        for (int i = 0; i < sc.Children.Count && before == null; ++i)
                        {
                            var cur = sc.Children[i];
                            foreach (var item in kv.Value)
                            {
                                if (item < cur || cur.IsAncesterOf(item))
                                {
                                    before = cur;
                                    break;
                                }
                            }
                        }

                        if (before == null)
                        {
                            // Add as last
                            sc.Add(new InitializeVariable(kv.Key));
                        }
                        else
                        {
                            (new InitializeVariable(kv.Key)).InsertBefore(before);
                        }
                    }
                    else
                    {
                        // The common node is f.ex. an if-then-else node. We can simply insert here
                        (new InitializeVariable(kv.Key)).InsertBefore(common);
                    }
                }
            }
        }

        private bool AllEqualScope(List<Scope>[] vals)
        {
            Scope obj = vals[0][vals[0].Count - 1];
            for (int i = 1; i < vals.Length; ++i)
            {
                if (!object.ReferenceEquals(vals[i][vals[i].Count - 1], obj))
                {
                    return false;
                }
            }
            return true;
        }

        private void GenerateName(VariableDeclaration variable)
        {
            // Generate name
            string name;
            if (variable.Type is ArrayType)
            {
                name = "array";
            }
            else if (variable.Type is BasicType)
            {
                switch (((BasicType)variable.Type).Name)
                {
                    case "System.Object": name = "obj"; break;
                    case "System.Boolean": name = "val"; break;
                    case "System.Byte": name = "b"; break;
                    case "System.SByte": name = "sb"; break;
                    case "System.Char": name = "ch"; break;
                    case "System.Int16": name = "s"; break;
                    case "System.UInt16": name = "us"; break;
                    case "System.Int32": name = "i"; break;
                    case "System.UInt32": name = "ui"; break;
                    case "System.Int64": name = "l"; break;
                    case "System.UInt64": name = "ul"; break;
                    case "System.Single": name = "f"; break;
                    case "System.Double": name = "d"; break;
                    case "System.Decimal": name = "d"; break;
                    case "System.String": name = "str"; break;
                    case "System.Type": name = "type"; break;
                    case "System.Exception": name = "ex"; break;
                    default:
                        name = "val"; break;
                }
            }
            else if (variable.Type is Pointer || variable.Type is Reference)
            {
                name = "ptr";
            }
            else
            {
                name = variable.Type.BaseDefinition.Name;
                if (name.Length > 0 && char.IsUpper(name, 0))
                {
                    name = "" + char.ToLower(name[0]) + name.Substring(1);
                }
                else
                {
                    name = "var";
                }
            }

            string dname = name;

            int n = 1;
            while (usedNames.Contains(dname))
            {
                dname = name + (n++).ToString();
            }

            variable.Name = dname;
            usedNames.Add(dname);
        }
    }
}
