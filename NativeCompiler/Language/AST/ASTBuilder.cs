﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Decompiler.Blocks;
using NativeCompiler.Dependencies;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.Meta;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Statements;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST
{
    public class ASTBuilder
    {
        private Dictionary<System.Reflection.Assembly, Structure.Assembly> assemblies = new Dictionary<System.Reflection.Assembly, Structure.Assembly>();
        private Dictionary<MethodBase, Method> methods = new Dictionary<MethodBase, Method>();
        private Dictionary<ConstructorInfo, Constructor> ctors = new Dictionary<ConstructorInfo, Constructor>();

        private Namespace root = new Namespace();

        private class PendingImplementation
        {
            public Class context;
            public Dictionary<FieldInfo, Field> fields = new Dictionary<FieldInfo, Field>();
            public List<Tuple<DecompiledMethod, Callable>> methods = new List<Tuple<DecompiledMethod, Callable>>();
        }

        public Namespace Build(IEnumerable<DependentType> types)
        {
            var pi = new List<PendingImplementation>();
            foreach (var type in types)
            {
                pi.Add(BuildStub(type));
            }

            VariableMapping mapping = new VariableMapping();

            foreach (var impl in pi)
            {
                foreach (var meth in impl.methods)
                {
                    mapping.Add(meth.Item2, BuildVariableMap(impl, meth));
                }
            }

            foreach (var impl in pi)
            {
                impl.context.IsDefined = true;
                foreach (var meth in impl.methods)
                {
                    BuildBody(impl, meth, mapping);
                }
            }

            return root;
        }

        private PendingImplementation BuildStub(DependentType type)
        {
            var currentType = FindOrCreateClass(type.Type);

            PendingImplementation pi = new PendingImplementation() { context = currentType };

            if (type.Type.IsInterface)
            {
                // Build interface type
                foreach (var method in type.Methods.Values)
                {
                    if (method.Method.DeclaringType != type.Type)
                    {
                        continue;
                    }

                    var meth = new Method(currentType, method.Method.Name, false, (method.Method.IsAbstract || method.Method.IsVirtual));
                    meth.ReturnType = FindOrCreateType(((MethodInfo)method.Method).ReturnType);

                    if (method.Method.IsGenericMethod)
                    {
                        var args = method.Method.GetGenericArguments();
                        meth.GenericArguments = new List<Class>();
                        foreach (var arg in args)
                        {
                            meth.GenericArguments.Add(this.FindOrCreateClass(arg));
                        }
                    }

                    foreach (var par in method.Method.GetParameters())
                    {
                        meth.Parameters.Add(new VariableDeclaration(VariableType.Parameter, FindOrCreateType(par), par.Name));
                    }
                    currentType.Methods.Add(meth);

                    methods.Add(method.Method, meth);
                }
            }
            else
            {
                // Build normal type
                foreach (var method in type.Methods.Values)
                {
                    if (method.Method.DeclaringType != type.Type)
                    {
                        continue;
                    }

                    Callable callable = null;
                    Scope body = null;
                    if (method.Method is ConstructorInfo)
                    {
                        var ctor = new Constructor(currentType, method.Method.IsStatic);
                        foreach (var par in method.Method.GetParameters())
                        {
                            ctor.Parameters.Add(new VariableDeclaration(VariableType.Parameter, FindOrCreateType(par), par.Name));
                        }
                        currentType.Constructors.Add(ctor);
                        body = ctor.MainScope;
                        callable = ctor;

                        ctors.Add((ConstructorInfo)method.Method, ctor);
                    }
                    else
                    {
                        var meth = new Method(currentType, method.Method.Name, method.Method.IsStatic, (method.Method.IsAbstract || method.Method.IsVirtual));
                        meth.ReturnType = FindOrCreateType(((MethodInfo)method.Method).ReturnType);

                        if (method.Method.IsGenericMethod)
                        {
                            var args = method.Method.GetGenericArguments();
                            meth.GenericArguments = new List<Class>();
                            foreach (var arg in args)
                            {
                                meth.GenericArguments.Add(this.FindOrCreateClass(arg));
                            }
                        }

                        foreach (var par in method.Method.GetParameters())
                        {
                            meth.Parameters.Add(new VariableDeclaration(VariableType.Parameter, FindOrCreateType(par), par.Name));
                        }
                        currentType.Methods.Add(meth);

                        if (!method.Method.IsAbstract)
                        {
                            body = meth.MainScope;
                        }
                        callable = meth;

                        methods.Add(method.Method, meth);
                    }

                    if (body != null && method.MethodBlock != null)
                    {
                        pi.methods.Add(new Tuple<DecompiledMethod, Callable>(method, callable));
                    }
                }

                foreach (var field in type.Fields)
                {
                    pi.fields.Add(field, new Field(FindOrCreateType(field.FieldType), field.Name, field.IsStatic));
                }
            }

            return pi;
        }

        private VariableMap BuildVariableMap(PendingImplementation impl, Tuple<DecompiledMethod, Callable> meth)
        {
            var method = meth.Item1.Method;

            // Figure out the variables:

            // Collect parameters
            List<VariableDeclaration> parameters = new List<VariableDeclaration>();
            bool hasThis = false;

            if (!method.IsStatic && (method is MethodInfo || method is ConstructorInfo))
            {
                hasThis = true;
                parameters.Add(null);
            }

            foreach (var par in meth.Item2.Parameters)
            {
                parameters.Add(par);
            }

            Dictionary<int, VariableDeclaration> localVariables = new Dictionary<int, VariableDeclaration>();
            foreach (var var in method.GetMethodBody().LocalVariables)
            {
                localVariables.Add(var.LocalIndex, new VariableDeclaration(VariableType.Local, FindOrCreateType(var.LocalType), null));
            }

            // Create temporary variables
            Dictionary<int, VariableDeclaration> storeStackVars = new Dictionary<int, VariableDeclaration>();
            
            Dictionary<CatchBlock, VariableDeclaration> catchObjects = new Dictionary<CatchBlock, VariableDeclaration>(ObjectEquality<CatchBlock>.Instance);
            var allCatchBlocks = meth.Item1.MethodBlock.AllCatchBlocks();
            foreach (var cur in allCatchBlocks)
            {
                var decl = new VariableDeclaration(VariableType.Exception, FindOrCreateType(cur.Type), null);
                catchObjects.Add(cur, decl);
            }

            var allBlocks = meth.Item1.MethodBlock.AllBlocks();
            foreach (var cur in allBlocks)
            {
                var bl = cur as ILBlock;
                if (bl != null)
                {
                    foreach (var op in bl.StackOperations)
                    {
                        var sas = op as StoreAdditionalStack;
                        if (sas != null)
                        {
                            storeStackVars[sas.Id] = new VariableDeclaration(VariableType.Local, FindOrCreateType(sas.ReturnType), null);
                        }

                        Stack<IStackOperation> sob = new Stack<IStackOperation>();
                        sob.Push(op);
                        while (sob.Count > 0)
                        {
                            var current = sob.Pop();
                            foreach (var child in current.Children)
                            {
                                sob.Push(child);
                            }
                            var eli = current as LoadExceptionObject;
                            if (eli != null)
                            {
                                var catchBlock = (CatchBlock)cur.Parent.Parent;

                                storeStackVars[eli.Id] = catchObjects[catchBlock];
                            }
                        }

                    }
                }
            }

            List<VariableDeclaration> allVars = new List<VariableDeclaration>(localVariables.Values);
            allVars.AddRange(storeStackVars.Values);
            meth.Item2.LocalVariables = allVars;

            return new VariableMap(impl.fields, hasThis, parameters, localVariables, storeStackVars, catchObjects);
        }

        private void BuildBody(PendingImplementation impl, Tuple<DecompiledMethod, Callable> meth, VariableMapping mapping)
        {
            var labelBuilder = new LabelBuilder();
            var expressionBuilder = new ExpressionBuilder(this, mapping, meth.Item2);
            var statementBuilder = new StatementBuilder(expressionBuilder, labelBuilder, this);
            var blockBuilder = new BlockBuilder(meth.Item2.MainScope, impl.context, statementBuilder, expressionBuilder, labelBuilder, mapping.GetMap(meth.Item2), this);

            meth.Item1.MethodBlock.Visit(blockBuilder);
        }

        public Class FindOrCreateClass(Type type)
        {
            if (type.IsGenericParameter)
            {
                IGenericContainer container = null;
                if (type.DeclaringType != null)
                {
                    container = FindOrCreateClass(type.DeclaringType);
                }
                else
                {
                    container = (Method)FindOrCreateMethod(type.DeclaringMethod);
                }

                if (container.GenericArguments == null)
                {
                    container.GenericArguments = new List<Class>();
                }

                GenericArgument arg = (GenericArgument)container.GenericArguments.FirstOrDefault((a) => a.Name == type.Name);
                if (arg != null)
                {
                    return arg;
                }
                else
                {
                    arg = new GenericArgument(type);
                    foreach (var constraint in type.GetGenericParameterConstraints())
                    {
                        arg.BaseClasses.Add(FindOrCreateType(constraint));
                    }

                    return arg;
                }
            }
            else
            {
                Namespace currentNamespace = root;
                foreach (var comp in (type.Namespace ?? "").Split('.'))
                {
                    currentNamespace = currentNamespace.GotoNamespace(comp);
                }

                var currentType = currentNamespace.FindOrCreateClass(type, this);
                return currentType;
            }
        }

        public IType FindOrCreateType(ParameterInfo parameter)
        {
            var type = FindOrCreateType(parameter.ParameterType);

            if (parameter.IsOut)
            {
                type = new NativeCompiler.Language.AST.Types.Pointer(type);
            }

            return type;
        }

        public IType FindOrCreateType(Type type)
        {
            var t = type;

            if (type.IsArray)
            {
                return new ArrayType(FindOrCreateType(type.GetElementType()));
            }
            else if (type.IsPointer)
            {
                return new NativeCompiler.Language.AST.Types.Pointer(FindOrCreateType(type.GetElementType()));
            }
            else if (type.IsByRef)
            {
                return new Reference(FindOrCreateType(type.GetElementType()));
            }
            else if (type.IsGenericType && !type.IsGenericTypeDefinition)
            {
                var def = FindOrCreateType(type.GetGenericTypeDefinition());
                List<IType> arguments = new List<IType>();
                foreach (var arg in type.GetGenericArguments())
                {
                    arguments.Add(FindOrCreateType(arg));
                }
                return new GenericType(def, arguments);
            }
            else if (BasicType.IsBasicType(type))
            {
                return new BasicType(type.FullName);
            }
            else
            {
                Class cl = FindOrCreateClass(type);
                return new ClassType(cl);
            }
        }

        public Callable FindOrCreateMethod(MethodBase method)
        {
            if (method is ConstructorInfo)
            {
                return FindOrCreateConstructor((ConstructorInfo)method);
            }
            else
            {
                Method meth;
                if (!methods.TryGetValue(method, out meth))
                {
                    var cl = FindOrCreateClass(method.DeclaringType);
                    meth = new Method(cl, method.Name, method.IsStatic, (method.IsAbstract || method.IsVirtual));

                    if (method.IsGenericMethod)
                    {
                        var args = method.GetGenericArguments();
                        meth.GenericArguments = new List<Class>();
                        foreach (var arg in args)
                        {
                            meth.GenericArguments.Add(this.FindOrCreateClass(arg));
                        }
                    }

                    meth.ReturnType = FindOrCreateType(((MethodInfo)method).ReturnType);
                    foreach (var par in method.GetParameters())
                    {
                        meth.Parameters.Add(new VariableDeclaration(VariableType.Parameter, FindOrCreateType(par), par.Name));
                    }

                    cl.Methods.Add(meth);
                    methods.Add(method, meth);
                }
                return meth;
            }
        }

        public Constructor FindOrCreateConstructor(Type container, Type[] parameters)
        {
            if (container.IsGenericParameter)
            {
                if (parameters.Length != 0)
                {
                    throw new Exception("Expected empty parameters list; should be a 'new' constraint.");
                }
                var cl = FindOrCreateClass(container);
                var constructor = new Constructor(cl, false);
                return constructor;
            }
            else
            {
                var constructor = container.GetConstructor(parameters);
                if (constructor == null && container.IsValueType && parameters.Length == 0)
                {
                    var defaultCtor = new DefaultConstructor(container);

                    return FindOrCreateConstructor(defaultCtor);
                }
                else
                {
                    return FindOrCreateConstructor(constructor);
                }
            }
        }

        public Constructor FindOrCreateConstructor(ConstructorInfo ctor)
        {
            Constructor meth;
            if (!ctors.TryGetValue(ctor, out meth))
            {
                var cl = FindOrCreateClass(ctor.DeclaringType);
                meth = new Constructor(cl, ctor.IsStatic);
                cl.Constructors.Add(meth);
                ctors.Add(ctor, meth);
            }
            return meth;
        }

        public Structure.Module FindOrCreateModule(System.Reflection.Module module)
        {
            var assembly = FindOrCreateAssembly(module.Assembly);
            var mod = assembly.Modules.FirstOrDefault((a) => a.FullyQualifiedName == module.FullyQualifiedName);
            if (mod == null)
            {
                mod = new Structure.Module(assembly, module.Name, module.FullyQualifiedName);
                assembly.Modules.Add(mod);
            }
            return mod;
        }

        private Structure.Assembly FindOrCreateAssembly(System.Reflection.Assembly assembly)
        {
            Structure.Assembly ass;
            if (!assemblies.TryGetValue(assembly, out ass))
            {
                ass = new Structure.Assembly(assembly);
                assemblies.Add(assembly, ass);
            }
            return ass;
        }
    }
}
