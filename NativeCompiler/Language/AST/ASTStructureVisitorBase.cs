﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST
{
    public abstract class ASTStructureVisitorBase
    {
        public virtual void Visit(Class clas)
        {
            clas.Fields.ForEach((a) => a.Accept(this));
            clas.Constructors.ForEach((a) => a.Accept(this));
            clas.Methods.ForEach((a) => a.Accept(this));
        }

        public virtual void Visit(Method method)
        {
        }

        public virtual void Visit(Constructor constructor)
        {
        }

        public virtual void Visit(Field field)
        {
        }

        public virtual void Visit(Namespace namesp)
        {
            foreach (var ns in namesp.Namespaces)
            {
                ns.Accept(this);
            }

            foreach (var cl in namesp.Classes)
            {
                cl.Accept(this);
            }
        }
    }
}
