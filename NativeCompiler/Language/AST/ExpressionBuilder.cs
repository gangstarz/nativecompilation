﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST
{
    public class ExpressionBuilder : IInstructionVisitor<Expression>
    {
        public ExpressionBuilder(ASTBuilder astBuilder, VariableMapping variableMap, Callable method)
        {
            this.astBuilder = astBuilder;

            this.variableMap = variableMap;
            this.method = method;
            this.currentMap = variableMap.GetMap(method);
        }

        private ASTBuilder astBuilder;

        private VariableMapping variableMap;
        private Callable method;
        private VariableMap currentMap;

        public Expression Build(IStackOperation op)
        {
            return op.Visit(this);
        }

        public Expression Visit(Instructions.IL.And item)
        {
            return Operators.And(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Add item)
        {
            return Operators.Addition(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Beq item)
        {
            return Operators.Equals(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Bge item)
        {
            return Operators.LargerEquals(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Bgt item)
        {
            return Operators.Larger(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Ble item)
        {
            return Operators.SmallerEquals(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Blt item)
        {
            return Operators.Smaller(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Bne_Un item)
        {
            return Operators.NotEquals(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Box item)
        {
            return new Box(astBuilder.FindOrCreateType(item.BoxType), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.Br item)
        {
            throw new NotSupportedException();
        }

        public Expression Visit(Instructions.IL.BrFalse item)
        {
            return Operators.LogicalNot(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.BrTrue item)
        {
            return item.child.Visit(this);
        }

        public Expression Visit(Instructions.IL.Call item)
        {
            if (item.Method.DeclaringType.FullName == "System.Runtime.CompilerServices.RuntimeHelpers")
            {
                // Something fancy is going to happen. :-)
                return RuntimeHelperBuilder.RewriteRuntimeHelpers(item, this);
            }

            var meth = astBuilder.FindOrCreateMethod(item.Method);
            List<Expression> parameters = new List<Expression>();
            foreach (var par in item.Parameters)
            {
                parameters.Add(par.Visit(this));
            }
            return new Call(meth, false, item.MethodObject == null ? null : item.MethodObject.Visit(this), parameters);
        }

        public Expression Visit(Instructions.IL.CallVirt item)
        {
            var meth = astBuilder.FindOrCreateMethod(item.Method);
            List<Expression> parameters = new List<Expression>();
            foreach (var par in item.Parameters)
            {
                parameters.Add(par.Visit(this));
            }
            return new Call(meth, true, item.MethodObject == null ? null : item.MethodObject.Visit(this), parameters);
        }

        public Expression Visit(Instructions.IL.CastClass item)
        {
            return new DynamicCast(astBuilder.FindOrCreateType(item.Type), true, item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.Ceq item)
        {
            return Operators.Equals(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Cgt item)
        {
            return Operators.Larger(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.CkFinite item)
        {
            return new IsFinite(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.Clt item)
        {
            return Operators.Smaller(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvI item)
        {
            // Not sure.
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.ConvI1 item)
        {
            return new BasicTypeCast(new BasicType("System.SByte"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvI2 item)
        {
            return new BasicTypeCast(new BasicType("System.Int16"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvI4 item)
        {
            return new BasicTypeCast(new BasicType("System.Int32"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvI8 item)
        {
            return new BasicTypeCast(new BasicType("System.Int64"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvR item)
        {
            return new BasicTypeCast(new BasicType("System.Single"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvR4 item)
        {
            return new BasicTypeCast(new BasicType("System.Single"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvR8 item)
        {
            return new BasicTypeCast(new BasicType("System.Double"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvU item)
        {
            // Not sure.
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.ConvU1 item)
        {
            return new BasicTypeCast(new BasicType("System.Byte"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvU2 item)
        {
            return new BasicTypeCast(new BasicType("System.UInt16"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvU4 item)
        {
            return new BasicTypeCast(new BasicType("System.UInt32"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.ConvU8 item)
        {
            return new BasicTypeCast(new BasicType("System.UInt64"), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.Div item)
        {
            return Operators.Division(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Dup item)
        {
            throw new NotSupportedException(); // Should be optimized away in 'zeroalign'.
        }

        public Expression Visit(Instructions.IL.EndFinally item)
        {
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.EndFilter item)
        {
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.InitObj item)
        {
            var type = astBuilder.FindOrCreateType(item.ValueType);

            if (item.ValueType.IsValueType)
            {
                NewObject ctorCall = null;

                var meth = astBuilder.FindOrCreateConstructor(item.ValueType, Type.EmptyTypes);
                List<Expression> parameters = new List<Expression>();
                ctorCall = new NewObject(meth, parameters);

                return new StoreIndirect(item.child.Visit(this), ctorCall);
            }
            else
            {
                return new StoreIndirect(item.child.Visit(this), new Constant<object>(null));
            }
        }

        public Expression Visit(Instructions.IL.IsInst item)
        {
            return new DynamicCast(astBuilder.FindOrCreateType(item.InstanceType), false, item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdArgN item)
        {
            if (this.currentMap.hasThis && item.N == 0)
            {
                return new This();
            }
            else
            {
                return new Variable(this.currentMap.arguments[item.N]);
            }
        }

        public Expression Visit(Instructions.IL.LdArgA item)
        {
            if (this.currentMap.hasThis && item.N == 0)
            {
                return Operators.AddressOf(new This());
            }
            else
            {
                if (this.currentMap.arguments[item.N].Type.BaseDefinition is GenericArgument)
                {
                    // Generics are always mapped to object* so we don't want the address-of of the type
                    return new Variable(this.currentMap.arguments[item.N]);
                }
                else
                {
                    return Operators.AddressOf(new Variable(this.currentMap.arguments[item.N]));
                }
            }
        }

        public Expression Visit(Instructions.IL.LdcI4N item)
        {
            return new Constant<int>(item.N);
        }

        public Expression Visit(Instructions.IL.LdcI8 item)
        {
            return new Constant<long>(item.N);
        }

        public Expression Visit(Instructions.IL.LdcR4 item)
        {
            return new Constant<float>(item.N);
        }

        public Expression Visit(Instructions.IL.LdcR8 item)
        {
            return new Constant<double>(item.N);
        }

        public Expression Visit(Instructions.IL.LdElem item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdElemA item)
        {
            return Operators.AddressOf(new Index(item.lhs.Visit(this), item.rhs.Visit(this)));
        }

        public Expression Visit(Instructions.IL.LdElemI item)
        {
            // Not sure
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.LdElemI1 item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdElemI2 item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdElemI4 item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdElemI8 item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdElemR4 item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdElemR8 item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdElemRef item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdElemU1 item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdElemU2 item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdElemU4 item)
        {
            return new Index(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdFld item)
        {
            return new Variable(variableMap.GetField(item.Field));
        }

        public Expression Visit(Instructions.IL.LdFldA item)
        {
            if (variableMap.GetField(item.Field).Type.BaseDefinition is GenericArgument)
            {
                // Generics are always mapped to object* so we don't want the address-of of the type
                return new Variable(variableMap.GetField(item.Field));
            }
            else
            {
                return Operators.AddressOf(new Variable(variableMap.GetField(item.Field)));
            }
        }

        public Expression Visit(Instructions.IL.LdFtn item)
        {
            return new FunctionPointer(astBuilder.FindOrCreateMethod(item.Method), false);
        }

        public Expression Visit(Instructions.IL.LdIndI item)
        {
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.LdIndI1 item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdIndI2 item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdIndI4 item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdIndI8 item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdIndR4 item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdIndR8 item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdIndRef item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdIndU1 item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdIndU2 item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdIndU4 item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdLen item)
        {
            return new ArrayLength(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdLocN item)
        {
            return new Variable(this.currentMap.locals[item.N]);
        }

        public Expression Visit(Instructions.IL.LdLocA item)
        {
            if (this.currentMap.locals[item.N].Type.BaseDefinition is GenericArgument)
            {
                // Generics are always mapped to object* so we don't want the address-of of the type
                return new Variable(this.currentMap.locals[item.N]);
            }
            else
            {
                return Operators.AddressOf(new Variable(this.currentMap.locals[item.N]));
            }
        }

        public Expression Visit(Instructions.IL.LdNull item)
        {
            return new Constant<object>(null);
        }

        public Expression Visit(Instructions.IL.LdObj item)
        {
            return Operators.Dereference(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.LdSFld item)
        {
            return new Variable(variableMap.GetField(item.Field));
        }

        public Expression Visit(Instructions.IL.LdSFldA item)
        {
            return Operators.AddressOf(new Variable(variableMap.GetField(item.Field)));
        }

        public Expression Visit(Instructions.IL.LdStr item)
        {
            return new Constant<string>(item.Data);
        }

        public Expression Visit(Instructions.IL.LdToken item)
        {
            // Load runtime field info
            if (item.Operand is FieldInfo)
            {
                var field = this.astBuilder.FindOrCreateClass(item.Operand.DeclaringType).Fields.First((a) => a.Name == item.Operand.Name);
                return Operators.AddressOf(new Variable(field));
            }
            else if (item.Operand is Type)
            {
                return new Constant<IType>(astBuilder.FindOrCreateType((Type)item.Operand));
            }

            // TODO. Normally used to load a token, such as a type, fieldinfo, etc.
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.LdVirtFtn item)
        {
            return new FunctionPointer(astBuilder.FindOrCreateMethod(item.Method), true);
        }

        public Expression Visit(Instructions.IL.LocAlloc item)
        {
            // TODO
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.Leave item)
        {
            // TODO
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.MkRefAny item)
        {
            // TODO
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.Mul item)
        {
            return Operators.Multiplication(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Neg item)
        {
            return Operators.Negate(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.NewArr item)
        {
            return new NewArray(astBuilder.FindOrCreateType(item.Type), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.NewObj item)
        {
            var meth = astBuilder.FindOrCreateConstructor(item.Constructor);
            List<Expression> parameters = new List<Expression>();
            foreach (var par in item.Parameters)
            {
                parameters.Add(par.Visit(this));
            }
            return new NewObject(meth, parameters);
        }

        public Expression Visit(Instructions.IL.Nop item)
        {
            throw new NotSupportedException(); // should be optimized away
        }

        public Expression Visit(Instructions.IL.Not item)
        {
            return Operators.BitwiseNot(item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.Or item)
        {
            return Operators.Or(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Pop item)
        {
            return item.child.Visit(this);
        }

        public Expression Visit(Instructions.IL.ReadOnly item)
        {
            return item.child.Visit(this);
        }

        public Expression Visit(Instructions.IL.Rem item)
        {
            return Operators.Modulo(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Ret item)
        {
            throw new NotSupportedException(); // Statement
        }

        public Expression Visit(Instructions.IL.Rethrow item)
        {
            throw new NotSupportedException(); // Statement
        }

        public Expression Visit(Instructions.IL.Shl item)
        {
            return Operators.ShiftLeft(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Shr item)
        {
            return Operators.ShiftRight(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.ShrUn item)
        {
            return Operators.ShiftRight(item.lhs.Visit(this), item.rhs.Visit(this));
        }


        private int CalculateSize(Type type)
        {
            if (type.IsValueType)
            {
                // One of the standard types?
                if (type == typeof(byte) || type == typeof(sbyte)) { return 1; }
                else if (type == typeof(short) || type == typeof(ushort) || type == typeof(char)) { return 2; }
                else if (type == typeof(int) || type == typeof(uint) || type == typeof(bool)) { return 4; }
                else if (type == typeof(long) || type == typeof(ulong)) { return 8; }
                else
                {
                    // Calculate value type size
                    int total = 0;
                    foreach (var field in type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
                    {
                        // Basically N bytes of data means it's N-byte aligned.
                        int cur = CalculateSize(field.FieldType);
                        if ((total % cur) != 0)
                        {
                            total += cur - (total % cur);
                        }
                        total += cur;
                    }

                    if (total == 0)
                    {
                        total = 8;
                    }
                    return total;
                }
            }
            else
            {
                return 8; // 64-bit pointer = 8 bytes.
            }
        }

        public Expression Visit(Instructions.IL.SizeOf item)
        {
            return new Constant<int>(CalculateSize(item.Type));
        }

        public Expression Visit(Instructions.IL.StArg item)
        {
            return new Store(null, (VariableDeclaration)this.currentMap.arguments[item.N], item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.StElem item)
        {
            return new StoreIndirect(new Index(item.array.Visit(this), item.index.Visit(this)), item.value.Visit(this));
        }

        public Expression Visit(Instructions.IL.StElemI item)
        {
            // not sure
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.StElemI1 item)
        {
            return new StoreIndirect(new Index(item.array.Visit(this), item.index.Visit(this)), item.value.Visit(this));
        }

        public Expression Visit(Instructions.IL.StElemI2 item)
        {
            return new StoreIndirect(new Index(item.array.Visit(this), item.index.Visit(this)), item.value.Visit(this));
        }

        public Expression Visit(Instructions.IL.StElemI4 item)
        {
            return new StoreIndirect(new Index(item.array.Visit(this), item.index.Visit(this)), item.value.Visit(this));
        }

        public Expression Visit(Instructions.IL.StElemI8 item)
        {
            return new StoreIndirect(new Index(item.array.Visit(this), item.index.Visit(this)), item.value.Visit(this));
        }

        public Expression Visit(Instructions.IL.StElemR4 item)
        {
            return new StoreIndirect(new Index(item.array.Visit(this), item.index.Visit(this)), item.value.Visit(this));
        }

        public Expression Visit(Instructions.IL.StElemR8 item)
        {
            return new StoreIndirect(new Index(item.array.Visit(this), item.index.Visit(this)), item.value.Visit(this));
        }

        public Expression Visit(Instructions.IL.StElemRef item)
        {
            return new StoreIndirect(new Index(item.array.Visit(this), item.index.Visit(this)), item.value.Visit(this));
        }

        public Expression Visit(Instructions.IL.StFld item)
        {
            if (item.rhs == null)
            {
                return new Store(null, variableMap.GetField(item.Field), item.lhs.Visit(this));
            }
            else
            {
                return new Store(item.rhs.Visit(this), variableMap.GetField(item.Field), item.lhs.Visit(this));
            }
        }

        public Expression Visit(Instructions.IL.StIndI item)
        {
            // Not sure
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.StIndI1 item)
        {
            return new StoreIndirect(Operators.Dereference(item.lhs.Visit(this)), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.StIndI2 item)
        {
            return new StoreIndirect(Operators.Dereference(item.lhs.Visit(this)), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.StIndI4 item)
        {
            return new StoreIndirect(Operators.Dereference(item.lhs.Visit(this)), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.StIndI8 item)
        {
            return new StoreIndirect(Operators.Dereference(item.lhs.Visit(this)), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.StIndR4 item)
        {
            return new StoreIndirect(Operators.Dereference(item.lhs.Visit(this)), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.StIndR8 item)
        {
            return new StoreIndirect(Operators.Dereference(item.lhs.Visit(this)), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.StIndRef item)
        {
            return new StoreIndirect(Operators.Dereference(item.lhs.Visit(this)), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.StLocN item)
        {
            return new Store(null, this.currentMap.locals[item.N], item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.StObj item)
        {
            // Copies value into memory address? Not sure...
            // That sounds like deref[lhs] = rhs. TODO: Figure out if this is correct.
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.StSFld item)
        {
            return new Store(null, variableMap.GetField(item.Field), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.Sub item)
        {
            return Operators.Subtraction(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(Instructions.IL.Switch item)
        {
            throw new NotImplementedException();
        }

        public Expression Visit(Instructions.IL.TailCall item)
        {
            throw new NotSupportedException(); // statement
        }

        public Expression Visit(Instructions.IL.Throw item)
        {
            throw new NotSupportedException(); // statement
        }

        public Expression Visit(Instructions.IL.Unaligned item)
        {
            // we don't care.
            return item.child.Visit(this);
        }

        public Expression Visit(Instructions.IL.Unbox item)
        {
            return new Unbox(astBuilder.FindOrCreateType(item.BoxType), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.Unbox_Any item)
        {
            return new Unbox(astBuilder.FindOrCreateType(item.BoxType), item.child.Visit(this));
        }

        public Expression Visit(Instructions.IL.Volatile item)
        {
            // TODO: optimize this prefix away?
            throw new NotSupportedException();
        }

        public Expression Visit(Instructions.IL.Xor item)
        {
            return Operators.Xor(item.lhs.Visit(this), item.rhs.Visit(this));
        }

        public Expression Visit(StackStub item)
        {
            throw new NotSupportedException();
        }

        public Expression Visit(Instructions.Meta.LoadExceptionObject item)
        {
            return new Variable(this.currentMap.temporaries[item.Id]);
        }

        public Expression Visit(Instructions.Meta.LoadInitialStack item)
        {
            return new Variable(this.currentMap.temporaries[item.Id]);
        }

        public Expression Visit(Instructions.Meta.StoreAdditionalStack item)
        {
            return new Store(null, this.currentMap.temporaries[item.Id], item.child.Visit(this));
        }
    }
}
