﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Statements;

namespace NativeCompiler.Language.AST
{
    public class LabelBuilder
    {
        private Dictionary<int, Label> labels = new Dictionary<int, Label>();

        public Label GetOrDefineLabel(int offset)
        {
            Label lbl;
            if (!labels.TryGetValue(offset, out lbl))
            {
                lbl = new Label(offset);
                labels.Add(offset, lbl);
            }

            return lbl;
        }
    }
}
