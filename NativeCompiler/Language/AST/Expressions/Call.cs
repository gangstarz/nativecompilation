﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST.Expressions
{
    public class Call : Expression
    {
        public Call(Callable callable, bool virtualCall, Expression obj = null, IEnumerable<Expression> pars = null)
        {
            this.Callable = callable;
            this.VirtualCall = virtualCall;

            if (obj != null && pars == null)
            {
                throw new Exception("Initialization order of 'call' must be parameters, then object.");
            }

            if (pars != null)
            {
                foreach (var par in pars)
                {
                    Add(par);
                }
            }
            if (obj != null)
            {
                hasObject = true;
                Add(obj);
            }
        }

        public override bool EvaluatesToBoolean()
        {
            var meth = Callable as Method;
            return (meth != null && meth.ReturnType is Types.BasicType && ((Types.BasicType)meth.ReturnType).Name == "System.Boolean");
        }

        private bool hasObject = false;

        public bool VirtualCall { get; private set; }

        public Callable Callable { get; private set; }

        public Expression Object
        {
            get
            {
                if (hasObject)
                {
                    return (Expression)children[children.Count - 1];
                }
                else
                {
                    return null;
                }
            }
        }

        public IEnumerable<Expression> Parameters
        {
            get
            {
                int delta = hasObject ? 1 : 0;
                for (int i = 0; i < children.Count - delta; ++i)
                {
                    yield return (Expression)children[i];
                }
            }
        }

        protected override ASTNode ShallowClone(object context)
        {
            var cl = new Call(this.Callable, this.VirtualCall);
            cl.hasObject = this.hasObject;
            return cl;
        }

        public override void Write(TextWriter tw)
        {
            if (Object != null)
            {
                Object.Write(tw);
                tw.Write('.');
            }
            if (Callable is Method)
            {
                tw.Write(((Method)Callable).Name);
            }
            else if (Callable is Constructor)
            {
                tw.Write(".ctor");
            }
            else
            {
                throw new NotSupportedException();
            }
            tw.Write('(');
            bool first = true;
            foreach (var par in Parameters)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    tw.Write(", ");
                }
                par.Write(tw);
            }
            tw.Write(')');
        }

        protected override int CalculateHashcode()
        {
            return Callable.GetHashCode();
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return Callable.Equals(((Call)o).Callable) && hasObject == ((Call)o).hasObject;
        }
    }
}
