﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Expressions
{
    public class This : Expression
    {
        public This() { }

        public override void Add(ASTNode child)
        {
            throw new Exception("This node doesn't accept any AST children");
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new This();
        }

        public override void Write(TextWriter tw)
        {
            tw.Write("this");
        }

        protected override int CalculateHashcode()
        {
            return 9;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }
    }
}
