﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Expressions
{
    public abstract class Expression : ASTNode
    {
        public virtual bool EvaluatesToBoolean()
        {
            return false;
        }
    }
}
