﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Expressions
{
    public class Index : Expression
    {
        public Index(Expression container = null, Expression value = null)
        {
            if (container != null && value == null)
            {
                throw new Exception("Cannot initialize with only a rhs");
            }
            if (container != null)
            {
                Add(container);
            }
            if (value != null)
            {
                Add(value);
            }
        }

        public Expression Container
        {
            get { return base.children.Count >= 1 ? base.children[0] as Expression : null; }
        }

        public Expression Value
        {
            get { return base.children.Count >= 2 ? base.children[1] as Expression : null; }
        }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren >= 2)
            {
                throw new Exception("Only 0-2 children are allowed");
            }
            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Index();
        }

        public override void Write(TextWriter tw)
        {
            Container.Write(tw);
            tw.Write('[');
            Value.Write(tw);
            tw.Write(']');
        }

        protected override int CalculateHashcode()
        {
            return 5;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }
    }
}
