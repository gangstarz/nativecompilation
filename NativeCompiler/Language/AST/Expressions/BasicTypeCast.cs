﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Expressions
{
    public class BasicTypeCast : Expression
    {
        public BasicTypeCast(IType targetType, Expression child = null)
        {
            this.Target = targetType;
            if (child != null)
            {
                Add(child);
            }
        }

        public override bool EvaluatesToBoolean()
        {
            return Target is BasicType && ((BasicType)Target).Name == "System.Boolean";
        }

        public IType Target;

        public Expression Child
        {
            get
            {
                return base.children.FirstOrDefault() as Expression;
            }
        }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren == 1)
            {
                throw new Exception("Only 0 or 1 child is allowed");
            }
            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new BasicTypeCast(Target);
        }

        public override void Write(TextWriter tw)
        {
            tw.Write('(');
            Target.Write(tw);
            tw.Write(')');
            Child.Write(tw);
        }

        protected override int CalculateHashcode()
        {
            return Target.GetHashCode() + 3;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return ((BasicTypeCast)o).Target.Equals(Target);
        }
    }
}
