﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST.Expressions
{
    public class Variable : Expression
    {
        public Variable(VariableDeclaration decl) 
        {
            this.Declaration = decl;
        }

        public VariableDeclaration Declaration { get; private set; }

        public override bool EvaluatesToBoolean()
        {
            return Declaration.Type is Types.BasicType && ((Types.BasicType)Declaration.Type).Name == "System.Boolean";
        }

        public override void Add(ASTNode child)
        {
            throw new Exception("Variable node doesn't accept any AST children");
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Variable(Declaration);
        }

        public override void Write(TextWriter tw)
        {
            tw.Write(Declaration.Name);
        }

        protected override int CalculateHashcode()
        {
            return 9;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }
    }
}
