﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Expressions
{
    public class PrefixUnaryOperator : Expression, IOperator
    {
        internal PrefixUnaryOperator(string oper, int precedence, Expression child = null)
        {
            this.oper = oper;
            this.precedence = precedence;
            if (child != null)
            {
                Add(child);
            }
        }

        private string oper;
        private int precedence;

        public string Operator { get { return oper; } }
        public int OperatorPrecedence { get { return precedence; } }

        public override bool EvaluatesToBoolean()
        {
            return Operator == "!";
        }

        public Expression Child
        {
            get
            {
                return base.children.FirstOrDefault() as Expression;
            }
        }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren == 1)
            {
                throw new Exception("Only 0 or 1 child is allowed");
            }
            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new PrefixUnaryOperator(oper, precedence);
        }

        public override void Write(TextWriter tw)
        {
            tw.Write(oper);

            if (Operators.RequiresParentheses(this, Child))
            {
                tw.Write('(');
                Child.Write(tw);
                tw.Write(')');
            }
            else
            {
                Child.Write(tw);
            }
        }

        protected override int CalculateHashcode()
        {
            return oper.GetHashCode();
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return oper.Equals(((PrefixUnaryOperator)o).oper);
        }
    }
}
