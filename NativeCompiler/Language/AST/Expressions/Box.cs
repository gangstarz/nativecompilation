﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Expressions
{
    public class Box : Expression
    {
        public Box(IType type, Expression target = null)
        {
            this.Type = type;
            if (target != null)
            {
                Add(target);
            }
        }

        public IType Type { get; private set; }

        public Expression Target
        {
            get
            {
                return base.children.FirstOrDefault() as Expression;
            }
        }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren == 1)
            {
                throw new Exception("Only 0 or 1 child is allowed");
            }
            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Box(Type);
        }

        public override void Write(TextWriter tw)
        {
            Target.Write(tw);
        }

        protected override int CalculateHashcode()
        {
            return Type.GetHashCode() + 4;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return ((Box)o).Type.Equals(Type);
        }
    }
}
