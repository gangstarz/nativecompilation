﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Expressions
{
    public class DynamicCast : Expression
    {
        public DynamicCast(IType type, bool throwOnInvalidCast, Expression target = null)
        {
            this.Type = type;
            this.ThrowOnInvalidCast = throwOnInvalidCast;
            if (target != null)
            {
                Add(target);
            }
        }

        public IType Type { get; private set; }

        public bool ThrowOnInvalidCast { get; private set; } // if false, returns 'null'.

        public Expression Target
        {
            get
            {
                return base.children.FirstOrDefault() as Expression;
            }
        }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren == 1)
            {
                throw new Exception("Only 0 or 1 child is allowed");
            }
            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new DynamicCast(Type, ThrowOnInvalidCast);
        }

        public override void Write(TextWriter tw)
        {
            if (ThrowOnInvalidCast)
            {
                tw.Write('(');
                Type.Write(tw);
                tw.Write(')');
                Target.Write(tw);
            }
            else
            {
                Target.Write(tw);
                tw.Write(" as ");
                Type.Write(tw);
            }
        }

        protected override int CalculateHashcode()
        {
            return Type.GetHashCode() + 4;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return ((DynamicCast)o).Type.Equals(Type) && ((DynamicCast)o).ThrowOnInvalidCast == ThrowOnInvalidCast;
        }
    }
}
