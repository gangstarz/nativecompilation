﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Expressions
{
    public class InitializeArray : Expression
    {
        public InitializeArray(IType elementType, IEnumerable<Expression> initializerList = null)
        {
            this.ElementType = elementType;

            if (initializerList != null)
            {
                foreach (var item in initializerList)
                {
                    Add(item);
                }
            }
        }

        public IType ElementType { get; private set; }

        public IEnumerable<Expression> InitializerList
        {
            get
            {
                return Children.Cast<Expression>();
            }
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new InitializeArray(ElementType);
        }

        public override void Write(TextWriter tw)
        {
            tw.Write("new ");
            ElementType.Write(tw);
            tw.Write('[');
            tw.Write("] {");
            for (int i = 0; i < children.Count; ++i)
            {
                if (i != 0) { tw.Write(", "); }
                children[i].Write(tw);
            }
            tw.Write("}");
        }

        protected override int CalculateHashcode()
        {
            return ElementType.GetHashCode();
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return ElementType.Equals(((InitializeArray)o).ElementType);
        }
    }
}
