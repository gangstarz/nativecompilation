﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Expressions
{
    public class StoreIndirect : Expression
    {
        public StoreIndirect(Expression destination = null, Expression value = null)
        {
            if (value != null && destination == null)
            {
                throw new Exception("Cannot initialize with only a value");
            }
            if (destination != null)
            {
                Add(destination);
            }
            if (value != null)
            {
                Add(value);
            }
        }

        public override bool EvaluatesToBoolean()
        {
            return Value.EvaluatesToBoolean();
        }

        public Expression Destination
        {
            get { return base.children.Count >= 1 ? base.children[0] as Expression : null; }
        }

        public Expression Value
        {
            get { return base.children.Count >= 2 ? base.children[1] as Expression : null; }
        }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren >= 2)
            {
                throw new Exception("Only 0-2 children are allowed");
            }
            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new StoreIndirect();
        }

        public override void Write(TextWriter tw)
        {
            Destination.Write(tw);
            tw.Write(" = ");
            Value.Write(tw);
        }

        protected override int CalculateHashcode()
        {
            return 9;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }
    }
}
