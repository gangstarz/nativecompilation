﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Expressions
{
    public class NewArray : Expression
    {
        public NewArray(IType elementType, Expression length = null)
        {
            this.ElementType = elementType;
            if (length != null)
            {
                Add(length);
            }
        }

        public IType ElementType { get; private set; }
        
        public Expression Length
        {
            get
            {
                return base.children.FirstOrDefault() as Expression;
            }
        }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren == 1)
            {
                throw new Exception("Only 0 or 1 child is allowed");
            }
            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new NewArray(ElementType);
        }

        public override void Write(TextWriter tw)
        {
            tw.Write("new ");
            ElementType.Write(tw);
            tw.Write('[');
            Length.Write(tw);
            tw.Write("]");
        }

        protected override int CalculateHashcode()
        {
            return ElementType.GetHashCode();
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return ElementType.Equals(((NewArray)o).ElementType);
        }
    }
}
