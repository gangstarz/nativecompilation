﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Expressions
{
    public class BinaryOperator : Expression, IOperator
    {
        internal BinaryOperator(string oper, int precedence, Expression lhs = null, Expression rhs = null)
        {
            this.oper = oper;
            this.precedence = precedence;
            if (rhs != null && lhs == null)
            {
                throw new Exception("Cannot initialize with only a rhs");
            }
            if (lhs != null)
            {
                Add(lhs);
            }
            if (rhs != null)
            {
                Add(rhs);
            }
        }

        private string oper;
        private int precedence;

        public override bool EvaluatesToBoolean()
        {
            switch (oper)
            {
                case ">":
                case "<":
                case ">=":
                case "<=":
                case "==":
                case "!=":
                case "&&":
                case "||":
                    return true;
                
                default:
                    return false;
            }
        }

        public Expression Lhs
        {
            get { return base.children.Count >= 1 ? base.children[0] as Expression : null; }
        }

        public Expression Rhs
        {
            get { return base.children.Count >= 2 ? base.children[1] as Expression : null; }
        }

        public string Operator { get { return oper; } }
        public int OperatorPrecedence { get { return precedence; } }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren >= 2)
            {
                throw new Exception("Only 0-2 children are allowed");
            }
            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new BinaryOperator(oper, precedence);
        }

        public override void Write(TextWriter tw)
        {
            if (Operators.RequiresParentheses(this, Lhs))
            {
                tw.Write('(');
                Lhs.Write(tw);
                tw.Write(')');
            }
            else
            {
                Lhs.Write(tw);
            }

            tw.Write(' ');
            tw.Write(oper);
            tw.Write(' ');

            if (Operators.RequiresParentheses(this, Rhs))
            {
                tw.Write('(');
                Rhs.Write(tw);
                tw.Write(')');
            }
            else
            {
                Rhs.Write(tw);
            }
        }

        protected override int CalculateHashcode()
        {
            return oper.GetHashCode() + 4;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return ((BinaryOperator)o).oper.Equals(oper);
        }
    }
}
