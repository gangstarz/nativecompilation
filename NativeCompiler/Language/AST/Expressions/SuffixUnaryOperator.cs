﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Expressions
{
    public class SuffixUnaryOperator : Expression, IOperator
    {
        internal SuffixUnaryOperator(string oper, int precedence, Expression child = null)
        {
            this.oper = oper;
            this.precedence = precedence;
            if (child != null)
            {
                Add(child);
            }
        }

        private string oper;
        private int precedence;

        public string Operator { get { return oper; } }
        public int OperatorPrecedence { get { return precedence; } }

        public Expression Child
        {
            get
            {
                return base.children.FirstOrDefault() as Expression;
            }
        }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren == 1)
            {
                throw new Exception("Only 0 or 1 child is allowed");
            }
            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new SuffixUnaryOperator(oper, precedence);
        }

        public override void Write(TextWriter tw)
        {
            if (Operators.RequiresParentheses(this, Child))
            {
                tw.Write('(');
                Child.Write(tw);
                tw.Write(')');
            }
            else
            {
                Child.Write(tw);
            }
            tw.Write(oper);
        }

        protected override int CalculateHashcode()
        {
            return oper.GetHashCode();
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return oper.Equals(((SuffixUnaryOperator)o).oper);
        }
    }
}
