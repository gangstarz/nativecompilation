﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Expressions
{
    public static class Operators
    {
        public static bool RequiresParentheses(this IOperator op, Expression child)
        {
            return (child is IOperator && ((IOperator)child).OperatorPrecedence >= op.OperatorPrecedence);
        }

        public static SuffixUnaryOperator PostIncrement(Expression child)
        {
            return new SuffixUnaryOperator("++", 2, child);
        }

        public static SuffixUnaryOperator PostDecrement(Expression child)
        {
            return new SuffixUnaryOperator("--", 2, child);
        }

        public static PrefixUnaryOperator PreIncrement(Expression child)
        {
            return new PrefixUnaryOperator("++", 3, child);
        }
        
        public static PrefixUnaryOperator PreDecrement(Expression child)
        {
            return new PrefixUnaryOperator("--", 3, child);
        }

        public static PrefixUnaryOperator LogicalNot(Expression child)
        {
            return new PrefixUnaryOperator("!", 3, child);
        }

        public static PrefixUnaryOperator BitwiseNot(Expression child)
        {
            return new PrefixUnaryOperator("~", 3, child);
        }

        public static PrefixUnaryOperator Dereference(Expression child)
        {
            return new PrefixUnaryOperator("*", 3, child);
        }

        public static PrefixUnaryOperator AddressOf(Expression child)
        {
            return new PrefixUnaryOperator("&", 3, child);
        }

        public static PrefixUnaryOperator Negate(Expression child)
        {
            return new PrefixUnaryOperator("-", 3, child);
        }

        public static BinaryOperator Multiplication(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("*", 5, lhs, rhs);
        }

        public static BinaryOperator Division(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("/", 5, lhs, rhs);
        }

        public static BinaryOperator Modulo(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("%", 5, lhs, rhs);
        }

        public static BinaryOperator Addition(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("+", 6, lhs, rhs);
        }

        public static BinaryOperator Subtraction(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("-", 6, lhs, rhs);
        }

        public static BinaryOperator ShiftLeft(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("<<", 7, lhs, rhs);
        }

        public static BinaryOperator ShiftRight(Expression lhs, Expression rhs)
        {
            return new BinaryOperator(">>", 7, lhs, rhs);
        }

        public static BinaryOperator Smaller(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("<", 8, lhs, rhs);
        }

        public static BinaryOperator SmallerEquals(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("<=", 8, lhs, rhs);
        }

        public static BinaryOperator Larger(Expression lhs, Expression rhs)
        {
            return new BinaryOperator(">", 8, lhs, rhs);
        }

        public static BinaryOperator LargerEquals(Expression lhs, Expression rhs)
        {
            return new BinaryOperator(">=", 8, lhs, rhs);
        }

        public static BinaryOperator Equals(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("==", 9, lhs, rhs);
        }

        public static BinaryOperator NotEquals(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("!=", 9, lhs, rhs);
        }

        public static BinaryOperator And(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("&", 10, lhs, rhs);
        }

        public static BinaryOperator Xor(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("^", 11, lhs, rhs);
        }

        public static BinaryOperator Or(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("|", 12, lhs, rhs);
        }

        public static BinaryOperator LogicalAnd(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("&&", 13, lhs, rhs);
        }

        public static BinaryOperator LogicalOr(Expression lhs, Expression rhs)
        {
            return new BinaryOperator("||", 14, lhs, rhs);
        }
    }
}
