﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Expressions
{
    public class NewObject : Expression
    {
        public NewObject(Constructor ctor, IEnumerable<Expression> parameters = null)
        {
            this.Constructor = ctor;
            if (parameters != null)
            {
                foreach (var par in parameters)
                {
                    Add(par);
                }
            }
        }

        public Constructor Constructor { get; private set; }

        public IEnumerable<Expression> Parameters
        {
            get { return base.children.Cast<Expression>(); }
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new NewObject(Constructor);
        }

        public override void Write(TextWriter tw)
        {
            tw.Write("new ");
            tw.Write(Constructor.Container.Name);
            tw.Write('(');
            for (int i = 0; i < children.Count; ++i)
            {
                if (i != 0)
                {
                    tw.Write(", ");
                }
                children[i].Write(tw);
            }
            tw.Write(')');
        }

        protected override int CalculateHashcode()
        {
            return Constructor.GetHashCode();
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return Constructor.Equals(((NewObject)o).Constructor);
        }
    }
}
