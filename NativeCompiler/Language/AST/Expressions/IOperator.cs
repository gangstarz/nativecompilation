﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Expressions
{
    public interface IOperator
    {
        int OperatorPrecedence
        {
            get;
        }
    }
}
