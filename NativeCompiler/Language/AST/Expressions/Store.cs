﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST.Expressions
{
    public class Store : Expression
    {
        public Store(Expression obj = null, VariableDeclaration target = null, Expression value = null)
        {
            if (value == null && obj != null)
            {
                throw new Exception("Cannot initialize in this expression order");
            }

            this.Target = target;

            if (value != null)
            {
                Add(value);
            }
            if (obj != null)
            {
                Add(obj);
            }
        }

        public override bool EvaluatesToBoolean()
        {
            return Value.EvaluatesToBoolean();
        }

        public VariableDeclaration Target { get; private set; }
        public Expression Value { get { return (children.Count >= 1) ? children[0] as Expression : null; } }
        public Expression Object { get { return (children.Count >= 2) ? children[1] as Expression : null; } }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren == 2)
            {
                throw new Exception("Only 0..1 children are allowed");
            }
            if (!(child is Expression))
            {
                throw new Exception("Child must be an expression in 'store' expression.");
            }

            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Store();
        }

        public override void Write(TextWriter tw)
        {
            if (Object != null)
            {
                Object.Write(tw);
                tw.Write(".");
            }
            Target.Write(tw);
            tw.Write(" = ");
            Value.Write(tw);
        }

        protected override int CalculateHashcode()
        {
            return 8;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }
    }
}
