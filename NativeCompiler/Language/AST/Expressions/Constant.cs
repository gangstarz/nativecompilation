﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Expressions
{
    public class Constant<T> : Expression
    {
        public Constant(T value)
        {
            this.Value = value;
        }

        public override bool EvaluatesToBoolean()
        {
            return typeof(T) == typeof(bool);
        }

        public T Value { get; set; }

        public override void Add(ASTNode child)
        {
            throw new Exception("No children are allowed");
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new Constant<T>(Value);
        }

        protected override int CalculateHashcode()
        {
            return Value.GetHashCode();
        }

        protected override bool NodeEquals(ASTNode o)
        {
            if (Value == null) { return ((Constant<T>)o).Value == null; }
            else { return ((Constant<T>)o).Value.Equals(Value); }
        }

        public override int GetHashCode()
        {
            return Value == null ? 0 : Value.GetHashCode();
        }

        public override void Write(TextWriter tw)
        {
            if (Value == null)
            {
                tw.Write("null");
            }
            else
            {
                tw.Write(Value.ToString());
            }
        }
    }
}
