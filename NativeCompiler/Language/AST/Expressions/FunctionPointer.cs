﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST.Expressions
{
    public class FunctionPointer : Expression
    {
        public FunctionPointer(Callable target, bool virtualCall)
        {
            this.Target = target;
            this.Virtual = virtualCall;
        }

        public Callable Target { get; private set; }
        public bool Virtual { get; private set; }

        public override void Add(ASTNode child)
        {
            throw new Exception("No child is allowed");
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new FunctionPointer(Target, Virtual);
        }

        public override void Write(TextWriter tw)
        {
            if (Target is Method)
            {
                tw.Write(((Method)Target).Name);
            }
            else
            {
                tw.Write(".ctor");
            }
        }

        protected override int CalculateHashcode()
        {
            return Target.GetHashCode() + 10 + (Virtual ? 1 : 0);
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return ((FunctionPointer)o).Target.Equals(Target) && ((FunctionPointer)o).Virtual.Equals(Virtual);
        }
    }
}
