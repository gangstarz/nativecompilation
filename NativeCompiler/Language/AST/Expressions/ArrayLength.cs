﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST.Expressions
{
    public class ArrayLength : Expression
    {
        public ArrayLength(Expression child = null)
        {
            if (child != null)
            {
                this.Add(child);
            }
        }

        public Expression Child
        {
            get
            {
                return base.children.FirstOrDefault() as Expression;
            }
        }

        public override void Add(ASTNode child)
        {
            if (this.NumberOfChildren == 1)
            {
                throw new Exception("Only 0 or 1 child is allowed");
            }
            base.Add(child);
        }

        protected override ASTNode ShallowClone(object context)
        {
            return new ArrayLength();
        }

        public override void Write(TextWriter tw)
        {
            Child.Write(tw);
            tw.Write(".Length");
        }

        protected override int CalculateHashcode()
        {
            return 2;
        }

        protected override bool NodeEquals(ASTNode o)
        {
            return true;
        }
    }
}
