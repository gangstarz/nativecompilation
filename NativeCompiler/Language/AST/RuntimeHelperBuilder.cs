﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions.IL;
using NativeCompiler.Language.AST.Expressions;
using NativeCompiler.Language.AST.Structure;
using NativeCompiler.Language.AST.Types;

namespace NativeCompiler.Language.AST
{
    /// <summary>
    /// These are basically the cheat codes of the compiler, most use unmanaged code for the execution.
    /// </summary>
    public class RuntimeHelperBuilder
    {
        public static Expressions.Expression RewriteRuntimeHelpers(Instructions.IL.Call item, ExpressionBuilder expressionBuilder)
        {
            switch (item.Method.Name)
            {
                case "Equals":
                    // object.ReferenceEquals(obj, obj)
                    return Operators.Equals(item.Parameters[0].Visit(expressionBuilder), item.Parameters[1].Visit(expressionBuilder));

                case "GetHashCode":
                    // returns the address of the object (not necessarily unique)
                    return new Expressions.BasicTypeCast(new BasicType("System.Int32"), item.Parameters[0].Visit(expressionBuilder));

                case "InitializeArray":
                    // Used a lot for things like 'int[] foo = new[]{1,2,3};' - iff foo is an array of value types.
                    //
                    // This works quite ugly - it basically copies bytes from someType to the array, which works because the array elements are value types...
                    var array = ((Variable)item.Parameters[0].Visit(expressionBuilder)).Declaration;
                    var elementType = ((ArrayType)array.Type).Element;
                    var dataField = (FieldInfo)((LdToken)item.Parameters[1]).Operand;
                    return CreateInitializeArray(array, elementType, dataField);

                // These methods are present, but don't seem to be used.
                case "ExecuteCodeWithGuaranteedCleanup":
                case "GetObjectValue":
                case "PrepareConstrainedRegions":
                case "PrepareConstrainedRegionsNoOP":
                case "PrepareContractedDelegate":
                case "PrepareDelegate":
                case "PrepareMethod":
                case "ProbeForSufficientStack":
                case "RunClassConstructor": // used in InteropServices.ManagedActivationFactory
                case "RunModuleConstructor":
                    throw new NotImplementedException();

                default:
                    throw new NotSupportedException("Unknown method.");
            }
        }

        private static Expression CreateInitializeArray(VariableDeclaration array, IType elementType, FieldInfo dataField)
        {
            int idx = dataField.FieldType.Name.IndexOf('=');

            if (idx <= 0)
            {
                throw new NotSupportedException("Incorrect initializer field.");
            }

            string name = dataField.FieldType.Name.Substring(0, idx);
            int size;

            // Some final checks before we do the ugly magic:
            if (elementType.StorageType == MemoryType.ValueType && name == "__StaticArrayInitTypeSize" && int.TryParse(dataField.FieldType.Name.Substring(idx + 1), out size) && size > 0)
            {
                byte[] data = new byte[size];
                RuntimeHelpers.InitializeArray(data, dataField.FieldHandle);

                // Map data on initializer.
                if (elementType is BasicType)
                {
                    Expression[] obj;

                    // This is where it gets tricky. Assignment of bool/byte/sbyte/short/ushort/char/int/uint all happen though int.
                    switch (((BasicType)elementType).Name)
                    {
                        case "System.Boolean": obj = Map<bool>(data, sizeof(bool)).Select((a) => new Constant<int>((a) ? 1 : 0)).ToArray(); break;
                        case "System.Byte": obj = Map<byte>(data, sizeof(byte)).Select((a) => new Constant<int>(a)).ToArray(); break;
                        case "System.SByte": obj = Map<sbyte>(data, sizeof(sbyte)).Select((a) => new Constant<int>(a)).ToArray(); break;
                        case "System.Int16": obj = Map<short>(data, sizeof(short)).Select((a) => new Constant<int>(a)).ToArray(); break;
                        case "System.UInt16": obj = Map<ushort>(data, sizeof(ushort)).Select((a) => new Constant<int>(a)).ToArray(); break;
                        case "System.Char": obj = Map<char>(data, sizeof(char)).Select((a) => new Constant<int>(a)).ToArray(); break;
                        case "System.Int32": obj = Map<int>(data, sizeof(int)).Select((a) => new Constant<int>(a)).ToArray(); break;
                        case "System.UInt32": obj = Map<uint>(data, sizeof(uint)).Select((a) => new Constant<int>((int)a)).ToArray(); break;
                        case "System.Int64": obj = Map<long>(data, sizeof(long)).Select((a) => new Constant<long>(a)).ToArray(); break;
                        case "System.UInt64": obj = Map<ulong>(data, sizeof(ulong)).Select((a) => new Constant<long>((long)a)).ToArray(); break;
                        case "System.Single": obj = Map<float>(data, sizeof(float)).Select((a) => new Constant<float>(a)).ToArray(); break;
                        case "System.Double": obj = Map<double>(data, sizeof(double)).Select((a) => new Constant<double>(a)).ToArray(); break;
                        //case "System.Decimal": obj = Map<decimal>(data, sizeof(decimal)).Select((a) => new NewObject(a)).ToArray(); break; -> not sure.
                        default:
                            throw new NotSupportedException("Basic type is not supported for array initialization: " + ((BasicType)elementType).Name);
                    }

                    return new Store(null, array, new InitializeArray(elementType, obj));
                }
                else
                {
                    // Depends on the type... { MyValueType(field1, field2, ...), MyValueType(field3, ..) }?
                    // TODO FIXME.
                    throw new NotImplementedException();
                }
            }
            else
            {
                throw new NotSupportedException("Incorrect initializer field.");
            }
        }

        private static T[] Map<T>(byte[] data, int elementSize) where T : struct
        {
            int nrElem = data.Length / elementSize;
            T[] ndata = new T[nrElem];
            Buffer.BlockCopy(data, 0, ndata, 0, data.Length);
            return ndata;
        }
    }
}
