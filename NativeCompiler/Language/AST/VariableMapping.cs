﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Decompiler.Blocks;
using NativeCompiler.Language.AST.Structure;

namespace NativeCompiler.Language.AST
{
    public class VariableMap
    {
        public VariableMap(Dictionary<FieldInfo, Field> fields, 
                           bool hasThis, List<VariableDeclaration> arguments,
                           Dictionary<int, VariableDeclaration> locals, Dictionary<int, VariableDeclaration> temporaries,
                           Dictionary<CatchBlock, VariableDeclaration> catchObjects)
        {
            this.fields = fields;
            this.hasThis = hasThis;
            this.arguments = arguments;
            this.locals = locals;
            this.temporaries = temporaries;
            this.catchObjects = catchObjects;
        }

        public Dictionary<FieldInfo, Field> fields { get; private set; }
        public bool hasThis { get; private set; }
        public List<VariableDeclaration> arguments { get; private set; }
        public Dictionary<int, VariableDeclaration> locals { get; private set; }
        public Dictionary<int, VariableDeclaration> temporaries { get; private set; }
        public Dictionary<CatchBlock, VariableDeclaration> catchObjects { get; private set; }

        public VariableDeclaration GetDeclaration(CatchBlock block)
        {
            return catchObjects[block];
        }
    }

    public class VariableMapping
    {
        private Dictionary<FieldInfo, Field> allFields = new Dictionary<FieldInfo, Field>();
        private Dictionary<Callable, VariableMap> variableMapping = new Dictionary<Callable, VariableMap>(ObjectEquality<Callable>.Instance);

        public VariableMap GetMap(Callable c)
        {
            return variableMapping[c];
        }

        public Field GetField(FieldInfo field)
        {
            return this.allFields[field];
        }

        public void Add(Callable meth, VariableMap variableMap)
        {
            variableMapping.Add(meth, variableMap);
            foreach (var item in variableMap.fields)
            {
                allFields[item.Key] = item.Value;
            }
        }
    }
}
