﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Language.AST.Statements;

namespace NativeCompiler.Language.AST
{
    public static class LabelFinderExtensions
    {
        public static Tuple<List<Goto>, List<Label>> FindLabels(this Statement scope)
        {
            List<Goto> gotos = new List<Goto>(scope.ChildrenOf<Goto>());
            List<Label> labels = new List<Label>(scope.ChildrenOf<Label>().Where((a) => a.UsedBy.Count > 0));
            return new Tuple<List<Goto>, List<Label>>(gotos, labels);
        }
    }
}
