﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Language.AST
{
    public class ASTTree
    {
        public ASTTree()
        {
            root = new ASTRootNode();
            AddToHierarchy(root);
        }

        private ASTNode root;
        public ASTNode Root { get { return root; } }

        private Dictionary<Type, HashSet<ASTNode>> dict = new Dictionary<Type, HashSet<ASTNode>>();

        internal void AddToHierarchy(ASTNode node)
        {
            HashSet<ASTNode> hs;
            if (!dict.TryGetValue(node.GetType(), out hs))
            {
                hs = new HashSet<ASTNode>(ObjectEquality<ASTNode>.Instance);
                dict.Add(node.GetType(), hs);
            }
            hs.Add(node);
        }

        internal void RemoveFromHierarchy(ASTNode node)
        {
            dict[node.GetType()].Remove(node);
        }

        internal ASTNode[] FindNodesOfType(Type t)
        {
            HashSet<ASTNode> nodes;
            if (dict.TryGetValue(t, out nodes))
            {
                return nodes.ToArray();
            }
            else
            {
                return new ASTNode[0];
            }
        }
    }
}
