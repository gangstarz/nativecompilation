﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using NativeCompiler.Decompiler.Blocks;

namespace NativeCompiler.Language.CSharp
{
    public class CSEmitter : ILanguageEmitter
    {
        static CSEmitter()
        {
            standardTypes.Add(typeof(object), "object");
            standardTypes.Add(typeof(byte), "byte");
            standardTypes.Add(typeof(sbyte), "sbyte");
            standardTypes.Add(typeof(ushort), "ushort");
            standardTypes.Add(typeof(short), "short");
            standardTypes.Add(typeof(uint), "uint");
            standardTypes.Add(typeof(int), "int");
            standardTypes.Add(typeof(ulong), "ulong");
            standardTypes.Add(typeof(long), "long");
            standardTypes.Add(typeof(float), "float");
            standardTypes.Add(typeof(double), "double");
            standardTypes.Add(typeof(decimal), "decimal");
            standardTypes.Add(typeof(string), "string");
        }

        private static Dictionary<Type, string> standardTypes = new Dictionary<Type, string>();

        public void Write(string filename, MethodBase main, IEnumerable<Dependencies.DependentType> types)
        {
            using (var fw = new System.IO.StreamWriter(filename))
            {
                Write(fw, types);
                fw.Close();
            }
        }

        public void Write(System.IO.TextWriter writer, IEnumerable<Dependencies.DependentType> types)
        {
            // Write include's. We assume 'System' libraries are there
            writer.WriteLine("using System;");
            writer.WriteLine();

            foreach (var type in types)
            {
                var t = type.Type;
                if (ShouldEmit(t))
                {
                    WriteClass(writer, type);
                }
            }
        }

        private void WriteClass(System.IO.TextWriter writer, Dependencies.DependentType type)
        {
            var t = type.Type;

            bool isInterface = t.IsInterface;

            // For simplicity, we make everything public.
            writer.Write("public ");

            // Write proper OO designation.
            if (t.IsInterface)
            {
                writer.Write("interface");
            }
            else if (t.IsAbstract)
            {
                writer.Write("abstract class");
            }
            else if (t.IsEnum)
            {
                writer.Write("enum");
            }
            else if (t.IsValueType)
            {
                writer.Write("struct");
            }
            else
            {
                writer.Write("class");
            }

            writer.Write(" {0}", CreateTypeName(t));

            // base class
            bool first = true;
            if (t.BaseType != typeof(object) && t.BaseType != null)
            {
                var baseName = GetNewTypeName(t.BaseType);
                writer.Write(" : ");
                writer.Write(baseName);
                first = false;
            }

            // base interfaces
            foreach (var intf in t.GetInterfaces())
            {
                if (first)
                {
                    writer.Write(" : ");
                    first = false;
                }
                else
                {
                    writer.Write(", ");
                }

                var intfName = GetNewTypeName(intf);
                writer.Write(intfName);
            }
            writer.WriteLine("{");

            // Fields

            // Store field mapping for the method body emitting
            Dictionary<MemberInfo, string> classMembers = new Dictionary<MemberInfo, string>();

            foreach (var field in type.Fields)
            {
                // For simplicity, make them public.
                writer.WriteLine("public {0} {1};", GetNewTypeName(field.FieldType), field.Name);
                classMembers.Add(field, field.Name);
            }

            // Events
            foreach (var evt in type.Events)
            {
                writer.WriteLine("public event {0} {1};", GetNewTypeName(evt.EventHandlerType), evt.Name);
                classMembers.Add(evt, evt.Name);
            }

            // Methods are also members, which can be referenced as 'function pointer'.
            foreach (var meth in type.Methods)
            {
                classMembers.Add(meth.Key, meth.Key.Name);
            }

            // Methods
            foreach (var meth in type.Methods)
            {
                // Method declaration:
                if (meth.Key is ConstructorInfo) 
                {
                    writer.Write("public {0}(", GetNewTypeName(type.Type));
                }
                else if (meth.Key is MethodInfo)
                {
                    MethodInfo mi = (MethodInfo)meth.Key;

                    var baseDef = mi.GetBaseDefinition();
                    bool isOverride = (baseDef != null && baseDef != mi);
                    bool isStatic = mi.IsStatic;

                    string returnType = mi.ReturnType == typeof(void)?"void":GetNewTypeName(mi.ReturnType);


                    if (isInterface)
                    {
                        writer.Write("{0} {1}(", returnType, mi.Name);
                    }
                    else if (meth.Value.MethodBlock == null)
                    {
                        writer.Write("public abstract {0} {1}(", returnType, mi.Name);
                    }
                    else 
                    {
                        writer.Write("public {0} {1} {2}(", isStatic ? "static" : (isOverride ? "override" : "virtual"), returnType, mi.Name);
                    }
                }
                else
                {
                    throw new NotSupportedException("Unknown method type: " + meth.Key.GetType().FriendlyName());
                }

                // parameters
                
                // Store parameters; we need them in the method body emitting
                List<Tuple<Type, string>> parameters = new List<Tuple<Type, string>>();

                if (!meth.Key.IsStatic && (meth.Key is MethodInfo || meth.Key is ConstructorInfo))
                {
                    parameters.Add(new Tuple<Type, string>(type.Type, "this"));
                }

                first = true;
                foreach (var par in meth.Key.GetParameters())
                {
                    var parTypeName = GetNewTypeName(par.ParameterType);

                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        writer.Write(", ");
                    }

                    writer.Write("{0} {1}", parTypeName, par.Name);

                    parameters.Add(new Tuple<Type, string>(par.ParameterType, par.Name));
                }

                // method body
                if (meth.Value.MethodBlock == null)
                {
                    // abstract or interface
                    writer.WriteLine(");");
                }
                else
                {
                    if (meth.Value.Method is ConstructorInfo)
                    {
                        writer.Write(")");
                    }
                    else
                    {
                        writer.WriteLine(") {");
                    }

                    // Output method body
                    CSBodyRewriter rewriter = new CSBodyRewriter(writer, this, meth.Key, classMembers, parameters);

                    meth.Value.MethodBlock.Rewrite(rewriter);

                    writer.WriteLine("}");
                }
            }

            writer.WriteLine("}");
        }

        #region Helpers for constructing names
        public string GetNewTypeName(Type t)
        {
            string name;
            if (standardTypes.TryGetValue(t, out name))
            {
                return name;
            }
            return ShouldEmit(t) ? CreateTypeName(t) : CreateFQN(t);
        }

        // Creates fully qualified names for types we won't be emitting.
        private string CreateFQN(Type type)
        {
            StringBuilder fqn = new StringBuilder();
            WriteFQN(type, fqn);
            return fqn.ToString();
        }

        private void WriteFQN(Type type, StringBuilder fqn)
        {
            fqn.Append(type.Namespace);
            fqn.Append('.');
            
            string tname = type.Name;
            int idx = tname.IndexOf('`');
            if (idx >= 0) { tname = tname.Substring(0, idx); }
            
            fqn.Append(tname);

            if (type.IsGenericType)
            {
                fqn.Append('<');
                bool first = true;
                foreach (var t in type.GetGenericArguments()) 
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        fqn.Append(',');
                    }

                    var innerName = GetNewTypeName(t);
                    fqn.Append(innerName);
                    //WriteFQN(t, fqn);
                }
                fqn.Append('>');
            }
        }

        private bool ShouldEmit(Type t)
        {
            if (t.Namespace.StartsWith("System"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private Dictionary<Type, string> knownNames = new Dictionary<Type, string>();
        private HashSet<string> knownTypeNames = new HashSet<string>();

        private string CreateTypeName(Type t)
        {
            string name;
            if (knownNames.TryGetValue(t, out name))
            {
                return name;
            }

            string tname = t.Name;
            int idx = tname.IndexOf('`');
            if (idx >= 0) { tname = tname.Substring(0, idx); }

            StringBuilder sb = new StringBuilder();
            sb.Append("C_");
            sb.Append(tname);
            if (t.IsGenericType)
            {
                foreach (var par in t.GetGenericArguments())
                {
                    sb.Append('_');
                    sb.Append(par.Name);
                }
            }

            string prefix = sb.ToString();
            int suffix = 0;

            // Ensure the name is unique
            while (knownTypeNames.Contains(prefix + ((suffix == 0) ? "" : suffix.ToString())))
            {
                ++suffix;
            }

            // Register for future use
            string typeName = prefix + ((suffix == 0) ? "" : suffix.ToString());
            knownTypeNames.Add(typeName);
            knownNames.Add(t, typeName);
            return typeName;
        }
        #endregion
    }
}
