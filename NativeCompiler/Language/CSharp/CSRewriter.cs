﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Decompiler.Blocks;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.IL;
using NativeCompiler.Instructions.Meta;

namespace NativeCompiler.Language.CSharp
{
    public class CSBodyRewriter : ExpressionRewriter
    {
        public CSBodyRewriter(TextWriter writer, CSEmitter emitter, MethodBase method, Dictionary<MemberInfo, string> classMembers, List<Tuple<Type, string>> parameters)
        {
            this.writer = writer;
            this.emitter = emitter;
            this.method = method;
            this.body = method.GetMethodBody();
            this.classMembers = classMembers;
            this.arguments = parameters;
            this.isConstructorBaseCall = method is ConstructorInfo;

            this.Init(method);
        }

        private CSEmitter emitter;
        private MethodBase method;
        private MethodBody body;
        private TextWriter writer;
        private bool isConstructorBaseCall = false;

        private List<Tuple<Type, string>> arguments; // starg_* / ldarg_*
        private List<Tuple<Type, string>> variables; // stloc_* / ldloc_*
        private Dictionary<MemberInfo, string> classMembers; // ldfld/stfld/ldsfld/stsfld

        private void Init(MethodBase method)
        {
            // Create a list of argument names
            this.arguments = new List<Tuple<Type, string>>();

            if (!method.IsStatic && method is MethodInfo)
            {
                arguments.Add(new Tuple<Type, string>(method.DeclaringType, "this"));
            }
            else if (!method.IsStatic && method is ConstructorInfo)
            {
                arguments.Add(new Tuple<Type, string>(method.DeclaringType, "base"));
            }

            foreach (var par in method.GetParameters())
            {
                arguments.Add(new Tuple<Type, string>(par.ParameterType, par.Name));
            }

            // Create a list of the variables
            this.variables = new List<Tuple<Type, string>>();
            Dictionary<string, int> dict = new Dictionary<string, int>();
            foreach (var var in body.LocalVariables)
            {
                var name = "var_" + (variables.Count + 1).ToString();

                this.variables.Add(new Tuple<Type, string>(var.LocalType, name));

                writer.WriteLine("{0} {1};", emitter.GetNewTypeName(var.LocalType), name);
            }
        }

        public override Block Rewrite(ExceptionBlock block)
        {
            bool hasFault = block.SubBlocks.Any((a) => a is FaultBlock);
            int numberFilters = block.SubBlocks.Count((a) => a is FilterBlock);

            if (isConstructorBaseCall)
            {
                writer.WriteLine("{");
                isConstructorBaseCall = false;
            }

            if (hasFault)
            {
                writer.WriteLine("bool __fault = true;");
                writer.WriteLine("try {");
            }
            for (int i = 0; i < numberFilters; ++i)
            {
                writer.WriteLine("try {");
            }
            foreach (var sb in block.SubBlocks)
            {
                if (sb is TryBlock)
                {
                    writer.WriteLine("try {");
                    sb.Rewrite(this);
                    if (hasFault)
                    {
                        writer.WriteLine("__fault = false;");
                    }
                    writer.WriteLine("}");
                }
                else if (sb is CatchBlock)
                {
                    CatchBlock cb = (CatchBlock)sb;
                    writer.Write("catch ");
                    if (cb.Type != typeof(object))
                    {
                        writer.Write("({0} ex)", this.emitter.GetNewTypeName(cb.Type));
                    }
                    writer.WriteLine("{");

                    sb.Rewrite(this);

                    writer.WriteLine("}");
                }
                else if (sb is FinallyBlock)
                {
                    writer.WriteLine("finally {");

                    sb.Rewrite(this);

                    writer.WriteLine("}");
                }
                else if (sb is FaultBlock)
                {
                    writer.WriteLine("}");
                    writer.WriteLine("finally {");
                    writer.WriteLine("if (__fault) {");

                    sb.Rewrite(this);

                    writer.WriteLine("}");
                    writer.WriteLine("}");
                }
                else if (sb is FilterBlock)
                {
                    writer.WriteLine("}");
                    writer.WriteLine("catch (Exception ex) {");

                    // TODO FIXME: filter
                    sb.Rewrite(this);

                    writer.WriteLine("}");
                }
            }
            return block;
        }

        public override Block Rewrite(ILBlock block)
        {
            if (!isConstructorBaseCall)
            {
                writer.WriteLine("label{0}:", block.MinOffset.ToString("x4"));
            }

            bool isBaseCall = false;
            foreach (var instr in block.StackOperations)
            {
                if (isConstructorBaseCall)
                {
                    // If we start with 'base(...) it's part of the constructor initialization.
                    if ((instr is Call || instr is CallVirt) && (instr.Children.FirstOrDefault() is LdArgN) && ((LdArgN)instr.Children.First()).N == 0)
                    {
                        writer.Write(" : ");
                        isBaseCall = true;
                    }

                    if (!isBaseCall)
                    {
                        writer.WriteLine("{");
                        writer.WriteLine("label{0}:", block.MinOffset.ToString("x4"));
                        isConstructorBaseCall = isBaseCall = false;
                    }
                }

                var op = instr.Rewrite(this);

                if (op is Code || op is Operator)
                {
                    op.Write(writer);
                    if (isBaseCall)
                    {
                        writer.WriteLine(" {");
                    }
                    else
                    {
                        writer.WriteLine(";");
                    }
                }
                else if (!(op is Nop))
                {
                    writer.Write("// ");
                    op.Write(writer);
                    writer.WriteLine();
                }

                isBaseCall = isConstructorBaseCall = false;
            }

            return block;
        }

        public override IStackOperation Rewrite(StoreAdditionalStack item)
        {
            return new Code(item, @"tmp_" + item.Id + " = {0}");
        }

        public override IStackOperation Rewrite(LoadInitialStack item)
        {
            return new Code(item, @"tmp_" + item.Id);
        }

        public override IStackOperation Rewrite(Beq item)
        {
            return new Code(item, @"if ({0} == {1}) {{ goto label" + item.TargetOffset.ToString("x4") + "; }}");
        }

        public override IStackOperation Rewrite(Bge item)
        {
            return new Code(item, @"if ({0} >= {1}) {{ goto label" + item.TargetOffset.ToString("x4") + "; }}");
        }

        public override IStackOperation Rewrite(Bgt item)
        {
            return new Code(item, @"if ({0} > {1}) {{ goto label" + item.TargetOffset.ToString("x4") + "; }}");
        }

        public override IStackOperation Rewrite(Ble item)
        {
            return new Code(item, @"if ({0} <= {1}) {{ goto label" + item.TargetOffset.ToString("x4") + "; }}");
        }

        public override IStackOperation Rewrite(Blt item)
        {
            return new Code(item, @"if ({0} < {1}) {{ goto label" + item.TargetOffset.ToString("x4") + "; }}");
        }

        public override IStackOperation Rewrite(Bne_Un item)
        {
            return new Code(item, @"if ({0} != {1}) {{ goto label" + item.TargetOffset.ToString("x4") + "; }}");
        }

        public override IStackOperation Rewrite(Br item)
        {
            return new Code(item, @"goto label" + item.TargetOffset.ToString("x4") + "");
        }

        public override IStackOperation Rewrite(BrFalse item)
        {
            return new Code(item, @"if (!({0})) {{ goto label" + item.TargetOffset.ToString("x4") + "; }}");
        }

        public override IStackOperation Rewrite(BrTrue item)
        {
            return new Code(item, @"if ({0}) {{ goto label" + item.TargetOffset.ToString("x4") + "; }}");
        }

        public override IStackOperation Rewrite(Box item)
        {
            return item.child;
        }

        public override IStackOperation Rewrite(Unbox item)
        {
            return item.child;
        }

        public override IStackOperation Rewrite(Unbox_Any item)
        {
            return item.child;
        }

        public override IStackOperation Rewrite(CastClass item)
        {
            return new Code(item, "(" + emitter.GetNewTypeName(item.Type) + "){0}");
        }

        public override IStackOperation Rewrite(Ceq item)
        {
            return new Code(item, "{0} == {1}");
        }

        public override IStackOperation Rewrite(Cgt item)
        {
            return new Code(item, "{0} > {1}");
        }

        public override IStackOperation Rewrite(Clt item)
        {
            return new Code(item, "{0} < {1}");
        }

        public override IStackOperation Rewrite(CkFinite item)
        {
            return new Code(item, "!double.IsInfinity({0})");
        }

        public override IStackOperation Rewrite(ConvI1 item)
        {
            return new Code(item, "(sbyte){0}");
        }

        public override IStackOperation Rewrite(ConvI2 item)
        {
            return new Code(item, "(short){0}");
        }

        public override IStackOperation Rewrite(ConvI4 item)
        {
            return new Code(item, "(int){0}");
        }

        public override IStackOperation Rewrite(ConvI8 item)
        {
            return new Code(item, "(long){0}");
        }

        public override IStackOperation Rewrite(ConvR item)
        {
            return new Code(item, "(float){0}");
        }

        public override IStackOperation Rewrite(ConvR4 item)
        {
            return new Code(item, "(float){0}");
        }

        public override IStackOperation Rewrite(ConvR8 item)
        {
            return new Code(item, "(double){0}");
        }

        public override IStackOperation Rewrite(ConvU1 item)
        {
            return new Code(item, "(byte){0}");
        }

        public override IStackOperation Rewrite(ConvU2 item)
        {
            return new Code(item, "(ushort){0}");
        }

        public override IStackOperation Rewrite(ConvU4 item)
        {
            return new Code(item, "(uint){0}");
        }

        public override IStackOperation Rewrite(ConvU8 item)
        {
            return new Code(item, "(ulong){0}");
        }

        public override IStackOperation Rewrite(LdArgA item)
        {
            return new Code(item, this.arguments[item.N].Item2);
        }

        public override IStackOperation Rewrite(LdArgN item)
        {
            return new Code(item, this.arguments[item.N].Item2);
        }

        public override IStackOperation Rewrite(LdcI4N item)
        {
            if (item.N < 0x10000)
            {
                return new Code(item, item.N.ToString());
            }
            else
            {
                return new Code(item, "0x" + item.N.ToString("x"));
            }
        }

        public override IStackOperation Rewrite(LdcI8 item)
        {
            if (item.N < 0x10000)
            {
                return new Code(item, item.N.ToString());
            }
            else
            {
                return new Code(item, "0x" + item.N.ToString("x"));
            }
        }

        public override IStackOperation Rewrite(LdFld item)
        {
            return new Code(item, this.classMembers[item.Field]);
        }

        public override IStackOperation Rewrite(LdFldA item)
        {
            return new Code(item, this.classMembers[item.Field]);
        }

        public override IStackOperation Rewrite(LdLen item)
        {
            return new Code(item, "{0}.Length");
        }

        public override IStackOperation Rewrite(LdLocA item)
        {
            return new Code(item, variables[item.N].Item2, false);
        }

        public override IStackOperation Rewrite(LdLocN item)
        {
            return new Code(item, variables[item.N].Item2, false);
        }

        public override IStackOperation Rewrite(LdNull item)
        {
            return new Code(item, "null");
        }

        public override IStackOperation Rewrite(LdSFld item)
        {
            return new Code(item, item.Field.Name);
        }

        public override IStackOperation Rewrite(LdSFldA item)
        {
            return new Code(item, item.Field.Name);
        }

        public override IStackOperation Rewrite(LdStr item)
        {
            return new Code(item, "@\"" + item.Data.Replace("\"", "\"\"") + "\"", false);
        }

        public override IStackOperation Rewrite(NewArr item)
        {
            return new Code(item, "new " + item.Type.FriendlyName() + "[{0}]");
        }

        public override IStackOperation Rewrite(Rethrow item)
        {
            return new Code(item, "throw");
        }

        public override IStackOperation Rewrite(SizeOf item)
        {
            return new Code(item, "sizeof(" + item.Type.FriendlyName() + ")", false);
        }

        public override IStackOperation Rewrite(StArg item)
        {
            return new Code(item, this.arguments[item.N] + " = {0}");
        }

        public override IStackOperation Rewrite(StElemI1 item)
        {
            return new Code(item, "{0}[{1}] = {2}");
        }

        public override IStackOperation Rewrite(StElemI2 item)
        {
            return new Code(item, "{0}[{1}] = {2}");
        }

        public override IStackOperation Rewrite(StElemI4 item)
        {
            return new Code(item, "{0}[{1}] = {2}");
        }

        public override IStackOperation Rewrite(StElemI8 item)
        {
            return new Code(item, "{0}[{1}] = {2}");
        }

        public override IStackOperation Rewrite(StElemR4 item)
        {
            return new Code(item, "{0}[{1}] = {2}");
        }

        public override IStackOperation Rewrite(StElemR8 item)
        {
            return new Code(item, "{0}[{1}] = {2}");
        }

        public override IStackOperation Rewrite(StElemRef item)
        {
            return new Code(item, "{0}[{1}] = {2}");
        }

        public override IStackOperation Rewrite(StFld item)
        {
            return new Code(item, item.Field.Name + " = {0}");
        }

        public override IStackOperation Rewrite(StLocN item)
        {
            return new Code(item, variables[item.N].Item2 + " = {0}");
        }

        public override IStackOperation Rewrite(StSFld item)
        {
            return new Code(item, item.Field.Name + " = {0}");
        }

        public override IStackOperation Rewrite(Throw item)
        {
            return new Code(item, "throw {0}");
        }

        public override IStackOperation Rewrite(Pop item)
        {
            return item.child;
        }

        public override IStackOperation Rewrite(Ret item)
        {
            if (item.ReturnType == typeof(void))
            {
                return new Code(item, "return");
            }
            else
            {
                return new Code(item, "return {0}");
            }
        }

        public override IStackOperation Rewrite(Volatile item)
        {
            return new Code(item, "volatile");
        }

        // newobj, call, callvirt

        public override IStackOperation Rewrite(Call item)
        {
            return CreateCall(item, item.Method, item.MethodObject, item.Parameters);
        }

        public override IStackOperation Rewrite(CallVirt item)
        {
            return CreateCall(item, item.Method, item.MethodObject, item.Parameters);
        }

        public override IStackOperation Rewrite(NewObj item)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("new ");
            sb.Append(item.Constructor.DeclaringType.FriendlyName());
            sb.Append("(");

            var list = item.Parameters;
            for (int i = 0; i < list.Count; ++i)
            {
                if (i != 0)
                {
                    sb.Append(", ");
                }
                sb.Append('{');
                sb.Append(i);
                sb.Append('}');
            }
            sb.Append(')');

            return new Code(item, sb.ToString());
        }

        private Code CreateCall(IStackOperation item, MethodBase methodBase, IStackOperation stackOperation, List<IStackOperation> list)
        {

            StringBuilder sb = new StringBuilder();

            // if it's a static call, we can simply put the FQN here.
            if (stackOperation == null)
            {
                sb.Append(this.emitter.GetNewTypeName(methodBase.DeclaringType));
                sb.Append('.');
                sb.Append(methodBase.Name);
            }
            else if (methodBase is ConstructorInfo)
            {
                sb.Append("{0}");
            }
            else
            {
                sb.Append("{" + list.Count + "}.");
                sb.Append(methodBase.Name);
            }

            int n = list.Count - 1;

            sb.Append("(");
            for (int i = 0; i < list.Count; ++i, --n)
            {
                if (i != 0)
                {
                    sb.Append(", ");
                }
                sb.Append('{');
                sb.Append(n);
                sb.Append('}');
            }
            sb.Append(')');

            return new Code(item, sb.ToString());
        }
    }
}
