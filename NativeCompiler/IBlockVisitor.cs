﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Decompiler.Blocks;

namespace NativeCompiler
{
    public interface IBlockVisitor<T>
    {
        T Visit(MethodBlock block);
        T Visit(ExceptionBlock block);
        T Visit(StatementBlock block);
        T Visit(IfThenElseBlock block);
        T Visit(DoWhileBlock block);
        T Visit(WhileBlock block);
        T Visit(ForBlock block);
        T Visit(TryBlock block);
        T Visit(CatchBlock block);
        T Visit(FinallyBlock block);
        T Visit(FaultBlock block);
        T Visit(CodeBlock block);
        T Visit(ILBlock block);
        T Visit(FilterBlock block);
    }
}
