﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.IL;
using NativeCompiler.Decompiler;
using NativeCompiler.Instructions.Meta;
using NativeCompiler.Decompiler.Blocks;

namespace NativeCompiler
{
    public abstract class StackRewriterBase
    {
        public virtual Block Rewrite(MethodBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(ExceptionBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(StatementBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(IfThenElseBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(DoWhileBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(WhileBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(ForBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(TryBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(CatchBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(FinallyBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(FaultBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(CodeBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }
        public virtual Block Rewrite(ILBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }

            var list = new List<IStackOperation>();
            foreach (var instr in block.StackOperations)
            {
                list.Add(instr.Rewrite(this));
            }
            block.StackOperations = list;

            return block;
        }
        public virtual Block Rewrite(FilterBlock block)
        {
            foreach (var bl in block.SubBlocks)
            {
                bl.Rewrite(this);
            }
            return block;
        }

        public virtual IStackOperation Rewrite(And item) { return item; }
        public virtual IStackOperation Rewrite(Add item) { return item; }
        public virtual IStackOperation Rewrite(Beq item) { return item; }
        public virtual IStackOperation Rewrite(Bge item) { return item; }
        public virtual IStackOperation Rewrite(Bgt item) { return item; }
        public virtual IStackOperation Rewrite(Ble item) { return item; }
        public virtual IStackOperation Rewrite(Blt item) { return item; }
        public virtual IStackOperation Rewrite(Bne_Un item) { return item; }
        public virtual IStackOperation Rewrite(Box item) { return item; }
        public virtual IStackOperation Rewrite(Br item) { return item; }
        public virtual IStackOperation Rewrite(BrFalse item) { return item; }
        public virtual IStackOperation Rewrite(BrTrue item) { return item; }
        public virtual IStackOperation Rewrite(Call item) { return item; }
        public virtual IStackOperation Rewrite(CallVirt item) { return item; }
        public virtual IStackOperation Rewrite(CastClass item) { return item; }
        public virtual IStackOperation Rewrite(Ceq item) { return item; }
        public virtual IStackOperation Rewrite(Cgt item) { return item; }
        public virtual IStackOperation Rewrite(CkFinite item) { return item; }
        public virtual IStackOperation Rewrite(Clt item) { return item; }
        public virtual IStackOperation Rewrite(ConvI item) { return item; }
        public virtual IStackOperation Rewrite(ConvI1 item) { return item; }
        public virtual IStackOperation Rewrite(ConvI2 item) { return item; }
        public virtual IStackOperation Rewrite(ConvI4 item) { return item; }
        public virtual IStackOperation Rewrite(ConvI8 item) { return item; }
        public virtual IStackOperation Rewrite(ConvR item) { return item; }
        public virtual IStackOperation Rewrite(ConvR4 item) { return item; }
        public virtual IStackOperation Rewrite(ConvR8 item) { return item; }
        public virtual IStackOperation Rewrite(ConvU item) { return item; }
        public virtual IStackOperation Rewrite(ConvU1 item) { return item; }
        public virtual IStackOperation Rewrite(ConvU2 item) { return item; }
        public virtual IStackOperation Rewrite(ConvU4 item) { return item; }
        public virtual IStackOperation Rewrite(ConvU8 item) { return item; }
        public virtual IStackOperation Rewrite(Div item) { return item; }
        public virtual IStackOperation Rewrite(Dup item) { return item; }
        public virtual IStackOperation Rewrite(EndFinally item) { return item; }
        public virtual IStackOperation Rewrite(EndFilter item) { return item; }
        public virtual IStackOperation Rewrite(InitObj item) { return item; }
        public virtual IStackOperation Rewrite(IsInst item) { return item; }
        public virtual IStackOperation Rewrite(LdArgN item) { return item; }
        public virtual IStackOperation Rewrite(LdArgA item) { return item; }
        public virtual IStackOperation Rewrite(LdcI4N item) { return item; }
        public virtual IStackOperation Rewrite(LdcI8 item) { return item; }
        public virtual IStackOperation Rewrite(LdcR4 item) { return item; }
        public virtual IStackOperation Rewrite(LdcR8 item) { return item; }
        public virtual IStackOperation Rewrite(LdElem item) { return item; }
        public virtual IStackOperation Rewrite(LdElemA item) { return item; }
        public virtual IStackOperation Rewrite(LdElemI item) { return item; }
        public virtual IStackOperation Rewrite(LdElemI1 item) { return item; }
        public virtual IStackOperation Rewrite(LdElemI2 item) { return item; }
        public virtual IStackOperation Rewrite(LdElemI4 item) { return item; }
        public virtual IStackOperation Rewrite(LdElemI8 item) { return item; }
        public virtual IStackOperation Rewrite(LdElemR4 item) { return item; }
        public virtual IStackOperation Rewrite(LdElemR8 item) { return item; }
        public virtual IStackOperation Rewrite(LdElemRef item) { return item; }
        public virtual IStackOperation Rewrite(LdElemU1 item) { return item; }
        public virtual IStackOperation Rewrite(LdElemU2 item) { return item; }
        public virtual IStackOperation Rewrite(LdElemU4 item) { return item; }
        public virtual IStackOperation Rewrite(LdFld item) { return item; }
        public virtual IStackOperation Rewrite(LdFldA item) { return item; }
        public virtual IStackOperation Rewrite(LdFtn item) { return item; }
        public virtual IStackOperation Rewrite(LdIndI item) { return item; }
        public virtual IStackOperation Rewrite(LdIndI1 item) { return item; }
        public virtual IStackOperation Rewrite(LdIndI2 item) { return item; }
        public virtual IStackOperation Rewrite(LdIndI4 item) { return item; }
        public virtual IStackOperation Rewrite(LdIndI8 item) { return item; }
        public virtual IStackOperation Rewrite(LdIndR4 item) { return item; }
        public virtual IStackOperation Rewrite(LdIndR8 item) { return item; }
        public virtual IStackOperation Rewrite(LdIndRef item) { return item; }
        public virtual IStackOperation Rewrite(LdIndU1 item) { return item; }
        public virtual IStackOperation Rewrite(LdIndU2 item) { return item; }
        public virtual IStackOperation Rewrite(LdIndU4 item) { return item; }
        public virtual IStackOperation Rewrite(LdLen item) { return item; }
        public virtual IStackOperation Rewrite(LdLocN item) { return item; }
        public virtual IStackOperation Rewrite(LdLocA item) { return item; }
        public virtual IStackOperation Rewrite(LdNull item) { return item; }
        public virtual IStackOperation Rewrite(LdObj item) { return item; }
        public virtual IStackOperation Rewrite(LdSFld item) { return item; }
        public virtual IStackOperation Rewrite(LdSFldA item) { return item; }
        public virtual IStackOperation Rewrite(LdStr item) { return item; }
        public virtual IStackOperation Rewrite(LdToken item) { return item; }
        public virtual IStackOperation Rewrite(LdVirtFtn item) { return item; }
        public virtual IStackOperation Rewrite(LocAlloc item) { return item; }
        public virtual IStackOperation Rewrite(Leave item) { return item; }
        public virtual IStackOperation Rewrite(MkRefAny item) { return item; }
        public virtual IStackOperation Rewrite(Mul item) { return item; }
        public virtual IStackOperation Rewrite(Neg item) { return item; }
        public virtual IStackOperation Rewrite(NewArr item) { return item; }
        public virtual IStackOperation Rewrite(NewObj item) { return item; }
        public virtual IStackOperation Rewrite(Nop item) { return item; }
        public virtual IStackOperation Rewrite(Not item) { return item; }
        public virtual IStackOperation Rewrite(Or item) { return item; }
        public virtual IStackOperation Rewrite(Pop item) { return item; }
        public virtual IStackOperation Rewrite(ReadOnly item) { return item; }
        public virtual IStackOperation Rewrite(Rem item) { return item; }
        public virtual IStackOperation Rewrite(Ret item) { return item; }
        public virtual IStackOperation Rewrite(Rethrow item) { return item; }
        public virtual IStackOperation Rewrite(Shl item) { return item; }
        public virtual IStackOperation Rewrite(Shr item) { return item; }
        public virtual IStackOperation Rewrite(ShrUn item) { return item; }
        public virtual IStackOperation Rewrite(SizeOf item) { return item; }
        public virtual IStackOperation Rewrite(StArg item) { return item; }
        public virtual IStackOperation Rewrite(StElem item) { return item; }
        public virtual IStackOperation Rewrite(StElemI item) { return item; }
        public virtual IStackOperation Rewrite(StElemI1 item) { return item; }
        public virtual IStackOperation Rewrite(StElemI2 item) { return item; }
        public virtual IStackOperation Rewrite(StElemI4 item) { return item; }
        public virtual IStackOperation Rewrite(StElemI8 item) { return item; }
        public virtual IStackOperation Rewrite(StElemR4 item) { return item; }
        public virtual IStackOperation Rewrite(StElemR8 item) { return item; }
        public virtual IStackOperation Rewrite(StElemRef item) { return item; }
        public virtual IStackOperation Rewrite(StFld item) { return item; }
        public virtual IStackOperation Rewrite(StIndI item) { return item; }
        public virtual IStackOperation Rewrite(StIndI1 item) { return item; }
        public virtual IStackOperation Rewrite(StIndI2 item) { return item; }
        public virtual IStackOperation Rewrite(StIndI4 item) { return item; }
        public virtual IStackOperation Rewrite(StIndI8 item) { return item; }
        public virtual IStackOperation Rewrite(StIndR4 item) { return item; }
        public virtual IStackOperation Rewrite(StIndR8 item) { return item; }
        public virtual IStackOperation Rewrite(StIndRef item) { return item; }
        public virtual IStackOperation Rewrite(StLocN item) { return item; }
        public virtual IStackOperation Rewrite(StObj item) { return item; }
        public virtual IStackOperation Rewrite(StSFld item) { return item; }
        public virtual IStackOperation Rewrite(Sub item) { return item; }
        public virtual IStackOperation Rewrite(Switch item) { return item; }
        public virtual IStackOperation Rewrite(TailCall item) { return item; }
        public virtual IStackOperation Rewrite(Throw item) { return item; }
        public virtual IStackOperation Rewrite(Unaligned item) { return item; }
        public virtual IStackOperation Rewrite(Unbox item) { return item; }
        public virtual IStackOperation Rewrite(Unbox_Any item) { return item; }
        public virtual IStackOperation Rewrite(Volatile item) { return item; }
        public virtual IStackOperation Rewrite(Xor item) { return item; }

        public virtual IStackOperation Rewrite(StackStub item) { return item; }

        public virtual IStackOperation Rewrite(LoadExceptionObject item) { return item; }
        public virtual IStackOperation Rewrite(LoadInitialStack item) { return item; }
        public virtual IStackOperation Rewrite(StoreAdditionalStack item) { return item; }
    }
}
