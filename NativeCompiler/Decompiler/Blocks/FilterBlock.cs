﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Decompiler.Blocks
{
    public class FilterBlock : Block
    {
        public FilterBlock(Block parent) : base(parent) { }

        public int HandlerOffset { get; set; }
        public int HandlerLength { get; set; }
        public int FilterOffset { get; set; }
        public int FilterLength { get; set; }

        protected override Block CreateCodeBlock(ILInstruction[] instructions, int startOffset, int endOffset)
        {
            return new CodeBlock(this, instructions, startOffset, endOffset, typeof(object));
        }

        public override void DumpAnalyzed()
        {
            Console.WriteLine("filter {");
            base.DumpAnalyzed();
            Console.WriteLine("}");
        }

        public override Block Rewrite(StackRewriterBase rewriter)
        {
            return rewriter.Rewrite(this);
        }

        public override T Visit<T>(IBlockVisitor<T> rewriter) { return rewriter.Visit(this); }
    }
}
