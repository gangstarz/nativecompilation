﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Decompiler.Blocks
{
    public class ILBlock : Block
    {
        public ILBlock(Block parent, ILInstruction[] instructions, int min, int max, Type additionalStackType = null)
            : base(parent)
        {
            this.Instructions = instructions;
            this.MinOffset = min;
            this.MaxOffset = max;
            this.additionalStackType = additionalStackType;

            InitialStackDepth = -1;
        }

        internal ILBlock(Block parent, ILBlock original, int firstOp, int lastOp)
            : base(parent)
        {
            this.RequiresLabel = original.RequiresLabel && firstOp == 0;
            this.InitialStackDepth = original.InitialStackDepth;
            this.StackOperations = new List<IStackOperation>(original.StackOperations.Skip(firstOp).Take(lastOp - firstOp));
            this.additionalStackType = firstOp == 0 ? original.additionalStackType : null;
            this.IntroducedVariables = original.IntroducedVariables;

            this.MinOffset = firstOp == 0 ? original.MinOffset : original.StackOperations[firstOp].Offset;
            this.MaxOffset = lastOp + 1 < original.StackOperations.Count ? original.StackOperations[lastOp + 1].Offset : original.MaxOffset;
        }

        public bool RequiresLabel = false;
        public ILInstruction[] Instructions = null;
        public int InitialStackDepth { get; set; }
        public List<IStackOperation> StackOperations;
        private Type additionalStackType; // The exception type can be pushed on the stack as 'extra'.

        public List<int> IntroducedVariables = new List<int>();

        public void Build(StackOperationBuilder builder, Dictionary<int, int> stackDepthAtOffset)
        {
            // Console.WriteLine("Building {0:x4} - {1:x4}", MinOffset, MaxOffset);

            int d = InitialStackDepth;
            if (additionalStackType != null)
            {
                d++;
            }

            this.StackOperations = new List<IStackOperation>();
            builder.BuildOperations(d, stackDepthAtOffset, Instructions, MaxOffset, StackOperations);

            // Check if this is a try -- if so, we can register the catch/finally/fault/filter clauses
            var cur = base.Parent;
            bool inTry = false;
            while (cur != null && !(cur is ExceptionBlock))
            {
                inTry |= (cur is TryBlock);
                cur = cur.Parent;
            }

            if (cur != null && inTry)
            {
                // Register the catch/finally/fault/filter clauses
                int depth = this.InitialStackDepth;
                foreach (var sb in cur.SubBlocks)
                {
                    var startTryDepth = ((ILBlock)this.Parent.SubBlocks[0]).InitialStackDepth;

                    int exceptionHandlerDepth;
                    if (stackDepthAtOffset.TryGetValue(sb.MinOffset, out exceptionHandlerDepth))
                    {
                        if (exceptionHandlerDepth != startTryDepth && startTryDepth != -1)
                        {
                            throw new Exception("Mismatch in exception handler stack depths.");
                        }
                    }
                    else
                    {
                        stackDepthAtOffset.Add(sb.MinOffset, exceptionHandlerDepth);
                    }
                }
            }


            this.Instructions = null; // to ensure these aren't used anymore.
        }

        private List<IStackOperation> IntroduceTemporaryVariables(Dictionary<int, ILBlock> jumps, ref int tmpVarCtr)
        {
            List<IStackOperation> ll = new List<IStackOperation>();

            // If this is the first block inside a 'catch' block, load the exception object.
            if (this.Parent != null && this.Parent.Parent != null &&
                object.ReferenceEquals(this, this.Parent.SubBlocks[0]) &&
                object.ReferenceEquals(this.Parent, this.Parent.Parent.SubBlocks[0]))
            {
                if (this.Parent.Parent is CatchBlock)
                {
                    int id = tmpVarCtr++;

                    var type = ((CatchBlock)this.Parent.Parent).Type;
                    ll.Add(new Instructions.Meta.LoadExceptionObject(type, id));
                }
                else if (this.Parent.Parent is FilterBlock)
                {
                    throw new NotImplementedException();
                }
            }

            // Load initial stack
            foreach (var id in IntroducedVariables)
            {
                ll.Add(new Instructions.Meta.LoadInitialStack(id));
            }

            bool lastIsUnconditional = false;
            foreach (var item in StackOperations)
            {
                if (item is Ret || item is Rethrow || item is Br || item is EndFilter || item is EndFinally || item is Leave || item is Throw)
                {
                    lastIsUnconditional = true;
                }
                else
                {
                    lastIsUnconditional = false;
                }

                // Rewrite 'dup' instruction, because it will give us a headache...
                if (item is Dup)
                {
                    int tmpVar = tmpVarCtr++;
                    ll.Add(new Instructions.Meta.StoreAdditionalStack(tmpVarCtr));
                    ll.Add(new Instructions.Meta.LoadInitialStack(tmpVarCtr));
                    ll.Add(new Instructions.Meta.LoadInitialStack(tmpVarCtr));
                }
                else if (item is IBranch)
                {
                    var branche = ((IBranch)item);

                    if (item is Leave)
                    {
                        ll.Add(item);
                    }
                    else if (item is Br)
                    {
                        if (item.StackDepth != 0)
                        {
                            // Store all variables
                            var vars = jumps[branche.TargetOffset].IntroducedVariables;
                            for (int i = vars.Count - 1; i >= 0; --i)
                            {
                                ll.Add(new Instructions.Meta.StoreAdditionalStack(vars[i]));
                            }
                        }
                        ll.Add(item);
                    }
                    else
                    {
                        int sd = item.StackDepth + item.DeltaStackDepth;
                        if (sd != 0)
                        {
                            // We have to ensure it's 0 again.

                            // Store the result
                            int tmpVar = tmpVarCtr++;
                            ll.Add(new Instructions.Meta.StoreAdditionalStack(tmpVarCtr));

                            // Store all variables
                            var vars = jumps[branche.TargetOffset].IntroducedVariables;
                            for (int i = vars.Count - 1; i >= 0; --i)
                            {
                                ll.Add(new Instructions.Meta.StoreAdditionalStack(vars[i]));
                            }

                            // Load the result again so we can branche
                            ll.Add(new Instructions.Meta.LoadInitialStack(tmpVarCtr));
                        }

                        // Branche if condition. Condition gets popped off the stack.
                        ll.Add(item);

                        if (sd != 0)
                        {
                            // Load back
                            foreach (var variable in jumps[branche.TargetOffset].IntroducedVariables)
                            {
                                ll.Add(new Instructions.Meta.LoadInitialStack(variable));
                            }
                        }
                    }
                }
                else
                {
                    ll.Add(item);
                }
            }

            if (!lastIsUnconditional)
            {
                // Prepare for next block
                var pushbackVars = jumps[MaxOffset].IntroducedVariables;
                for (int i = pushbackVars.Count - 1; i >= 0; --i)
                {
                    ll.Add(new Instructions.Meta.StoreAdditionalStack(pushbackVars[i]));
                }
            }
            return ll;
        }

        public void Execute(MethodBase method, Dictionary<int, ILBlock> ilBlocks, ref int tmpVarCtr)
        {
            var allInstructions = IntroduceTemporaryVariables(ilBlocks, ref tmpVarCtr);

            ILStack stack = new ILStack(method, 0);
            List<IStackOperation> statements = new List<IStackOperation>();

            if (method.ToString() == "Void .cctor()")
            {
                Debugger.Break();
            }

            foreach (var instr in allInstructions)
            {
                // Get rid of the NOP's here
                if (!(instr is Nop))
                {
                    instr.Execute(stack);

                    // TODO FIXME: if a 'call' or something else that throws is on the stack, it MUST be executed before this statement. 
                    // We MUST introduce temporary variables if this is the case.
                    // 
                    // For example: a = (b = foo()) + 2; ==> tmp = foo(), b = tmp; a = tmp + 2; 
                    // foo() might throw, so we must introduce a temporary.

                    if (instr is IStatementOperation || instr.IsImplicitStatement || stack.Empty) // Stack.Empty = expression statement.
                    {
                        statements.Add(instr);
                    }
                }
            }

            this.StackOperations = statements;
        }

        public override void DumpAnalyzed()
        {
            if (StackOperations != null)
            {
                Console.WriteLine("Start block {0:x4}", this.MinOffset);

                foreach (var instr in StackOperations)
                {
                    string spaces = instr.StackDepth < 0 ? "* " : (" |" + new string(' ', instr.StackDepth * 2));
                    string param = (instr is IBranch) ? ((IBranch)instr).TargetOffset.ToString("x4") : "";

                    Console.WriteLine("{0:x4} {1}{2} {3}", instr.Offset, spaces, instr.ToString(), param);
                }
            }
            else
            {
                Console.WriteLine("Start block {0:x4} -- not analyzed yet", this.MinOffset);
            }
        }

        public override Block Rewrite(StackRewriterBase rewriter)
        {
            return rewriter.Rewrite(this);
        }

        public override T Visit<T>(IBlockVisitor<T> rewriter) { return rewriter.Visit(this); }
    }
}
