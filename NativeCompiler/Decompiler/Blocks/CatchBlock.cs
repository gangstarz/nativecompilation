﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Decompiler.Blocks
{
    public class CatchBlock : Block
    {
        public CatchBlock(Block parent, Type t)
            : base(parent)
        {
            this.Type = t ?? typeof(object);
        }

        public Type Type { get; set; }

        protected override Block CreateCodeBlock(ILInstruction[] instructions, int startOffset, int endOffset)
        {
            return new CodeBlock(this, instructions, startOffset, endOffset, typeof(object));
        }

        public override Block Rewrite(StackRewriterBase rewriter)
        {
            return rewriter.Rewrite(this);
        }
        public override T Visit<T>(IBlockVisitor<T> rewriter) { return rewriter.Visit(this); }

    }
}
