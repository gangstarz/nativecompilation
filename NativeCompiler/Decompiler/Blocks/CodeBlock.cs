﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Decompiler.Blocks
{
    public class CodeBlock : Block
    {
        public CodeBlock(Block parent, ILInstruction[] instructions, int min, int max, Type additionalStackType = null) : base(parent)
        {
            this.Instructions = instructions;
            this.MinOffset = min;
            this.MaxOffset = max;
            this.additionalStackType = additionalStackType;

            InitialStackDepth = -1;
        }

        public ILInstruction[] Instructions = null;
        public int InitialStackDepth { get; set; }
        private Type additionalStackType; // The exception type can be pushed on the stack as 'extra'.

        public IEnumerable<int> CollectSplitPoints()
        {
            // Figure out the labels our unconditional branches point to. These are block starts.
            for (int i = 0; i < Instructions.Length; ++i)
            {
                if (StackOperationBuilder.IsBrancheToLabel(Instructions[i].Code))
                {
                    // operand is an offset (label):
                    yield return (int)Instructions[i].Operand;
                }
                else if (Instructions[i].Code == OpCodes.Switch)
                {
                    // operand is a jump table
                    foreach (var item in (int[])Instructions[i].Operand)
                    {
                        yield return item;
                    }
                }
            }
        }

        public IEnumerable<ILBlock> Split(HashSet<int> labelSplitPoints)
        {
            List<Block> subBlocks = new List<Block>();

            // Split instructions into multiple IL blocks.
            int prev = 0;
            for (int i = 0; i < Instructions.Length; ++i)
            {
                if ((i+1<Instructions.Length && labelSplitPoints.Contains(Instructions[i+1].Offset)) || 
                    StackOperationBuilder.IsUnconditionalBranche(Instructions[i].Code)) 
                {
                    var tmp = new ILInstruction[i - prev + 1];
                    Array.Copy(Instructions, prev, tmp, 0, tmp.Length);

                    int endOffset = (i + 1 < Instructions.Length) ? Instructions[i + 1].Offset : MaxOffset;

                    var cb = new ILBlock(this, tmp, Instructions[prev].Offset, endOffset, (prev == 0) ? additionalStackType : null);
                    cb.RequiresLabel = labelSplitPoints.Contains(cb.MinOffset);

                    subBlocks.Add(cb);

                    prev = i + 1;
                }
            }

            if (prev != Instructions.Length)
            {
                var tmp = new ILInstruction[Instructions.Length - prev];
                Array.Copy(Instructions, prev, tmp, 0, tmp.Length);

                var cb = new ILBlock(this, tmp, Instructions[prev].Offset, MaxOffset, (prev == 0) ? additionalStackType : null);
                cb.RequiresLabel = labelSplitPoints.Contains(cb.MinOffset);

                subBlocks.Add(cb);
            }

            this.Instructions = null; // Ensure we don't use the instructions anymore
            this.SubBlocks = subBlocks;

            return subBlocks.Cast<ILBlock>();
        }

        public override Block Rewrite(StackRewriterBase rewriter)
        {
            return rewriter.Rewrite(this);
        }
        public override T Visit<T>(IBlockVisitor<T> rewriter) { return rewriter.Visit(this); }

    }
}
