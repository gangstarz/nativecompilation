﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.Meta;

namespace NativeCompiler.Decompiler.Blocks
{
    public abstract class Block
    {
        public Block(Block parent)
        {
            this.Parent = parent;
        }

        public List<Block> SubBlocks = new List<Block>();
        public Block Parent { get; set; }

        public int MinOffset { get; set; }
        public int MaxOffset { get; set; }

        public int Length { get { return MaxOffset - MinOffset; } }

        public void Dump(int depth = 0)
        {
            string indent = new string(' ', depth * 2);
            Console.WriteLine("{0}{1:x4} ({2})", indent, MinOffset, this.GetType().Name);
            foreach (var item in SubBlocks)
            {
                item.Dump(depth + 1);
            }
            Console.WriteLine("{0}{1:x4}", indent, MaxOffset);
        }

        public virtual void DumpAnalyzed()
        {
            foreach (var block in SubBlocks)
            {
                block.DumpAnalyzed();
            }
        }

        public virtual void Fill(ILInstruction[] instructions, int maxlen, ref int ptr)
        {
            if (instructions[ptr].Offset != MinOffset)
            {
                throw new Exception();
            }

            List<Block> newSubBlocks = new List<Block>();

            int currentSubBlock = 0;
            int limit = currentSubBlock < SubBlocks.Count ? SubBlocks[currentSubBlock].MinOffset : MaxOffset;

            int prev = ptr;
            while (ptr < instructions.Length && instructions[ptr].Offset < MaxOffset)
            {
                if (instructions[ptr].Offset == limit)
                {
                    var localInstructions = new ILInstruction[ptr - prev];
                    Array.Copy(instructions, prev, localInstructions, 0, localInstructions.Length);
                    newSubBlocks.Add(CreateCodeBlock(localInstructions, instructions[prev].Offset, instructions[ptr].Offset));

                    SubBlocks[currentSubBlock].Fill(instructions, maxlen, ref ptr);
                    newSubBlocks.Add(SubBlocks[currentSubBlock]);
                    prev = ptr;

                    ++currentSubBlock;
                    limit = currentSubBlock < SubBlocks.Count ? SubBlocks[currentSubBlock].MinOffset : MaxOffset;
                }
                else
                {
                    ++ptr;
                }
            }

            if ((ptr < instructions.Length && instructions[ptr].Offset == limit) || maxlen == limit)
            {
                var localInstructions = new ILInstruction[ptr - prev];
                Array.Copy(instructions, prev, localInstructions, 0, localInstructions.Length);
                newSubBlocks.Add(CreateCodeBlock(localInstructions, instructions[prev].Offset, limit));

                if (currentSubBlock < SubBlocks.Count)
                {
                    // Shouldn't ever happen
                    Console.WriteLine("Filling empty trailing block?");

                    SubBlocks[currentSubBlock].Fill(instructions, maxlen, ref ptr);
                    newSubBlocks.Add(SubBlocks[currentSubBlock]);

                    ++currentSubBlock;
                }
            }

            if (currentSubBlock != SubBlocks.Count)
            {
                throw new Exception("Trailing sub blocks. This means we have incorrect min/max offset bounds.");
            }
            else if ((ptr < instructions.Length && instructions[ptr].Offset != MaxOffset) || (ptr == instructions.Length && maxlen != MaxOffset))
            {
                throw new Exception("Expected more instructions. This means we have incorrect min/max offset bounds.");
            }

            this.SubBlocks = newSubBlocks;
        }

        protected virtual Block CreateCodeBlock(ILInstruction[] instructions, int startOffset, int endOffset)
        {
            return new CodeBlock(this, instructions, startOffset, endOffset);
        }

        public void Analyze(MethodBase method, MethodBody body)
        {
            Console.WriteLine("Analyzing {0}", method.FriendlyName());
            // Collect all code blocks            

            List<CodeBlock> codeBlocks = new List<CodeBlock>();
            Collect(codeBlocks);

            if (codeBlocks[0].MinOffset != 0)
            {
                throw new Exception("The first code block must start at offset 0.");
            }

            // Split code blocks to the largest non-branching blocks of code

            HashSet<int> labelSplitPoints = new HashSet<int>();
            foreach (var codeBlock in codeBlocks)
            {
                foreach (var sp in codeBlock.CollectSplitPoints())
                {
                    labelSplitPoints.Add(sp);
                }
            }

            List<ILBlock> ilBlocks = new List<ILBlock>();
            foreach (var codeBlock in codeBlocks)
            {
                ilBlocks.AddRange(codeBlock.Split(labelSplitPoints));
            }

            if (ilBlocks[0].MinOffset != 0)
            {
                throw new Exception("The first IL block must start at offset 0.");
            }

            // Initialize stack builder:

            var builder = new StackOperationBuilder(method, body);

            Dictionary<int, int> stackAtDepth = new Dictionary<int, int>();
            stackAtDepth.Add(0, 0);

            List<ILBlock> processNow = new List<ILBlock>();
            HashSet<int> processed = new HashSet<int>();
            processNow.Add(ilBlocks[0]);
            ilBlocks[0].InitialStackDepth = 0;
            processed.Add(0);

            while (processNow.Count > 0)
            {
                foreach (var pn in processNow)
                {
                    pn.Build(builder, stackAtDepth);
                }

                processNow.Clear();

                // Create new processNow blocks
                for (int i = 0; i < ilBlocks.Count; ++i)
                {
                    int depth;
                    if (stackAtDepth.TryGetValue(ilBlocks[i].MinOffset, out depth) && processed.Add(i))
                    {
                        ilBlocks[i].InitialStackDepth = depth;
                        processNow.Add(ilBlocks[i]);
                    }
                }
            }

            if (processed.Count != ilBlocks.Count)
            {
                throw new Exception("Error: not all blocks were processed.");
            }
        }

        public IEnumerable<ILBlock> AllBlocks()
        {
            List<ILBlock> bl = new List<ILBlock>();
            Collect<ILBlock>(bl);
            return bl;
        }

        public IEnumerable<CatchBlock> AllCatchBlocks()
        {
            List<CatchBlock> bl = new List<CatchBlock>();
            Collect<CatchBlock>(bl);
            return bl;
        }

        private void Collect<T>(List<T> codeBlocks) where T : Block
        {
            foreach (var item in SubBlocks)
            {
                if (item is T)
                {
                    // Always a leaf node.
                    codeBlocks.Add((T)item);
                }
                else
                {
                    // Depth first.
                    item.Collect(codeBlocks);
                }
            }
        }

        public void ZeroAlign(MethodBase Method, MethodBody body)
        {
            int n = 0;

            // Gather all ILBlock instances.
            List<ILBlock> ilBlocks = new List<ILBlock>();
            Collect(ilBlocks);

            // Introduce variables where required
            foreach (var block in ilBlocks)
            {
                if (block.InitialStackDepth > 0)
                {
                    // Introduce variables to ensure it's 0. Ignore exception objects, they are fine.
                    for (int i = 0; i < block.InitialStackDepth; ++i)
                    {
                        block.IntroducedVariables.Add(n++);
                    }
                }
            }

            // Console.WriteLine("- Introduced {0} temporary variables for {1}", n, Method.FriendlyName());

            var dict = ilBlocks.ToDictionary((a)=>a.MinOffset, (a)=>a);

            Dictionary<int, Type> typeMap = new Dictionary<int, Type>();
            
            // Construct statements
            foreach (var block in ilBlocks)
            {
                block.Execute(Method, dict, ref n);
            }

            Console.WriteLine("- After statement reconstruction we have {0} temporary variables.", n, Method.FriendlyName());

            // Fix 'load initial stack'
            foreach (var block in ilBlocks)
            {
                foreach (var so in block.StackOperations)
                {
                    StoreAdditionalStack sat = so as StoreAdditionalStack;
                    if (sat!=null)
                    {
                        typeMap[sat.Id] = sat.ReturnType;
                    }

                    Stack<IStackOperation> sob = new Stack<IStackOperation>();
                    sob.Push(so);
                    while (sob.Count > 0)
                    {
                        var cur = sob.Pop();
                        foreach (var child in cur.Children)
                        {
                            sob.Push(child);
                        }
                        var eli = cur as LoadExceptionObject;
                        if (eli != null)
                        {
                            typeMap[eli.Id] = eli.Type;
                        }
                    }
                }
            }
            foreach (var block in ilBlocks)
            {
                foreach (var so in block.StackOperations)
                {
                    LoadInitialStack lis = so as LoadInitialStack;
                    if (lis != null)
                    {
                        Type type;
                        if (typeMap.TryGetValue(lis.Id, out type))
                        {
                            lis.ReturnType = type;
                        }
                        else
                        {
                            throw new NotSupportedException("Load type ID not found in store ID");
                        }
                    }
                }
            }
        }

        public abstract Block Rewrite(StackRewriterBase rewriter);
        public abstract T Visit<T>(IBlockVisitor<T> rewriter);
    }
}
