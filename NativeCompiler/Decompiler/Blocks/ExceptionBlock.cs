﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Decompiler.Blocks
{
    public class ExceptionBlock : Block
    {
        public ExceptionBlock(Block parent, int tryOffset, int tryLength) : base(parent)
        {
            this.TryOffset = tryOffset;
            this.MinOffset = tryOffset;
            this.TryLength = tryLength;
            this.MaxOffset = tryOffset + tryLength;

            this.SubBlocks.Add(new TryBlock(this)
            {
                MinOffset = tryOffset,
                MaxOffset = tryLength + tryOffset
            });
        }

        public int TryOffset { get; private set; }
        public int TryLength { get; private set; }

        public List<ExceptionHandlingClause> Clauses = new List<ExceptionHandlingClause>();

        public override void Fill(ILInstruction[] instructions, int maxlen, ref int ptr)
        {
            base.Fill(instructions, maxlen, ref ptr);
        }

        public Block Add(ExceptionHandlingClause cl)
        {
            this.MaxOffset = cl.HandlerOffset + cl.HandlerLength;

            Block subBlock;
            switch (cl.Flags)
            {
                case ExceptionHandlingClauseOptions.Clause:
                    subBlock = new CatchBlock(this, cl.CatchType)
                    {
                        MinOffset = cl.HandlerOffset,
                        MaxOffset = cl.HandlerOffset + cl.HandlerLength
                    };
                    break;
                case ExceptionHandlingClauseOptions.Fault:
                    subBlock = new FaultBlock(this)
                    {
                        MinOffset = cl.HandlerOffset,
                        MaxOffset = cl.HandlerOffset + cl.HandlerLength
                    };
                    break;
                case ExceptionHandlingClauseOptions.Filter:
                    subBlock = new FilterBlock(this)
                    {
                        MinOffset = cl.FilterOffset,
                        MaxOffset = cl.HandlerOffset + cl.HandlerLength,
                        HandlerOffset = cl.HandlerOffset,
                        HandlerLength = cl.HandlerLength,
                        FilterOffset = cl.FilterOffset,
                        FilterLength = cl.HandlerOffset - cl.FilterOffset
                    };
                    break;
                case ExceptionHandlingClauseOptions.Finally:
                    subBlock = new FinallyBlock(this)
                    {
                        MinOffset = cl.HandlerOffset,
                        MaxOffset = cl.HandlerOffset + cl.HandlerLength
                    };
                    break;
                default:
                    throw new NotSupportedException(cl.Flags.ToString());
            }

            this.SubBlocks.Add(subBlock);

            return subBlock;
        }

        public void AddSubBlock(Block block)
        {
            foreach (var sb in this.SubBlocks)
            {
                if (sb.MinOffset <= block.MinOffset && sb.MaxOffset >= block.MaxOffset)
                {
                    block.Parent = sb;
                    sb.SubBlocks.Add(block);
                    return;
                }
            }
            throw new InvalidProgramException("Exception block partially overlaps other blocks and handlers");
        }

        public override Block Rewrite(StackRewriterBase rewriter)
        {
            return rewriter.Rewrite(this);
        }

        public override T Visit<T>(IBlockVisitor<T> rewriter) { return rewriter.Visit(this); }

    }

}
