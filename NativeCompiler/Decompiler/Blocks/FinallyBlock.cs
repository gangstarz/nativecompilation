﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Decompiler.Blocks
{
    public class FinallyBlock : Block
    {
        public FinallyBlock(Block parent) : base(parent) { }

        public override void DumpAnalyzed()
        {
            Console.WriteLine("finally {");
            base.DumpAnalyzed();
            Console.WriteLine("}");
        }

        public override Block Rewrite(StackRewriterBase rewriter)
        {
            return rewriter.Rewrite(this);
        }

        public override T Visit<T>(IBlockVisitor<T> rewriter) { return rewriter.Visit(this); }
    }
}
