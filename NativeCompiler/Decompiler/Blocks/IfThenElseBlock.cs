﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Decompiler.Blocks
{
    public class IfThenElseBlock : Block
    {
        public IfThenElseBlock(Block parent, bool hasThen)
            : base(parent)
        {
            if (hasThen)
            {
                Then = new StatementBlock(this);
                this.SubBlocks.Add(Then);
            }
            Else = new StatementBlock(this);
            this.SubBlocks.Add(Else);
        }

        public IStackOperation Condition;
        public StatementBlock Then = null;
        public StatementBlock Else = null;

        public override Block Rewrite(StackRewriterBase rewriter)
        {
            return rewriter.Rewrite(this);
        }

        public override T Visit<T>(IBlockVisitor<T> rewriter) { return rewriter.Visit(this); }
    }
}
