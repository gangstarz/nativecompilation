﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;

namespace NativeCompiler.Decompiler.Blocks
{
    public class WhileBlock : Block
    {
        public WhileBlock(Block parent)
            : base(parent)
        {
            this.Body = new StatementBlock(this);
            this.SubBlocks.Add(Body);
        }

        public IStackOperation Condition;
        public StatementBlock Body = null;

        public override Block Rewrite(StackRewriterBase rewriter)
        {
            return rewriter.Rewrite(this);
        }

        public override T Visit<T>(IBlockVisitor<T> rewriter) { return rewriter.Visit(this); }
    }
}
