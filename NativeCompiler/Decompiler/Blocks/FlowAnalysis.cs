﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Decompiler.Blocks
{
    public class FlowAnalysis
    {
        private List<Tuple<int, int>> brancheLookup;

        public static bool IsConditionalBranche(IStackOperation op)
        {
            return op is IBranch && !(op is Br || op is Leave);
        }

        public void Analyze(Block method)
        {
            this.brancheLookup = new List<Tuple<int, int>>();
            foreach (var bl in method.AllBlocks())
            {
                foreach (var op in bl.StackOperations)
                {
                    IBranch br = op as IBranch;
                    if (br != null)
                    {
                        brancheLookup.Add(new Tuple<int, int>(br.TargetOffset, op.Offset));
                    }
                }
            }
            brancheLookup.Sort();

            Recurse(method);
            RemoveObsoleteLabels(method);
        }

        private void RemoveObsoleteLabels(Block method)
        {
            HashSet<int> branchDestinations = new HashSet<int>();
            CollectLabels(method, branchDestinations);

            SetLabelRequirements(method, branchDestinations);
        }

        private void SetLabelRequirements(Block block, HashSet<int> branchDestinations)
        {
            foreach (var sb in block.SubBlocks)
            {
                SetLabelRequirements(sb, branchDestinations);
            }

            ILBlock bl = block as ILBlock;
            if (bl != null)
            {
                bl.RequiresLabel = branchDestinations.Contains(bl.MinOffset);
            }
        }

        private void CollectLabels(Block block, HashSet<int> branchDestinations)
        {
            foreach (var sb in block.SubBlocks)
            {
                CollectLabels(sb, branchDestinations);
            }

            ILBlock bl = block as ILBlock;
            if (bl != null)
            {
                foreach (var op in bl.StackOperations)
                {
                    IBranch br = op as IBranch;
                    if (br != null)
                    {
                        branchDestinations.Add(br.TargetOffset);
                    }
                    else if (op is Switch)
                    {
                        Switch sw = (Switch)op;
                        foreach (var target in sw.JumpTable)
                        {
                            branchDestinations.Add(target);
                        }
                    }
                }
            }
        }

        private void Recurse(Block block)
        {
            if (block.SubBlocks.All((a) => a is ILBlock))
            {
                // Process
                if (PerformFlowAnalysis(block))
                {
                    foreach (var sb in block.SubBlocks)
                    {
                        Recurse(sb);
                    }
                }
            }
            else
            {
                foreach (var sb in block.SubBlocks)
                {
                    Recurse(sb);
                }
            }
        }

        private bool PerformFlowAnalysis(Block block)
        {
            List<Block> results = new List<Block>();
            int start = 0;

            for (int sb = 0; sb < block.SubBlocks.Count; ++sb)
            {
                bool emitted = false;

                var bl = (ILBlock)block.SubBlocks[sb];
                var so = bl.StackOperations;

                for (int i = 0; i < so.Count && !emitted; ++i)
                {
                    if (IsConditionalBranche(so[i]))
                    {
                        // if-then, if-then-else or while loop.
                        var to = ((IBranch)so[i]).TargetOffset;

                        if (to > so[i].Offset)
                        {
                            // find target block
                            for (int j = sb; j < block.SubBlocks.Count && !emitted; ++j)
                            {
                                if (block.SubBlocks[j].MaxOffset == to)
                                {
                                    ++j; // Note: we check on MaxOffset, so we have to add 1.

                                    var prevBlock = (ILBlock)block.SubBlocks[j - 1];
                                    var prevBranch = prevBlock.StackOperations[prevBlock.StackOperations.Count - 1] as Br;
                                    var prevOff = prevBlock.StackOperations[prevBlock.StackOperations.Count - 1].Offset;

                                    // 'If-then' doesn't have a jump.
                                    if (prevBranch == null)
                                    {
                                        // All jumps within the block have to be internal. (Closure condition)
                                        if (CheckClosure(so[i].Offset, to))
                                        {
                                            // Add everything before if condition
                                            for (int k = start; k < sb; ++k)
                                            {
                                                results.Add(block.SubBlocks[k]);
                                            }
                                            if (i > 0 || bl.RequiresLabel)
                                            {
                                                results.Add(new ILBlock(block, bl, 0, i));
                                            }

                                            // Construct if-then block
                                            IfThenElseBlock ite = new IfThenElseBlock(block, false);
                                            ite.Condition = so[i];

                                            List<Block> elsePart = ite.Else.SubBlocks;
                                            elsePart.Add(new ILBlock(block, bl, i + 1, so.Count));

                                            for (int k = sb + 1; k < j; ++k)
                                            {
                                                elsePart.Add(block.SubBlocks[k]);
                                            }

                                            results.Add(ite);

                                            // Prepare for next iteration of flow analysis algorithm
                                            emitted = true;
                                            start = j;
                                            sb = j - 1;
                                        }
                                    }
                                    // 'If-then-else' jumps past the 'then' part
                                    else if (prevBranch != null && prevBranch.TargetOffset > block.SubBlocks[j].MinOffset)
                                    {
                                        // find the block containing the branch target
                                        int endThen = j;
                                        while (endThen < block.SubBlocks.Count && block.SubBlocks[endThen].MaxOffset != prevBranch.TargetOffset)
                                        {
                                            ++endThen;
                                        }
                                        if (endThen < block.SubBlocks.Count) // should be true
                                        {
                                            ++endThen;

                                            // Check for closure in the 'else' and the 'then' part.
                                            if (CheckClosure(so[i].Offset, prevOff) &&
                                                CheckClosure(to + 1, prevBranch.TargetOffset))
                                            {
                                                // Add everything before if condition
                                                for (int k = start; k < sb; ++k)
                                                {
                                                    results.Add(block.SubBlocks[k]);
                                                }
                                                if (i > 0 || bl.RequiresLabel)
                                                {
                                                    results.Add(new ILBlock(block, bl, 0, i));
                                                }

                                                // Construct if-then-else block
                                                IfThenElseBlock ite = new IfThenElseBlock(block, true);
                                                ite.Condition = so[i];

                                                // Construct the 'else' part
                                                List<Block> elsePart = ite.Else.SubBlocks;
                                                elsePart.Add(new ILBlock(ite, bl, i + 1, so.Count));

                                                for (int k = sb + 1; k < j; ++k)
                                                {
                                                    elsePart.Add(block.SubBlocks[k]);
                                                }

                                                // Remove the unconditional branche
                                                var lastElseBlock = (ILBlock)elsePart[elsePart.Count - 1];
                                                elsePart[elsePart.Count - 1] = new ILBlock(ite, lastElseBlock, 0, lastElseBlock.StackOperations.Count - 1);

                                                //Construct the 'then' part.

                                                List<Block> thenPart = ite.Then.SubBlocks;
                                                for (int k = j; k < endThen; ++k)
                                                {
                                                    thenPart.Add(block.SubBlocks[k]);
                                                }

                                                results.Add(ite);

                                                // Prepare for next iteration of flow analysis algorithm
                                                emitted = true;
                                                start = endThen;
                                                sb = endThen - 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (start > 0)
            {
                while (start < block.SubBlocks.Count)
                {
                    results.Add(block.SubBlocks[start]);
                    ++start;
                }

                block.SubBlocks = results;
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckClosure(int minOffset, int maxOffset)
        {
            foreach (var off in GetBranches(minOffset, maxOffset))
            {
                if (off < minOffset || off > maxOffset) { return false; }
            }
            return true;
        }

        private IEnumerable<int> GetBranches(int minOffset, int maxOffset)
        {
            int lower = brancheLookup.BinarySearch(new Tuple<int, int>(minOffset, 0));
            if (lower < 0) { lower = ~lower; }

            while (lower < brancheLookup.Count && brancheLookup[lower].Item1 < maxOffset)
            {
                yield return brancheLookup[lower].Item2;
                ++lower;
            }
        }
    }
}
