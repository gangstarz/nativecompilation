﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Decompiler.Blocks
{
    public static class BlockBuilder
    {
        public static Block Build(MethodBody body)
        {
            // According to MS Partition I: http://msdn.microsoft.com/en-us/vstudio/aa569283.aspx , try-catch-finally blocks are either overlapping (inclusive) or 
            // sequential. 

            List<Block> currentLevel = new List<Block>();

            foreach (var cl in body.ExceptionHandlingClauses)
            {
                if (currentLevel.Count == 0)
                {
                    // Not part of an exception
                    ExceptionBlock eb = new ExceptionBlock(null, cl.TryOffset, cl.TryLength);
                    eb.Add(cl);

                    currentLevel.Add(eb);
                }
                else
                {
                    var top = (ExceptionBlock)currentLevel[currentLevel.Count - 1];
                    if (top.TryOffset == cl.TryOffset && top.TryLength == cl.TryLength)
                    {
                        // Same exception block (and consecutive), so we can simply add it
                        top.Add(cl);
                    }
                    else if (top.MinOffset >= cl.TryOffset && top.MaxOffset <= cl.HandlerOffset + cl.HandlerLength)
                    {
                        // Update current level with all blocks that are contained in this block
                        int i;
                        for (i = currentLevel.Count - 1; i >= 0; --i)
                        {
                            var c = currentLevel[i];
                            if (c.MaxOffset <= cl.TryOffset)
                            {
                                break;
                            }
                        }
                        ++i;

                        // 2 possibilities: contain current block or add as handler

                        var lbl = (ExceptionBlock)currentLevel[i];
                        if (lbl.TryOffset == cl.TryOffset && lbl.TryLength == cl.TryLength)
                        {
                            var handlerBlock = lbl.Add(cl);
                            for (int j = i + 1; j < currentLevel.Count; ++j)
                            {
                                handlerBlock.SubBlocks.Add(currentLevel[j]);
                                currentLevel[j].Parent = handlerBlock;
                            }

                            // Remove blocks that are now contained
                            currentLevel.RemoveRange(i + 1, currentLevel.Count - (i + 1));
                        }
                        else
                        {
                            // Nested exception block. 
                            ExceptionBlock eb = new ExceptionBlock(null, cl.TryOffset, cl.TryLength);
                            eb.Add(cl);

                            for (int j = i; j < currentLevel.Count; ++j)
                            {
                                eb.AddSubBlock(currentLevel[j]);
                            }

                            // Remove blocks that are now contained
                            currentLevel.RemoveRange(i, currentLevel.Count - i);

                            // Add the new block
                            currentLevel.Add(eb);
                        }
                    }
                    else if (top.MaxOffset <= cl.TryOffset)
                    {
                        // Sequential exception blocks; simply add
                        ExceptionBlock eb = new ExceptionBlock(null, cl.TryOffset, cl.TryLength);
                        eb.Add(cl);
                        currentLevel.Add(eb);
                    }
                    else
                    {
                        // Strange scenario's. Shouldn't happen according to MS Partition I:
                        // http://msdn.microsoft.com/en-us/vstudio/aa569283.aspx , try-catch-finally blocks are either 
                        // overlapping (inclusive) or consecutive. 

                        throw new InvalidProgramException("Exception blocks must be either consecutive or inclusive.");
                    }
                }
            }

            Block methodBlock = new MethodBlock()
            {
                MinOffset = 0,
                MaxOffset = body.GetILAsByteArray().Length
            };
            
            foreach (var item in currentLevel)
            {
                item.Parent = methodBlock;
            }
            
            methodBlock.SubBlocks = currentLevel;
            
            return methodBlock;
        }
    }
}
