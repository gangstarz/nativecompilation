﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection.Emit;

namespace NativeCompiler.Decompiler
{
    public class ILInstruction
    {
        public ILInstruction(int offset, OpCode code, object operand)
        {
            this.Offset = offset;
            this.Code = code;
            this.Operand = operand;
        }

        // Fields
        public int Offset { get; private set; }
        public OpCode Code { get; private set; }
        public object Operand { get; private set; }
    }
}
