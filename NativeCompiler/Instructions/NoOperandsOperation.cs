using System;
using System.Collections.Generic;
using System.Text;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Instructions
{
    public abstract class NoOperandsOperation : StackOperationBase
    {
        public override void Execute(ILStack stack)
        {
        }

        private static IStackOperation[] noChildren = new IStackOperation[0];

        public override Type ReturnType { get; set; }
        public override IEnumerable<IStackOperation> Children { get { return noChildren; } }

        protected void RewriteChildren(StackRewriterBase rewriter)
        {
        }

        public override void Write(System.IO.TextWriter sb)
        {
            sb.Write(this.GetType().Name);
            sb.Write("()");
        }

        public override string ToString()
        {
            System.IO.StringWriter sb = new System.IO.StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }
}