using System;
using System.Collections.Generic;
using System.Text;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Instructions
{
    public abstract class SingleOperandsOperation : StackOperationBase
    {
        public IStackOperation child;

        public override void Execute(ILStack stack)
        {
            this.child = stack.Pop();
        }

        public override Type ReturnType { get; set; }
        public override IEnumerable<IStackOperation> Children { get { yield return child; } }

        protected void RewriteChildren(StackRewriterBase rewriter)
        {
            child = child.Rewrite(rewriter);
        }

        public override void Write(System.IO.TextWriter sb)
        {
            sb.Write(this.GetType().Name);
            sb.Write("(");
            if (child == null)
            {
                sb.Write("??");
            }
            else
            {
                child.Write(sb);
            }
            sb.Write(')');
        }

        public override string ToString()
        {
            System.IO.StringWriter sb = new System.IO.StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }
}