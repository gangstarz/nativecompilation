﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Instructions.IL;
using NativeCompiler.Instructions;
using NativeCompiler.Decompiler;
using NativeCompiler.Instructions.Meta;

namespace NativeCompiler.Instructions
{
    public class StackOperationBuilder
    {
        static StackOperationBuilder()
        {
            Add(OpCodes.Add, (i) => new Add());
            Add(OpCodes.Add_Ovf, (i) => new Add());
            Add(OpCodes.Add_Ovf_Un, (i) => new Add());
            Add(OpCodes.And, (i) => new And());
            //Add(OpCodes.Arglist, (i) => new Arglist(i));
            Add(OpCodes.Beq, (i) => new Beq(i));
            Add(OpCodes.Beq_S, (i) => new Beq(i));
            Add(OpCodes.Bge, (i) => new Bge(i));
            Add(OpCodes.Bge_S, (i) => new Bge(i));
            Add(OpCodes.Bge_Un, (i) => new Bge(i));
            Add(OpCodes.Bge_Un_S, (i) => new Bge(i));
            Add(OpCodes.Bgt, (i) => new Bgt(i));
            Add(OpCodes.Bgt_S, (i) => new Bgt(i));
            Add(OpCodes.Bgt_Un, (i) => new Bgt(i));
            Add(OpCodes.Bgt_Un_S, (i) => new Bgt(i));
            Add(OpCodes.Ble, (i) => new Ble(i));
            Add(OpCodes.Ble_S, (i) => new Ble(i));
            Add(OpCodes.Ble_Un, (i) => new Ble(i));
            Add(OpCodes.Ble_Un_S, (i) => new Ble(i));
            Add(OpCodes.Blt, (i) => new Blt(i));
            Add(OpCodes.Blt_S, (i) => new Blt(i));
            Add(OpCodes.Blt_Un, (i) => new Blt(i));
            Add(OpCodes.Blt_Un_S, (i) => new Blt(i));
            Add(OpCodes.Bne_Un, (i) => new Bne_Un(i));
            Add(OpCodes.Bne_Un_S, (i) => new Bne_Un(i));
            Add(OpCodes.Box, (i) => new Box(i));
            Add(OpCodes.Br, (i) => new Br(i));
            Add(OpCodes.Br_S, (i) => new Br(i));
            Add(OpCodes.Break, (i) => new Break());
            Add(OpCodes.Brfalse, (i) => new BrFalse(i));
            Add(OpCodes.Brfalse_S, (i) => new BrFalse(i));
            Add(OpCodes.Brtrue, (i) => new BrTrue(i));
            Add(OpCodes.Brtrue_S, (i) => new BrTrue(i));
            Add(OpCodes.Call, (i) => new Call(i));
            Add(OpCodes.Calli, (i) => new Call(i));
            Add(OpCodes.Callvirt, (i) => new CallVirt(i));
            Add(OpCodes.Castclass, (i) => new CastClass(i));
            Add(OpCodes.Ceq, (i) => new Ceq(i));
            Add(OpCodes.Cgt, (i) => new Cgt(i));
            Add(OpCodes.Cgt_Un, (i) => new Cgt(i));
            Add(OpCodes.Ckfinite, (i) => new CkFinite(i));
            Add(OpCodes.Clt, (i) => new Clt(i));
            Add(OpCodes.Clt_Un, (i) => new Clt(i));
            Add(OpCodes.Constrained, (i) => new Nop());
            Add(OpCodes.Conv_I, (i) => new ConvI(i));
            Add(OpCodes.Conv_I1, (i) => new ConvI1(i));
            Add(OpCodes.Conv_I2, (i) => new ConvI2(i));
            Add(OpCodes.Conv_I4, (i) => new ConvI4(i));
            Add(OpCodes.Conv_I8, (i) => new ConvI8(i));
            Add(OpCodes.Conv_Ovf_I, (i) => new ConvI(i));
            Add(OpCodes.Conv_Ovf_I_Un, (i) => new ConvI(i));
            Add(OpCodes.Conv_Ovf_I1, (i) => new ConvI1(i));
            Add(OpCodes.Conv_Ovf_I1_Un, (i) => new ConvI1(i));
            Add(OpCodes.Conv_Ovf_I2, (i) => new ConvI2(i));
            Add(OpCodes.Conv_Ovf_I2_Un, (i) => new ConvI2(i));
            Add(OpCodes.Conv_Ovf_I4, (i) => new ConvI4(i));
            Add(OpCodes.Conv_Ovf_I4_Un, (i) => new ConvI4(i));
            Add(OpCodes.Conv_Ovf_I8, (i) => new ConvI8(i));
            Add(OpCodes.Conv_Ovf_I8_Un, (i) => new ConvI8(i));
            Add(OpCodes.Conv_Ovf_U, (i) => new ConvU(i));
            Add(OpCodes.Conv_Ovf_U_Un, (i) => new ConvU(i));
            Add(OpCodes.Conv_Ovf_U1, (i) => new ConvU1(i));
            Add(OpCodes.Conv_Ovf_U1_Un, (i) => new ConvU1(i));
            Add(OpCodes.Conv_Ovf_U2, (i) => new ConvU2(i));
            Add(OpCodes.Conv_Ovf_U2_Un, (i) => new ConvU2(i));
            Add(OpCodes.Conv_Ovf_U4, (i) => new ConvU4(i));
            Add(OpCodes.Conv_Ovf_U4_Un, (i) => new ConvU4(i));
            Add(OpCodes.Conv_Ovf_U8, (i) => new ConvU8(i));
            Add(OpCodes.Conv_Ovf_U8_Un, (i) => new ConvU8(i));
            Add(OpCodes.Conv_R_Un, (i) => new ConvR(i));
            Add(OpCodes.Conv_R4, (i) => new ConvR4(i));
            Add(OpCodes.Conv_R8, (i) => new ConvR8(i));
            Add(OpCodes.Conv_U, (i) => new ConvU(i));
            Add(OpCodes.Conv_U1, (i) => new ConvU1(i));
            Add(OpCodes.Conv_U2, (i) => new ConvU2(i));
            Add(OpCodes.Conv_U4, (i) => new ConvU4(i));
            Add(OpCodes.Conv_U8, (i) => new ConvU8(i));
            //Add(OpCodes.Cpblk, (i) => new Cpblk(i));
            //Add(OpCodes.Cpobj, (i) => new Cpobj(i));
            Add(OpCodes.Div, (i) => new Div());
            Add(OpCodes.Div_Un, (i) => new Div());
            Add(OpCodes.Dup, (i) => new Dup());
            Add(OpCodes.Endfilter, (i) => new EndFilter(i));
            Add(OpCodes.Endfinally, (i) => new EndFinally(i));
            //Add(OpCodes.Initblk, (i) => new Initblk(i));
            Add(OpCodes.Initobj, (i) => new InitObj(i));
            Add(OpCodes.Isinst, (i) => new IsInst(i));
            //Add(OpCodes.Jmp, (i) => new Jmp(i));
            Add(OpCodes.Ldarg, (i) => new LdArg(i));
            Add(OpCodes.Ldarg_0, (i) => new LdArgN(0));
            Add(OpCodes.Ldarg_1, (i) => new LdArgN(1));
            Add(OpCodes.Ldarg_2, (i) => new LdArgN(2));
            Add(OpCodes.Ldarg_3, (i) => new LdArgN(3));
            Add(OpCodes.Ldarg_S, (i) => new LdArgS(i));
            Add(OpCodes.Ldarga, (i) => new LdArgA(i));
            Add(OpCodes.Ldarga_S, (i) => new LdArgA(i));
            Add(OpCodes.Ldc_I4, (i) => new LdcI4(i));
            Add(OpCodes.Ldc_I4_0, (i) => new LdcI4N(0));
            Add(OpCodes.Ldc_I4_1, (i) => new LdcI4N(1));
            Add(OpCodes.Ldc_I4_2, (i) => new LdcI4N(2));
            Add(OpCodes.Ldc_I4_3, (i) => new LdcI4N(3));
            Add(OpCodes.Ldc_I4_4, (i) => new LdcI4N(4));
            Add(OpCodes.Ldc_I4_5, (i) => new LdcI4N(5));
            Add(OpCodes.Ldc_I4_6, (i) => new LdcI4N(6));
            Add(OpCodes.Ldc_I4_7, (i) => new LdcI4N(7));
            Add(OpCodes.Ldc_I4_8, (i) => new LdcI4N(8));
            Add(OpCodes.Ldc_I4_M1, (i) => new LdcI4N(-1));
            Add(OpCodes.Ldc_I4_S, (i) => new LdcI4S(i));
            Add(OpCodes.Ldc_I8, (i) => new LdcI8(i));
            Add(OpCodes.Ldc_R4, (i) => new LdcR4(i));
            Add(OpCodes.Ldc_R8, (i) => new LdcR8(i));
            Add(OpCodes.Ldelem, (i) => new LdElem(i));
            Add(OpCodes.Ldelem_I, (i) => new LdElemI(i));
            Add(OpCodes.Ldelem_I1, (i) => new LdElemI1(i));
            Add(OpCodes.Ldelem_I2, (i) => new LdElemI2(i));
            Add(OpCodes.Ldelem_I4, (i) => new LdElemI4(i));
            Add(OpCodes.Ldelem_I8, (i) => new LdElemI8(i));
            Add(OpCodes.Ldelem_R4, (i) => new LdElemR4(i));
            Add(OpCodes.Ldelem_R8, (i) => new LdElemR8(i));
            Add(OpCodes.Ldelem_Ref, (i) => new LdElemRef(i));
            Add(OpCodes.Ldelem_U1, (i) => new LdElemU1(i));
            Add(OpCodes.Ldelem_U2, (i) => new LdElemU2(i));
            Add(OpCodes.Ldelem_U4, (i) => new LdElemU4(i));
            Add(OpCodes.Ldelema, (i) => new LdElemA(i));
            Add(OpCodes.Ldfld, (i) => new LdFld(i));
            Add(OpCodes.Ldflda, (i) => new LdFldA(i));
            Add(OpCodes.Ldftn, (i) => new LdFtn(i));
            Add(OpCodes.Ldind_I, (i) => new LdIndI());
            Add(OpCodes.Ldind_I1, (i) => new LdIndI1());
            Add(OpCodes.Ldind_I2, (i) => new LdIndI2());
            Add(OpCodes.Ldind_I4, (i) => new LdIndI4());
            Add(OpCodes.Ldind_I8, (i) => new LdIndI8());
            Add(OpCodes.Ldind_R4, (i) => new LdIndR4());
            Add(OpCodes.Ldind_R8, (i) => new LdIndR8());
            Add(OpCodes.Ldind_Ref, (i) => new LdIndRef());
            Add(OpCodes.Ldind_U1, (i) => new LdIndU1());
            Add(OpCodes.Ldind_U2, (i) => new LdIndU2());
            Add(OpCodes.Ldind_U4, (i) => new LdIndU4());
            Add(OpCodes.Ldlen, (i) => new LdLen());
            Add(OpCodes.Ldloc, (i) => new LdLoc(i));
            Add(OpCodes.Ldloc_0, (i) => new LdLocN(0));
            Add(OpCodes.Ldloc_1, (i) => new LdLocN(1));
            Add(OpCodes.Ldloc_2, (i) => new LdLocN(2));
            Add(OpCodes.Ldloc_3, (i) => new LdLocN(3));
            Add(OpCodes.Ldloc_S, (i) => new LdLocS(i));
            Add(OpCodes.Ldloca, (i) => new LdLocA(i));
            Add(OpCodes.Ldloca_S, (i) => new LdLocA(i));
            Add(OpCodes.Ldnull, (i) => new LdNull());
            Add(OpCodes.Ldobj, (i) => new LdObj());
            Add(OpCodes.Ldsfld, (i) => new LdSFld(i));
            Add(OpCodes.Ldsflda, (i) => new LdSFldA(i));
            Add(OpCodes.Ldstr, (i) => new LdStr(i));
            Add(OpCodes.Ldtoken, (i) => new LdToken(i));
            Add(OpCodes.Ldvirtftn, (i) => new LdVirtFtn(i));
            Add(OpCodes.Leave, (i) => new Leave(i));
            Add(OpCodes.Leave_S, (i) => new Leave(i));
            Add(OpCodes.Localloc, (i) => new LocAlloc());
            Add(OpCodes.Mkrefany, (i) => new MkRefAny(i));
            Add(OpCodes.Mul, (i) => new Mul());
            Add(OpCodes.Mul_Ovf, (i) => new Mul());
            Add(OpCodes.Mul_Ovf_Un, (i) => new Mul());
            Add(OpCodes.Neg, (i) => new Neg());
            Add(OpCodes.Newarr, (i) => new NewArr(i));
            Add(OpCodes.Newobj, (i) => new NewObj(i));
            Add(OpCodes.Nop, (i) => new Nop());
            Add(OpCodes.Not, (i) => new Not());
            Add(OpCodes.Or, (i) => new Or());
            Add(OpCodes.Pop, (i) => new Pop());
            //Add(OpCodes.Prefix1, (i) => new Prefix1(i));
            //Add(OpCodes.Prefix2, (i) => new Prefix2(i));
            //Add(OpCodes.Prefix3, (i) => new Prefix3(i));
            //Add(OpCodes.Prefix4, (i) => new Prefix4(i));
            //Add(OpCodes.Prefix5, (i) => new Prefix5(i));
            //Add(OpCodes.Prefix6, (i) => new Prefix6(i));
            //Add(OpCodes.Prefix7, (i) => new Prefix7(i));
            //Add(OpCodes.Prefixref, (i) => new Prefixref(i));
            Add(OpCodes.Readonly, (i) => new ReadOnly());
            //Add(OpCodes.Refanytype, (i) => new Refanytype(i));
            //Add(OpCodes.Refanyval, (i) => new Refanyval(i));
            Add(OpCodes.Rem, (i) => new Rem());
            Add(OpCodes.Rem_Un, (i) => new Rem());
            Add(OpCodes.Ret, (i) => new Ret());
            Add(OpCodes.Rethrow, (i) => new Rethrow());
            Add(OpCodes.Shl, (i) => new Shl());
            Add(OpCodes.Shr, (i) => new Shr());
            Add(OpCodes.Shr_Un, (i) => new Shr());
            Add(OpCodes.Sizeof, (i) => new SizeOf(i));
            Add(OpCodes.Starg, (i) => new StArg(i));
            Add(OpCodes.Starg_S, (i) => new StArg(i));
            Add(OpCodes.Stelem, (i) => new StElem(i));
            Add(OpCodes.Stelem_I, (i) => new StElemI(i));
            Add(OpCodes.Stelem_I1, (i) => new StElemI1());
            Add(OpCodes.Stelem_I2, (i) => new StElemI2());
            Add(OpCodes.Stelem_I4, (i) => new StElemI4());
            Add(OpCodes.Stelem_I8, (i) => new StElemI8());
            Add(OpCodes.Stelem_R4, (i) => new StElemR4());
            Add(OpCodes.Stelem_R8, (i) => new StElemR8());
            Add(OpCodes.Stelem_Ref, (i) => new StElemRef());
            Add(OpCodes.Stfld, (i) => new StFld(i));
            Add(OpCodes.Stind_I, (i) => new StIndI());
            Add(OpCodes.Stind_I1, (i) => new StIndI1());
            Add(OpCodes.Stind_I2, (i) => new StIndI2());
            Add(OpCodes.Stind_I4, (i) => new StIndI4());
            Add(OpCodes.Stind_I8, (i) => new StIndI8());
            Add(OpCodes.Stind_R4, (i) => new StIndR4());
            Add(OpCodes.Stind_R8, (i) => new StIndR8());
            Add(OpCodes.Stind_Ref, (i) => new StIndRef());
            Add(OpCodes.Stloc, (i) => new StLoc(i));
            Add(OpCodes.Stloc_0, (i) => new StLocN(0));
            Add(OpCodes.Stloc_1, (i) => new StLocN(1));
            Add(OpCodes.Stloc_2, (i) => new StLocN(2));
            Add(OpCodes.Stloc_3, (i) => new StLocN(3));
            Add(OpCodes.Stloc_S, (i) => new StLocS(i));
            Add(OpCodes.Stobj, (i) => new StObj(i));
            Add(OpCodes.Stsfld, (i) => new StSFld(i));
            Add(OpCodes.Sub, (i) => new Sub());
            Add(OpCodes.Sub_Ovf, (i) => new Sub());
            Add(OpCodes.Sub_Ovf_Un, (i) => new Sub());
            Add(OpCodes.Switch, (i) => new Switch(i));
            Add(OpCodes.Tailcall, (i) => new TailCall());
            Add(OpCodes.Throw, (i) => new Throw(i));
            Add(OpCodes.Unaligned, (i) => new Unaligned());
            Add(OpCodes.Unbox, (i) => new Unbox(i));
            Add(OpCodes.Unbox_Any, (i) => new Unbox_Any(i));
            Add(OpCodes.Volatile, (i) => new Volatile());
            Add(OpCodes.Xor, (i) => new Xor());

            var branches = new OpCode[] 
            {
                OpCodes.Beq, OpCodes.Beq_S, OpCodes.Bge, OpCodes.Bge_S, OpCodes.Bge_Un, OpCodes.Bge_Un_S, OpCodes.Bgt, OpCodes.Bgt_S, OpCodes.Bgt_Un, OpCodes.Bgt_Un_S,
                OpCodes.Ble,OpCodes.Ble_S, OpCodes.Ble_Un, OpCodes.Ble_Un_S, OpCodes.Blt, OpCodes.Blt_S, OpCodes.Blt_Un,OpCodes.Blt_Un_S, OpCodes.Bne_Un, OpCodes.Bne_Un_S,
                OpCodes.Br, OpCodes.Br_S, OpCodes.Brfalse, OpCodes.Brfalse_S, OpCodes.Brtrue, OpCodes.Brtrue_S, OpCodes.Leave, OpCodes.Leave_S
            };
            labelBranches = new HashSet<OpCode>(branches);
        }

        private static void Add(OpCode opcode, Func<ILInstruction, IStackOperation> fcn)
        {
            builders.Add(opcode, fcn);
        }

        private static Dictionary<OpCode, Func<ILInstruction, IStackOperation>> builders =
                   new Dictionary<OpCode, Func<ILInstruction, IStackOperation>>();

        private static HashSet<OpCode> labelBranches;

        public MethodBase method; // TODO: Make private!
        private Type returnType;

        public StackOperationBuilder(MethodBase definition, MethodBody body)
        {
            this.method = definition;
            this.returnType = definition is MethodInfo ? ((MethodInfo)definition).ReturnType : typeof(void);
        }

        public static bool IsUnconditionalBranche(OpCode code)
        {
            return (code == OpCodes.Ret || code == OpCodes.Throw ||
                    code == OpCodes.Br || code == OpCodes.Br_S ||
                    code == OpCodes.Endfilter || code == OpCodes.Endfinally ||
                    code == OpCodes.Leave || code == OpCodes.Leave_S ||
                    code == OpCodes.Jmp || code == OpCodes.Rethrow);
        }

        public static bool IsBrancheToLabel(OpCode code)
        {
            return labelBranches.Contains(code);
        }

        public void BuildOperations(int initialStackDepth, Dictionary<int, int> stackDepthAtOffset, ILInstruction[] instructions, int endOffset, List<IStackOperation> operations)
        {
            foreach (var instruction in instructions)
            {
                Func<ILInstruction, IStackOperation> builder;
                if (builders.TryGetValue(instruction.Code, out builder))
                {
                    var so = builder(instruction);
                    so.Offset = instruction.Offset;
                    so.StackDepth = int.MinValue;

                    operations.Add(so);

                    // Exception case for 'ret': if it's not void, it will return something, else it will return void.
                    Ret ret = so as Ret;
                    if (ret != null)
                    {
                        ret.ReturnType = returnType;
                    }
                }
                else
                {
                    Console.WriteLine("Warning: Opcode {0} not understood", instruction.Code);
                }
            }

            // IL blocks are already split, so we *know* that there won't be any strange jumps for which we cannot predict the stack.
            int currentStackDepth = initialStackDepth;
            
            foreach (var op in operations)
            {
                op.StackDepth = currentStackDepth;
                currentStackDepth += op.DeltaStackDepth;

                if (currentStackDepth < 0)
                {
                    DumpOperations(operations);
                    throw new Exception("Stack depth < 0 at offset " + op.Offset.ToString("x4"));
                }
                else if (op is IBranch)
                {
                    int targetOffset = ((IBranch)op).TargetOffset;
                    UpdateStackDepth(targetOffset, currentStackDepth, stackDepthAtOffset, operations);
                }
                else if (op is Switch)
                {
                    foreach (var addr in ((Switch)op).JumpTable)
                    {
                        UpdateStackDepth(addr, currentStackDepth, stackDepthAtOffset, operations);
                    }
                }
                else if (op is Ret)
                {
                    if (currentStackDepth != 0)
                    {
                        DumpOperations(operations);
                        throw new Exception("Arguments still on the stack after 'ret'.");
                    }
                }
            }

            // This depends on the last instruction. If it was an unconditional branche, we do nothing - otherwise,
            // we update the stack depth of the next offset

            var lastInstruction = instructions[instructions.Length - 1];
            if (!IsUnconditionalBranche(lastInstruction.Code))
            {
                UpdateStackDepth(endOffset, currentStackDepth, stackDepthAtOffset, operations);
            }
        }

        private void UpdateStackDepth(int offset, int depth, Dictionary<int, int> stackDepthAtOffset, List<IStackOperation> operations)
        {
            int d;
            if (stackDepthAtOffset.TryGetValue(offset, out d))
            {
                if (d != depth)
                {
                    DumpOperations(operations);
                    throw new Exception("Different stack depths found at offset " + offset.ToString("x4"));
                }
            }
            else
            {
                stackDepthAtOffset[offset] = depth;
            }
        }

        private static void DumpOperations(List<IStackOperation> operations)
        {
            for (int i = 0; i < operations.Count; ++i)
            {
                var op = operations[i];

                string spaces = op.StackDepth < 0 ? "* " : (" |" + new string(' ', op.StackDepth * 2));

                string param = (op is IBranch) ? ((IBranch)op).TargetOffset.ToString("x4") : "";

                Console.WriteLine("{4:0000} {0:x4}{1}{2} {3}", op.Offset, spaces, op.GetType().Name.ToString(), param, i);
            }

        }
    }

}
