using System;
using System.Collections.Generic;
using System.Text;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Instructions
{
    public abstract class TwoOrSingleOperandsOperation : StackOperationBase
    {
        public IStackOperation lhs;
        public IStackOperation rhs;

        protected virtual void ExecuteImpl(ILStack stack, bool twoOperands)
        {
            this.lhs = stack.Pop();
            if (twoOperands)
            {
                this.rhs = stack.Pop();
            }
            else
            {
                this.rhs = null;
            }
        }

        public override Type ReturnType { get; set; }

        public override IEnumerable<IStackOperation> Children { get { yield return lhs; yield return rhs; } }

        protected void RewriteChildren(StackRewriterBase rewriter)
        {
            lhs = lhs.Rewrite(rewriter);
            if (rhs != null)
            {
                rhs = rhs.Rewrite(rewriter);
            }
        }

        public override void Write(System.IO.TextWriter sb)
        {
            sb.Write(this.GetType().Name);
            sb.Write("(");
            if (lhs != null) { lhs.Write(sb); } else { sb.Write("??"); }
            if (rhs != null)
            {
                sb.Write(',');
                rhs.Write(sb);
            }
            sb.Write(')');
        }

        public override string ToString()
        {
            System.IO.StringWriter sb = new System.IO.StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }
}