using System;
using System.Collections.Generic;
using System.Text;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Instructions
{
    public abstract class TwoOperandsOperation : StackOperationBase
    {
        public IStackOperation lhs;
        public IStackOperation rhs;

        public override void Execute(ILStack stack)
        {
            this.rhs = stack.Pop();
            this.lhs = stack.Pop();
        }

        public override Type ReturnType { get; set; }

        public override IEnumerable<IStackOperation> Children { get { yield return lhs; yield return rhs; } }

        protected void RewriteChildren(StackRewriterBase rewriter)
        {
            lhs = lhs.Rewrite(rewriter);
            rhs = rhs.Rewrite(rewriter);
        }

        public override void Write(System.IO.TextWriter sb)
        {
            sb.Write(this.GetType().Name);
            sb.Write("(");
            if (lhs != null)
            {
                lhs.Write(sb);
            }
            else
            {
                sb.Write("??");
            }
            sb.Write(',');
            if (rhs != null)
            {
                rhs.Write(sb);
            }
            else
            {
                sb.Write("??");
            }
            sb.Write(')');
        }

        public override string ToString()
        {
            System.IO.StringWriter sb = new System.IO.StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }
}