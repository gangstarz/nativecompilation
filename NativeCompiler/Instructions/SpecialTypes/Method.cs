﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Instructions.SpecialTypes
{
    public class Method
    {
        public static Type Type { get { return typeof(Method); } }
    }
}
