using System;
using System.Collections.Generic;
using System.Text;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Instructions
{
    public abstract class StackOperationBase : IStackOperation
    {
        public abstract Type ReturnType { get; set; }
        public abstract void Execute(ILStack stack);
        public abstract IEnumerable<IStackOperation> Children { get; }

        public abstract IStackOperation Rewrite(StackRewriterBase rewriter);
        public abstract T Visit<T>(IInstructionVisitor<T> rewriter);
        public abstract void Write(System.IO.TextWriter sb);

        public int Offset { get; set; }
        public int StackDepth { get; set; }
        public abstract int DeltaStackDepth { get; }

        public virtual bool IsImplicitStatement { get { return false; } }
    }
}