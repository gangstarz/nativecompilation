using System.Reflection.Emit;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class StArg : SingleOperandsOperation, IStatementOperation
    {
        public StArg(ILInstruction instruction)
        {
            if (instruction.Code == OpCodes.Starg_S)
            {
                this.N = (byte)instruction.Operand;
            }
            else
            {
                this.N = (int)instruction.Operand;
            }
        }

        public int N { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(void);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return -1; }
        }
    }
}