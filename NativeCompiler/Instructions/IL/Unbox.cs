using System;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class Unbox : SingleOperandsOperation
    {
        public Unbox(ILInstruction instruction)
        {
            this.instruction = instruction;
        }

        private ILInstruction instruction;

        public Type BoxType
        {
            get { return (Type)instruction.Operand; }
        }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = (Type)instruction.Operand;
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }
    }
}