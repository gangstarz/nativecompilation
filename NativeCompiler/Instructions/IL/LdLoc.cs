using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdLoc : LdLocN
    {
        public LdLoc(ILInstruction op) : base((int)op.Operand) { }
    }
}