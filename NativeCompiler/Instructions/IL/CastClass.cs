using System;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class CastClass : SingleOperandsOperation
    {
        public CastClass(ILInstruction instruction)
        {
            this.Type = (Type)instruction.Operand;
        }

        public Type Type { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = this.Type;
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }
    }
}