using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class ConvI : SingleOperandsOperation
    {
        public ConvI(ILInstruction instruction)
        {
            this.instruction = instruction;
        }

        private ILInstruction instruction;

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            if (child.ReturnType.IsByRef)
            {
                this.ReturnType = child.ReturnType.GetElementType().MakePointerType();
            }
            else
            {
                this.ReturnType = child.ReturnType.MakePointerType();
            }
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }
    }
}