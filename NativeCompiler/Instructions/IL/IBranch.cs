namespace NativeCompiler.Instructions.IL
{
    public interface IBranch : IStatementOperation
    {
        int TargetOffset { get; }
    }
}