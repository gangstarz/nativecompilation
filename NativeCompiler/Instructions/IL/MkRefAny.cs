﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class MkRefAny : SingleOperandsOperation
    {
        public MkRefAny(ILInstruction instr) {
            this.Type = (Type)instr.Operand;
        }

        public Type Type { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);

            var type = Type.MakeByRefType();
            this.ReturnType = type;

            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }
    }
}
