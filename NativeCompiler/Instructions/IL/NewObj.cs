using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class NewObj : StackOperationBase
    {
        public NewObj(ILInstruction instruction)
        {
            Constructor = (ConstructorInfo)instruction.Operand;
        }

        public ConstructorInfo Constructor { get; private set; }
        public List<IStackOperation> Parameters = null;

        public override void Execute(ILStack stack)
        {
            Parameters = new List<IStackOperation>();
            for (int i = 0; i < Constructor.GetParameters().Length; ++i)
            {
                Parameters.Add(stack.Pop());
            }
            Parameters.Reverse();

            this.ReturnType = Constructor.DeclaringType;

            stack.Push(this);
        }

        public override Type ReturnType { get; set; }

        public override IEnumerable<IStackOperation> Children { get { return Parameters; } }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override IStackOperation Rewrite(StackRewriterBase rewriter)
        {
            for (int i = 0; i < Parameters.Count; ++i)
            {
                Parameters[i] = Parameters[i].Rewrite(rewriter);
            }

            return rewriter.Rewrite(this);
        }

        public override int DeltaStackDepth
        {
            get
            {
                return 1 - Constructor.GetParameters().Length;
            }
        }

        public override void Write(System.IO.TextWriter sb)
        {
            sb.Write("new ");
            sb.Write(Constructor.DeclaringType.FriendlyName());
            sb.Write('(');
            if (Parameters == null)
            {
                sb.Write("??");
            }
            else
            {
                bool first = true;
                foreach (var child in Parameters)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        sb.Write(", ");
                    }
                    child.Write(sb);
                }
            }
            sb.Write(')');
        }

        public override string ToString()
        {
            System.IO.StringWriter sb = new System.IO.StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }
}