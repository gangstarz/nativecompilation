using System;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdElemI : TwoOperandsOperation
    {
        public LdElemI(ILInstruction op)
        {
            this.instruction = op;
        }

        private ILInstruction instruction;

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = ((Type)instruction.Operand).MakePointerType();
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return -1; }
        }
    }
}