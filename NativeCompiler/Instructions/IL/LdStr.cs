using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdStr : NoOperandsOperation
    {
        public LdStr(ILInstruction instruction)
        {
            this.Data = (string)instruction.Operand;
        }

        public string Data { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(string);
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 1; }
        }
    }
}