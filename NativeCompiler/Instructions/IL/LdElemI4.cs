using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdElemI4 : TwoOperandsOperation
    {
        public LdElemI4(ILInstruction op)
        {
            this.instruction = op;
        }

        private ILInstruction instruction;

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(int);
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return -1; }
        }
    }
}