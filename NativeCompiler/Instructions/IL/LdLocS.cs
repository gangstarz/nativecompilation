using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdLocS : LdLocN
    {
        public LdLocS(ILInstruction op) : base((byte)op.Operand) { }
    }
}