using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdcI4S : LdcI4N
    {
        public LdcI4S(ILInstruction instruction)
            : base((int)((sbyte)instruction.Operand))
        {
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }
    }
}