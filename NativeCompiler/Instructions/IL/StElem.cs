using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class StElem : ArrayStoreOperandsOperation, IStatementOperation
    {
        public StElem(ILInstruction instruction)
        {
            this.instruction = instruction;
        }

        private ILInstruction instruction;

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(void);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return -3; }
        }
    }
}