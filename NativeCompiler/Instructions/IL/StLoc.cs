using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class StLoc : StLocN, IStatementOperation
    {
        public StLoc(ILInstruction instruction)
            : base((int)instruction.Operand)
        {
        }
    }
}