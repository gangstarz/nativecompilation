using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class StLocS : StLocN, IStatementOperation
    {
        public StLocS(ILInstruction instruction)
            : base((byte)instruction.Operand)
        {
        }
    }
}