using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class Switch : SingleOperandsOperation, IStatementOperation
    {
        public Switch(ILInstruction instruction)
        {
            this.Instruction = instruction;
        }

        private ILInstruction Instruction;

        public int[] JumpTable { get { return (int[])Instruction.Operand; } }

        public override void Execute(ILStack stack)
        {
            foreach (var item in (int[])Instruction.Operand)
            {
                stack.MarkOffset(item);
            }

            base.Execute(stack);
            this.ReturnType = typeof(void);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return -1; }
        }
    }
}