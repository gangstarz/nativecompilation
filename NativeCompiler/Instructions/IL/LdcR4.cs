using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdcR4 : NoOperandsOperation
    {
        public LdcR4(ILInstruction op)
        {
            this.N = (float)op.Operand;
        }

        public float N { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(float);
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 1; }
        }
    }
}