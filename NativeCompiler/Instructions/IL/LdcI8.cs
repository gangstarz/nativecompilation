using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdcI8 : NoOperandsOperation
    {
        public LdcI8(ILInstruction op)
        {
            if (op.Operand is ulong)
            {
                this.N = (long)((ulong)op.Operand);
            }
            else
            {
                this.N = (long)op.Operand;
            }
        }

        public long N { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(long);
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 1; }
        }
    }
}