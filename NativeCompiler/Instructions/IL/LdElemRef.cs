using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdElemRef : TwoOperandsOperation
    {
        public LdElemRef(ILInstruction op)
        {
            this.instruction = op;
        }

        private ILInstruction instruction;

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = this.lhs.ReturnType.GetElementType();
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return -1; }
        }
    }
}