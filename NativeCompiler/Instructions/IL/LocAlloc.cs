﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Instructions.IL
{
    public class LocAlloc : NoOperandsOperation
    {
        public LocAlloc() { }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);

            stack.Pop(); // some pointer

            this.ReturnType = typeof(byte).MakePointerType();
            
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }
    }
}
