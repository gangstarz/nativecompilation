using System.Reflection;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdFldA : SingleOperandsOperation
    {
        public LdFldA(ILInstruction instruction)
        {
            this.Field = (FieldInfo)instruction.Operand;
        }

        public FieldInfo Field { get; private set; }

        public override void Execute(ILStack stack)
        {
            if (!Field.IsStatic)
            {
                base.Execute(stack);
            }

            this.ReturnType = Field.FieldType.MakeByRefType();
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }
    }
}