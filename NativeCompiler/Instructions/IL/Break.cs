namespace NativeCompiler.Instructions.IL
{
    public class Break : NoOperandsOperation, IStatementOperation
    {
        // Trigger breakpoint; ignore.
        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(void);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { return this; }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return default(T); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }
    }
}