using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace NativeCompiler.Instructions.IL
{
    public class Ret : StackOperationBase, IStatementOperation
    {
        private IStackOperation argument = null;

        public IStackOperation Argument { get { return argument; } }

        public override Type ReturnType { get; set; }
        public override void Execute(ILStack stack)
        {
            var method = stack.Method as MethodInfo;
            ReturnType = typeof(void);

            if (method != null && method.ReturnType != typeof(void))
            {
                this.argument = stack.Pop();
            }
        }

        public override IEnumerable<IStackOperation> Children { get { if (argument != null) { yield return argument; } } }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override IStackOperation Rewrite(StackRewriterBase rewriter)
        {
            if (argument != null)
            {
                argument = argument.Rewrite(rewriter);
            }
            return rewriter.Rewrite(this);
        }

        public override int DeltaStackDepth
        {
            get { return ReturnType == typeof(void) ? 0 : -1; }
        }

        public override void Write(System.IO.TextWriter sb)
        {
            if (argument == null)
            {
                sb.Write("return;");
            }
            else
            {
                sb.Write("return ");
                argument.Write(sb);
                sb.Write(';');
            }
        }

        public override string ToString()
        {
            System.IO.StringWriter sb = new System.IO.StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }
}