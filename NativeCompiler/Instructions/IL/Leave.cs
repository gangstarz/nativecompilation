using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class Leave : NoOperandsOperation, IBranch, IStatementOperation
    {
        public Leave(ILInstruction instruction)
        {
            LeaveOffset = (int)instruction.Operand;
        }

        public int LeaveOffset;

        public override void Execute(ILStack stack)
        {
            stack.MarkOffset(LeaveOffset);

            base.Execute(stack);
            this.ReturnType = typeof(void);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }

        public int TargetOffset
        {
            get { return LeaveOffset; }
        }
    }
}