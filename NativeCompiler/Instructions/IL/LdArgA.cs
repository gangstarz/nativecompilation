using System.Reflection.Emit;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdArgA : NoOperandsOperation
    {
        public LdArgA(ILInstruction instruction)
        {
            if (instruction.Code == OpCodes.Ldarga)
            {
                this.N = (int)instruction.Operand;
            }
            else
            {
                this.N = (byte)instruction.Operand;
            }
        }

        public LdArgA(int n)
        {
            this.N = n;
        }

        public int N { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);

            if (stack.Method.IsStatic)
            {
                this.ReturnType = stack.MethodParameters[N].ParameterType.MakeByRefType();
            }
            else if (N == 0)
            {
                this.ReturnType = stack.Method.DeclaringType.MakeByRefType();
            }
            else
            {
                this.ReturnType = stack.MethodParameters[N - 1].ParameterType.MakeByRefType();
            }

            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 1; }
        }
    }
}