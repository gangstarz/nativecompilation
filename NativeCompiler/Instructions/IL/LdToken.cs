using System;
using System.Reflection;
using NativeCompiler.Decompiler;
using NativeCompiler.Instructions.SpecialTypes;

namespace NativeCompiler.Instructions.IL
{
    public class LdToken : NoOperandsOperation
    {
        public LdToken(ILInstruction instruction)
        {
            this.Operand = (MemberInfo)instruction.Operand;
        }

        public MemberInfo Operand { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(MemberInfo);
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 1; }
        }
    }
}