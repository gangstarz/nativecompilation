using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdArg : LdArgN
    {
        public LdArg(ILInstruction instruction) : base((int)instruction.Operand) { }
    }
}