using System.Reflection;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class StFld : TwoOrSingleOperandsOperation, IStatementOperation
    {
        public StFld(ILInstruction instruction)
        {
            this.Field = (FieldInfo)instruction.Operand;
        }

        public FieldInfo Field { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.ExecuteImpl(stack, !Field.IsStatic);
            this.ReturnType = typeof(void);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return Field.IsStatic ? -1 : -2; }
        }
    }
}