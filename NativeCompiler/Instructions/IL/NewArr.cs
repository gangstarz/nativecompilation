using System;
using System.Text;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class NewArr : SingleOperandsOperation
    {
        public NewArr(ILInstruction instruction)
        {
            this.Type = (Type)instruction.Operand;
        }

        public Type Type { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = Type.MakeArrayType();
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }

        public override void Write(System.IO.TextWriter sb)
        {
            sb.Write("new ");
            sb.Write(Type.FullName);
            sb.Write('[');
            if (child == null) { sb.Write("??"); } else { child.Write(sb); }
            sb.Write(']');
        }

        public override string ToString()
        {
            System.IO.StringWriter sb = new System.IO.StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }
}