using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdcR8 : NoOperandsOperation
    {
        public LdcR8(ILInstruction op)
        {
            this.N = (double)op.Operand;
        }

        public double N { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(double);
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 1; }
        }
    }
}