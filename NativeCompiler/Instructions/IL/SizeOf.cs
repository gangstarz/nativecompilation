using System;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class SizeOf : NoOperandsOperation
    {
        public SizeOf(ILInstruction instruction)
        {
            this.Type = (Type)instruction.Operand;
        }

        public Type Type { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(int);
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 1; }
        }
    }
}