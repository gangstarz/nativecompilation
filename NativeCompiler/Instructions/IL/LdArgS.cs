using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdArgS : LdArgN
    {
        public LdArgS(ILInstruction instruction) : base((byte)instruction.Operand) { }
    }
}