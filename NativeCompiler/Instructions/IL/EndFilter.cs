﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class EndFilter : NoOperandsOperation, IStatementOperation
    {
        public EndFilter(ILInstruction instruction)
        {
            this.instruction = instruction;
        }

        private ILInstruction instruction;

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }
    }
}
