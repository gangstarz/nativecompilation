namespace NativeCompiler.Instructions.IL
{
    public class LdArgN : NoOperandsOperation
    {
        public LdArgN(int n)
        {
            this.N = n;
        }

        public int N { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);

            if (stack.Method.IsStatic)
            {
                this.ReturnType = stack.MethodParameters[N].ParameterType;
            }
            else if (N == 0)
            {
                this.ReturnType = stack.Method.DeclaringType;
            }
            else
            {
                this.ReturnType = stack.MethodParameters[N - 1].ParameterType;
            }

            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 1; }
        }
    }
}