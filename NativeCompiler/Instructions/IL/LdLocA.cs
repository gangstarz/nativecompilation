using System.Reflection.Emit;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdLocA : NoOperandsOperation
    {
        public LdLocA(ILInstruction op)
        {
            if (op.Code == OpCodes.Ldloca_S)
            {
                this.N = (byte)op.Operand;
            }
            else
            {
                this.N = (int)op.Operand;
            }
        }

        public int N { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = stack.Variables[N];
            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 1; }
        }
    }
}