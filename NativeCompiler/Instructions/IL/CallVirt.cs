using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class CallVirt : StackOperationBase
    {
        public CallVirt(ILInstruction instruction)
        {
            this.Method = (MethodBase)instruction.Operand;
        }

        public MethodBase Method { get; private set; }
        public IStackOperation MethodObject = null;
        public List<IStackOperation> Parameters = null;

        public override void Execute(ILStack stack)
        {
            Parameters = new List<IStackOperation>();

            for (int i = 0; i < Method.GetParameters().Length; ++i)
            {
                Parameters.Add(stack.Pop());
            }
            Parameters.Reverse();

            if (!Method.IsStatic)
            {
                MethodObject = stack.Pop();
            }

            this.ReturnType = typeof(void);
            if (Method is MethodInfo)
            {
                var rt = ((MethodInfo)Method).ReturnType;
                if (rt != typeof(void))
                {
                    this.ReturnType = rt;
                    stack.Push(this);
                }
            }
        }

        public override bool IsImplicitStatement { get { return this.ReturnType == typeof(void); } }

        public override Type ReturnType { get; set; }

        public override IEnumerable<IStackOperation> Children
        {
            get
            {
                if (MethodObject != null)
                {
                    yield return MethodObject;
                }
                foreach (var par in Parameters)
                {
                    yield return par;
                }
            }
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter)
        {
            if (MethodObject != null)
            {
                MethodObject = MethodObject.Rewrite(rewriter);
            }
            for (int i = 0; i < Parameters.Count; ++i)
            {
                Parameters[i] = Parameters[i].Rewrite(rewriter);
            }
            return rewriter.Rewrite(this);
        }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get
            {
                MethodInfo mi = Method as MethodInfo;
                int n = (mi != null && mi.ReturnType != typeof(void)) ? 1 : 0;
                n -= Method.GetParameters().Length;
                if (!Method.IsStatic)
                {
                    n--;
                }
                return n;
            }
        }

        public override void Write(System.IO.TextWriter sb)
        {
            if (this.MethodObject == null)
            {
                sb.Write(Method.DeclaringType.FriendlyName());
            }
            else
            {
                this.MethodObject.Write(sb);
            }

            sb.Write('.');
            sb.Write(Method.Name);
            sb.Write('(');
            bool first = true;
            if (Parameters == null)
            {
                sb.Write("??");
            }
            else
            {
                foreach (var par in Parameters)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        sb.Write(", ");
                    }
                    par.Write(sb);
                }
            }
            sb.Write(')');
        }

        public override string ToString()
        {
            System.IO.StringWriter sb = new System.IO.StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }
}