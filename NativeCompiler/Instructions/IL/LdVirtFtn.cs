﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Instructions.IL
{
    public class LdVirtFtn : SingleOperandsOperation
    {
        public LdVirtFtn(ILInstruction il)
        {
            this.Method = (MethodBase)il.Operand;
        }

        public MethodBase Method { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);

            this.ReturnType = SpecialTypes.Method.Type;

            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }
        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 0; }
        }
    }
}
