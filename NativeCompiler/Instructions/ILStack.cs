﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace NativeCompiler.Instructions
{
    public class ILStack
    {
        public ILStack(MethodBase method, int initialStackDepth)
        {
            this.Method = method;
            this.MethodParameters = method.GetParameters();
            this.Variables = method.GetMethodBody().LocalVariables.Select((a) => a.LocalType).ToArray();
        }

        public MethodBase Method { get; private set; }
        public ParameterInfo[] MethodParameters { get; private set; }
        public Type[] Variables { get; private set; }

        public Stack<IStackOperation> operations = new Stack<IStackOperation>();
        public List<StackStub> negativeStack = new List<StackStub>();
        private int negativeStackCtr = 0;

        public HashSet<int> LabelOffsets = new HashSet<int>();

        public void MarkOffset(int offset)
        {
            LabelOffsets.Add(offset);
        }

        public IStackOperation Pop()
        {
            if (operations.Count > 0)
            {
                return operations.Pop();
            }
            else
            {
                ++negativeStackCtr;
                if (negativeStack.Count < negativeStackCtr)
                {
                    negativeStack.Add(new StackStub(negativeStackCtr));
                }

                return negativeStack[negativeStackCtr - 1];
            }
        }

        public void Push(IStackOperation op)
        {
            operations.Push(op);
        }

        public bool Empty { get { return this.operations.Count == 0; } }

        public void Dump()
        {
            var ops = operations.ToList();
            for (int i = 0; i < ops.Count; ++i)
            {
                Console.WriteLine("Stack {0}: {1}", i, ops[i]);
            }
            Console.WriteLine();
        }

        public void AssertEmpty(int offset)
        {
            if (operations.Count > 0)
            {
                Console.WriteLine("Stack not empty after end of method. Offset {0:x4}, stack:", offset);
                Dump();

                throw new Exception("Stack is not empty");
            }
        }
    }
}
