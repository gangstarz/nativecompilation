using System;
using System.Collections.Generic;
using System.Text;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Instructions
{
    public abstract class ArrayStoreOperandsOperation : StackOperationBase
    {
        public IStackOperation array;
        public IStackOperation index;
        public IStackOperation value;

        public override void Execute(ILStack stack)
        {
            this.array = stack.Pop();
            this.index = stack.Pop();
            this.value = stack.Pop();
        }

        public override Type ReturnType { get; set; }

        public override IEnumerable<IStackOperation> Children { get { yield return array; yield return index; yield return value; } }

        protected void RewriteChildren(StackRewriterBase rewriter)
        {
            array = array.Rewrite(rewriter);
            index = index.Rewrite(rewriter);
            value = value.Rewrite(rewriter);
        }

        public override void Write(System.IO.TextWriter sb)
        {
            sb.Write(this.GetType().Name);
            sb.Write('(');
            if (array == null) { sb.Write("??"); } else { array.Write(sb); }
            sb.Write(',');
            if (index == null) { sb.Write("??"); } else { index.Write(sb); }
            sb.Write(',');
            if (value == null) { sb.Write("??"); } else { value.Write(sb); }
            sb.Write(')');
        }

        public override string ToString()
        {
            System.IO.StringWriter sb = new System.IO.StringWriter();
            Write(sb);
            return sb.ToString();
        }
    }
}