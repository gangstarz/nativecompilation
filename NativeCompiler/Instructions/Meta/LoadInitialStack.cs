﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Instructions.Meta
{
    public class LoadInitialStack : NoOperandsOperation
    {
        public LoadInitialStack(int id)
        {
            this.Id = id;
        }

        public int Id { get; private set; }

        public override void Execute(ILStack stack)
        {
            base.Execute(stack);
            this.ReturnType = typeof(object); // TODO FIXME!

            stack.Push(this);
        }

        public override IStackOperation Rewrite(StackRewriterBase rewriter) { base.RewriteChildren(rewriter); return rewriter.Rewrite(this); }

        public override T Visit<T>(IInstructionVisitor<T> rewriter) { return rewriter.Visit(this); }

        public override int DeltaStackDepth
        {
            get { return 1; }
        }
    }
}
