using System;
using System.Collections.Generic;
using System.Text;
using NativeCompiler.Instructions.IL;

namespace NativeCompiler.Instructions
{
    public interface IStackOperation
    {
        Type ReturnType { get; set; }
        void Execute(ILStack stack);

        IEnumerable<IStackOperation> Children { get; }
        IStackOperation Rewrite(StackRewriterBase rewriter);
        T Visit<T>(IInstructionVisitor<T> visitor);

        void Write(System.IO.TextWriter sb);

        int Offset { get; set; }
        int StackDepth { get; set; }
        int DeltaStackDepth { get; }

        bool IsImplicitStatement { get; }
    }
}