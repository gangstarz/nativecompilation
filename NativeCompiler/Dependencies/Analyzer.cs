﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Dependencies
{
    public class Analyzer
    {
        public Analyzer(string assembly, bool stayInAssembly = false)
        {
            this.assemblyFile = assembly;
            this.stayInAssembly = stayInAssembly;
        }

        private string assemblyFile;
        private bool stayInAssembly;
        private Assembly originatingAssembly;

        private Dictionary<Type, DependentType> dependencies = new Dictionary<Type, DependentType>();
        private MethodBase main;

        public MethodBase Main { get { return main; } }

        internal const BindingFlags AllMembers =
            BindingFlags.CreateInstance | BindingFlags.GetField | BindingFlags.GetProperty | BindingFlags.Instance |
            BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Public |
            BindingFlags.SetField | BindingFlags.SetProperty | BindingFlags.Static;

        public void AnalyzeSingleAssembly(Assembly ass)
        {
            AnalyzeTypes(ass.GetTypes());
        }

        public void AnalyzeSingleType(Type initType)
        {
            AnalyzeTypes(initType);
        }

        private void AnalyzeTypes(params Type[] initType)
        {
            HashSet<Type> done = new HashSet<Type>();
            Stack<Type> todo = new Stack<Type>();
            foreach (var it in initType)
            {
                todo.Push(it);
                done.Add(it);
            }

            while (todo.Count > 0)
            {
                var type = todo.Pop();
                if (type.IsGenericType && !type.IsGenericTypeDefinition)
                {
                    type = type.GetGenericTypeDefinition();
                }

                DependentType dt = new DependentType(type);
                dependencies.Add(dt.Type, dt);

                foreach (var member in dt.Type.GetMembers(AllMembers))
                {
                    PropertyInfo prop = member as PropertyInfo;
                    if (prop != null)
                    {
                        var getter = prop.GetGetMethod();
                        var setter = prop.GetSetMethod();

                        if (getter != null)
                        {
                            dt.AddMethod(getter).Count();
                        }

                        if (setter != null)
                        {
                            dt.AddMethod(setter).Count();
                        }
                    }
                    else if (member is MethodBase)
                    {
                        MethodBase mb = (MethodBase)member;
                        dt.AddMethod(mb).Count();
                    }
                    else if (member is Type)
                    {
                        if (done.Add((Type)member))
                        {
                            todo.Push((Type)member);
                        }
                    }
                    else if (member is EventInfo)
                    {
                        var e = (EventInfo)member;
                        dt.AddEvent(e);
                    }
                    else if (member is FieldInfo)
                    {
                        var f = (FieldInfo)member;
                        dt.AddField(f);
                    }
                    else
                    {
                        throw new NotSupportedException();
                    }
                }
            }

            FinalizeTypes();

            Console.WriteLine("Done analyzing type.");
        }

        public void AnalyzeDependencies(MethodBase main = null)
        {
            if (main == null)
            {
                this.main = main = FindMain();
            }
            else
            {
                this.main = main;
            }

            Dictionary<Type, List<Type>> inheritance = new Dictionary<Type, List<Type>>();

            Dictionary<Type, DependentType> types = new Dictionary<Type, DependentType>();
            HashSet<MethodBase> hash = new HashSet<MethodBase>();
            Stack<MethodBase> methods = new Stack<MethodBase>();

            this.originatingAssembly = stayInAssembly ? main.DeclaringType.Assembly : null;

            methods.Push(main);
            while (methods.Count > 0)
            {
                var current = methods.Pop();

                DependentType dt;
                var mtype = current.DeclaringType;

                if (originatingAssembly != null && mtype.Assembly != originatingAssembly)
                {
                    continue;
                }

                if (!types.TryGetValue(mtype, out dt) && mtype != typeof(object))
                {
                    if (!mtype.IsArray && !mtype.IsGenericTypeDefinition)
                    {
                        dt = AddToInheritanceChain(mtype, types, methods);
                    }
                }

                foreach (var method in dt.AddMethod(current))
                {
                    if (hash.Add(method))
                    {
                        methods.Push(method);
                    }
                }

                //Console.WriteLine("Handling {0}", current);

                var typesToProcess = new HashSet<Type>();
                var decompiled = new DecompiledMethod(current);

                #region Handle decompiled items
                foreach (var instr in decompiled.Instructions)
                {
                    var member = instr.Operand as MemberInfo;
                    if (member != null)
                    {
                        var type = member.DeclaringType;

                        if (type != null && !types.TryGetValue(type, out dt) && type != typeof(object))
                        {
                            if (!type.IsArray && !type.IsGenericTypeDefinition)
                            {
                                dt = AddToInheritanceChain(type, types, methods);
                            }
                        }

                        PropertyInfo prop = member as PropertyInfo;
                        if (prop != null)
                        {
                            var getter = prop.GetGetMethod();
                            var setter = prop.GetSetMethod();

                            if (getter != null && hash.Add(getter))
                            {
                                methods.Push(getter);
                            }

                            if (setter != null && hash.Add(setter))
                            {
                                methods.Push(setter);
                            }

                            if (prop.PropertyType.IsClass || prop.PropertyType.IsInterface)
                            {
                                typesToProcess.Add(prop.PropertyType);
                            }
                        }
                        else if (member is MethodBase)
                        {
                            MethodBase mb = (MethodBase)member;

                            if (hash.Add(mb) && mb.DeclaringType != typeof(object))
                            {
                                methods.Push(mb);

                                var mi = mb as MethodInfo;
                                if (mi != null && mi.ReturnType != typeof(void) && (mi.ReturnType.IsClass || mi.ReturnType.IsInterface))
                                {
                                    typesToProcess.Add(mi.ReturnType);
                                }

                                typesToProcess.Add(mb.DeclaringType);

                                foreach (var par in mb.GetParameters())
                                {
                                    if (par.ParameterType.IsClass || par.ParameterType.IsInterface)
                                    {
                                        typesToProcess.Add(par.ParameterType);
                                    }
                                }
                            }
                        }
                        else if (member is Type)
                        {
                            Type t = (Type)member;

                            if (t.IsClass || t.IsInterface)
                            {
                                typesToProcess.Add(t);
                            }
                        }
                        else if (member is EventInfo)
                        {
                            var e = (EventInfo)member;
                            dt.AddEvent(e);

                            if (e.EventHandlerType.IsClass || e.EventHandlerType.IsInterface)
                            {
                                typesToProcess.Add(e.EventHandlerType);
                            }
                        }
                        else if (member is FieldInfo)
                        {
                            var f = (FieldInfo)member;
                            dt.AddField(f);

                            if (f.FieldType.IsClass || f.FieldType.IsInterface)
                            {
                                typesToProcess.Add(f.FieldType);
                            }
                        }
                        else
                        {
                            throw new NotSupportedException();
                        }
                    }
                }
                #endregion

                foreach (var type in typesToProcess)
                {
                    // Voeg type toe bij alle base types als 'derived';
                    // Voeg alle derived methods toe

                    if (!types.ContainsKey(type) && type != typeof(object) && !type.HasElementType)
                    {
                        if (this.originatingAssembly == null || type.Assembly == this.originatingAssembly)
                        {
                            Console.WriteLine("Adding type {0}", type);

                            if (!type.IsArray && !type.IsGenericTypeDefinition)
                            {
                                AddToInheritanceChain(type, types, methods);
                            }
                        }
                    }
                }
            }

            this.dependencies = types;

            Console.WriteLine("Done analyzing dependencies.");

            FinalizeTypes();
        }

        private void FinalizeTypes()
        {
            foreach (var dt in this.dependencies.Values)
            {
                dt.FinalizeType();
            }
        }

        public Dictionary<Type, DependentType> Results { get { return dependencies; } }

        public void Dump()
        {
            foreach (var val in Results.Values)
            {
                Console.WriteLine("== Type: {0} ==", val.Type.FriendlyName());
                Console.WriteLine();
                foreach (var method in val.Methods.Values)
                {
                    method.Dump();
                    Console.WriteLine();
                }
            }
        }

        private DependentType AddToInheritanceChain(Type type, Dictionary<Type, DependentType> types, Stack<MethodBase> methods)
        {
            // Console.WriteLine(" - Adding type to inheritance chain: {0}", type);

            DependentType dt = new DependentType(type);

            // Make a lookup of all base methods of all methods in type
            HashSet<MethodBase> lookup = new HashSet<MethodBase>();
            foreach (var method in type.GetMembers(AllMembers).OfType<MethodBase>())
            {
                var tmp = method;
                while (tmp != null)
                {
                    lookup.Add(tmp);
                    if (tmp is MethodInfo)
                    {
                        var newTmp = ((MethodInfo)tmp).GetBaseDefinition();
                        if (!object.ReferenceEquals(newTmp, tmp))
                        {
                            tmp = newTmp;
                        }
                        else
                        {
                            tmp = null;
                        }
                    }
                    else
                    {
                        tmp = null;
                    }
                }

                lookup.Add(tmp);
            }

            // Add all derived types to this type
            foreach (var t in types)
            {
                if (type.IsAssignableFrom(t.Key))
                {
                    //                    Console.WriteLine(" - Found derived type {0}", t.Key);
                    dt.Derived.Add(t.Value);
                }
            }

            // Add this type to all base types
            Stack<Type> baseTypes = new Stack<Type>();
            baseTypes.Push(type);
            while (baseTypes.Count > 0)
            {
                var cur = baseTypes.Pop();
                foreach (var intf in cur.GetInterfaces())
                {
                    baseTypes.Push(intf);
                }
                if (cur.BaseType != null && cur.BaseType != typeof(object))
                {
                    baseTypes.Push(cur.BaseType);
                }

                // handle current type
                DependentType baseType;
                if (types.TryGetValue(cur, out baseType))
                {
                    // Add 'dt' to derived types list in 'baseType'
                    if (baseType.Derived.Add(dt))
                    {
                        if (baseType.Type.IsInterface)
                        {
                            if (!type.IsInterface)
                            {
                                var map = type.GetInterfaceMap(baseType.Type);
                                for (int i = 0; i < map.TargetMethods.Length; ++i)
                                {
                                    if (map.TargetMethods[i].DeclaringType == type)
                                    {
                                        if (lookup.Contains(map.TargetMethods[i]))
                                        {
                                            //                                            Console.WriteLine(" - Found derived method {0}", map.TargetMethods[i]);
                                            methods.Push(map.TargetMethods[i]);
                                        }

                                    }
                                }
                            }
                            // else there is no implementation since interfaces work like contracts
                        }
                        else
                        {
                            // Add all methods in the derived type
                            foreach (var method in baseType.Methods)
                            {
                                if (lookup.Contains(method.Key))
                                {
                                    //                                    Console.WriteLine(" - Found derived method {0}", method.Key);
                                    methods.Push(method.Key);
                                }
                            }
                        }
                    }
                }
            }

            types.Add(type, dt);

            return dt;
        }

        private MethodInfo FindMain()
        {
            // Find all dependencies:
            // 
            // Makes base/intf -> derived combinations.
            Assembly ass = Assembly.LoadFrom(assemblyFile);

            // Find 'Main'
            MethodInfo main = null;
            foreach (var type in ass.GetTypes())
            {
                foreach (var meth in type.GetMethods(AllMembers).OfType<MethodInfo>())
                {
                    if (meth.Name == "Main")
                    {
                        main = meth;
                        break;
                    }
                }
                if (main != null)
                {
                    break;
                }
            }

            if (main == null)
            {
                throw new Exception("No start point found.");
            }
            else
            {
                return main;
            }
        }
    }
}
