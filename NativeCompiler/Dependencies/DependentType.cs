﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler.Dependencies
{
    public class DependentType : IEquatable<DependentType>
    {
        public DependentType(Type t)
        {
            this.Type = t;
        }

        public Type Type { get; private set; }
        public HashSet<DependentType> Derived = new HashSet<DependentType>();
        public Dictionary<MethodBase, DecompiledMethod> Methods = new Dictionary<MethodBase, DecompiledMethod>();
        public Dictionary<MemberInfo, string> ClassMembers { get; private set; }

        public HashSet<EventInfo> Events = new HashSet<EventInfo>();
        public HashSet<FieldInfo> Fields = new HashSet<FieldInfo>();

        private void CreateMemberMap()
        {
            Dictionary<MemberInfo, string> classMembers = new Dictionary<MemberInfo, string>();

            // Fields
            foreach (var field in this.Fields)
            {
                // For simplicity, make them public.
                classMembers.Add(field, field.Name);
            }

            // Events
            foreach (var evt in this.Events)
            {
                classMembers.Add(evt, evt.Name);
            }

            // Methods are also members, which can be referenced as 'function pointer'.
            foreach (var meth in this.Methods)
            {
                classMembers.Add(meth.Key, meth.Key.Name);
            }
            
            this.ClassMembers = classMembers;
        }

        public void FinalizeType()
        {
            CreateMemberMap();
        }

        public void AddEvent(EventInfo member)
        {
            Events.Add(member);
        }

        public void AddField(FieldInfo member)
        {
            Fields.Add(member);
        }

        public IEnumerable<MethodBase> AddMethod(MethodBase method)
        {
            // Add method
            if (!Methods.ContainsKey(method))
            {
                //                Console.WriteLine("Adding method {0} to type {1}", method, this.Type);
                Methods.Add(method, new DecompiledMethod(method));

                // Update derived classes
                foreach (var derived in this.Derived)
                {
                    //Console.WriteLine(" - Scanning for derived method in type {0}", derived.Type);

                    // If the method is also implemented in the derived class, add it.
                    if (this.Type.IsInterface)
                    {
                        if (derived.Type.IsClass)
                        {
                            InterfaceMapping map = derived.Type.GetInterfaceMap(this.Type);

                            for (int i = 0; i < map.TargetMethods.Length; ++i)
                            {
                                if (map.InterfaceMethods[i].Equals(method))
                                {
                                    var target = map.TargetMethods[i];
                                    if (target.DeclaringType == derived.Type)
                                    {
                                        if (!derived.Methods.ContainsKey(target))
                                        {
                                            derived.Methods.Add(target, new DecompiledMethod(target));
                                            yield return target;
                                        }

                                        break;
                                    }
                                }
                            }
                        }
                        // else cannot derive interface from interface
                    }
                    else
                    {
                        foreach (var derMethod in derived.Type.GetMembers(Analyzer.AllMembers).OfType<MethodBase>())
                        {
                            if (derMethod.DeclaringType == derived.Type)
                            {
                                var m = derMethod;
                                while (m != null)
                                {
                                    if (method == m && !derived.Methods.ContainsKey(derMethod))
                                    {
                                        derived.Methods.Add(derMethod, new DecompiledMethod(derMethod));
                                        yield return derMethod;

                                        break;
                                    }

                                    if (m is MethodInfo)
                                    {
                                        var tmp = ((MethodInfo)m).GetBaseDefinition();
                                        if (object.ReferenceEquals(tmp, m))
                                        {
                                            m = null;
                                        }
                                        else
                                        {
                                            m = tmp;
                                        }
                                    }
                                    else
                                    {
                                        m = null;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public bool Equals(DependentType other)
        {
            return other.Type.Equals(Type);
        }

        public override bool Equals(object obj)
        {
            if (obj is Type)
            {
                Type t = (Type)obj;
                return t.Equals(Type);
            }
            else if (obj is DependentType)
            {
                DependentType dt = (DependentType)obj;
                return dt.Type.Equals(Type);
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode();
        }

        public override string ToString()
        {
            return Type.ToString();
        }

    }
}
