﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NativeCompiler.Decompiler;

namespace NativeCompiler.Dependencies
{
    public class DecompiledMethod
    {
        public DecompiledMethod(MethodBase method)
        {
            this.Method = method;
            DecompileMethod(method);
        }

        public MethodBase Method;
        public ILInstruction[] Instructions;
        public Decompiler.Blocks.Block MethodBlock;

        private static ILInstruction[] NoInstructions = new ILInstruction[0];

        private void DecompileMethod(MethodBase method)
        {
            this.Instructions = NoInstructions;
            var body = method.GetMethodBody();
            if (body != null)
            {
                var bytes = body.GetILAsByteArray();
                if (bytes != null)
                {
                    this.Instructions = Decompiler.Decompiler.Decompile(method, bytes).ToArray();
                }
            }
        }

        public void GenerateBlocks()
        {
            var body = this.Method.GetMethodBody();
            if (body != null)
            {
                this.MethodBlock = Decompiler.Blocks.BlockBuilder.Build(body);
                int ptr = 0;

                // MethodBlock.Length is always the # bytes of IL code
                this.MethodBlock.Fill(Instructions, MethodBlock.Length, ref ptr);

                if (ptr != Instructions.Length)
                {
                    throw new Exception("Error doing blocking operation: instructions are still present in body after blocking.");
                }
            }
            else
            {
                this.MethodBlock = null;
            }
        }

        public void AnalyzeBlocks()
        {
            try
            {
                var body = this.Method.GetMethodBody();
                if (body != null)
                {
                    this.MethodBlock.Analyze(Method, body);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
                this.MethodBlock.DumpAnalyzed();
                throw;
            }
        }

        public void PrepareDecompile()
        {
            try
            {
                var body = this.Method.GetMethodBody();
                if (body != null)
                {
                    this.MethodBlock.ZeroAlign(Method, body);

                    // Flow analysis
                    var fa = new Decompiler.Blocks.FlowAnalysis();
                    fa.Analyze(this.MethodBlock);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
                this.MethodBlock.DumpAnalyzed();
                throw;
            }
        }

        public void Dump()
        {
            Console.WriteLine("Method: {0}", Method.FriendlyName());
            Console.WriteLine("--------------------------------------");
            if (MethodBlock == null)
            {
                Console.WriteLine("No IL implementation; abstract, interface or P/Invoke.");
            }
            else
            {
                MethodBlock.DumpAnalyzed();
            }
        }

        public IEnumerable<Type> GetDependentTypes()
        {
            foreach (var instr in this.Instructions)
            {
                if (instr.Operand is Type)
                {
                    yield return (Type)instr.Operand;
                }
                else if (instr.Operand is FieldInfo)
                {
                    var field = (FieldInfo)instr.Operand;
                    yield return field.FieldType;
                    if (field.DeclaringType != this.Method.DeclaringType)
                    {
                        yield return field.DeclaringType;
                    }
                }
                else if (instr.Operand is MethodBase)
                {
                    MethodBase mb = (MethodBase)instr.Operand;
                    if (mb.DeclaringType != this.Method.DeclaringType)
                    {
                        yield return mb.DeclaringType;
                    }
                    if (mb is MethodInfo)
                    {
                        yield return ((MethodInfo)mb).ReturnType;
                    }
                    foreach (var par in mb.GetParameters())
                    {
                        yield return par.ParameterType;
                    }
                }
            }
        }

        public override bool Equals(object obj)
        {
            return obj is DecompiledMethod && Method.Equals(((DecompiledMethod)obj).Method);
        }

        public override int GetHashCode()
        {
            return this.Method.GetHashCode();
        }

        public override string ToString()
        {
            return Method.ToString();
        }
    }
}
