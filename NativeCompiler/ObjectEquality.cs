﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NativeCompiler
{
    internal class ObjectEquality<T> : IEqualityComparer<T>
    {
        private ObjectEquality() { }

        public static readonly ObjectEquality<T> Instance = new ObjectEquality<T>();

        public bool Equals(T x, T y)
        {
            return object.ReferenceEquals(x, y);
        }

        public int GetHashCode(T obj)
        {
            return RuntimeHelpers.GetHashCode(obj);
        }
    }
}
