﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
    public interface MyInterface
    {
        Type GetTypeNS1();
        Type GetTypeNS2();
    }

    public class InnerClassTest<T>
    {
        public class InnerClass<U> : MyInterface
        {
            public static Type GetType1()
            {
                return typeof(T);
            }

            public static Type GetType2()
            {
                return typeof(U);
            }

            public Type GetTypeNS1()
            {
                return typeof(T);
            }

            public Type GetTypeNS2()
            {
                return typeof(U);
            }

            public void Test()
            {
                AccessibleMethod<T>();
            }
        }

        public static void AccessibleMethod<U>()
        {
            if (typeof(U) != typeof(T))
            {
                Console.WriteLine("Expected types T and U to be the same.");
            }
        }
    }

    public class InnerClassTest
    {
        public static void Test()
        {
            if (InnerClassTest<string>.InnerClass<object>.GetType1() != typeof(string))
            {
                Console.WriteLine("Expected inner type to be typeof string");
            }

            if (InnerClassTest<string>.InnerClass<object>.GetType2() != typeof(object))
            {
                Console.WriteLine("Expected inner type to be typeof object");
            }

            var ic1 = new InnerClassTest<string>.InnerClass<object>();
            ic1.Test();
            Test2(ic1);
        }

        private static void Test2<T>(T instance) where T : MyInterface
        {
            if (instance.GetTypeNS1() != typeof(string))
            {
                Console.WriteLine("Expected inner type to be typeof string");
            }

            if (instance.GetTypeNS2() != typeof(object))
            {
                Console.WriteLine("Expected inner type to be typeof object");
            }
        }
    }
}
