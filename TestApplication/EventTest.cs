﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
    public delegate void TestEvent(int id);

    public class EventTest
    {
        public event TestEvent MyEvent;
        private int ctr = 0;

        public void IncDel1(int id)
        {
            ++ctr;
        }

        public void IncDel2(int id)
        {
            ctr += id;
        }

        public void TestEvent()
        {
            MyEvent += IncDel1;
            MyEvent(2);
            if (ctr != 1)
            {
                Console.WriteLine("Event should have triggered IncDel1");
            }
            ctr = 0;

            MyEvent += IncDel2;
            MyEvent(2);
            if (ctr != 3)
            {
                Console.WriteLine("Event should have triggered IncDel1, IncDel2");
            }
            ctr = 0;

            MyEvent += IncDel2;
            MyEvent(3);
            if (ctr != 7)
            {
                Console.WriteLine("Event should have triggered IncDel1, IncDel2, IncDel2");
            }
            ctr = 0;

            MyEvent -= IncDel2;
            MyEvent(4);
            if (ctr != 5)
            {
                Console.WriteLine("Event should have triggered IncDel1, IncDel2");
            }

            MyEvent -= IncDel2;
            MyEvent -= IncDel1;

            if (MyEvent != null)
            {
                Console.WriteLine("Empty events are 'null'.");
            }

            bool ok = false;
            try
            {
                MyEvent(0);
            }
            catch (NullReferenceException)
            {
                ok = true;
            }

            if (!ok)
            {
                Console.WriteLine("Empty events should evaluate to null and therefore throw a null reference exception");
            }
        }
    }
}
