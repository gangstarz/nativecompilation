﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
    public class Program
    {
        static void TestIf1(int n, int m) // n = 1, m = 2
        {
            if (n == m || n > m || n >= m)
            {
                Console.WriteLine("Error in 'TestIf1'");
            }
        }

        static void TestIf2(int n, int m) // n = 2, m = 1
        {
            if ((n == m || n < m || n <= m) && (m > 2 || (n > 2 && m > 10)))
            {
                Console.WriteLine("Error in 'TestIf2'");
            }
        }

        static void TestImplicitArrayAssign()
        {
            int sum = 0;
            var ids = new int[] { 1, 2, 3, 4, 5 };
            foreach (var id in ids)
            {
                sum += id;
            }

            if (sum != 15)
            {
                Console.WriteLine("Error in 'TestImplicitArrayAssign'");
            }
        }

        static void TestFor()
        {
            int sum = 0;
            for (int i = 0; i < 10; ++i)
            {
                sum += i;
            }
            if (sum != 45)
            {
                Console.WriteLine("Error in 'TestFor'");
            }
        }

        static void TestWhile()
        {
            int sum = 0;
            int i = 0;
            while (sum < 45)
            {
                i++;
                sum += i;
            }

            if (sum != 45)
            {
                Console.WriteLine("Error in 'TestWhile'");
            }
        }

        static void TestDoWhile()
        {
            int sum = 0;
            int i = 0;
            do
            {
                i++;
                sum += i;
            }
            while (sum < 45);

            if (sum != 45)
            {
                Console.WriteLine("Error in 'TestDoWhile'");
            }
        }
        static void TestSwitch(int id)
        {
            switch (id)
            {
                case 1: return;
                case 2:
                case 3:
                    TestSwitch(id - 1);
                    return;
                default:
                    Console.WriteLine("Error in 'TestSwitch'");
                    break;
            }
        }
  
        static void Main(string[] args)
        {
            /*
            TestIf1(1, 2);
            TestIf2(2, 1);
            TestImplicitArrayAssign();
            TestFor();
            TestWhile();
            TestDoWhile();
            TestSwitch(1);
            TestSwitch(3);

            // Class hierarchy
            DerivedClass1 dc = new DerivedClass1();
            dc.Test();

            TestInterface ti = new TestInterface();
            ti.Test();

            GenericsTest<TestInterface> gt = new GenericsTest<TestInterface>();
            gt.Test();
            //InnerClassTest.Test();

            ValueTypes lf = new ValueTypes();
            lf.TestValueTypes();

            StringTest.Test();

            new ExceptionTest().Test();

            new DelegateTest().Test();
            new DerivedDelegateTest().TestDerived1();
            new DerivedDelegateTest().TestDerived2();
            new EventTest().TestEvent();

            Console.WriteLine("All done.");
             */
        }
    }
}
