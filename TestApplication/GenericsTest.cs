﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
    public interface GenericsInterface<T> where T : IInterface1, new()
    {
        void Test();
    }

    public class GenericsTest<T> : GenericsInterface<T> where T:IInterface1, new()
    {
        public void Test()
        {
            T t = new T();
            if (t.Foo() != 1)
            {
                Console.WriteLine("Generics parameter should resolve to IInterface1.Foo() -> TestInterface.Foo().");
            }
        }
    }
}
