﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
    public abstract class BaseClass
    {
        public void TestInBase()
        {
            if (Foo() != 1)
            {
                Console.WriteLine("Non-virtual call should call BaseClass.Foo()");
            }
            if (VirtualCall() != 4)
            {
                Console.WriteLine("Virtual call with overload base should call DerivedClass1.VirtualCall()");
            }

            if (NewFoo() != 5)
            {
                Console.WriteLine("Non-call with new implementation should call BaseClass.NewFoo()");
            }
        }

        public int Foo()
        {
            return 1;
        }

        public virtual int VirtualCall()
        {
            return 2;
        }

        public abstract int AbstractCall();

        public int NewFoo()
        {
            return 5;
        }
    }

    public class DerivedClass1 : BaseClass
    {
        public void Test()
        {
            if (VirtualCall() != 4)
            {
                Console.WriteLine("Virtual call should call current DerivedClass1.VirtualCall()");
            }
            if (AbstractCall() != 3)
            {
                Console.WriteLine("Abstract call should call current DerivedClass1.AbstractCall()");
            }
            if (base.VirtualCall() != 2)
            {
                Console.WriteLine("Virtual call with explicit base should call BaseClass.VirtualCall()");
            }
            if (Foo() != 1)
            {
                Console.WriteLine("Non-virtual call should call BaseClass.Foo()");
            }
            if (NewFoo() != 6)
            {
                Console.WriteLine("Non-virtual call should call DerivedClass1.NewFoo()");
            }

            TestInBase();
        }

        public override int AbstractCall()
        {
            return 3;
        }

        public override int VirtualCall()
        {
            return 4;
        }

        public new int NewFoo()
        {
            return 6;
        }
    }

    public interface IInterface1
    {
        int Foo();
    }

    public interface IInterface2 : IInterface1
    {
        int Foo();
        int Bar();
    }

    public interface IInterface3
    {
        int Foo();
    }

    public class TestInterface : IInterface2
    {
        public void Test1(IInterface1 intf1)
        {
            if (intf1.Foo() != 1)
            {
                Console.WriteLine("Interface should resolve to TestInterface.Foo().");
            }
        }
        public void Test2(IInterface2 intf2)
        {
            if (intf2.Foo() != 1)
            {
                Console.WriteLine("Interface should resolve to TestInterface.Foo().");
            }
            if (intf2.Bar() != 2)
            {
                Console.WriteLine("Interface should resolve to TestInterface.Bar().");
            }
        }

        public void Test3(TestInterface intf3)
        {
            if (intf3.Foo() != 1)
            {
                Console.WriteLine("Interface should resolve to TestInterface.Foo().");
            }
            if (intf3.Bar() != 2)
            {
                Console.WriteLine("Interface should resolve to TestInterface.Bar().");
            }
        }

        public void Test4(TestInterface cl)
        {
            // Test 'as' cast
            var c1 = cl as IInterface1;
            if (c1 == null)
            {
                Console.WriteLine("Class is instance of IInterface1");
            }
            var c2 = cl as IInterface2;
            if (c2 == null)
            {
                Console.WriteLine("Class is instance of IInterface2");
            }
            var c3 = cl as IInterface3;
            if (c3 != null)
            {
                Console.WriteLine("Class is NOT instance of IInterface3");
            }

            // Test normal cast
            var c4 = (IInterface1)c1;
            if (c4.Foo() != 1)
            {
                Console.WriteLine("Class cast to interface1 should call virtual method impl of Foo.");
            }
            var c5 = (IInterface2)c1;
            if (c5.Foo() != 1)
            {
                Console.WriteLine("Class cast to interface1 should call virtual method impl of Foo.");
            }

            // Test 'is' cast
            if (!(cl is IInterface1))
            {
                Console.WriteLine("Class IS actually an IInterface1.");
            }
            if (!(cl is IInterface2))
            {
                Console.WriteLine("Class IS actually an IInterface1.");
            }
            if (cl is IInterface3)
            {
                Console.WriteLine("Class is actually NOT an IInterface1.");
            }
        }

        public void Test()
        {
            Test1(this);
            Test2(this);
            Test3(this);
            Test4(this);
        }

        public int Foo() { return 1; }
        public int Bar() { return 2; }
    }
}
