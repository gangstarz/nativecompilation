﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
    public delegate int TestDelegate(int id);

    public class DerivedDelegateTest : DelegateTest
    {
        public override int Test1(int id)
        {
            return base.Test1(id) + 10;
        }

        public void TestDerived1()
        {
            TestDelegate del = Test1;
            if (del(1) != 12)
            {
                Console.WriteLine("Delegate 1 with 'this' ptr should return 2.");
            }
            Test2(del, 12);

            del = Test2;
            if (del(1) != 3)
            {
                Console.WriteLine("Delegate 2 with 'this' ptr should return 3.");
            }
            Test2(del, 3);

            this.add = 3;
            if (del(1) != 4)
            {
                Console.WriteLine("Delegate 2 with 'this' ptr should return 4 after changing variable.");
            }
            Test2(del, 4);
        }

    }

    public class DelegateTest
    {
        public void TestDerived2()
        {
            TestDelegate del = Test1;
            if (del(1) != 12)
            {
                Console.WriteLine("Delegate 1 with 'this' ptr should return 2.");
            }
            Test2(del, 12);

            del = Test2;
            if (del(1) != 3)
            {
                Console.WriteLine("Delegate 2 with 'this' ptr should return 3.");
            }
            Test2(del, 3);

            this.add = 3;
            if (del(1) != 4)
            {
                Console.WriteLine("Delegate 2 with 'this' ptr should return 4 after changing variable.");
            }
            Test2(del, 4);
        }

        public int add = 2;

        public int Foo() { return add; }

        public virtual int Test1(int id)
        {
            return id + 1;
        }
        public int Test2(int id)
        {
            return id + Foo();
        }

        public static int STest1(int id)
        {
            return id + 1;
        }
        public static int STest2(int id)
        {
            return id + 2;
        }

        public void Test()
        {
            TestDelegate del = Test1;
            if (del(1) != 2)
            {
                Console.WriteLine("Delegate 1 with 'this' ptr should return 2.");
            }
            Test2(del, 2);

            del = Test2;
            if (del(1) != 3)
            {
                Console.WriteLine("Delegate 2 with 'this' ptr should return 3.");
            }
            Test2(del, 3);

            this.add = 3;
            if (del(1) != 4)
            {
                Console.WriteLine("Delegate 2 with 'this' ptr should return 4 after changing variable.");
            }
            Test2(del, 4);

            del = STest1;
            if (del(1) != 2)
            {
                Console.WriteLine("Delegate 3 with static should return 2.");
            }
            Test2(del, 2);

            del = STest2;
            if (del(1) != 3)
            {
                Console.WriteLine("Delegate 4 with static should return 3.");
            }
            Test2(del, 3);
        }

        public void Test2(TestDelegate del, int val)
        {
            if (del(1) != val)
            {
                Console.WriteLine("Passed delegate value != val");
            }
        }
    }
}
