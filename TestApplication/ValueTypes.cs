﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
    public struct MyValueType
    {
        public int Item;

        public override bool Equals(object obj)
        {
            return obj is MyValueType && ((MyValueType)obj).Item == Item;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class ValueTypes
    {
        public void TestValueTypes()
        {
            MyValueType vt1;
            vt1.Item = 1;

            MyValueType vt2 = new MyValueType() { Item = 2 };

            if (vt1.Item == vt2.Item || vt1.Item != 1 || vt2.Item != 2)
            {
                Console.WriteLine("Error: value type items shouldn't be the same - should be 1 and 2");
            }

            object o1 = vt1;
            MyValueType vt3 = (MyValueType)o1;
            if (object.ReferenceEquals(vt1, vt3))
            {
                Console.WriteLine("Boxing then unboxing should call a copy constructor");
            }

            if (vt1.Equals(vt2))
            {
                Console.WriteLine("Equals call on value type should result in a not-equals");
            }

            vt1.Item = 2;

            if (!vt1.Equals(vt2))
            {
                Console.WriteLine("Equals call on value type should result in a equals");
            }

            if (vt3.Equals(vt2) || vt3.Item != 1)
            {
                Console.WriteLine("Since vt3 is copied, it should still have value 1");
            }

            TestCopyByPass(vt1);
            if (vt1.Item != 2)
            {
                Console.WriteLine("Value type vt1 should have been copied instead of passed by reference");
            }

            TestCopyByRef(ref vt1);
            if (vt1.Item != 5)
            {
                Console.WriteLine("Value type vt1 should have been passed by reference");
            }

            var vt4 = CreateValueType(6);
            if (vt4.Item != 6)
            {
                Console.WriteLine("Create value type and returning it should have copied it.");
            }

            var vt5 = CreateValueType(ref vt4);
            vt4.Item++;
            if (vt4.Item != 7)
            {
                Console.WriteLine("Create value type and returning it should have copied it.");
            }
            if (vt5.Item != 6)
            {
                Console.WriteLine("Returning value type should have copied it.");
            }

            var boxed = BoxValueType(ref vt5);
            if (object.ReferenceEquals(boxed, vt5))
            {
                Console.WriteLine("Boxing value type should have copied it.");
            }

            TestBasicTypeWrapping();
        }

        public void TestBasicTypeWrapping()
        {
            int i = 1;
            int j = 1;
            object o = i;
            ++i;
            int k = (int)o;
            if (!k.Equals(j))
            {
                Console.WriteLine("Basic type boxing/wrapping doesn't work as it should.");
            }
        }

        public MyValueType CreateValueType(int val)
        {
            return new MyValueType() { Item = val };
        }

        public MyValueType CreateValueType(ref MyValueType vt)
        {
            return vt;
        }

        public object BoxValueType(ref MyValueType vt)
        {
            return vt;
        }

        public void TestCopyByPass(MyValueType vt)
        {
            vt.Item = 4;
        }

        public void TestCopyByRef(ref MyValueType vt)
        {
            vt.Item = 5;
        }
    }
}
