﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
    public class StringTest
    {
        public void Test(string s)
        {
            string t = s + "bar";
            
            switch (t)
            {
                case "foobar":
                    return;

                default:
                    Console.WriteLine("Incorrect string built after concatenation.");
                    break;
            }
        }

        public static void Test()
        {
            StringTest st = new StringTest();
            st.Test("foo");
        }
    }
}
