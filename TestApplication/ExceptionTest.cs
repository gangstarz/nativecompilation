﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
    public class ExceptionTest
    {
        public void Test()
        {
            TestTryCatch();
            TestTryCatchFinally();
            TestNestingExceptions1();
            TestNestingExceptions2();
        }

        public void TestTryCatch()
        {
            int state = 0;
            try
            {
                Throw1();
            }
            catch (ArgumentException e)
            {
                if (e.Message != "foo")
                {
                    Console.WriteLine("Expected message in exception to say 'foo'");
                }
                state = 1;
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine("Incorrect exception found.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Incorrect exception caught; base exception shouldn't be caught.");
            }

            if (state != 1)
            {
                Console.WriteLine("Try/catch not called in the correct order or not at all");
            }
        }

        public void TestTryCatchFinally()
        {
            int state = 0;
            try
            {
                state = 0;
                Throw1();
            }
            catch (ArgumentException e)
            {
                if (e.Message != "foo")
                {
                    Console.WriteLine("Expected message in exception to say 'foo'");
                }
                state = 1;
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine("Incorrect exception found.");
            }
            finally
            {
                if (state != 1)
                {
                    Console.WriteLine("Expected catch before finally.");
                }
                state = 2;
            }

            if (state != 2)
            {
                Console.WriteLine("Try/catch/finally not called in the correct order or not at all");
            }
        }

        public void TestNestingExceptions1()
        {
            int state = 0;
            try
            {
                try
                {
                    Throw1();
                }
                catch (ArgumentException e)
                {
                    if (e.Message != "foo")
                    {
                        Console.WriteLine("Expected message in exception to say 'foo'");
                    }
                    state = 10;
                    throw;
                }
            }
            catch (ArgumentException e)
            {
                if (e.Message != "foo")
                {
                    Console.WriteLine("Expected message in exception to say 'foo'");
                }
                state++;
            }

            if (state != 11)
            {
                Console.WriteLine("Exceptions not caught in the right order or exception was not re-thrown.");
            }
        }

        public void TestNestingExceptions2()
        {
            int state = 0;
            try
            {
                try
                {
                    Throw1();
                }
                catch (Exception e)
                {
                    if (e.Message != "foo")
                    {
                        Console.WriteLine("Expected message in exception to say 'foo'");
                    }
                    state = 10;
                    throw;
                }
            }
            catch (Exception e)
            {
                if (e.Message != "foo")
                {
                    Console.WriteLine("Expected message in exception to say 'foo'; overload failed");
                }
                state++;
            }

            if (state != 11)
            {
                Console.WriteLine("Exceptions not caught in the right order or exception catching was not overloaded correctly.");
            }
        }

        public void Throw1()
        {
            throw new ArgumentException("foo");
        }
    }
}
