﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CPlusPlusCodeGeneration
{
    class Program
    {
        static Dictionary<Type, string> standardTypes;

        static void Main(string[] args)
        {
            //string outputPath = @"c:\tmp\cpp";
            string path = Assembly.GetExecutingAssembly().Location;
            path = path.Substring(0, path.LastIndexOf("bin")).TrimEnd('\\', '/');
            path = Path.GetFullPath(Path.Combine(path, "..\\CPlusPlus\\Corlib\\"));

            if (!Directory.Exists(path))
            {
                Console.WriteLine("Error: Output path not found: {0}", path);
                Console.ReadLine();
                Environment.Exit(0);
            }

            Console.WriteLine("This will overwrite all files in {0}! Are you sure (y/n)?", path);

            if (Console.ReadLine().Trim() != "y")
            {
                Environment.Exit(0);
            }

            standardTypes = new Dictionary<Type, string>();
            standardTypes.Add(typeof(object), "System::Object");
            standardTypes.Add(typeof(void), "void");
            standardTypes.Add(typeof(bool), "int");
            standardTypes.Add(typeof(IntPtr), "int*");
            standardTypes.Add(typeof(UIntPtr), "int*");
            standardTypes.Add(typeof(char), "wchar_t");
            standardTypes.Add(typeof(byte), "uint8_t");
            standardTypes.Add(typeof(sbyte), "int8_t");
            standardTypes.Add(typeof(ushort), "uint16_t");
            standardTypes.Add(typeof(short), "int16_t");
            standardTypes.Add(typeof(uint), "uint32_t");
            standardTypes.Add(typeof(int), "int32_t");
            standardTypes.Add(typeof(ulong), "uint64_t");
            standardTypes.Add(typeof(long), "int64_t");
            standardTypes.Add(typeof(float), "float");
            standardTypes.Add(typeof(double), "double");
            standardTypes.Add(typeof(decimal), "System::Decimal");
            standardTypes.Add(typeof(string), "System::String");

            HashSet<Type> nonemit = new HashSet<Type>(standardTypes.Keys);
            nonemit.Remove(typeof(object));
            //nonemit.Add(typeof(Array));
            nonemit.Remove(typeof(string));
            nonemit.Remove(typeof(decimal));

            //string path = @"C:\tmp\cpp\";
            HashSet<Type> dependencies = new HashSet<Type>();

            Stack<Type> todo = new Stack<Type>();
            foreach (var type in typeof(string).Assembly.GetTypes())
            {
                todo.Push(type);
            }
            while (todo.Count > 0)
            {
                var type = todo.Pop();

                foreach (var method in type.GetMethods(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                {
                    if (//method.MethodImplementationFlags == MethodImplAttributes.Runtime ||
                        method.MethodImplementationFlags == MethodImplAttributes.Native ||
                        method.MethodImplementationFlags == MethodImplAttributes.InternalCall)
                    {
                        if (method.DeclaringType == type)
                        {
                            Console.WriteLine("{0} {1}", type.Name, method.Name);

                            AddDep(dependencies, type);
                            AddDep(dependencies, method.ReturnType);

                            foreach (var par in method.GetParameters())
                            {
                                AddDep(dependencies, par.ParameterType);
                            }
                        }
                    }
                }
            }

            // Add some stubs
            dependencies.Add(typeof(NotImplementedException));
            dependencies.Add(typeof(ArgumentException));

            int count = 0;
            HashSet<string> stubs = new HashSet<string>();
            foreach (var type in dependencies.Where((a) => a != null && !a.IsByRef && !a.IsPointer && !a.IsArray))
            {
                bool hasArray = false;

                if (nonemit.Contains(type)) { continue; }

                HashSet<Type> localdep = new HashSet<Type>();
                foreach (var method in type.GetMethods(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                {
                    if (//method.MethodImplementationFlags == MethodImplAttributes.Runtime ||
                        method.MethodImplementationFlags == MethodImplAttributes.Native ||
                        method.MethodImplementationFlags == MethodImplAttributes.InternalCall)
                    {

                        if (method.DeclaringType == type)
                        {
                            hasArray |= method.ReturnType.IsArray;
                            AddDep(localdep, method.ReturnType);

                            foreach (var par in method.GetParameters())
                            {
                                AddDep(localdep, par.ParameterType);

                                var t = par.ParameterType;
                                do
                                {
                                    hasArray |= t.IsArray;
                                    t = t.GetElementType();
                                }
                                while (t != null);
                            }
                        }
                    }
                }

                // Generate C++ header stub:
                // Generate header file
                string name = type.Namespace + "." + type.Name;
                if (name.IndexOf('`') >= 0) { name = name.Substring(0, name.IndexOf('`')); }
                bool isstub = true;

                using (var sw = new StreamWriter(path + name + ".h"))
                {
                    using (var cppsw = new StreamWriter(path + name + ".cpp"))
                    {
                        // cpp file stuff
                        cppsw.WriteLine("#include \"{0}\"", name + ".h");
                        // Include everything that's _used_ and forward-declared earlier:
                        cppsw.WriteLine("#include \"System.NotImplementedException.h\"");
                        foreach (var dep in localdep.Where((a) => a != null).OrderBy((a) => a.FullName))
                        {
                            if (!nonemit.Contains(dep) && dep != type)
                            {
                                string dname = dep.Namespace + "." + dep.Name;
                                if (dname.IndexOf('`') >= 0) { dname = dname.Substring(0, dname.IndexOf('`')); }
                                cppsw.WriteLine("#include \"{0}.h\"", dname);
                            }
                        }
                        cppsw.WriteLine();

                        cppsw.WriteLine();

                        sw.WriteLine("#ifndef __" + name.Replace(".", "_").ToUpper());
                        sw.WriteLine("#define __" + name.Replace(".", "_").ToUpper());
                        sw.WriteLine();

                        // Always forward-declare the class:

                        string[] ns = type.Namespace.Split('.');
                        string tabs = "";

                        sw.WriteLine("#include \"../gc.h\"");
                        if (hasArray)
                        {
                            sw.WriteLine("#include \"../Array.h\"");
                        }

                        sw.WriteLine("#include <cstdint>");

                        // Include inheritance
                        if (type.BaseType != null && !nonemit.Contains(type.BaseType))
                        {
                            string dname = type.BaseType.Namespace + "." + type.BaseType.Name;
                            if (dname.IndexOf('`') >= 0) { dname = dname.Substring(0, dname.IndexOf('`')); }

                            sw.WriteLine("#include \"{0}.h\"", dname);
                        }

                        sw.WriteLine();

                        // Update namespace in CPP file:
                        foreach (var namesp in ns)
                        {
                            cppsw.WriteLine(tabs + "namespace {0}", namesp);
                            cppsw.WriteLine(tabs + "{");
                            tabs += "\t";
                        }

                        // Reset tabs because we continue with the header file:
                        tabs = "";

                        // Remove all base types from local dependencies
                        var bt = type.BaseType;
                        while (bt != null)
                        {
                            localdep.Remove(bt);
                            bt = bt.BaseType;
                        }
                        bool hasForward = false;

                        // Forward-declare everything that's used:
                        List<string> curns = new List<string>();
                        foreach (var item in localdep.Where((a) => a != null).OrderBy((a) => a.Namespace).ThenBy((a) => a.Name))
                        {
                            if (!nonemit.Contains(item) && item != type)
                            {
                                var nsItems = item.Namespace.Split('.');

                                while (ShouldPopNS(curns, nsItems))
                                {
                                    tabs = tabs.Substring(0, tabs.Length - 1);
                                    sw.WriteLine(tabs + "}");
                                    curns.RemoveAt(curns.Count - 1);
                                }
                                while (curns.Count < nsItems.Length)
                                {
                                    sw.WriteLine(tabs + "namespace {0}", nsItems[curns.Count]);
                                    sw.WriteLine(tabs + "{");
                                    curns.Add(nsItems[curns.Count]);
                                    tabs += "\t";
                                }

                                // Forward declare name:
                                if (item.IsGenericType && item.GetGenericTypeDefinition() == typeof(WeakReference<>))
                                {
                                    sw.WriteLine(tabs + "template <class T> class WeakReference;", item.Name);
                                }
                                else
                                {
                                    sw.WriteLine(tabs + "class {0};", item.Name);
                                }
                                hasForward = true;
                            }
                        }

                        // Emit namespace of current class:
                        while (ShouldPopNS(curns, ns))
                        {
                            tabs = tabs.Substring(0, tabs.Length - 1);
                            sw.WriteLine(tabs + "}");
                            curns.RemoveAt(curns.Count - 1);
                        }
                        if (hasForward)
                        {
                            sw.WriteLine();
                        }
                        while (curns.Count < ns.Length)
                        {
                            sw.WriteLine(tabs + "namespace {0}", ns[curns.Count]);
                            sw.WriteLine(tabs + "{");
                            curns.Add(ns[curns.Count]);
                            tabs += "\t";
                        }

                        if (type.IsEnum)
                        {
                            // Emit enums as classes; this is in fact closer to C#
                            sw.WriteLine(tabs + "class {0}", type.Name);
                            sw.WriteLine(tabs + "{");
                            sw.WriteLine(tabs + "public:");

                            var baseType = type.GetEnumUnderlyingType();
                            var btypeName = GetCPPName(baseType, type.Namespace, false, false);

                            foreach (var ename in Enum.GetNames(type))
                            {
                                // find value
                                var value = Convert.ToInt32(type.GetField(ename).GetValue(null));

                                sw.WriteLine(tabs + "\tstatic const {2} {0} = {1};", ename, Convert.ToInt32(value), btypeName);
                            }
                            sw.WriteLine();
                        }
                        else
                        {
                            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(WeakReference<>))
                            {
                                sw.WriteLine(tabs + "template <class T>");
                                sw.Write(tabs + "class WeakReference", type.Name);
                            }
                            else
                            {
                                sw.Write(tabs + "class {0}", type.Name);
                            }
                            if (type != typeof(object) && type.BaseType != null)
                            {
                                sw.Write(" : public " + GetCPPName(type.BaseType, type.Namespace, false, false).TrimEnd(' ', '*', '&'));
                            }

                            sw.WriteLine();
                            sw.WriteLine(tabs + "{");
                            sw.WriteLine(tabs + "public:");
                            foreach (var method in type.GetMethods(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                            {
                                if (//method.MethodImplementationFlags.HasFlag(MethodImplAttributes.Runtime) ||
                                    method.MethodImplementationFlags == MethodImplAttributes.Native ||
                                    method.MethodImplementationFlags == MethodImplAttributes.InternalCall)
                                {
                                    if (method.DeclaringType == type)
                                    {
                                        if (type == typeof(Action))
                                        {
                                            System.Diagnostics.Debugger.Break();
                                        }
                                        isstub = false;

                                        sw.Write(tabs + "\t{0} {1}(", GetCPPName(method.ReturnType, type.Namespace, false, false), method.Name);
                                        cppsw.Write(tabs + "{0} {1}::{2}(", GetCPPName(method.ReturnType, type.Namespace, false, false), type.Name, method.Name);

                                        bool first = true;
                                        foreach (var par in method.GetParameters())
                                        {
                                            if (first) { first = false; } else { sw.Write(", "); cppsw.Write(", "); }
                                            sw.Write("{0} {1}", GetCPPName(par.ParameterType, type.Namespace, par.ParameterType.IsByRef, par.IsOut), GetCPPVarName(par.Name));
                                            cppsw.Write("{0} {1}", GetCPPName(par.ParameterType, type.Namespace, par.ParameterType.IsByRef, par.IsOut), GetCPPVarName(par.Name));
                                        }
                                        sw.WriteLine(");");
                                        cppsw.WriteLine(")");
                                        cppsw.WriteLine(tabs + "{");
                                        cppsw.WriteLine(tabs + "\t// Implement this...");
                                        cppsw.WriteLine(tabs + "\tthrow new System::NotImplementedException();");
                                        cppsw.WriteLine(tabs + "}");
                                        cppsw.WriteLine();

                                        ++count;
                                    }
                                }
                            }
                            sw.WriteLine();
                            sw.WriteLine(tabs + "\t// [place holder for auto-generated recompiled members]");
                        }
                        sw.WriteLine(tabs + "};");
                        while (tabs.Length > 0)
                        {
                            tabs = tabs.Substring(0, tabs.Length - 1);
                            sw.WriteLine(tabs + "}");
                            cppsw.WriteLine(tabs + "}");
                        }
                        sw.WriteLine();

                        sw.WriteLine("#endif");
                    }
                }

                if (type.IsGenericType || isstub)
                {
                    stubs.Add(path + name + ".h");
                    File.Delete(path + name + ".cpp");
                }
            }
            Console.WriteLine("Total number of methods: {0}", count);

            Console.ReadLine();
        }

        private static bool ShouldPopNS(List<string> curns, string[] nsItems)
        {
            if (nsItems.Length < curns.Count)
            {
                return true;
            }
            for (int i = 0; i < curns.Count; ++i)
            {
                if (curns[i] != nsItems[i])
                {
                    return true;
                }
            }
            return false;
        }

        private static void AddDep(HashSet<Type> dependencies, Type dep)
        {
            do
            {
                while (dep.IsPointer || dep.IsArray || dep.IsByRef)
                {
                    dep = dep.GetElementType();
                }
                if (dep != null)
                {
                    dependencies.Add(dep);
                    dep = dep.BaseType;
                }
            }
            while (dep != null);
        }

        private static string GetCPPVarName(string name)
        {
            switch (name)
            {
                case "asm": return "assembly";
                default: return name;
            }
        }

        private static string GetCPPName(Type type, string curns, bool byref, bool isout)
        {
            StringBuilder sb = new StringBuilder();

            if (type.IsPointer)
            {
                sb.Append(GetCPPName(type.GetElementType(), curns, false, false));
                sb.Append("*");
            }
            else
            {
                try
                {
                    if (!type.IsArray)
                    {
                        var tmp = type.GetElementType();
                        if (tmp != null)
                        {
                            type = tmp;
                        }
                    }
                }
                catch { }

                string val;
                if (standardTypes.TryGetValue(type, out val))
                {
                    sb.Append(val);
                }
                else if (type.IsArray)
                {
                    type = type.GetElementType();
                    sb.Append("System::NormalArray<");
                    sb.Append(GetCPPName(type, curns, false, false));
                    sb.Append(">");
                }
                else
                {

                    if (type.Namespace != curns)
                    {
                        sb.Append(type.Namespace.Replace(".", "::"));
                        sb.Append("::");
                    }

                    int idx = type.Name.IndexOf('`');
                    if (idx >= 0)
                    {
                        sb.Append(type.Name.Substring(0, idx));
                    }
                    else
                    {
                        sb.Append(type.Name);
                    }
                }

                if (!type.IsValueType && type != typeof(void))
                {
                    sb.Append("*");
                }

            }

            if (isout)
            {
                sb.Append("*");
            }
            else if (byref)
            {
                sb.Append("&");
            }

            return sb.ToString();

        }
    }
}
